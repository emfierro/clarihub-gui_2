import React from 'react';
import CONSTANTS from './Constants';
import SENSORS_IMAGES from './SensorImages';
import * as Hub from './Network';

export const BTNS_RETURNS = {
    APPLY_FORM  : "applyFormBtn",
    BACK        : "backBtn",
    ADD_NEW     : "addNewBtn",
    SCAN        : "scanBtn",
    REMOVE      : "removeBtn",
    SETTINGS    : "settingsBtn",
    SAVE        : "saveBtn",
    RUN         : "runBtn",
    CONSOLE     : "consoleBtn",
    PLUS        : "plusBtn",
    RELOAD      : "reloadBtn",
    CHART       : "chartBtn"
}

export const DEFAULT_LIST_ELEMENT_COLOR = "#CCCCCC";

export const APPLYING_SUCCESSFUL = "applyingSuccessfull";

export class ContainerHeaderText extends React.Component {
    render() {
        return <div>
            <h5>
                {this.props.headerText}
            </h5>
            <hr/>
            </div>;
    }
}

export class ApplyFormBtn extends React.Component {
    render() {
        if (this.props.applying === true) {
            return <button type = "submit" className="btn btn-primary mx-1" disabled>
                <div id = "loadingView" className = "spinner-border spinner-border-sm" role="status">
                    <span className = "visually-hidden">Applying...</span>
                </div>
                &nbsp;
                Applying
            </button>;
        } else {
            if (this.props.disabled === true) {
                return <button type = "submit" className="btn btn-primary mx-1" disabled>
                    <i className="bi bi-check"></i>Apply
                </button>
                
                ;
            } else {
               return <button type = "submit" className="btn btn-primary mx-1" >
                    <i className="bi bi-check"></i>Apply
                </button>; 
            }
            
        }
    }
}

export class ReturnBackBtn extends React.Component {
    render() {
        return <button type = "button" className="btn btn-secondary mx-1" onClick={() => this.props.onChange(["backBtn"])}>
                    <i className="bi bi-arrow-left-short"></i>Back
                </button>;
    }
}

export class AddNewBtn extends React.Component {
    render() {
        return <button type = "button" className="btn btn-primary" onClick={() => this.props.onChange(["addNewBtn"])} disabled={this.props.disabled}>
                    <i className="bi bi-plus"></i>Add new
                </button>;
    }
}

export class AccessDeniedMessage extends React.Component {
    render() {
        return <div> Access denied. Please contact your HUB administrator. </div>
    }
}

export class ScanBtn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {disable: false}
    }
    const  = () =>{

    }
    fetchData  () {
        //block button
        this.setState({disable: true})
        // substitute query address if you need
        Hub.sendGET('/search_devs/')
           .then(response => {
                response.json().then(json => {
                    if (json.result === "done") {
                        // unblock button
                        this.setState({disable: false})
                    }
                    })})}
    render() {
        return <button type = "button" className={"btn btn-info"} style={{margin: '0 10px'}}  disabled={this.state.disable}
                       onClick={this.fetchData.bind(this)} >
            <i className="bi"></i>Scan
        </button>;
    }
}

export class BACK_APPLY_BTNS extends React.Component {
    render() {
        return <div>
            <ReturnBackBtn onChange = {this.props.onChange} />
            <ApplyFormBtn disabled = {this.props.disabled} applying = {this.props.applying} />
        </div>;
    }
}

export class Back_Apply_Remove_Btns extends React.Component {       
    render() {
        return <div className = "row">
            <div className = "col-6">
                <ReturnBackBtn onChange = {this.props.onChange}/>
                <ApplyFormBtn applying = {this.props.applying} />
            </div>
            <div className = "col-2">
                
            </div>
            <div className = "col-4">
                <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                    <RemoveBtn removing = {this.props.removing} onChange = {this.props.onChange}/> 
                </div>
            </div>
        </div>;
    }
}

export class Back_Save_Remove_Btns extends React.Component {
    render() {
        return <div className = "row">
            <div className = "col-6">
                <ReturnBackBtn onChange = {this.props.onChange}/>
                <SaveBtn saving = {this.props.saving} onChange = {this.props.onChange}/>
            </div>
            <div className = "col-2">

            </div>
            <div className = "col-4">
                <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                    <RemoveBtn removing = {this.props.removing} onChange = {this.props.onChange}/> 
                </div>
            </div>
        </div>;
    }
}

export class RemoveBtn extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = this.props.onChange.bind(this);
    }
    render() {
        if (this.props.removing === true) {
            return <button type = "button" className="btn btn-danger mx-1" onClick={() => this.props.onChange(["removeBtn",this.props.removeElement])} disabled>
                <div id = "loadingView" className = "spinner-border spinner-border-sm" role="status">
                    <span className = "visually-hidden">Removing...</span>
                </div>
                &nbsp;
                Removing
            </button>;
        } else {
            return <button type = "button" className="btn btn-danger mx-1" onClick={() => this.props.onChange(["removeBtn",this.props.removeElement])} disabled={this.props.disabled}>
                <i className="bi bi-trash"></i>Remove
            </button>;
        }
        
    }
}

export class RemoveSmBtn extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = this.props.onChange.bind(this);
    }
    render() {
        if (this.props.removing === true) {
            return <button type = "button" className="btn btn-light mx-1" disabled>
                <div id = "loadingView" className = "spinner-border spinner-border-sm" role="status">
                    <span className = "visually-hidden">Removing...</span>
                </div>
            </button>;
        } else {
            return <button type = "button" className="btn btn-light mx-1" onClick={this.props.onChange}>
                <i className="bi bi-trash-fill"></i>
            </button>;
        }
        
    }
}

export class SettingsBtn extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = this.props.onChange.bind(this);
    }
    
    render() {
        return <button 
            type="button" 
            className="btn btn-light my-1 mx-1 border" 
            disabled = {this.props.disabled}
            onClick={() => this.props.onChange(["settingsBtn",this.props.settingsElement])}
        >
            <i className="bi bi-gear"></i>
        </button>;
    }
}

export class PlusBtn extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = this.props.onChange.bind(this);
    }
    
    render() {
        return <button type="button" className="btn btn-light border" onClick={() => this.props.onChange([BTNS_RETURNS.PLUS,this.props.plusElement])} disabled = {this.props.disabled}>
                    <i className="bi bi-plus"></i>
                </button>;
    }
}

export class ChartBtn extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = this.props.onChange.bind(this);
    }
    
    render() {
        return <button type="button" className="btn btn-light border" onClick={() => this.props.onChange([BTNS_RETURNS.CHART,this.props.chartElement])} disabled = {this.props.disabled}>
                    <i className = "bi bi-graph-up"></i>
                </button>;
    }
}

export class CalibrationButton extends React.Component {
    render() {
        return <button  type="button" className="btn btn-light  my-1 mx-1 border" onClick = {() => this.props.onChange(["calibrationBtn",this.props.device])}>
            <i className="bi bi-sliders"></i>
        </button>;
    }
}

export class SaveBtn extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = this.props.onChange.bind(this);
    }
    render() {
        if (this.props.saving === true) {
            return <button type="button" className="btn btn-primary mx-1" onClick={() => this.props.onChange(["saveBtn",this.props.saveElement])} disabled>
                <div id = "loadingView" className = "spinner-border spinner-border-sm" role="status">
                    <span className = "visually-hidden">Saving...</span>
                </div>
                &nbsp;
                Saving
            </button>;
        } else {
            return <button type="button" className="btn btn-primary mx-1" onClick={() => this.props.onChange(["saveBtn",this.props.saveElement])}>
                <i className="bi bi-save px-1"></i>Save
            </button>;  
        }
    }
}

export class RunBtn extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = this.props.onChange.bind(this);
    }
    render() {
        if (this.props.running === true) {
            return <button type="button" className="btn btn-light border" onClick={() => this.props.onChange(["runBtn",this.props.runElement])} disabled>
                <div id = "loadingView" className = "spinner-border spinner-border-sm" role="status">
                    <span className = "visually-hidden">Applying...</span>
                </div>
            </button>;
        } else {
            return <button type="button" className="btn btn-light border" onClick={() => this.props.onChange(["runBtn",this.props.runElement])}>
                <i className="bi bi-caret-right-fill"></i>
            </button>; 
        }
        
    }
}

export class ReloadBtn extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = this.props.onChange.bind(this);
    }
    render() { 
        return <button type="button" className="btn btn-light border" onClick={() => this.props.onChange(["reloadBtn"])}>
           <i className="bi bi-arrow-repeat mx-1"></i> Reload
        </button>;
    }
}

export class ConsoleBtn extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = this.props.onChange.bind(this);
    }
    render() {
        return <button type="button" className="btn btn-light border" onClick={() => this.props.onChange(["consoleBtn"])}>
            <i class="bi bi-terminal"></i>
        </button>;
    }
}

export class SelectImage extends React.Component {
    constructor(props) {
        super(props);
        this.key = 999;
    }
    render() {
        const cursorPointer = {
            cursor : "pointer"
        }
        const listItems = Object.values(SENSORS_IMAGES).map((data) => 
            <div key = {--this.key} className = "col-3 my-3" style = {cursorPointer}>
                <img alt="" width = {CONSTANTS.LIST_ELEM_IMG_WIDTH} src = {data} className = "img-fluid" onClick = {() => this.props.onChange(["imgSrc",data])}></img>
            </div>
        )
        return <div className = "row">
            <h5>Select the icon</h5>
            {listItems}
        </div>;
    }
}

export class ImageOnListElement extends React.Component {
    render() {
        return <img width={this.props.width} src = {this.props.src} className ='img-fluid' alt= "..."></img>;
    }
}

export class LoadingField extends React.Component {
    render() {
        return <div className="d-flex align-items-center">
            <strong>Loading...</strong>
            <div className="spinner-border ms-auto" role="status" aria-hidden="true"></div>
        </div>;
    }
} 

export class ReloadErrorField extends React.Component {
    render() {
        return <div className = "col-12">
            <div className="alert alert-danger d-grid gap-2 col-8 mx-auto text-center" role="alert" >
                <h5>{this.props.error}</h5>
                <ReloadBtn onChange = {this.props.onChange}/>
            </div> 
        </div>
    }
} 

export class OnOffCheck extends React.Component {
    render() {
        let isEnableInfo = "";
        if (this.props.applying === true) {
            isEnableInfo = <div className = "spinner-border spinner-border-sm" role="status">
                <span className = "visually-hidden">applying...</span>
            </div>;
        } else {
            if (this.props.checked) {
                isEnableInfo = "On"
            } else {
                isEnableInfo = "Off"
            }
        }
        return <div className="form-check form-switch my-2">
            <input 
                className="form-check-input" 
                type="checkbox" 
                name="onOffCheck" 
                checked = {this.props.checked} 
                onChange = {this.props.onChange}
                disabled = {this.props.applying}
            /> 
            <label className="form-check-label" htmlFor="onOffCheck">
               {isEnableInfo}
            </label>
        </div>
    }
}