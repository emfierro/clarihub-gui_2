import React from 'react';
import {DEVICES_TXT_TYPES, updateDevicesColors,updateDevicesMap, updateDevicesNamesMap} from './DevicesData';
import SENSORS_IMAGES from './SensorImages';
import * as CommonComponents from './CommonComponents';
import * as Hub from './Network';
import CalibrationScreen from './Calibration';
import ChartScreen from './Charts';

import {
	Mega,
	MegaSensor,
	MegaSensorChannel,
	PowerStrip,
	Outlet,
	PWM_Output,
	Solenoid24vac,
	AnalogInput
} from "./DevicesData";


export const DEVICES_VIEW_MODES = {
	DEFAULT : 0,
	SETTINGS : 1,
	CONDITION : 2,
	ACTION : 3
}

class GlobalDevicesList extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		let listItems;
		if (this.props.mode === DEVICES_VIEW_MODES.ACTION) {
			listItems = this.props.devicesMap.toArray().map((data) => 
				(data.value instanceof PowerStrip) &&
					<GlobalDeviceElement showVersion={this.props.showVersion} key = {data.key} device = {data.value} onChange = {this.props.onChange}/>
			);
		} else {
			listItems = this.props.devicesMap.toArray().map((data) => 
				(data.value instanceof Mega || data.value instanceof PowerStrip) &&
					<GlobalDeviceElement showVersion={this.props.showVersion} devicesMap = {this.props.devicesMap} mode = {this.props.mode} key = {data.key} device = {data.value} onChange = {this.props.onChange}/>
			);
		}
		return (
			<div>
				{listItems}
			</div>
		);
	}
}

class GlobalDeviceElementVersionBtn extends React.Component {
	constructor(props) {
		super(props);
    this.onClick = this.onClick.bind(this);
    this.STATES = {
		  DEFAULT: 0,
      	LOADING: 1,
      	ERROR: 2,
      	LOADED: 3,
		}
    this.device = props.device
    this.state = {
      mode: this.STATES.DEFAULT,
      version: ""
    }
	}
  onClick(e) {
    e.preventDefault();
    e.stopPropagation();
    this.setState({mode: this.STATES.LOADING});

    Hub.getDeviceVersion(this.device.id)
    .then(vers => { this.setState({version: vers, mode: this.STATES.LOADED})})
    .catch(err => { this.setState({mode: this.STATES.ERROR})});
  }
  render() {
    switch(this.state.mode) {
      case (this.STATES.DEFAULT) : {
        return <button type="button" 
        className="btn btn-light my-1 mx-1 border" 
        disabled = {this.props.disabled}
        onClick={this.onClick}>
          Get software version  
        </button>
      }
      case (this.STATES.LOADING) : {
        return <div> Loading... </div>;
      }
      case (this.STATES.ERROR) : {
        return <div> Error </div>;
      }
      case (this.STATES.LOADED) : {
        return <div> Version: {this.state.version} </div>;
      }
    }
  }
}

class GlobalDeviceElement extends React.Component {
	constructor(props) {
		super(props);
		this.onClick = this.props.onChange.bind(this);
		this.remove = this.remove.bind(this);
		this.state = {
			removing : false
		}
	}
	remove(e) {
		let remove = window.confirm("remove?");
		if (remove) {
			this.setState ({
				removing : true
			})
			Hub.removeGlovalDevice(this.props.device.id)
			.then(response => {
				this.setState ({
					removing : false
				})
				this.props.onChange(["reload"]);
			})
			.catch(err =>{
				console.error(err);
				this.setState ({
					removing : false
				})
				alert(err + "");
			})
		}
	}
	render() {
		const deviceStyle = {
			backgroundColor : this.props.device.color,
			borderRadius : "5px"
		}
		let deviceDataStyle = {
			cursor : "pointer"
		};
		let lastUpdateTime  =  Math.trunc(this.props.device.lastUpdate / 1000);
		// let lastUpdateInfo = lastUpdateTime + " seconds ago";

		let deviceDataClassName = "col-12 my-auto p-2";
		let deviceBtns = "";
		let settingsButtonDisabled = false;
		let deviceOnClick = () => alert("Devise disabled!");
	//    let deviceOnClick = () => this.props.onChange([this.props.device]);
		if (this.props.device.isEnable == 1) {
		//    deviceDataStyle = {
		//        cursor : "pointer"
		//    }
		//    settingsButtonDisabled = false;
			if (lastUpdateTime < 5000) {
				deviceOnClick = () => this.props.onChange([this.props.device]);
			}
		}
		if (this.props.mode === DEVICES_VIEW_MODES.SETTINGS) {
			deviceDataClassName = "col-10 my-auto p-2";
			deviceBtns = <div className="col-2 my-auto">
				<CommonComponents.SettingsBtn settingsElement = {this.props.device} onChange = {this.props.onChange} disabled = {settingsButtonDisabled}/>
				<CommonComponents.RemoveSmBtn removing = {this.state.removing} onChange = {this.remove}/>
			</div>;
		}
		let enabledInfo;
		if (this.props.device.isEnable == 1) {
			if (lastUpdateTime < 5000) {
				enabledInfo =  <li className = "list-group-item list-group-item-success">
					<strong>enabled</strong>
				</li>;
			} else {
				enabledInfo = <li className = "list-group-item list-group-item-danger">
					<strong>disabled</strong>
				</li>
			}
		} else {
			enabledInfo = <li className = "list-group-item list-group-item-danger">
				<strong>disabled</strong>
			</li>
		}
		let versionInfo = null;
		if(this.props.showVersion == true){
			versionInfo = 
				<li className = "list-group-item list-group-item-info">
					<GlobalDeviceElementVersionBtn device = {this.props.device} />
				</li>
		}
		return  <div className = "row my-3 py-1 border" style={deviceStyle}>
			<div style = {deviceDataStyle} className = {deviceDataClassName} onClick = {deviceOnClick}>
				<div className = "row">
					<div  className = "col-2 my-auto" >
						<DeviceImage imgWidth = "150" device = {this.props.device}/>
					</div>
					<div className = "col-10 my-auto p-2">
						<ul className = "list-group">
							<li className = "list-group-item list-group-item-secondary">
								<h5>{this.props.device.specifedName}</h5>
							</li>
							<li className = "list-group-item">
								update :
								<strong> &nbsp;{
									(lastUpdateTime < 5) ? ' just now' :
									(lastUpdateTime <= 60) ? lastUpdateTime + " seconds ago":
									(lastUpdateTime <= 3440) ?  Math.trunc(lastUpdateTime/60)  + " min ago" :
									(lastUpdateTime <= 86400) ?  Math.trunc(lastUpdateTime/3440)+ " hours ago" : Math.trunc(lastUpdateTime /  86400) + " days ago"}
								</strong>
							</li>
							{enabledInfo}
							{versionInfo}
						</ul>
					</div>
				</div>
			</div>
			{deviceBtns}
		</div>;
	}
}

class MegaSensorsList extends React.Component {
	render() {
		const listItems = this.props.device.sensors.map((sensor) => 
			<MegaSensorListElement
				mode = {this.props.mode} 
				key = {sensor.id} 
				megaSensor = {sensor} 
				onChange = {this.props.onChange}
				isOpen = {this.props.openedMegaSensor === sensor.id || this.props.device.sensors.length === 1}
				onChange = {this.props.onChange}
			/>           
		);
		return <div>
			<div className="accordion accordion-flush my-2" id="accordionMegaSensors">
				{listItems}
			</div>
			<CommonComponents.ReturnBackBtn onChange = {this.props.onChange}/>
		</div>;
	}
}

class MegaSensorListElement extends React.Component {
	constructor(props) {
		super(props);
		this.editAccordionItemState = this.editAccordionItemState.bind(this);
		this.state = {
			isFirstLoad : true
		}
	}
	editAccordionItemState() {
		if (this.state.isOpen) {
			this.props.onChange(["openedMegaSensor",0])
			this.setState({
				isOpen : false,
				isFirstLoad : false
			})
		} else {
			this.props.onChange(["openedMegaSensor",this.props.megaSensor.id])
			this.setState({
				isOpen : true,
				isFirstLoad : false
			})
		}
	}
	render() {
		const flushHeadingID = "flush-heading-"+this.props.megaSensor.id;
		const btnID = "btn-"+this.props.megaSensor.id;
		const flushColapseID = "flush-collapse-"+this.props.megaSensor.id;
		const grid_flushColapseID = "#flush-collapse-"+this.props.megaSensor.id;
		const listItems = this.props.megaSensor.channels.map((channel) => 
			channel.ch_type !== 0 &&
				<MegaSensorChannelListElement mode = {this.props.mode} key = {channel.id} channel = {channel} onChange = {this.props.onChange}/>
			);

		let accordionColapseClassName = "accordion-collapse collapse";
		let btnColapsed = "accordion-button collapsed";
		let ariaExpanded = "false";

		if (this.state.isFirstLoad) {
			if (this.props.isOpen) {
				accordionColapseClassName = "accordion-collapse collapse show";
				btnColapsed = "accordion-button";
				ariaExpanded = "true";
			}
		}

		let btnClassName = this.props.megaSensor.isEnable == 1 ? btnColapsed : btnColapsed + " text-danger";
		let btnSizeClassName = this.props.mode === DEVICES_VIEW_MODES.SETTINGS ? "col-10" : "col-12";

		

		
		return <div className = "row">
					<div className = {btnSizeClassName}>
						<div className="accordion-item">
						<h2 className="accordion-header" id={flushHeadingID}>
							<button 
								id={btnID} 
								className = {btnClassName}
								type="button" data-bs-toggle="collapse"
								data-bs-target={grid_flushColapseID} 
								aria-expanded={ariaExpanded}
								aria-controls={flushColapseID}
								onClick = {this.editAccordionItemState}
								disabled = {this.props.megaSensor.isEnable != 1}
							>
								{this.props.megaSensor.specifedName}
								&nbsp;
								(
								<strong className = ""> 
									{this.props.megaSensor.isEnable == 1 ? 'enabled' : 'disabled'}
								</strong>
								)
							</button>
						</h2>
						<div id={flushColapseID} className={accordionColapseClassName} aria-labelledby={flushHeadingID} data-bs-parent="#accordionMegaSensors">
							<div className="accordion-body">
								{listItems}
							</div>
						</div>
					</div>
				</div>
				{ this.props.mode === DEVICES_VIEW_MODES.SETTINGS &&
					<div className = "col-1">
						<CommonComponents.SettingsBtn settingsElement = {this.props.megaSensor} onChange = {this.props.onChange}/>
					</div>
				}
			</div>;
	}
}

class MegaSensorChannelListElement extends React.Component {
	render() {
		return (<div className = "row my-2 gy-1">
			<div className = "col-4">
				<div className = "text-center">
					<figure className = "figure">
						<img width="50" src={this.props.channel.imgSrc} className="figure-img img-fluid rounded" alt="..."/>
						<figcaption className="figure-caption">{this.props.channel.specifedName}</figcaption>
					</figure>
				</div>
			</div>
			<div className="col-6 my-auto">
				{this.props.channel.description} : <strong>{Number.parseFloat(this.props.channel.value).toFixed(1)}</strong> {this.props.channel.units}
			</div>
			<div className="col-1 my-auto">
			{ this.props.mode === DEVICES_VIEW_MODES.DEFAULT &&
				<CommonComponents.ChartBtn chartElement = {this.props.channel} onChange = {this.props.onChange}/>
			}
			{ this.props.mode === DEVICES_VIEW_MODES.SETTINGS &&
				<>
					<CommonComponents.SettingsBtn settingsElement = {this.props.channel} onChange = {this.props.onChange}/>
					<CommonComponents.CalibrationButton device = {this.props.channel} onChange = {this.props.onChange}/>
				</>
			}
			{ this.props.mode === DEVICES_VIEW_MODES.CONDITION &&
				<CommonComponents.PlusBtn plusElement = {this.props.channel} onChange = {this.props.onChange}/>
			}
			</div>
		</div>);
	}
}

class PowerStripDevicesList extends React.Component {
	render() {
		const OUTLET_LIST = <OutletsList 
			mode = {this.props.mode}
			device = {this.props.device} 
			onChange = {this.props.onChange}
			isOpen = {this.props.openedPSChild === DEVICES_TXT_TYPES.OUTLET}
		 />;
		const PWMS_LIST = <PWMsList 
			mode = {this.props.mode} 
			device = {this.props.device} 
			onChange = {this.props.onChange}
			isOpen = {this.props.openedPSChild === DEVICES_TXT_TYPES.PWM_OUTPUT}
		/>;
		const SOLENOIDS_LIST = <SolenoidsList 
			mode = {this.props.mode} 
			device = {this.props.device} 
			onChange = {this.props.onChange}
			isOpen = {this.props.openedPSChild === DEVICES_TXT_TYPES.SOLENOID}
		/>
		const ANALOG_INPUTS_LIST =  <AnalogInputsList 
			mode = {this.props.mode} 
			device = {this.props.device} 
			onChange = {this.props.onChange}
			isOpen = {this.props.openedPSChild === DEVICES_TXT_TYPES.ANALOG_INPUT}
		/>
		return (<div>
			{
				(this.props.mode === DEVICES_VIEW_MODES.SETTINGS ||
					this.props.mode === DEVICES_VIEW_MODES.DEFAULT) &&

				<div className="accordion accordion-flush my-2" id="accordionPSDevices">
					{OUTLET_LIST}
					{PWMS_LIST}
					{SOLENOIDS_LIST}
					{ANALOG_INPUTS_LIST}
				</div>
			}

			{
				this.props.mode === DEVICES_VIEW_MODES.CONDITION &&
					
				<div className="accordion accordion-flush my-2" id="accordionPSDevices">
					{ANALOG_INPUTS_LIST}
				</div>
			}

			{
				this.props.mode === DEVICES_VIEW_MODES.ACTION &&

				<div className="accordion accordion-flush my-2" id="accordionPSDevices">
					{OUTLET_LIST}
					{PWMS_LIST}
					{SOLENOIDS_LIST}
				</div>
			}
				
			
			<CommonComponents.ReturnBackBtn onChange = {this.props.onChange}/>
		</div>);
	}
}



class OutletsList extends React.Component {
	constructor(props) {
		super(props);
		this.editAccordionItemState = this.editAccordionItemState.bind(this);
		this.state = {
			isFirstLoad : true
		}
	}
	editAccordionItemState() {
		if (this.state.isOpen) {
			this.setState({
				isOpen : false,
				isFirstLoad : false
			})
		} else {
			this.props.onChange(["openedPSChild",DEVICES_TXT_TYPES.OUTLET])
			this.setState({
				isOpen : true,
				isFirstLoad : false
			})
		}
	}
	render() {
		const uniqName = "Outlets"
		const flushHeadingID = "flush-heading-"+uniqName;
		const btnID = "btn-"+uniqName;
		const flushColapseID = "flush-collapse-"+uniqName;
		const grid_flushColapseID = "#flush-collapse-"+uniqName;

		let accordionColapseClassName = "accordion-collapse collapse";
		let btnColapsed = "accordion-button collapsed";
		let ariaExpanded = "false";

		if (this.state.isFirstLoad) {
			if (this.props.isOpen) {
				accordionColapseClassName = "accordion-collapse collapse show";
				btnColapsed = "accordion-button";
				ariaExpanded = "true";
			}
		}

		const listItems = this.props.device.outlets.map((subdev) => 
				<OutletListElement mode = {this.props.mode} key = {subdev.id} device = {subdev} onChange = {this.props.onChange}/>
			);
		return (<div className="accordion-item">
				<h2 className="accordion-header" id={flushHeadingID}>
					<button 
						id={btnID} 
						className= {btnColapsed}
						type="button" 
						data-bs-toggle="collapse" 
						data-bs-target={grid_flushColapseID} 
						aria-expanded={ariaExpanded}
						aria-controls={flushColapseID}
						onClick = {this.editAccordionItemState}
					>
						{uniqName}
					</button>
				</h2>
				<div id={flushColapseID} 
					className = {accordionColapseClassName} 
					aria-labelledby={flushHeadingID} 
					data-bs-parent="#accordionPSDevices"
				>
					<div className="accordion-body">
						{listItems}
					</div>
				</div>
			</div>);
	}
}

class SolenoidsList extends React.Component {
	constructor(props) {
		super(props);
		this.editAccordionItemState = this.editAccordionItemState.bind(this);
		this.state = {
			isFirstLoad : true
		}
	}
	editAccordionItemState() {
		if (this.state.isOpen) {
			this.setState({
				isOpen : false,
				isFirstLoad : false
			})
		} else {
			this.props.onChange(["openedPSChild",DEVICES_TXT_TYPES.SOLENOID])
			this.setState({
				isOpen : true,
				isFirstLoad : false
			})
		}
	}
	render() {
		const uniqName = "Solenoids"
		const flushHeadingID = "flush-heading-"+uniqName;
		const btnID = "btn-"+uniqName;
		const flushColapseID = "flush-collapse-"+uniqName;
		const grid_flushColapseID = "#flush-collapse-"+uniqName;

		let accordionColapseClassName = "accordion-collapse collapse";
		let btnColapsed = "accordion-button collapsed";
		let ariaExpanded = "false";

		if (this.state.isFirstLoad) {
			if (this.props.isOpen) {
				accordionColapseClassName = "accordion-collapse collapse show";
				btnColapsed = "accordion-button";
				ariaExpanded = "true";
			}
		}

		const listItems = this.props.device.solenoids24vac.map((subdev) => 
				<SolenoidListElement mode = {this.props.mode} key = {subdev.id} device = {subdev} onChange = {this.props.onChange}/>
			);
			return (<div className="accordion-item">
				<h2 className="accordion-header" id={flushHeadingID}>
					<button 
						id={btnID} 
						className= {btnColapsed}
						type="button" 
						data-bs-toggle="collapse" 
						data-bs-target={grid_flushColapseID} 
						aria-expanded={ariaExpanded}
						aria-controls={flushColapseID}
						onClick = {this.editAccordionItemState}
					>
						{uniqName}
					</button>
				</h2>
				<div id={flushColapseID} 
					className = {accordionColapseClassName} 
					aria-labelledby={flushHeadingID} 
					data-bs-parent="#accordionPSDevices"
				>
					<div className="accordion-body">
						{listItems}
					</div>
				</div>
			</div>);
	}
}

class PWMsList extends React.Component {
	constructor(props) {
		super(props);
		this.editAccordionItemState = this.editAccordionItemState.bind(this);
		this.state = {
			isFirstLoad : true
		}
	}
	editAccordionItemState() {
		if (this.state.isOpen) {
			this.setState({
				isOpen : false,
				isFirstLoad : false
			})
		} else {
			this.props.onChange(["openedPSChild",DEVICES_TXT_TYPES.PWM_OUTPUT])
			this.setState({
				isOpen : true,
				isFirstLoad : false
			})
		}
	}
	render() {
		const _uniqName = "12v PWMs";
		const uniqName = "12vPWMs";
		const flushHeadingID = "flush-heading-"+uniqName;
		const btnID = "btn-"+uniqName;
		const flushColapseID = "flush-collapse-"+uniqName;
		const grid_flushColapseID = "#flush-collapse-"+uniqName;

		let accordionColapseClassName = "accordion-collapse collapse";
		let btnColapsed = "accordion-button collapsed";
		let ariaExpanded = "false";

		if (this.state.isFirstLoad) {
			if (this.props.isOpen) {
				accordionColapseClassName = "accordion-collapse collapse show";
				btnColapsed = "accordion-button";
				ariaExpanded = "true";
			}
		}

		const listItems = this.props.device.pwmsOutput12V.map((subdev) => 
				<PWMListElement mode = {this.props.mode} key = {subdev.id} device = {subdev} onChange = {this.props.onChange}/>
			);
			return (<div className="accordion-item">
				<h2 className="accordion-header" id={flushHeadingID}>
					<button 
						id={btnID} 
						className= {btnColapsed}
						type="button" 
						data-bs-toggle="collapse" 
						data-bs-target={grid_flushColapseID} 
						aria-expanded={ariaExpanded}
						aria-controls={flushColapseID}
						onClick = {this.editAccordionItemState}
					>
						{uniqName}
					</button>
				</h2>
				<div id={flushColapseID} 
					className = {accordionColapseClassName} 
					aria-labelledby={flushHeadingID} 
					data-bs-parent="#accordionPSDevices"
				>
					<div className="accordion-body">
						{listItems}
					</div>
				</div>
			</div>);
	}
}

class AnalogInputsList extends React.Component {
	constructor(props) {
		super(props);
		this.editAccordionItemState = this.editAccordionItemState.bind(this);
		this.state = {
			isFirstLoad : true
		}
	}
	editAccordionItemState() {
		if (this.state.isOpen) {
			this.setState({
				isOpen : false,
				isFirstLoad : false
			})
		} else {
			this.props.onChange(["openedPSChild",DEVICES_TXT_TYPES.ANALOG_INPUT])
			this.setState({
				isOpen : true,
				isFirstLoad : false
			})
		}
	}
	render() {
		const uniqName = "Analoginputs"
		const _uniqName = "Analog inputs"
		const flushHeadingID = "flush-heading-"+uniqName;
		const btnID = "btn-"+uniqName;
		const flushColapseID = "flush-collapse-"+uniqName;
		const grid_flushColapseID = "#flush-collapse-"+uniqName;

		let accordionColapseClassName = "accordion-collapse collapse";
		let btnColapsed = "accordion-button collapsed";
		let ariaExpanded = "false";

		if (this.state.isFirstLoad) {
			if (this.props.isOpen) {
				accordionColapseClassName = "accordion-collapse collapse show";
				btnColapsed = "accordion-button";
				ariaExpanded = "true";
			}
		}

		const listItems = this.props.device.analogInputs.map((subdev) => 
				<AnalogInputListElement mode = {this.props.mode} key = {subdev.id} device = {subdev} onChange = {this.props.onChange}/>
			);
			return (<div className="accordion-item">
				<h2 className="accordion-header" id={flushHeadingID}>
					<button 
						id={btnID} 
						className= {btnColapsed}
						type="button" 
						data-bs-toggle="collapse" 
						data-bs-target={grid_flushColapseID} 
						aria-expanded={ariaExpanded}
						aria-controls={flushColapseID}
						onClick = {this.editAccordionItemState}
					>
						{uniqName}
					</button>
				</h2>
				<div id={flushColapseID} 
					className = {accordionColapseClassName} 
					aria-labelledby={flushHeadingID} 
					data-bs-parent="#accordionPSDevices"
				>
					<div className="accordion-body">
						{listItems}
					</div>
				</div>
			</div>);
	}
}



class OutletListElement extends React.Component {
	constructor(props) {
		super(props);
		this.onChange = this.onChange.bind(this);
		this.state = {
			deviceIsEnabled : this.props.device.isEnable == 1,
			enableCheckApplying : false
		}
	}
	onChange(params) {
		let checked = params.target.checked;
		this.setState ({
			enableCheckApplying : true
		})
		let eventFunc;
		if (checked) {
			eventFunc = () => Hub.onPowerStripDevice(this.props.device);
		} else {
			eventFunc = () => Hub.offPowerStripDevice(this.props.device);
		}
		eventFunc()
		.then(response => {
			this.setState({
				enableCheckApplying : false,
				deviceIsEnabled : checked
			})
		})
		.catch(err => {
			this.setState ({
				enableCheckApplying : false
			})
			console.error(err);
		})
	}
	render() {
		return <div className = "row my-2 gy-1">
			<div className = "col-4">
				<div className = "text-center">
					<figure className = "figure">
						<img width="50" src={SENSORS_IMAGES.outlet} className="figure-img img-fluid rounded" alt="..."/>
						<figcaption className="figure-caption">{this.props.device.specifedName}</figcaption>
					</figure>
				</div>
			</div>
			<div className="col-6 my-auto">
				<ul className = "list-group">
					<li className = "list-group-item">
						currentSensor : <strong>{this.props.device.currentSensor}</strong>
					</li>
					{(this.state.deviceIsEnabled) &&
						<li className = "list-group-item text-success">
							<strong>enabled</strong>
						</li>
					}
					{(this.state.deviceIsEnabled) ||
						<li className = "list-group-item text-danger">
							<strong>disabled</strong>
						</li>
					}
				</ul>
			</div>
			<div className="col-1 my-auto">
				{ this.props.mode === DEVICES_VIEW_MODES.DEFAULT &&
					<CommonComponents.ChartBtn chartElement = {this.props.device} onChange = {this.props.onChange}/>
				}
				{ this.props.mode === DEVICES_VIEW_MODES.SETTINGS &&
					<div>
						<CommonComponents.OnOffCheck applying = {this.state.enableCheckApplying} checked = {this.state.deviceIsEnabled} onChange = {this.onChange} />
						<CommonComponents.SettingsBtn settingsElement = {this.props.device} onChange = {this.props.onChange}/>
					</div>
				}
				{ this.props.mode === DEVICES_VIEW_MODES.ACTION &&
					<CommonComponents.PlusBtn  plusElement = {this.props.device} onChange = {this.props.onChange}/>
				}
			</div>
		</div>;
	}
}

class PWMListElement extends React.Component {
	constructor(props) {
		super(props);
		this.onOnOff = this.onOnOff.bind(this);
		this.powerChange = this.powerChange.bind(this);
		this.powerApply = this.powerApply.bind(this);
		this.state = {
			deviceIsEnabled : this.props.device.isEnable == 1,
			enableCheckApplying : false,
			power : this.props.device.value,
			powerApplying : false,
		}
	}
	powerChange(e) {
		this.setState({
			power : e.target.value
		})
	}
	powerApply(e)  {
		this.setState({
			powerApplying : true
		})
		Hub.setPowerStripPower(this.props.device,this.state.power)
		.then(response => {
			this.setState({
				powerApplying : false
			})
		})
		.catch(err => {
			console.error(err);
			alert(err + "");
			this.setState({
				power : this.props.device.value,
				powerApplying : false
			})
		})
	}
	onOnOff(params) {
		let checked = params.target.checked;
		this.setState ({
			enableCheckApplying : true
		})
		let eventFunc;
		if (checked) {
			eventFunc = () => Hub.onPowerStripDevice(this.props.device);
		} else {
			eventFunc = () => Hub.offPowerStripDevice(this.props.device);
		}
		eventFunc()
		.then(response => {
			this.setState({
				enableCheckApplying : false,
				deviceIsEnabled : checked,
			})
		})
		.catch(err => {
			this.setState ({
				enableCheckApplying : false
			})
			console.error(err);
		})
	}
	render() {
		return (<div className = "row my-2 gy-1">
			<div className = "col-4">
				<div className = "text-center">
					<figure className = "figure">
						<img width="50" src={SENSORS_IMAGES.pwm_output} className="figure-img img-fluid rounded" alt="..."/>
						<figcaption className="figure-caption">{this.props.device.specifedName}</figcaption>
					</figure>
				</div>
			</div>
			<div className="col-6 my-auto">
			{ this.props.mode === DEVICES_VIEW_MODES.SETTINGS &&
				<div>
					{
						this.state.powerApplying &&
						<label htmlFor="pwmPowerID" className="form-label">
							<div className = "spinner-border spinner-border-sm" role="status">
								<span className = "visually-hidden">applying...</span>
							</div>
						</label>
					}
					 {
						this.state.powerApplying ||
						<label htmlFor="pwmPowerID" className="form-label">power : {this.state.power}</label>
					 }
					
					<input
						type="range"
						className="form-range"
						min="0"
						max="100"
						step="5"
						id = "pwmPowerID"
						name = "power"
						value = {this.state.power}
						onChange = {this.powerChange}
						onMouseUp = {this.powerApply}
					/>
				</div>
			}
			{ this.props.mode === DEVICES_VIEW_MODES.SETTINGS || 
				<ul className = "list-group">
					<li className = "list-group-item">
						Power : <strong>{this.props.device.value}</strong> %
					</li>
					{(this.state.deviceIsEnabled) &&
						<li className = "list-group-item text-success">
							<strong>enabled</strong>
						</li>
					}
					{(this.state.deviceIsEnabled) ||
						<li className = "list-group-item text-danger">
							<strong>disabled</strong>
						</li>
					}
				</ul>
			} 
			</div>
			<div className="col-1 my-auto">
			{ this.props.mode === DEVICES_VIEW_MODES.DEFAULT &&
				<CommonComponents.ChartBtn chartElement = {this.props.device} onChange = {this.props.onChange}/>
			}
			{ this.props.mode === DEVICES_VIEW_MODES.SETTINGS &&
				<div>
					<CommonComponents.OnOffCheck applying = {this.state.enableCheckApplying} checked = {this.state.deviceIsEnabled} onChange = {this.onOnOff} />
					<CommonComponents.SettingsBtn settingsElement = {this.props.device} onChange = {this.props.onChange}/>
				</div>
			}
			{ this.props.mode === DEVICES_VIEW_MODES.ACTION &&
				<CommonComponents.PlusBtn  plusElement = {this.props.device} onChange = {this.props.onChange}/>
			}
			</div>
		</div>);
	}
}

class SolenoidListElement extends React.Component {
	constructor(props) {
		super(props);
		this.onChange = this.onChange.bind(this);
		this.state = {
			deviceIsEnabled : this.props.device.isEnable == 1,
			enableCheckApplying : false
		}
	}
	onChange(params) {
		let checked = params.target.checked;
		this.setState ({
			enableCheckApplying : true
		})
		let eventFunc;
		if (checked) {
			eventFunc = () => Hub.onPowerStripDevice(this.props.device);
		} else {
			eventFunc = () => Hub.offPowerStripDevice(this.props.device);
		}
		eventFunc()
		.then(response => {
			this.setState({
				enableCheckApplying : false,
				deviceIsEnabled : checked
			})
		})
		.catch(err => {
			this.setState ({
				enableCheckApplying : false
			})
			console.error(err);
		})
	}
	render() {
		return (<div className = "row my-2 gy-1">
			<div className = "col-4">
				<div className = "text-center">
					<figure className = "figure">
						<img width="50" src={SENSORS_IMAGES.solenoid_water_valve} className="figure-img img-fluid rounded" alt="..."/>
						<figcaption className="figure-caption">{this.props.device.specifedName}</figcaption>
					</figure>
				</div>
			</div>
			<div className="col-6 my-auto">
				{
					this.state.deviceIsEnabled ||
					<strong className = "text-danger">DISABLED</strong>
				}
				{
					this.state.deviceIsEnabled &&
					<strong className = "text-success">ENABLED</strong>
				}
			</div>
			<div className="col-1 my-auto">
			{ this.props.mode === DEVICES_VIEW_MODES.DEFAULT &&
				<CommonComponents.ChartBtn chartElement = {this.props.device} onChange = {this.props.onChange}/>
			}
			{ this.props.mode === DEVICES_VIEW_MODES.SETTINGS &&
				 <div>
					<CommonComponents.OnOffCheck applying = {this.state.enableCheckApplying} checked = {this.state.deviceIsEnabled} onChange = {this.onChange} />
					<CommonComponents.SettingsBtn  settingsElement = {this.props.device} onChange = {this.props.onChange}/>
				</div>
			}
			{ this.props.mode === DEVICES_VIEW_MODES.ACTION &&
				<CommonComponents.PlusBtn  plusElement = {this.props.device} onChange = {this.props.onChange}/>
			}
			</div>
		</div>);
	}
}

class AnalogInputListElement extends React.Component {
	render() {
		return (<div className = "row my-2 gy-1">
			<div className = "col-4">
				<div className = "text-center">
					<figure className = "figure">
						<img width="50" src={SENSORS_IMAGES.analog_input} className="figure-img img-fluid rounded" alt="..."/>
						<figcaption className="figure-caption">{this.props.device.specifedName}</figcaption>
					</figure>
				</div>
			</div>
			<div className="col-6 my-auto">
				value : <strong>{this.props.device.value}</strong>
			</div>
			<div className="col-1 my-auto">
			{ this.props.mode === DEVICES_VIEW_MODES.DEFAULT &&
				<CommonComponents.ChartBtn chartElement = {this.props.device} onChange = {this.props.onChange}/>
			}
			{ this.props.mode === DEVICES_VIEW_MODES.SETTINGS &&
				<CommonComponents.SettingsBtn settingsElement = {this.props.device} onChange = {this.props.onChange}/>
			}
			{ this.props.mode === DEVICES_VIEW_MODES.CONDITION &&
				<CommonComponents.PlusBtn plusElement = {this.props.device} onChange = {this.props.onChange}/>
			}
			</div>
		</div>);
	}
}

class AddNewGlobalDevice extends React.Component {
	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.deviceTypeChange = this.deviceTypeChange.bind(this);
		this.idChange = this.idChange.bind(this);
		this.state = {
			type : "Mega",
			applyDisabled : true
		};
	}
	deviceTypeChange(e) {
		this.setState({type : e.target.value});
	}
	idChange(e)  {
		let value = e.target.value;
		let regex = /^[a-fA-F0-9]{12}$/;
		if (regex.exec(value)) {
			this.setState({
				applyDisabled : false
			})
		} else {
			this.setState({
				applyDisabled : true
			})
		}
		this.setState({id : e.target.value});
	}
	handleSubmit(e) {
		e.preventDefault();
		let data = {
			type : this.state.type,
			id : this.state.id
		};
		this.props.onChange([CommonComponents.BTNS_RETURNS.APPLY_FORM,data]);
	}
	render() {
		return <form onSubmit = {this.handleSubmit}>
				<div className="mb-3">
				<label htmlFor="dev-name-input" className="form-label">
					Device ID
				</label>
				<input type="text" className="form-control" id="dev-id-input" required pattern="[abcdefABCDEF0-9]{12}" onChange = {this.idChange}/>
				<div className="form-text my-1">
					hex 12 chars
				</div>
				<select name = "device-type" className = "form-select"  onChange = {this.deviceTypeChange}>
					<option value = "Mega">Mega</option>
					<option value = "PowerStrip">PowerStrip</option>
				</select>
				<div className="form-text">
					Type of device
				</div>
			</div>
			<CommonComponents.BACK_APPLY_BTNS disabled = {this.state.applyDisabled} applying = {this.props.applying} onChange = {this.props.onChange}/>
		</form>;
	}
}

class GlobalDevicesSettings extends React.Component {
	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.colorChange = this.colorChange.bind(this);
		this.nameChange = this.nameChange.bind(this);
		this.state = {  
			device : this.props.device,
			color :   this.props.device.color || "#8FBC8F",
			specifedName :   this.props.device.specifedName || ""
		}
	}
	colorChange(e) {
		this.setState({color : e.target.value});
	}
	nameChange(e) {
		this.setState({specifedName : e.target.value});
	}
	handleSubmit(e) {
		e.preventDefault();
		this.state.device.color = this.state.color;
		this.state.device.specifedName = this.state.specifedName;
		this.props.onChange([CommonComponents.BTNS_RETURNS.APPLY_FORM,this.state.device]);
	}
	render() {
		return <form onSubmit = {this.handleSubmit}>
				<div className="mb-3">
				<label htmlFor="dev-name-input" className="form-label">
					Device name
				</label>
				<input type="text" className="form-control" id="dev-name-input" defaultValue = {this.state.specifedName} required pattern="[a-zA-Z0-9_-]{1,16}" onChange = {this.nameChange}/>
				<div className="form-text">
					Only letters , numbers and _ or - (from 1 to 16 characters)
				</div>
				<label htmlFor="device-color" className="form-label">
					Color for <strong>Device</strong>
				</label>
				<input type="color" defaultValue = {this.state.color} className="form-control form-control-lg" id="device-color" onChange = {this.colorChange}/>
			</div>
			<CommonComponents.BACK_APPLY_BTNS applying = {this.props.applying} onChange = {this.props.onChange}/>
		</form>;
	}
}

class MegaSensorSettings extends React.Component {
	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.nameChange = this.nameChange.bind(this);
		this.isEnableChange = this.isEnableChange.bind(this);
		this.state = {   
			device : this.props.device,
			specifedName :   this.props.device.specifedName || "",
			isEnable    : this.props.device.isEnable == 1,
			enableApplying : false,
			enableCkeckDisabled : false
		}
	}
	nameChange(e) {
		this.setState({specifedName : e.target.value});
	}
	isEnableChange(e) {
		this.setState({
			enableApplying : true,
			enableCkeckDisabled : true
		})
		if (e.target.checked === true) {
			Hub.onMegaSensor(this.props.device)
			.then(response => {
				this.setState({
					isEnable : e.target.checked,
					enableApplying : false,
					enableCkeckDisabled : false
				})
			})
			.catch(err => {
				console.error(err);
				this.setState({
					enableApplying : false,
					enableCkeckDisabled : false
				})
			})
		} else {
			Hub.offMegaSensor(this.props.device)
			.then(response => {
				this.setState({
					isEnable : e.target.checked,
					enableApplying : false,
					enableCkeckDisabled : false
				})
			})
			.catch(err => {
				console.error(err);
				this.setState({
					enableApplying : false,
					enableCkeckDisabled : false
				})
			})
		}
	} 
	handleSubmit(e) {
		e.preventDefault();
		this.state.device.specifedName = this.state.specifedName;
		this.props.onChange([CommonComponents.BTNS_RETURNS.APPLY_FORM,this.state.device]);
	}
	render() {
		let isEnableInfo = "";
		if (this.state.enableApplying === true) {
			isEnableInfo = <div className = "spinner-border spinner-border-sm" role="status">
				<span className = "visually-hidden">applying...</span>
			</div>;
		} else {
			if (this.state.isEnable) {
				isEnableInfo = "enabled"
			} else {
				isEnableInfo = "disabled"
			}
		}
		return <form onSubmit = {this.handleSubmit}>
				<div className="mb-3">
				<label htmlFor="dev-name-input" className="form-label">
					Device name
				</label>
				<input type="text" className="form-control" id="dev-name-input" defaultValue = {this.state.specifedName} required pattern="[a-zA-Z0-9_-]{1,16}"  onChange = {this.nameChange}/>
				<div className="form-text">
					Only letters , numbers and _ or - (from 1 to 16 characters)
				</div>
				<div className="form-check form-switch my-2">
					<input 
						className="form-check-input" 
						type="checkbox" 
						name="onOffCheck" 
						checked = {this.state.isEnable} 
						onChange = {this.isEnableChange}
						disabled = {true}
					/> 
					<label className="form-check-label" htmlFor="onOffCheck">
						{isEnableInfo}
					</label>
				</div>
				<hr/>
			</div>
			<CommonComponents.BACK_APPLY_BTNS applying = {this.props.applying} onChange = {this.props.onChange}/>
		</form>;
	}
}

class MegaSensorChannelSettings extends React.Component {
	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.nameChange = this.nameChange.bind(this);
		this.state = {   
			device : this.props.device,
			specifedName :   this.props.device.specifedName || ""
		}
	}
	nameChange(e) {
		this.setState({specifedName : e.target.value});
	}
	handleSubmit(e) {
		e.preventDefault();
		this.state.device.color.setState(this.state.color);
		this.state.device.specifedName = this.state.specifedName;
		this.props.onChange([CommonComponents.BTNS_RETURNS.APPLY_FORM,this.state.device]);
	}
	render() {
		return <form onSubmit = {this.handleSubmit}>
				<div className="mb-3">
				<label htmlFor="dev-name-input" className="form-label">
					Device name
				</label>
				<input type="text" className="form-control" id="dev-name-input" defaultValue = {this.state.specifedName} required pattern="[a-zA-Z0-9_-]{1,16}"  onChange = {this.nameChange}/>
				<div className="form-text">
					Only letters , numbers and _ or - (from 1 to 16 characters)
				</div>
				<hr/>
			</div>
			<CommonComponents.BACK_APPLY_BTNS applying = {this.props.applying} onChange = {this.props.onChange}/>
		</form>;
	}
}

class OutletSettings extends React.Component {
	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.nameChange = this.nameChange.bind(this);
		this.state = {   
			device : this.props.device,
			specifedName :   this.props.device.specifedName || ""
		}
	}
	nameChange(e) {
		this.setState({specifedName : e.target.value});
	}
	handleSubmit(e) {
		e.preventDefault();
		this.state.device.color = this.state.color;
		this.state.device.specifedName = this.state.specifedName;
		this.props.onChange([CommonComponents.BTNS_RETURNS.APPLY_FORM,this.state.device]);
	}
	render() {
		return <form onSubmit = {this.handleSubmit}>
				<div className="mb-3">
				<label htmlFor="dev-name-input" className="form-label">
					Device name
				</label>
				<input type="text" className="form-control" id="dev-name-input" defaultValue = {this.state.specifedName} required pattern="[a-zA-Z0-9_-]{1,16}"  onChange = {this.nameChange}/>
				<div className="form-text">
					Only letters , numbers and _ or - (from 1 to 16 characters)
				</div>
				<hr/>
			</div>
			<CommonComponents.BACK_APPLY_BTNS applying = {this.props.applying} onChange = {this.props.onChange}/>
		</form>;
	}
}

class PWMSettings extends React.Component {
	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.nameChange = this.nameChange.bind(this);
		this.state = {   
			device : this.props.device,
			specifedName :   this.props.device.specifedName || ""
		}
	}
	nameChange(e) {
		this.setState({specifedName : e.target.value});
	}
	handleSubmit(e) {
		e.preventDefault();
		this.state.device.color = this.state.color;
		this.state.device.specifedName = this.state.specifedName;
		this.props.onChange([CommonComponents.BTNS_RETURNS.APPLY_FORM,this.state.device]);
	}
	render() {
		return <form onSubmit = {this.handleSubmit}>
				<div className="mb-3">
				<label htmlFor="dev-name-input" className="form-label">
					Device name
				</label>
				<input type="text" className="form-control" id="dev-name-input" defaultValue = {this.state.specifedName} required pattern="[a-zA-Z0-9_-]{1,16}"  onChange = {this.nameChange}/>
				<div className="form-text">
					Only letters , numbers and _ or - (from 1 to 16 characters)
				</div>
				<hr/>
			</div>
			<CommonComponents.BACK_APPLY_BTNS applying = {this.props.applying} onChange = {this.props.onChange}/>
		</form>;
	}
}

class SolenoidSettings extends React.Component {
	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.nameChange = this.nameChange.bind(this);
		this.state = {   
			device : this.props.device,
			specifedName :   this.props.device.specifedName || ""
		}
	}
	nameChange(e) {
		this.setState({specifedName : e.target.value});
	}
	handleSubmit(e) {
		e.preventDefault();
		this.state.device.color = this.state.color;
		this.state.device.specifedName = this.state.specifedName;
		this.props.onChange([CommonComponents.BTNS_RETURNS.APPLY_FORM,this.state.device]);
	}
	render() {
		return <form onSubmit = {this.handleSubmit}>
				<div className="mb-3">
				<label htmlFor="dev-name-input" className="form-label">
					Device name
				</label>
				<input type="text" className="form-control" id="dev-name-input" defaultValue = {this.state.specifedName} required pattern="[a-zA-Z0-9_-]{1,16}"  onChange = {this.nameChange}/>
				<div className="form-text">
					Only letters , numbers and _ or - (from 1 to 16 characters)
				</div>
				<hr/>
			</div>
			<CommonComponents.BACK_APPLY_BTNS applying = {this.props.applying} onChange = {this.props.onChange}/>
		</form>;
	}
}

class AnalogInputSettings extends React.Component {
	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.nameChange = this.nameChange.bind(this);
		this.state = {   
			device : this.props.device,
			specifedName :   this.props.device.specifedName || ""
		}
	}
	nameChange(e) {
		this.setState({specifedName : e.target.value});
	}
	handleSubmit(e) {
		e.preventDefault();
		this.state.device.color = this.state.color;
		this.state.device.specifedName = this.state.specifedName;
		this.props.onChange([CommonComponents.BTNS_RETURNS.APPLY_FORM,this.state.device]);
	}
	render() {
		return <form onSubmit = {this.handleSubmit}>
				<div className="mb-3">
				<label htmlFor="dev-name-input" className="form-label">
					Device name
				</label>
				<input type="text" className="form-control" id="dev-name-input" defaultValue = {this.state.specifedName} required pattern="[a-zA-Z0-9_-]{1,16}"  onChange = {this.nameChange}/>
				<div className="form-text">
					Only letters , numbers and _ or - (from 1 to 16 characters)
				</div>
				<hr/>
			</div>
			<CommonComponents.BACK_APPLY_BTNS applying = {this.props.applying} onChange = {this.props.onChange}/>
		</form>;
	}
}

class DeviceImage extends React.Component {
	render() {
		return <img width={this.props.imgWidth} src = {this.props.device.imgSrc} className ='img-fluid' alt={this.props.device.description}></img>
	}
}

export class DevicesScreen extends React.Component {
	constructor(props) {
		super(props);

		this.onChange = this.onChange.bind(this);
		this.reload = this.reload.bind(this);
		this.componentWillUnmount = this.componentWillUnmount.bind(this);

		this.STATES = {
			EXTERNALLY_CAUSED : -1,
			GLOBAL_DEVICES : 0,
			MEGA_CHILDS : 1,
			PS_CHILDS : 2,
			GLOBAL_DEVICES_SETTINGS : 3,
			SENSOR_SETTINGS : 4,
			CHANNEL_SETTINGS : 5,
			OUTLET_SETTINGS : 6,
			PWM_SETTINGS : 7,
			SOLENOID_SETTINGS : 8,
			ANALOG_IN_SETTINGS : 9,
			CALIBRATION_SCREEN : 10,
			ADD_NEW_DEVICE : 11,
			CHART_SCREEN : 12
		}

		this.UPDATE_TIME = 4000;

		this.state = {
			error : false,
			loading : true,
			openedMegaSensor : 0
		}
		
		this.statesHistory = [];
	}
	componentDidMount() {
		this.setState({
			loading : true,
			error : false
		})

		let devicesMap = new Map();

		updateDevicesNamesMap()
		.then(namesMap => {
			Hub.getDevices()
			.then(devicesJSON => {
				try {
					devicesMap = updateDevicesMap(devicesMap,namesMap,devicesJSON);
					Hub.getDevicesColors()
					.then(devicesColors => {
						devicesMap = updateDevicesColors(devicesMap,devicesColors);
						this.setState({
							applying : false,
							namesMap : namesMap,
							devicesMap : devicesMap,
							loading : false,
							error : false,
							mode : this.props.mode,
							view :   this.props.onChange ? this.STATES.EXTERNALLY_CAUSED : this.STATES.GLOBAL_DEVICES,
							header : this.props.headerText || "Devices",
							device : null
						});
						this.updateDevicesTimer = setInterval(
							() => this.update(),
							this.UPDATE_TIME
						);
					})
				} catch (error) {
					this.setState ({
						error : "devices data not received (details in the console)",
						loading : false
					});
					throw(error);
				}
			})

		})
		.catch(error => {
			this.setState ({
				error : error,
				loading : false
			});
		})  
	}

	componentWillUnmount() {
		clearInterval(this.updateDevicesTimer);
	}
	update() {
		Hub.getDevices()
		.then(devicesJSON => {
			this.setState({
				devicesMap : updateDevicesMap(this.state.devicesMap,this.state.namesMap,devicesJSON)
			})
		})
	}
	reload() {
		this.setState({
			loading : true,
			error : false
		})

		let devicesMap = new Map();

		updateDevicesNamesMap()
		.then(namesMap => {
			Hub.getDevices()
			.then(devicesJSON => {
				try {
					devicesMap = updateDevicesMap(devicesMap,namesMap,devicesJSON);
					Hub.getDevicesColors()
					.then(devicesColors => {
						devicesMap = updateDevicesColors(devicesMap,devicesColors);
						this.setState({
							applying : false,
							namesMap : namesMap,
							devicesMap : devicesMap,
							loading : false,
							error : false,
							mode : this.props.mode,
							view :   this.props.onChange ? this.STATES.EXTERNALLY_CAUSED : this.STATES.GLOBAL_DEVICES,
							header : this.props.headerText || "Devices",
							device : null
						});
					})
				} catch (error) {
					this.setState ({
						error : "devices data not received (details in the console)",
						loading : false
					});
					throw(error);
				}
			})

		})
		.catch(error => {
			this.setState ({
				error : error,
				loading : false
			});
		})  
	}
	onChange(params) { // device or backButton
		if (params[0] instanceof Mega) {
			this.statesHistory.push(this.state);
			this.setState({header : `Sensors on Mega [${params[0].specifedName}]:`});
			this.setState({view : this.STATES.MEGA_CHILDS});
			this.setState({device : params[0]});

		} else if (params[0] instanceof PowerStrip) {
			this.statesHistory.push(this.state);
			this.setState({header : `Devices on PowerStrip [${params[0].specifedName}]:`});
			this.setState({view : this.STATES.PS_CHILDS});
			this.setState({device : params[0]});

		} else if (params[0] === CommonComponents.BTNS_RETURNS.BACK) {
			//-------------------
			// display charts stopped timer
			// we need to restart them
				if (this.updateDevicesTimer === null) {
				this.updateDevicesTimer = setInterval(
					() => this.update(),
					this.UPDATE_TIME
				);
			}
			//-------------------
			this.setState(this.statesHistory.pop());

		} else if (params[0] === CommonComponents.BTNS_RETURNS.SETTINGS) {
			this.statesHistory.push(this.state);
			if (params[1] instanceof Mega) {
				this.setState({header : `Mega settings [${params[1].specifedName}]:`});
				this.setState({view : this.STATES.GLOBAL_DEVICES_SETTINGS});
				this.setState({device : params[1]});

			} else if (params[1] instanceof PowerStrip) {
				this.setState({header : `PowerStrip settings [${params[1].specifedName}]:`});
				this.setState({view : this.STATES.GLOBAL_DEVICES_SETTINGS});
				this.setState({device : params[1]});

			} else if (params[1] instanceof Outlet) {
				this.setState({header : `Outlet settings [${params[1].specifedName}]:`});
				this.setState({view : this.STATES.OUTLET_SETTINGS});
				this.setState({device : params[1]});

			} else if (params[1] instanceof PWM_Output) {
				this.setState({header : `PWM out settings [${params[1].specifedName}]:`});
				this.setState({view : this.STATES.PWM_SETTINGS});
				this.setState({device : params[1]});

			} else if (params[1] instanceof Solenoid24vac) {
				this.setState({header : `Solenoid settings [${params[1].specifedName}]:`});
				this.setState({view : this.STATES.SOLENOID_SETTINGS});
				 this.setState({device : params[1]});

			} else if (params[1] instanceof AnalogInput) {
				this.setState({header : `Analog input settings [${params[1].specifedName}]:`});
				this.setState({view : this.STATES.ANALOG_IN_SETTINGS});
				this.setState({device : params[1]});

			} else if (params[1] instanceof MegaSensor) {
				this.setState({header : `Sensor settings [${params[1].specifedName}]:`});
				this.setState({view : this.STATES.SENSOR_SETTINGS});
				this.setState({device : params[1]});

			} else if (params[1] instanceof MegaSensorChannel) {
				this.setState({header : `Channel settings [${params[1].specifedName}]:`});
				this.setState({view : this.STATES.CHANNEL_SETTINGS});
				this.setState({device : params[1]});

			}
		} else if (params[0] === CommonComponents.BTNS_RETURNS.PLUS) { 
			this.props.onChange(params);
		} else if (params[0] === CommonComponents.BTNS_RETURNS.APPLY_FORM) {
			if (this.state.view === this.STATES.ADD_NEW_DEVICE)  {
				if (this.state.devicesMap.get(params[1].id.toUpperCase())) {
					alert ("Device with this ID already exists")
				} else {
					this.setState({
						applying : true
					});
					Hub.addNewGlobalDevice(params[1].id,params[1].type)
					.then(res => {
						this.reload();
					})
					.catch(err => {
						this.setState({
							applying : false
						});
						alert("Error on adding device (details in console)");
						throw(err);
					})
				}
			} else {
				this.setState({
					applying : true
				});
				Hub.setDevicesColors(this.state.devicesMap)
				.then(res => {
					Hub.setDevicesNames(this.state.devicesMap)
					.then(res => {
						const applyDevice = params[1];
						this.setState(this.statesHistory.pop());
						this.state.devicesMap.set(applyDevice.id,applyDevice);
					})
				})
				.catch(err => {
					this.setState({
						applying : false
					});
					alert("Error on uploading devices data (details in console)");
					throw(err);
				})
			}
		} else if (params[0] === "calibrationBtn") {
			this.statesHistory.push(this.state);
			this.setState({
				view : this.STATES.CALIBRATION_SCREEN,
				device : params[1]
			})
		} else if (params[0] === CommonComponents.BTNS_RETURNS.ADD_NEW) {
			this.statesHistory.push(this.state);
			this.setState({ 
				header : `Add new device settings :`,
				view : this.STATES.ADD_NEW_DEVICE,
				device : null
			});
		} else if (params[0] === "reload") {
			this.reload();
		} else if (params[0] === CommonComponents.BTNS_RETURNS.CHART) {
			this.statesHistory.push(this.state);
			this.setState({
				view : this.STATES.CHART_SCREEN,
				device : params[1] 
			})
			 
		} else if (params[0] === "openedMegaSensor") {
			this.setState({
				openedMegaSensor : params[1]
			})
		} else if (params[0] === "openedPSChild") {
			this.setState({
				openedPSChild : params[1]
			})
		}
	}
	
	render() {
		console.log("DevicesScreen render")
		if (this.state.loading) {
			return <CommonComponents.LoadingField />
		} else if (this.state.error) {
			return <CommonComponents.ReloadErrorField onChange = {this.reload} error = {this.state.error}/>
		} else {
			switch(this.state.view) {
				case (this.STATES.EXTERNALLY_CAUSED) : {
					return  <div >
						<GlobalDevicesList showVersion={this.props.showVersion} devicesMap = {this.state.devicesMap} mode = {this.state.mode} onChange = {this.onChange}/>
						<CommonComponents.ReturnBackBtn onChange = {this.props.onChange}/>
						<CommonComponents.ScanBtn/>
						{
							this.props.mode === DEVICES_VIEW_MODES.SETTINGS &&
							<CommonComponents.AddNewBtn onChange = {this.onChange}/>

						}
					</div>;
				}
				case (this.STATES.GLOBAL_DEVICES) : {
					return  <div >
								<CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
								<GlobalDevicesList showVersion={this.props.showVersion} devicesMap = {this.state.devicesMap} mode = {this.state.mode} onChange = {this.onChange}/>
							</div>;
				}
				case (this.STATES.MEGA_CHILDS) : {
				   return  <div >
								<CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
								<MegaSensorsList 
									mode = {this.state.mode} 
									device = {this.state.device} 
									onChange = {this.onChange}
									openedMegaSensor = {this.state.openedMegaSensor}
								/>
							</div>;
				}
				case (this.STATES.PS_CHILDS) : {
					return  <div >
								<CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
								<PowerStripDevicesList 
									mode = {this.state.mode} 
									device = {this.state.device} 
									onChange = {this.onChange}
									openedPSChild = {this.state.openedPSChild}
								/>
							</div>;
				}
				case (this.STATES.GLOBAL_DEVICES_SETTINGS) : {
				   return  <div >
								<CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
								<GlobalDevicesSettings applying = {this.state.applying} device = {this.state.device} onChange = {this.onChange}/>
							</div>;
				}
				case (this.STATES.ADD_NEW_DEVICE) : {
					return  <div >
								 <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
								 <AddNewGlobalDevice applying = {this.state.applying} onChange = {this.onChange}/>
							 </div>;
				 }
				case (this.STATES.SENSOR_SETTINGS) : {
					return  <div >
								<CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
								<MegaSensorSettings applying = {this.state.applying} device = {this.state.device} onChange = {this.onChange}/>
							</div>;
				}
				case (this.STATES.CHANNEL_SETTINGS) : {
					return  <div >
								<CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
								<MegaSensorChannelSettings applying = {this.state.applying} device = {this.state.device} onChange = {this.onChange}/>
							</div>;
				}
				case (this.STATES.OUTLET_SETTINGS) : {
					return  <div >
								<CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
								<OutletSettings applying = {this.state.applying} device = {this.state.device} onChange = {this.onChange}/>
							</div>;
				}
				case (this.STATES.PWM_SETTINGS) : {
					return  <div >
						<CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
						<PWMSettings applying = {this.state.applying} device = {this.state.device} onChange = {this.onChange}/>
					</div>;
				}
				case (this.STATES.SOLENOID_SETTINGS) : {
					return  <div >
								<CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
								<SolenoidSettings applying = {this.state.applying} device = {this.state.device} onChange = {this.onChange}/>
							</div>;
				}
				case (this.STATES.ANALOG_IN_SETTINGS) : {
					return  <div >
								<CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
								<AnalogInputSettings applying = {this.state.applying} device = {this.state.device} onChange = {this.onChange}/>
							</div>;
				}
				case (this.STATES.CALIBRATION_SCREEN) : {
					return <CalibrationScreen onChange = {this.onChange} device = {this.state.device}/>
				}
				case (this.STATES.CHART_SCREEN) : {
					clearInterval(this.updateDevicesTimer);
					this.updateDevicesTimer = null; 
					return <ChartScreen onChange = {this.onChange} device = {this.state.device}/>
				}
				default : {
				   return  <div >
								<CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
								<GlobalDevicesList showVersion={this.props.showVersion} onChange = {this.onChange}/>
							</div>;
				}
			}
		}
	}
}