

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import 'bootstrap-icons/font/bootstrap-icons.css'
//import 'bootstrap-select/dist/js/bootstrap-select.js'
//import 'bootstrap-select/dist/css/bootstrap-select.css'

import reportWebVitals from './reportWebVitals';


Map.prototype.toArray = function() {
  let a = []; 
  for (let[k,v] of this) {
    a.push({key:k,value:v})
  } 
  return a;
}

Date.prototype.YYYYMMDD = function() {
	var mm = this.getMonth() + 1;
	var dd = this.getDate();
  
	return [this.getFullYear(),
			(mm>9 ? '' : '0') + mm,
			(dd>9 ? '' : '0') + dd
		   ].join('-');
 }

Date.prototype.DDMM = function() {	
	var mm = this.getMonth() + 1;
	var dd = this.getDate();
  
	return [
		(dd>9 ? '' : '0') + dd,
		(mm>9 ? '' : '0') + mm
	].join('.');
 }

Date.prototype.HHmm = function() {
	var hh = this.getHours();
	var mm = this.getMinutes();
  
	return [(hh>9 ? '' : '0') + hh,
			(mm>9 ? '' : '0') + mm
		   ].join(':');
 }


ReactDOM.render(
    <App />,
  document.getElementById('root')
);


reportWebVitals();
