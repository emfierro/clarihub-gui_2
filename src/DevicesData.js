import SENSORS_IMAGES from './SensorImages'
import * as Hub from './Network';

export const DEVICES_TXT_TYPES = {
	MEGA : "mega",
	POWER_STRIP : "powerstrip",
	MEGA_SENSOR : "sensor",
	MEGA_SENSOR_CHANNEL : "channel",
	OUTLET : "outlet",
	PWM_OUTPUT : "pwm",
	SOLENOID : "solenoid",
	ANALOG_INPUT : "AnalogInput",
	SYSTEM : "system"
}

export class Mega {
	constructor(id,lastUpdate, specifedName, isEnable, isOnline) {
		this.type = DEVICES_TXT_TYPES.MEGA;
        this.imgSrc = SENSORS_IMAGES.Mega;
		this.id = id;
		this.lastUpdate = lastUpdate;
		this.specifedName = specifedName;
		this.isEnable = isEnable;
		this.isOnline = isOnline;
		this.sensors = [];
		this.color = "#8FBC8F";
	}
}
export class MegaSensor {
	constructor(id,isEnable, specifedName, sensorType, parent) {
		this.parent = parent;
		this.type = DEVICES_TXT_TYPES.MEGA_SENSOR;
		this.sensorType = sensorType;
		this.id = id;
		this.specifedName = specifedName;
		this.isEnable = isEnable;
		this.channels = [];
	}
}

export class MegaSensorChannel {
	constructor(id, type, value, corrector1, corrector2, specifedName, parent, arrayIndex) {
		this.parent = parent;
		this.id = id;
		this.ch_type = Number.parseInt(type);
		this.type = DEVICES_TXT_TYPES.MEGA_SENSOR_CHANNEL;
		this.value = value;
		this.corrector1 = corrector1;
		this.corrector2 = corrector2;
		this.specifedName = specifedName;
		this.nestingLevel = 3;
		this.schEnable = true;
		this.arrayIndex = arrayIndex;

        switch(this.ch_type) {
            case (1) :{
                this.imgSrc = SENSORS_IMAGES.air_humidity;
                this.description = 'air humidity';
				this.units = '%';
                break;
            }
            case (2) :{
                this.imgSrc = SENSORS_IMAGES.temperature;
                this.description = 'temperature';
				this.units = '\u00b0С';
                break;
            }
            case (3) :{
                this.imgSrc = SENSORS_IMAGES.luminosity;
                this.description = 'luminosity';
				this.units = 'lx';
                break;
            }
            case (4) :{
                this.imgSrc = SENSORS_IMAGES.co2_level;
                this.description = 'co2 level';
				this.units = 'ppm';
                break;
            }
            case (5) :{
                this.imgSrc = SENSORS_IMAGES.soil_humidity;
                this.description = 'soil humidity';
				this.units = '%';
                break;
            }
			default : {
				this.imgSrc = SENSORS_IMAGES.analog_input;
                this.description = 'no data';
				this.units = '?';
                break;
			}
	    }
    }
}

export class PowerStrip {
	constructor(id,lastUpdate,specifedName, isEnable, isOnline) {
		this.type = DEVICES_TXT_TYPES.POWER_STRIP;
		this.id = id;
		this.lastUpdate = lastUpdate;
		this.specifedName = specifedName;
		this.isEnable = isEnable;
		this.isOnline = isOnline;
		this.outlets = [];
		this.pwmsOutput12V = [];
		this.solenoids24vac = [];
		this.analogInputs = [];
        this.imgSrc = SENSORS_IMAGES.PowerStrip;
        this.description = 'Power Strip';
		this.color = "#8FBC8F";
	}
}

export class Outlet {
	constructor(id,currentSensor, isEnable, overCurrentFlag, specifedName, parent, arrayIndex){
		this.arrayIndex = arrayIndex;
		this.parent = parent;
		this.id = id;
		this.type = DEVICES_TXT_TYPES.OUTLET;
		this.currentSensor = currentSensor;
		this.isEnable = isEnable;
		this.overCurrentFlag = overCurrentFlag;
		this.specifedName = specifedName;
		this.schEnable = true;
        this.imgSrc = SENSORS_IMAGES.outlet;
        this.description = 'outlet';
        this.errorLevel = 0;
	}
}

export class PWM_Output {
	constructor(id, value, isEnable, specifedName, parent, arrayIndex) {
		this.parent = parent;
		this.arrayIndex = arrayIndex;
		this.id = id;
		this.type = DEVICES_TXT_TYPES.PWM_OUTPUT;
		this.value = value;
		this.isEnable = isEnable;
		this.specifedName = specifedName;
        this.imgSrc = SENSORS_IMAGES.pwm_output;
        this.description = 'pwm output';
	}
}


export class Solenoid24vac {
	constructor(id ,isEnable, specifedName, parent, arrayIndex) {
		this.arrayIndex = arrayIndex;
		this.parent = parent;
		this.id = id;
		this.type = DEVICES_TXT_TYPES.SOLENOID;
		this.isEnable = isEnable;
		this.specifedName = specifedName;
        this.imgSrc = SENSORS_IMAGES.solenoid_water_valve;
        this.description = "valve";
	}
}

export class AnalogInput {
	constructor(id, value , specifedName, parent, arrayIndex) {
		this.arrayIndex = arrayIndex;
		this.parent = parent;
		this.id = id;
		this.type = DEVICES_TXT_TYPES.ANALOG_INPUT;
		this.value = value;
		this.specifedName = specifedName;
		this.schEnable = true;
        this.imgSrc = SENSORS_IMAGES.analog_input;
        this.description = "analog input";
	}
}

export class DeviceTimer {
    constructor() {
		this.type = "timer";
        this.id = "timer";
        this.name = "timer";
        this.imgSrc = SENSORS_IMAGES.timer;
        this.description = "timer";
    }
}

export class DeviceClock {
    constructor() {
		this.type = "clock";
        this.id = "clock";
        this.name = "clock";
        this.imgSrc = SENSORS_IMAGES.clock;
        this.description = "clock";
    }
}

export class DeviceScriptAction {
	constructor(name) {
		this.type = "DeviceScriptAction";
		this.imgSrc = SENSORS_IMAGES.script;
		this.name = name;
	}
}

export class DeviceManualControl {
	constructor() {
		this.type = "DeviceManualControl";
		this.imgSrc = SENSORS_IMAGES.manual;
	}
}

export class DeviceDaily {
	constructor() {
		this.type = "DeviceDaily";
		this.imgSrc = SENSORS_IMAGES.daily;
	}
}

export function updateDevicesMap(devicesMap,namesMap,jsonData) {
 
	let devData = JSON.parse(jsonData);
	
	for (var i = 0; i < devData.length; i++) {
		devData[i].device = devData[i].device.toLowerCase();
		var specifedName;
		switch (devData[i].device) {
		 	case ("system") : { 
		 		break;
		 	}
			case ("mega") : {
				var mega = devicesMap.get(devData[i].hwUid);
				if (mega) {
					mega.lastUpdate = devData[i].lastUpdate;
					mega.isEnable = devData[i].isEnable;
					mega.isOnline = devData[i].isOnline;
				} else {
					 specifedName = namesMap.get(devData[i].hwUid);
					if (!specifedName) {
						specifedName = devData[i].hwUid;
					}
					mega = new Mega (	devData[i].hwUid,
						devData[i].lastUpdate,
						specifedName,
						devData[i].isEnable,
						devData[i].isOnline
					) ;
					devicesMap.set(mega.id,mega);
				}
				for (var sind = 0; sind < devData[i].sensors.length; sind++) {
					
					//ignore sensors [id === 000000000000]
					if (devData[i].sensors[sind].hwUid === "000000000000") continue;
					//----------------------------------
					
					var sensor = devicesMap.get(devData[i].sensors[sind].hwUid);
					if (sensor) {
						sensor.isEnable = devData[i].sensors[sind].isEnable;
					} else {
						 specifedName = namesMap.get(devData[i].sensors[sind].hwUid);
						if (!specifedName) {
							specifedName = devData[i].sensors[sind].hwUid;
						}
						sensor = new MegaSensor	(	devData[i].sensors[sind].hwUid,
							devData[i].sensors[sind].isEnable,
							specifedName,
							devData[i].sensors[sind].type,
							mega
						);
						devicesMap.set(sensor.id,sensor);
						mega.sensors.push(sensor);
					}
					for (var chnum = 0; chnum < devData[i].sensors[sind].channels.length; chnum++) {
						var channel = devicesMap.get(sensor.id+"-"+chnum);
						if (channel) {
							channel.value = devData[i].sensors[sind].channels[chnum].value;
						} else {
							 specifedName = namesMap.get(sensor.id+"-"+chnum);
							if (!specifedName) {
								specifedName = "channel-"+Number.parseInt(chnum+1);
							}
							var channel = new MegaSensorChannel	(	sensor.id+"-"+chnum,
																	devData[i].sensors[sind].channels[chnum].type,
																	devData[i].sensors[sind].channels[chnum].value,
																	devData[i].sensors[sind].channels[chnum].corrector1,
																	devData[i].sensors[sind].channels[chnum].corrector2,
																	specifedName,
																	sensor,
																	chnum
																);
							devicesMap.set(channel.id,channel);
							sensor.channels.push(channel);
						}
					}

				}
				break;
			}
			case ("powerstrip") : {
				var powerStrip = devicesMap.get(devData[i].hwUid);
				if (powerStrip) {
					powerStrip.lastUpdate = devData[i].lastUpdate;
					powerStrip.isEnable = devData[i].isEnable;
					powerStrip.isOnline = devData[i].isOnline;
				} else {
					 specifedName = namesMap.get(devData[i].hwUid);
					if (!specifedName) {
						specifedName = devData[i].hwUid;
					}
					powerStrip = new PowerStrip	(	devData[i].hwUid,
														devData[i].lastUpdate,
														specifedName,
														devData[i].isEnable,
														devData[i].isOnline
													) ;
					devicesMap.set(powerStrip.id,powerStrip);
				}
				for (var j = 0; j < devData[i].outlets.length; j++) {
					var outlet = devicesMap.get(powerStrip.id+"-outlet-"+j);
					if (outlet) {
						outlet.overCurrentFlag = devData[i].outlets[j].overCurrentFlag;
						outlet.currentSensor = devData[i].outlets[j].currentSensor;
						outlet.isEnable = devData[i].outlets[j].isEnable;
					} else {
						 specifedName = namesMap.get(powerStrip.id+"-outlet-"+j);
						if (!specifedName) {
							specifedName = "outlet-"+Number.parseInt(j+1);
						}
						var outlet = new Outlet	(	powerStrip.id+"-outlet-"+j,
													devData[i].outlets[j].currentSensor,
													devData[i].outlets[j].isEnable,
													devData[i].outlets[j].overCurrentFlag,
													specifedName,
													powerStrip,
													j
												);
						devicesMap.set(outlet.id,outlet);
						powerStrip.outlets.push(outlet);
					}
				}
				for (var j = 0; j < devData[i].pwmOutput12V.length; j++) {
					var out12v = devicesMap.get(powerStrip.id+"-pwm-"+j);
					if (out12v) {
						out12v.value = devData[i].pwmOutput12V[j].value;
						out12v.isEnable = devData[i].pwmOutput12V[j].isEnable;
					} else {
						 specifedName = namesMap.get(powerStrip.id+"-pwm-"+j);
						if (!specifedName) {
							specifedName = "pwm-"+Number.parseInt(j+1);
						}
						var out12v = new PWM_Output	(	powerStrip.id+"-pwm-"+j,
															devData[i].pwmOutput12V[j].value,
															devData[i].pwmOutput12V[j].isEnable,
															specifedName,
															powerStrip,
															j
														);
						devicesMap.set(out12v.id,out12v);
						powerStrip.pwmsOutput12V.push(out12v);
					}
				}
				for (var j = 0; j < devData[i].solenoid24vac.length; j++) {
					var solenoid = devicesMap.get(powerStrip.id+"-solenoid-"+j);
					if (solenoid) {
						solenoid.isEnable = devData[i].solenoid24vac[j]; 
					} else {
						 specifedName = namesMap.get(powerStrip.id+"-solenoid-"+j);
						if (!specifedName) {
							specifedName = "valve-"+Number.parseInt(j+1);
						}
						var solenoid = new Solenoid24vac	(	powerStrip.id+"-solenoid-"+j,
																devData[i].solenoid24vac[j],
																specifedName,
																powerStrip,
																j
															);
						devicesMap.set(solenoid.id,solenoid);
						powerStrip.solenoids24vac.push(solenoid);
					}
				}
				for (var j = 0; j < devData[i].analogInputs.length; j++) {
					var analog = devicesMap.get(powerStrip.id+"-analog-"+j);
					if (analog) {
						analog.value = devData[i].analogInputs[j];
					} else {
						 specifedName = namesMap.get(powerStrip.id+"-analog-"+j);
						if (!specifedName) {
							specifedName = "analog-"+Number.parseInt(j+1);
						}
						var analog = new AnalogInput	(	powerStrip.id+"-analog-"+j,
															devData[i].analogInputs[j],
															specifedName,
															powerStrip,
															j
														);
						devicesMap.set(analog.id,analog);
						powerStrip.analogInputs.push(analog);
					}
				}
				break;
			}
			
			default:{
				console.error("error at DevicesData.js  switch (devData[i].device) reaches default");break; 
			}  
		}
	}
	return devicesMap;
}

export function createNamesFile(devicesMap) {
	let data = "";
	for (let [key,value] of devicesMap) {
		data += key+" = "+value.specifedName+"\n";
	}
	return new File([data], "names", {
						type: "text/plain",
				});
}

//functions below are never used
/*
function addNewDevice(devicesMap) {
	return new Promise((resolve,reject) => {
		let id = document.getElementById("deviceIDInput").value;
		let name = document.getElementById("deviceNameInput").value; 
		let type = document.getElementById("deviceTypeSelect").value;
		let url = null;

		if (devicesMap.get(id.toUpperCase())) {
			reject("a device with this ID already exists");
			return;
		}

		switch (type) {
			case ('mega') : {
				url = '/addMega/'+id;
				break;
			}
			case ('powerStrip') : {
				url = '/addSPS/'+id;
				break;
			}
		}

		fetch(url)
		.then(response => {
			if (response.status !== 200) {
				reject ("response status : " + response.status)
			} else {
				response.json()
				.then(answer => {
					if (answer.result === "OK") {
						//Mega = function(id,lastUpdate, specifedName, isEnable, isOnline)
						if (type === 'mega') {
							var mega = new Mega (	id,
								null,
								name,
								null,
								null,
								null
							) ;
							devicesMap.set(id,mega);
						}
						if (type === 'powerStrip') {
							var ps = new PowerStrip (	id,
								null,
								name,
								null,
								null,
								null
							) ;
							devicesMap.set(id,ps);
						}
						resolve(true);
					} else {
						if (answer.result === "error1") {
							reject("Mega limit exceeded ("+answer.hubMaxSPS+")")
						}
						if (answer.result === "error2") {
							reject("PowerStrip limit exceeded ("+answer.hubMaxMEGAS+")")
						}
					}
				})
			}
		})
		.catch(error => {
			reject(error);
		})
	})
}

function getDisEnErrDescr(errorName) {
	switch (errorName) {
		case ('sl_TO') : {return  'slave timeout (not answer from PowerStrip)'}
		case ('ID_nf') : {return  'ID is not found'}
		case ('s_err') : {return  'syntax error'}
		case ('el_or') : {return  'element out of range'}
		case ('pa_or') : {return  'paramenter out of range'}
		default : {return  'unknown error'}
	}
}



function deviceNameIsUnique(devicesMap,device,name) {
	if (device instanceof Mega 
		|| device instanceof PowerStrip) {
			for (let [id,devInMap] of devicesMap) {
				if (devInMap instanceof Mega || (devInMap instanceof PowerStrip)) {
					if (devInMap.specifedName === name && device.specifedName != name && device.specifedName != name) {
						return false;
					}
				}
			}

    } else if (device instanceof MegaSensor) {

		for (let sensor of device.parent.sensors) {
			if (sensor.specifedName === name && device.specifedName != name) {
				return false;
			}
		}

    } else if (device instanceof MegaSensorChannel) {

        for (let channel of device.parent.channels) {
			if (channel.specifedName === name && device.specifedName != name) {
				return false;
			}
		}

    } else if (device instanceof Outlet) {

       for (let outlet of device.parent.outlets) {
		   if (outlet.specifedName === name && device.specifedName != name) {
			   return false;
		   }
	   }

    } else if (device instanceof PWM_Output) {

        for (let pwm of device.parent.pwmsOutput12V) {
			if (pwm.specifedName === name && device.specifedName != name) {
				return false;
			}
		}

    } else if (device instanceof Solenoid24vac) {

        for (let solenoid of device.parent.solenoids24vac) {
			if (solenoid.specifedName === name && device.specifedName != name) {
				return false;
			}
		}

    } else if (device instanceof AnalogInput) {

		for (let analog of device.parent.analogInputs) {
			if (analog.specifedName === name && device.specifedName != name) {
				return false;
			}
		}

    }
	return true;
}*/

export function updateDevicesColors(devicesMap,colorsData) {
	try{
		colorsData = JSON.parse(colorsData);
		for (let colorObj of colorsData.DevicesColors) {
			let device = devicesMap.get(colorObj.deviceID);
			if (device) {
				device.color = colorObj.deviceColor;
			}
		}
 	} catch (e) {

	} finally {
		return devicesMap;
	}
}

export function updateDevicesNamesMap() {
	return new Promise((resolve,reject) => {
		Hub.getDevicesNames()
		.then(response => {
			let namesMap = new Map();
			let idNames = response.split("\n");
			for ( let idName of idNames ) {
				let data = idName.trim();
				data = data.split("=");
				if (data.length != 2) continue;
				namesMap.set(data[0].trim(),data[1].trim());
			}
			resolve(namesMap);
		})
		.catch(error => {
			reject(error);
		})
	})
}