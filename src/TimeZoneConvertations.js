export function convertTimeZoneToViewFormat(timeZone) {
    timeZone = timeZone || new Date().getTimezoneOffset() * -1;
    let tzHours = Math.trunc(timeZone / 60);
    let tzMinutes = Math.abs(timeZone % 60);

    tzMinutes = tzMinutes < 10 ? "0" + tzMinutes : tzMinutes;
    
    if (tzHours >= 0) {
        tzHours = tzHours < 10 ? "+0" + tzHours : tzHours;
    } else {
        tzHours = tzHours > -10 ? "-0" + tzHours * -1 : tzHours;
    }
    return tzHours + ":" + tzMinutes;
}