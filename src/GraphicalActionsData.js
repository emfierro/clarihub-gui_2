import SENSORS_IMAGES from "./SensorImages";
import crc16_BUYPASS from './crc16';
import * as Hex from './HexOperations';


import {  MegaSensor,DeviceClock, DeviceScriptAction, DeviceTimer, Outlet, PWM_Output, Solenoid24vac, MegaSensorChannel, AnalogInput, PowerStrip, DeviceManualControl, DeviceDaily } from "./DevicesData";

import * as CommonComponents from './CommonComponents';
import { GraphicalActionsList } from "./GraphicalActionsView";

const GRAPH_ACTIONS_FILE_PATH = "/gs.dat";
const GRAPH_ACTIONS_NUMBER_LIMIT = 200;

const GA_HEX_TYPES = {
    dev_manual : "FFFF",
    dev_daily : "FFFA",
    dev_virtual : "0000",
    dev_sensor : "4D53",
    dev_timer : "746D",
    dev_clock : "5354",
    dev_sunset : "5353",
    dev_sunrise : "5352",
    dev_ps : "5053",
    dev_hub : "4855",
    dev_outlet : "444F",
    dev_solenoid : "4453",
    dev_analog_in : "4441",
    dev_pwm : "4450",
    dev_script : "5453",
    type_default : "00",
    type_default_waiting : "01",
    type_complementary : "02"
}

export class ConditionDeviceTrigger {
    constructor(device,value) {
        this.device = device || null;
        this.comparison = "=";
        this.value = value || 0;
    }
    getGAObject() {
        let devCondition;
        if (this.device instanceof MegaSensorChannel) {
            devCondition = {
                devType : GA_HEX_TYPES.dev_sensor,
                sensorId : this.device.parent.id,
                channelNumber : this.device.arrayIndex
            }
        } else if (this.device instanceof AnalogInput) {
            devCondition = {
                devType : GA_HEX_TYPES.dev_ps,
                psId : this.device.parent.id,
                psNode : GA_HEX_TYPES.dev_analog_in,
                nodeIndex : this.device.arrayIndex
            }
        }  else if (this.device instanceof DeviceClock) {
            devCondition = {
                devType : GA_HEX_TYPES.dev_clock,
            }
        }  else if (this.device instanceof DeviceTimer) {
            devCondition = {
                devType : GA_HEX_TYPES.dev_timer,
            }
        }  else if (this.device instanceof DeviceScriptAction) {
            devCondition = {
                devType : GA_HEX_TYPES.dev_script,
                scriptName : this.device.name
            }
        } else if (this.device instanceof DeviceManualControl) {
            devCondition = {
                devType : GA_HEX_TYPES.dev_manual
            }
        } else if (this.device instanceof DeviceDaily) {
            devCondition = {
                devType : GA_HEX_TYPES.dev_daily
            }
        }
        return devCondition;
    }
}

export class ConditionDeviceTermination {
    constructor(device,value) {
        this.device = device || null;
        this.comparison = "=";
        this.value = value || 0;
    }
    getGAObject() {
        let devCondition;
        if (this.device instanceof MegaSensorChannel) {
            devCondition = {
                devType : GA_HEX_TYPES.dev_sensor,
                sensorId : this.device.parent.id,
                channelNumber : this.device.arrayIndex
            }
        } else if (this.device instanceof AnalogInput) {
            devCondition = {
                devType : GA_HEX_TYPES.dev_ps,
                psId : this.device.parent.id,
                psNode : GA_HEX_TYPES.dev_analog_in,
                nodeIndex : this.device.arrayIndex
            }
        }  else if (this.device instanceof DeviceClock) {
            devCondition = {
                devType : GA_HEX_TYPES.dev_clock,
            }
        }  else if (this.device instanceof DeviceTimer) {
            devCondition = {
                devType : GA_HEX_TYPES.dev_timer,
            }
        }  else if (this.device instanceof DeviceScriptAction) {
            devCondition = {
                devType : GA_HEX_TYPES.dev_script,
                scriptName : this.device.name
            }
        } 
        return devCondition;
    }
}

export class ActionDevice {
    constructor(device,value) {
        this.device = device || null;
        this.value = value || 0;
    }
    getGAObject() {
        let deviceUsed;
        if (this.device instanceof Outlet) {
            deviceUsed = {
                devType : GA_HEX_TYPES.dev_ps,
                psId : this.device.parent.id,
                psNode : GA_HEX_TYPES.dev_outlet,
                nodeIndex : this.device.arrayIndex
            }
        } else if (this.device instanceof PWM_Output) {
            deviceUsed = {
                devType : GA_HEX_TYPES.dev_ps,
                psId : this.device.parent.id,
                psNode : GA_HEX_TYPES.dev_pwm,
                nodeIndex : this.device.arrayIndex
            }
        } else if (this.device instanceof Solenoid24vac) {
            deviceUsed = {
                devType : GA_HEX_TYPES.dev_ps,
                psId : this.device.parent.id,
                psNode : GA_HEX_TYPES.dev_solenoid,
                nodeIndex : this.device.arrayIndex
            }
        } else if (this.device instanceof DeviceScriptAction) { 
            deviceUsed = {
                devType : GA_HEX_TYPES.dev_script,
                scriptName : this.device.name
            }
        }
        return deviceUsed;
    }
}

export class GraphicalAction {
    constructor(settings) {
        /**
         *  settins = {
         *      hexContext : hexContext,
         *      devicesMap : devicesMap,
         *      graphicalActionsMap : graphicalActionsMap
         * }
         */

        if (settings.devicesMap) {
            this.devicesMap = settings.devicesMap;
        }
        if (settings.graphicalActionsMap) {
            this.graphicalActionsMap = settings.graphicalActionsMap;
        }

        this.conditionDeviceTrigger = new ConditionDeviceTrigger();
        this.actionDevice = new ActionDevice();
        this.conditionDeviceTermination = new ConditionDeviceTermination();

        if (settings.hexContext) {
            this.hexContext = settings.hexContext;
            this.initFromHex();
        } else {
            this.name = "NO_NAME";
            this.imgSrc = SENSORS_IMAGES.script;
            this.color = CommonComponents.DEFAULT_LIST_ELEMENT_COLOR;
            this.handlerVersion = 1;

            this.id = this.createGraphicalActionId();

            this.status = {
                active : true,
                type : "00"
            }
        }
    }
    initFromHex() {
        let gaDataArr = this.parseHexContext();
        this.handlerVersion = gaDataArr[0];
        this.id = gaDataArr[1];
        let devCondition1 = gaDataArr[2]; 
        let comparison1 = gaDataArr[3];
        let valueForCondition1 = gaDataArr[4];
        let devCondition2 = gaDataArr[5];
        let comparison2 = gaDataArr[6];
        let valueForCondition2 = gaDataArr[7];
        let deviceUsed = gaDataArr[8];
        let valueForDevice = gaDataArr[9];
        this.status = gaDataArr[10]; // ga type + is ga active
        this.crc16 = gaDataArr[11];

        if (devCondition1.devType === GA_HEX_TYPES.dev_sensor) { 
            valueForCondition1 = valueForCondition1 / 100;
        }

        if (devCondition2.devType === GA_HEX_TYPES.dev_sensor) { 
            valueForCondition2 = valueForCondition2 / 100;
        }

        if (devCondition1.devType === GA_HEX_TYPES.dev_ps) { 
            valueForCondition1 = valueForCondition1 / 100;
        }

        if (devCondition2.devType === GA_HEX_TYPES.dev_ps) { 
            valueForCondition2 = valueForCondition2 / 100;
        }

        this.conditionDeviceTrigger.device = this.getDeviceByGaObject(devCondition1);
        this.conditionDeviceTrigger.comparison = comparison1;
        this.conditionDeviceTrigger.value = valueForCondition1;

        this.actionDevice.device = this.getDeviceByGaObject(deviceUsed);
        this.actionDevice.value = valueForDevice;

        this.conditionDeviceTermination.device = this.getDeviceByGaObject(devCondition2);
        this.conditionDeviceTermination.comparison = comparison2;
        this.conditionDeviceTermination.value = valueForCondition2;
    }
    parseHexContext() {
        this.hex = this.hexContext; // hex for cutting
        let paramsArray = []
        while(this.hex.length > 0) {
            paramsArray.push(this.cutNextHexParamFromHexContext(paramsArray));
        }
        return paramsArray;
    }
    cutNextHexParamFromHexContext(paramsArray) {
        switch (paramsArray.length) {
            case (0) : { // handler version
                let version = parseInt(this.cutNextStaticHexParam(2),16);
                return version;
            }
            case (1) : { // id
                let id = parseInt(this.cutNextStaticHexParam(2),16);
                return id;
            }
            case (2) : { // dev condotion 1
                return this.cutDevConditionParam();
            }
            case (3) : { // comparison 1
                let comparison = Hex.decodeStrHex(this.cutNextStaticHexParam(2))
                return comparison;
            }
            case (4) : { // int value for condition 1 (4 bytes)
                let value = Hex.hexToNumber4(this.cutNextStaticHexParam(8));
                return value;
            }
            case (5) : { // dev condotion 2
                return this.cutDevConditionParam();
            }
            case (6) : { // comparison 2
                let comparison = Hex.decodeStrHex(this.cutNextStaticHexParam(2))
                return comparison;
            }
            case (7) : { // int value for condition 2 (4 bytes)
                let value = Hex.hexToNumber4(this.cutNextStaticHexParam(8));
                return value;
            }
            case (8) : { // devise used param
                return this.cutDeviceUsedParam();
            }
            case (9) : { // value for used device
                let value = Hex.hexToNumber4(this.cutNextStaticHexParam(8));
                return value;
            }
            case(10) : { // graphic action status
                let statusHex = this.cutNextStaticHexParam(4);
                let status = {
                    type : statusHex.substring(0,2),
                    active : Hex.decodeBoolHex(statusHex.substring(3,4))
                }
                return status;
            }
            case(11) : {
                return this.cutNextStaticHexParam(4);
            }
            default : {
                this.hex = "";
            }
        }
    }

    cutDevConditionParam() {
        let devType = this.hex.substring(0,4);
        this.hex =  this.hex.substring(4,this.hex.length);

        if (devType === GA_HEX_TYPES.dev_sensor) { // mega sensor
            let sensorId = this.hex.substring(0,12);
            this.hex =  this.hex.substring(12,this.hex.length);
            let channelNumber = this.hex.substring(0,2);
            this.hex =  this.hex.substring(2,this.hex.length);
            this.hex =  this.hex.substring(4,this.hex.length); // for \x00\x00
            return {
                devType : devType,
                sensorId : sensorId,
                channelNumber : channelNumber
            }
        } else if (devType === GA_HEX_TYPES.dev_ps) {
            let psId = this.hex.substring(0,12);
            this.hex =  this.hex.substring(12,this.hex.length);
            let psNode = this.hex.substring(0,4);
            this.hex =  this.hex.substring(4,this.hex.length);
            let nodeIndex = this.hex.substring(0,4);
            this.hex =  this.hex.substring(4,this.hex.length);
            this.hex =  this.hex.substring(8,this.hex.length); // for \x00\x00\x00\x00
            return {
                devType : devType,
                psId : psId,
                psNode : psNode,
                nodeIndex : nodeIndex
            }
        } else if (devType === GA_HEX_TYPES.dev_script) {
            let scriptName =  this.hex.substring(0,50);
            scriptName = Hex.decodeStrHex(scriptName).replace(/^.*[\\\/]/, '').split('.')[0];
            this.hex =  this.hex.substring(50,this.hex.length);
            return {
                devType : devType,
                scriptName : scriptName
            }
        } else {
            return {devType : devType}
        }
    }

    cutDeviceUsedParam() {
        let devType = this.hex.substring(0,4);
        this.hex =  this.hex.substring(4,this.hex.length);
        if (devType === GA_HEX_TYPES.dev_ps) { // powerstrip
            let psId = this.hex.substring(0,12);
            this.hex =  this.hex.substring(12,this.hex.length);
            let psNode = this.hex.substring(0,4);
            this.hex =  this.hex.substring(4,this.hex.length);
            let nodeIndex = this.hex.substring(0,4);
            this.hex =  this.hex.substring(4,this.hex.length);
            this.hex =  this.hex.substring(8,this.hex.length); // for \x00\x00\x00\x00
            return {
                devType : devType,
                psId : psId,
                psNode : psNode,
                nodeIndex : nodeIndex
            }
        } else if (devType === GA_HEX_TYPES.dev_hub) { // main hub
            let hubNode = this.hex.substring(0,4);
            this.hex =  this.hex.substring(4,this.hex.length);
            return {
                devType : devType,
                hubNode : hubNode
            }
        } else if (devType === GA_HEX_TYPES.dev_script) {
            let scriptName =  this.hex.substring(0,50);
            scriptName = Hex.decodeStrHex(scriptName).replace(/^.*[\\\/]/, '').split('.')[0];
            this.hex =  this.hex.substring(50,this.hex.length);
            return {
                devType : devType,
                scriptName : scriptName
            }
        }
    }

    cutNextStaticHexParam(paramSize) {
        let data = this.hex.substring(0,paramSize);
        this.hex =  this.hex.substring(paramSize,this.hex.length);
        return data;
    }

    createHexContext() {
        if (this.conditionDeviceTrigger.device.parent instanceof MegaSensor ||
            this.conditionDeviceTrigger.device.parent instanceof PowerStrip) {
                this.conditionDeviceTrigger.value =  Math.round(this.conditionDeviceTrigger.value * 100);
            }
        if (this.conditionDeviceTermination.device.parent instanceof MegaSensor ||
            this.conditionDeviceTermination.device.parent instanceof PowerStrip) {
                this.conditionDeviceTermination.value =  Math.round(this.conditionDeviceTermination.value * 100);
            }
        
        if (this.conditionDeviceTrigger.device instanceof DeviceManualControl) {
            this.status.active = true;
        }

        let tempHex = "";
        tempHex += Hex.numToHex(this.handlerVersion,1);
        tempHex += Hex.numToHex(this.id,1);
        // condition 1
        const devCondition1 = this.conditionDeviceTrigger.getGAObject();
        tempHex += devCondition1.devType;
        if (devCondition1.devType === GA_HEX_TYPES.dev_sensor) {
            tempHex += devCondition1.sensorId;
            tempHex += Hex.numToHex(devCondition1.channelNumber,1);
            tempHex += "0000";
        }
        if (devCondition1.devType === GA_HEX_TYPES.dev_ps) {
            tempHex += devCondition1.psId;
            tempHex += devCondition1.psNode;
            tempHex += Hex.numToHex(devCondition1.nodeIndex,2);
            tempHex += "00000000";
        }
        if (devCondition1.devType === GA_HEX_TYPES.dev_script) {
            if(devCondition1.scriptName === "null") {
                throw new Error("Action not selected");
            }
            tempHex += Hex.strToHex(devCondition1.scriptName+".wws",25);
        }
        tempHex += Hex.strToHex(this.conditionDeviceTrigger.comparison,1);
        tempHex += Hex.numToHex(this.conditionDeviceTrigger.value,4,true);
        // end condition 1
        //condition 2
        const devCondition2 = this.conditionDeviceTermination.getGAObject();
        tempHex += devCondition2.devType;
        if (devCondition2.devType === GA_HEX_TYPES.dev_sensor) {
            tempHex += devCondition2.sensorId;
            tempHex += Hex.numToHex(devCondition2.channelNumber,1);
            tempHex += "0000";
        }
        if (devCondition2.devType === GA_HEX_TYPES.dev_ps) {
            tempHex += devCondition2.psId;
            tempHex += devCondition2.psNode;
            tempHex += Hex.numToHex(devCondition2.nodeIndex,2);
            tempHex += "00000000";
        }
        if (devCondition2.devType === GA_HEX_TYPES.dev_script) {
            if(devCondition2.scriptName === "null") {
                throw new Error("Action not selected");
            }
            tempHex += Hex.strToHex(devCondition2.scriptName+".wws",25);
        }
        tempHex += Hex.strToHex(this.conditionDeviceTermination.comparison,1);
        tempHex += Hex.numToHex(this.conditionDeviceTermination.value,4,true);
        //end condition 2
        const deviceUsed = this.actionDevice.getGAObject();
        tempHex += deviceUsed.devType;
        if (deviceUsed.devType === GA_HEX_TYPES.dev_ps) { // powerstrip
            tempHex += deviceUsed.psId;
            tempHex += deviceUsed.psNode;
            tempHex += Hex.numToHex(deviceUsed.nodeIndex,2);
            tempHex += "00000000";
        } else if (deviceUsed.devType === GA_HEX_TYPES.dev_hub) { // main hub
            tempHex += deviceUsed.hubNode;  
        }
        else if (deviceUsed.devType === GA_HEX_TYPES.dev_script) { // text script
            if(deviceUsed.scriptName === "null") {
                throw new Error("Action not selected");
            }
            tempHex += Hex.strToHex(deviceUsed.scriptName+".wws",25);
        }
        tempHex += Hex.numToHex(this.actionDevice.value,4,true);
        tempHex += this.status.type;
        tempHex += Hex.boolToHex(this.status.active,1);
        tempHex = tempHex.toUpperCase();
        tempHex += crc16_BUYPASS(tempHex);
        return tempHex;
    }
    
    getDeviceByGaObject(GADeviceObject) {
        try{
            switch (GADeviceObject.devType) {
                case (GA_HEX_TYPES.dev_ps) : {
                    GADeviceObject.nodeIndex = Number.parseInt(GADeviceObject.nodeIndex);
                    switch (GADeviceObject.psNode) {
                        case (GA_HEX_TYPES.dev_outlet) : {
                            if (this.devicesMap.get([GADeviceObject.psId,"outlet",GADeviceObject.nodeIndex].join("-"))) {
                                return this.devicesMap.get([GADeviceObject.psId,"outlet",GADeviceObject.nodeIndex].join("-"));
                            } else {
    
                            }
                            
                        }
                        case (GA_HEX_TYPES.dev_pwm) : {
                            return this.devicesMap.get([GADeviceObject.psId,"pwm",GADeviceObject.nodeIndex].join("-"));
                        }
                        case (GA_HEX_TYPES.dev_analog_in) : {
                            return this.devicesMap.get([GADeviceObject.psId,"analog",GADeviceObject.nodeIndex].join("-"));
                        }
                        case (GA_HEX_TYPES.dev_solenoid) : {
                            return this.devicesMap.get([GADeviceObject.psId,"solenoid",GADeviceObject.nodeIndex].join("-"));
                        }
                    }
                    break;
                }
                case (GA_HEX_TYPES.dev_sensor) : {
                    GADeviceObject.channelNumber = Number.parseInt(GADeviceObject.channelNumber);
                    return this.devicesMap.get([GADeviceObject.sensorId,GADeviceObject.channelNumber].join("-"));
                }
                case (GA_HEX_TYPES.dev_timer) : {
                    return new DeviceTimer();
                }
                case (GA_HEX_TYPES.dev_clock) : {
                    return new DeviceClock();
                }
                case (GA_HEX_TYPES.dev_script) : {
                    return new DeviceScriptAction(GADeviceObject.scriptName);
                }
                case (GA_HEX_TYPES.dev_manual) : {
                    return new DeviceManualControl();
                }
                case (GA_HEX_TYPES.dev_daily) : {
                    return new DeviceDaily();
                }
            }
        } catch(e) {

        }
        
    }

    createGraphicalActionId() {
        let freeIDsArr = [];
            for (let i = 0; i < 256; i++) {
                if (!this.graphicalActionsMap.get(i))
                    freeIDsArr.push(i);
            }
            if (freeIDsArr.length === 0) {
                throw("Limit of GraphicalActions reached (on createGraphicalActionId())"); 
            } else {
                let randPos = Math.random()*freeIDsArr.length;
                randPos = Math.trunc(randPos);
                return freeIDsArr[randPos];
            }
    }

    crc16match() {
        if (!this.hexContext) return false;
        let crc16 = this.hexContext.substring(this.hexContext.length - 4);
	    let gaData = this.hexContext.substring(0,this.hexContext.length-4);
	    return crc16_BUYPASS(gaData) === crc16;
    }
}

export function copyGraphicalAction(ga) {
    let copy = Object.assign({} , ga);

    Object.setPrototypeOf( ga, GraphicalAction.prototype );

    let _conditionDeviceTriggerClone = Object.assign({}, ga.conditionDeviceTrigger)
    Object.setPrototypeOf(_conditionDeviceTriggerClone, ConditionDeviceTrigger.prototype);

    let _conditionDeviceTerminationClone = Object.assign({}, ga.conditionDeviceTermination);
    Object.setPrototypeOf(_conditionDeviceTerminationClone, ConditionDeviceTermination.prototype);

    let _actionDeviceClone = Object.assign({}, ga.actionDevice);
    Object.setPrototypeOf(_actionDeviceClone, ActionDevice.prototype);

    copy.conditionDeviceTermination = _conditionDeviceTerminationClone;
    copy.conditionDeviceTrigger = _conditionDeviceTriggerClone;
    copy.actionDevice = _actionDeviceClone;

    return copy;
}

export function getManualGraphicalActionsList(graphicalActionsMap){
    let gasList = [];
    for (let [k,v] of graphicalActionsMap) {
        if (v.conditionDeviceTrigger.device instanceof DeviceManualControl) {
            gasList.push(v);
        }
    }
    return gasList;
}

export function getConditionGraphicalActionsList(graphicalActionsMap) {
    let gasList = [];
    for (let [k,v] of graphicalActionsMap) {
        if (!(v.conditionDeviceTrigger.device instanceof DeviceManualControl)) {
            gasList.push(v);
        }
    }
    return gasList;
}

export function initGraphicalActionsMap(graphicalActionsHex,devicesMap) {
    let graphicalActionsMap = new Map();
    let gaArray = graphicalActionsHex.split("\n");
    for (let gaHex of gaArray) {

        if (gaHex === "") continue;

        let ga = new GraphicalAction({
            hexContext : gaHex.trim(),
            devicesMap : devicesMap
        });
        if (ga.crc16match()) {
            graphicalActionsMap.set(ga.id,ga);
        }
    }
    return graphicalActionsMap;
}

export function getGraphicalActionIdByName(graphicalActionName , graphicalActionList) {
    for (let ga of graphicalActionList) {
        if (ga.name === graphicalActionName) {
            return ga.id;
        }
    }
    throw("Graphical action with name ["+graphicalActionName+"] not exist");
}

export function initGaViewData (json, graphicalActionsMap) {
	try{
		json = JSON.parse(json);
		for (let viewObj of json.gaViewData) {
            let ga = graphicalActionsMap.get(viewObj.gaID);
            if (ga) {
                ga.color = viewObj.gaColor;
                ga.name = viewObj.gaName;
                ga.imgSrc = viewObj.gaImgSrc;
            }
		}
 	} catch (e) {

	} finally {
        return graphicalActionsMap;
    }
}