import * as Hex from './HexOperations';
import {
    getDevicesMap,
    MegaSensorChannel,
    Outlet,
    AnalogInput
} from './DevicesData';
import crc16_BUYPASS from './crc16';

export const SCHEDULES_TYPES = {
    PERIODICALLY    	: "periodically",
	PERIODICALLY_INT 	: "03",

    ONE_TIME        	: "oneTime",
	ONE_TIME_INT		: "01",

    DAILY           	: "daily",
	DAILY_INT			: "02",

    BY_VALUE        	: "byValue",
	BY_VALUE_MEGA_INT	: "04",
	BY_VALUE_PS_INT		: "05",

    SUNSET          	: "sunset",
	SUNSET_INT			: "06",

    SUNRISE         	: "sunrise",
	SUNRISE_INT			: "07"
}

export class Schedule {
	constructor(id,schName,isActive,type) {
		this.scriptData = {};
		this.id = id;
		this.schName = schName;
		this.isActive = isActive;
		this.type = type;
		this.dateTime = 0;
		this.dailyTime = null;
		this.period = null;
		this.sensorId = null;
		this.sensorValue = null;
		this.valueComparison = null;
		this.timeShifting = null;
		this.currentSensor = null;
		this.overCurrentFlag = null;
	}
	getIconClass() {
		switch (this.type) {
			case (SCHEDULES_TYPES.PERIODICALLY) : {
				return "bi bi-arrow-clockwise";
			}
			case (SCHEDULES_TYPES.ONE_TIME) : {
				return "bi bi-clock";
			}
			case (SCHEDULES_TYPES.DAILY) : {
				return "bi bi-sun";
			}
			case (SCHEDULES_TYPES.BY_VALUE) : {
				return "bi bi-arrow-clockwise";
			}
			case (SCHEDULES_TYPES.SUNSET) : {
				return "bi bi-sunset";
			}
			case (SCHEDULES_TYPES.SUNRISE) : {
				return "bi bi-sunrise";
			}
		}
	}
	periodToSeconds() {
		return this.period;
	}
	getHex() {
		var result = "";
		switch (this.type) {
			case (SCHEDULES_TYPES.ONE_TIME): {
				var result = SCHEDULES_TYPES.ONE_TIME_INT;
				result += Hex.numToHex(this.dateTime,4);
				break;
			}
			case (SCHEDULES_TYPES.DAILY): {
				var result = SCHEDULES_TYPES.DAILY_INT;
				result += Hex.numToHex(this.dailyTime,4);
				break;
			}
			case (SCHEDULES_TYPES.PERIODICALLY): {
				var periodicity = this.periodToSeconds();
				var result = SCHEDULES_TYPES.PERIODICALLY_INT;
				result += Hex.numToHex(periodicity,4);
				break;
			}
		/*	case ("byValue"): {
				var valueComparison = "=";
				switch (this.valueComparison) {
					case ("more") : {
						valueComparison = ">";
						break;
					}
					case ("less") : {
						valueComparison = "<";
						break;
					}
				}
				var result = "";
				let device = devicesMap.get(this.sensorId);
				if (device instanceof MegaSensorChannel) {
					var channelData = this.sensorId.split(":");
					let sensorId = channelData[0];
					let megaId = devicesMap.get(sensorId).parent.id;
					let channelNumber = channelData[1];
					result += "04" + megaId.padStart(12,"0") + sensorId.padStart(12,"0") + Hex.numToHex(channelNumber,1) + Hex.strToHex(valueComparison,1) + Hex.numToHex(this.sensorValue,4);
					break;
				} else if (device instanceof Outlet) {
					var result = "05";
					let deviceData = this.sensorId.split(":");
					let psId = deviceData[0];
					let devNum = deviceData[2];
					result += psId.padStart(12,"0"); // powerStrip id
					result += "01"; // sensor type
					result += Hex.numToHex(devNum,1); // sensor number
					if (this.currentSensor) {
						result += "01"; // value type
						result += Hex.strToHex(valueComparison,1);
						result += Hex.numToHex(this.currentSensor,4); // value
					} else if (this.overCurrentFlag) {
						result += "02"; // value type
						result += Hex.strToHex(valueComparison,1);
						result += Hex.numToHex(this.overCurrentFlag,4); // value
					} else {
						return "";
					}
					break;
				} else if (device instanceof AnalogInput) {
					var result = "05";
					let deviceData = this.sensorId.split(":");
					let psId = deviceData[0];
					let devNum = deviceData[2];
					result += psId; // powerStrip id
					result += "02"; // sensor type = analog
					result += Hex.numToHex(devNum,1); // sensor number
					result += "01";
					result += Hex.strToHex(valueComparison,1);
					result += Hex.numToHex(this.sensorValue,4);
					break;
				}
			} */
			case (SCHEDULES_TYPES.SUNSET): {
				var result = "";
				var result = SCHEDULES_TYPES.SUNSET_INT;
				result += Hex.numToHex(this.timeShifting,4,true);
				break;
			}
			case (SCHEDULES_TYPES.SUNRISE): {
				var result = "";
				var result = SCHEDULES_TYPES.SUNRISE_INT;
				result += Hex.numToHex(this.timeShifting,4,true);
				break;
			}
			default : return;
		}
		switch(this.scriptData.scriptType) {
			case ("manual") : {
				result += "47";
				result += Hex.numToHex(this.scriptData.scriptId,1);
				break;
			}
			case ("text") : {
				result += "54";
				result += Hex.strToHex(this.scriptData.scriptName,16);
				break;
			}
			case ("condition") : {
				result += "48";
				result += Hex.numToHex(this.scriptData.scriptId,1);
				result += Hex.boolToHex(this.scriptData.conditionActionToState,1);
				break;
			}
		}
		result += Hex.boolToHex(this.isActive,1);
		result += Hex.numToHex(this.id,1);
		result += crc16_BUYPASS(result);
		return result;
	}
}
export function initSchedulesNamesMap(namesHexData) {
	let namesMap = new Map();
	let idNames = namesHexData.split("\n");
	for ( let idName of idNames ) {
		let data = idName.trim();
		data = data.split("=");
		if (data.length != 2) continue;
		namesMap.set(
			Number.parseInt(data[0].trim()),
			data[1].trim()
		);
	}
	return namesMap;
}

export function initSchedulesMap(schedulesHexData,schedulesNamesMap,gaNamesMap) { 
	var schedulesArr = schedulesHexData.split("\n");
	let schedulesMap = new Map();
	for (var i = 0; i < schedulesArr.length; i++) {
		if (!checkScheduleCRC16 (schedulesArr[i]) ) {
			continue;
		}
		var scheduleType = schedulesArr[i].substring(0,2);
		var schedule = null;
		switch (scheduleType) {
			case (SCHEDULES_TYPES.ONE_TIME_INT) : {
				schedule = createOneTimeSchedule(schedulesNamesMap,schedulesArr[i],gaNamesMap);
				break;
			}
			case (SCHEDULES_TYPES.DAILY_INT) : {
				schedule = createDailySchedule(schedulesNamesMap,schedulesArr[i],gaNamesMap)
				break;
			}
			case (SCHEDULES_TYPES.PERIODICALLY_INT) : {
				schedule = createPeriodicallySchedule(schedulesNamesMap,schedulesArr[i],gaNamesMap);
				break;
			}
			case (SCHEDULES_TYPES.BY_VALUE_MEGA_INT) : {
			//	schedule = createByValueMegaSchedule(schedulesNamesMap,schedulesArr[i]);
				break;
			}
			case (SCHEDULES_TYPES.BY_VALUE_PS_INT) : {
			//	schedule = createByValuePowerStripSchedule(schedulesArr[i]);
				break;
			}
			case (SCHEDULES_TYPES.SUNSET_INT) : {
				schedule = createSunsetSchedule(schedulesNamesMap,schedulesArr[i],gaNamesMap);
				break;
			}
			case (SCHEDULES_TYPES.SUNRISE_INT) : {
				schedule = createSunriseSchedule(schedulesNamesMap,schedulesArr[i],gaNamesMap);
				break;
			}
		}
		schedulesMap.set(schedule.id,schedule);
	}
	return schedulesMap;
}

function checkScheduleCRC16(data) {
	var crc16 = data.substring(data.length - 4);
	var scheduleData = data.substring(0,data.length-4);
	return crc16_BUYPASS(scheduleData) === crc16;
}
function createOneTimeSchedule(schedulesNamesMap,data,gaNamesMap) {
	let hex = data;
	hex = hex.substring(2,hex.length);
	let unixTime = parseInt(hex.substring(0,8),16);
	hex = hex.substring(8,hex.length);
	let scriptData = {};
	let scriptTypeHex = hex.substring(0,2);
	hex = hex.substring(2,hex.length);
	switch (scriptTypeHex) {
		case ("54") : { // text
			scriptData = {
				scriptType : "text",
				scriptName : Hex.decodeStrHex(hex.substring(0,32))
			}
			hex = hex.substring(32,hex.length);
			break;
		}
		case ("47") : { // manual ga
			let scriptId = parseInt(hex.substring(0,2),16)
			hex = hex.substring(2,hex.length);
			scriptData = {
				scriptType : "manual",
				scriptId: scriptId,
				scriptName : gaNamesMap.get(scriptId)
			}
			break;
		}
		case ("48") : { // condition ga
			let scriptId = parseInt(hex.substring(0,2),16)
			hex = hex.substring(2,hex.length);
			let conditionActionToState = Hex.decodeBoolHex(hex.substring(0,2));
			hex = hex.substring(2,hex.length);
			scriptData = {
				scriptType : "condition",
				scriptId: scriptId,
				conditionActionToState : conditionActionToState,
				scriptName : gaNamesMap.get(scriptId)
			}
			break;
		}
	}
	var isActive = Hex.decodeBoolHex(hex.substring(0,2));
	hex = hex.substring(2,hex.length);
	var id = parseInt(hex.substring(0,2),16);
	hex = hex.substring(2,hex.length);
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = SCHEDULES_TYPES.ONE_TIME+"_"+id;
	}
	var schedule = new Schedule(id,scheduleName,isActive,SCHEDULES_TYPES.ONE_TIME);
	schedule.dateTime = unixTime;
	schedule.scriptData = scriptData;
	return schedule;
}

function createDailySchedule(schedulesNamesMap,data,gaNamesMap) {
	let hex = data;
	hex = hex.substring(2,hex.length);
	let dailyTime = parseInt(hex.substring(0,8),16);
	hex = hex.substring(8,hex.length);
	let scriptData = {};
	let scriptTypeHex = hex.substring(0,2);
	hex = hex.substring(2,hex.length);
	switch (scriptTypeHex) {
		case ("54") : { // text
			scriptData = {
				scriptType : "text",
				scriptName : Hex.decodeStrHex(hex.substring(0,32))
			}
			hex = hex.substring(32,hex.length);
			break;
		}
		case ("47") : { // manual ga
			let scriptId = parseInt(hex.substring(0,2),16)
			hex = hex.substring(2,hex.length);
			scriptData = {
				scriptType : "manual",
				scriptId: scriptId,
				scriptName : gaNamesMap.get(scriptId)
			}
			break;
		}
		case ("48") : { // condition ga
			let scriptId = parseInt(hex.substring(0,2),16)
			hex = hex.substring(2,hex.length);
			let conditionActionToState = Hex.decodeBoolHex(hex.substring(0,2));
			hex = hex.substring(2,hex.length);
			scriptData = {
				scriptType : "condition",
				scriptId: scriptId,
				conditionActionToState : conditionActionToState,
				scriptName : gaNamesMap.get(scriptId)
			}
			break;
		}
	}
	var isActive = Hex.decodeBoolHex(hex.substring(0,2));
	hex = hex.substring(2,hex.length);
	var id = parseInt(hex.substring(0,2),16);
	hex = hex.substring(2,hex.length);
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = SCHEDULES_TYPES.DAILY+"_"+id;
	}
	var schedule = new Schedule(id,scheduleName,isActive,SCHEDULES_TYPES.DAILY);
	schedule.dailyTime = dailyTime;
	schedule.scriptData = scriptData;
	return schedule;
}
function createPeriodicallySchedule(schedulesNamesMap,data,gaNamesMap) {
	let hex = data;
	hex = hex.substring(2,hex.length);
	let periodicity = parseInt(hex.substring(0,8),16);
	hex = hex.substring(8,hex.length);
	let scriptData = {};
	let scriptTypeHex = hex.substring(0,2);
	hex = hex.substring(2,hex.length);
	switch (scriptTypeHex) {
		case ("54") : { // text
			scriptData = {
				scriptType : "text",
				scriptName : Hex.decodeStrHex(hex.substring(0,32))
			}
			hex = hex.substring(32,hex.length);
			break;
		}
		case ("47") : { // manual ga
			let scriptId = parseInt(hex.substring(0,2),16)
			hex = hex.substring(2,hex.length);
			scriptData = {
				scriptType : "manual",
				scriptId: scriptId,
				scriptName : gaNamesMap.get(scriptId)
			}
			break;
		}
		case ("48") : { // condition ga
			let scriptId = parseInt(hex.substring(0,2),16)
			hex = hex.substring(2,hex.length);
			let conditionActionToState = Hex.decodeBoolHex(hex.substring(0,2));
			hex = hex.substring(2,hex.length);
			scriptData = {
				scriptType : "condition",
				scriptId: scriptId,
				conditionActionToState : conditionActionToState,
				scriptName : gaNamesMap.get(scriptId)
			}
			break;
		}
	}
	var isActive = Hex.decodeBoolHex(hex.substring(0,2));
	hex = hex.substring(2,hex.length);
	var id = parseInt(hex.substring(0,2),16);
	hex = hex.substring(2,hex.length);
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = SCHEDULES_TYPES.PERIODICALLY+"_"+id;
	}
	var schedule = new Schedule(id,scheduleName,isActive,SCHEDULES_TYPES.PERIODICALLY);
	schedule.period = periodicity;
	schedule.scriptData = scriptData;
	return schedule;
}
/*function createByValueMegaSchedule(schedulesNamesMap,data) {
	var sensorId = data.substring(14,14+12);
	var chNum = parseInt(data.substring(26,26+2),16);
	var valueComparison = Hex.decodeStrHex(data.substring(28,30));
	if (valueComparison === '>') {
		valueComparison = "more";
	}
	if (valueComparison === '<') {
		valueComparison = "less";
	}
	var sensorValue = parseInt(data.substring(30,38),16);
	var scriptName = Hex.decodeStrHex(data.substring(38,38+32));
	var isActive = Hex.decodeBoolHex(data.substring(70,72));
	var id = parseInt(data.substring(72,74),16).toString();
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = "byValue:"+id;
	}
	var schedule = new Schedule(id,scheduleName,isActive,"byValue")
	schedule.sensorValue = sensorValue;
	schedule.valueComparison = valueComparison;
	schedule.sensorId = sensorId+":"+chNum;
	return schedule;
}
function createByValuePowerStripSchedule(schedulesNamesMap,data) {
	var psId = data.substring(2,2+12);
	var sensorType = data.substring(14,16);
	var sensorNumber =  parseInt(data.substring(16,18),16);
	var valueType = data.substring(18,20);
	var valueComparison = Hex.decodeStrHex(data.substring(20,22));
	if (valueComparison === '>') {
		valueComparison = "more";
	}
	if (valueComparison === '<') {
		valueComparison = "less";
	}
	var value = parseInt(data.substring(22,30),16);
	var scriptName = Hex.decodeStrHex(data.substring(30,62));
	var isActive = Hex.decodeBoolHex(data.substring(62,64));
	var id = parseInt(data.substring(64,66),16).toString();
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = "byValue:"+id;
	}
	var schedule = new Schedule(id,scheduleName,scriptName,isActive,"byValue")
	schedule.valueComparison = valueComparison;
	switch (sensorType) {
		case ("01") : {
			schedule.sensorId = psId+":outlet:"+sensorNumber;
			switch (valueType) {
				case ("01") : {
					schedule.currentSensor = value;
					break;}
				case ("02") : {
					schedule.overCurrentFlag = value;
					break;}
			}
			break;
		}
		case ("02") : {
			schedule.sensorId = psId+":analog:"+sensorNumber;
			schedule.sensorValue = value;
			break;
		}
	}
	return schedule;
}*/
function createSunsetSchedule(schedulesNamesMap,data,gaNamesMap) {
	let hex = data;
	hex = hex.substring(2,hex.length);
	let timeShifting = parseInt(hex.substring(0,8),16);
	if (timeShifting > (4294967296/2)-1) {
		timeShifting = (4294967296 - timeShifting) * -1;
	}
	hex = hex.substring(8,hex.length);
	let scriptData = {};
	let scriptTypeHex = hex.substring(0,2);
	hex = hex.substring(2,hex.length);
	switch (scriptTypeHex) {
		case ("54") : { // text
			scriptData = {
				scriptType : "text",
				scriptName : Hex.decodeStrHex(hex.substring(0,32))
			}
			hex = hex.substring(32,hex.length);
			break;
		}
		case ("47") : { // manual ga
			let scriptId = parseInt(hex.substring(0,2),16)
			hex = hex.substring(2,hex.length);
			scriptData = {
				scriptType : "manual",
				scriptId: scriptId,
				scriptName : gaNamesMap.get(scriptId)
			}
			break;
		}
		case ("48") : { // condition ga
			let scriptId = parseInt(hex.substring(0,2),16)
			hex = hex.substring(2,hex.length);
			let conditionActionToState = Hex.decodeBoolHex(hex.substring(0,2));
			hex = hex.substring(2,hex.length);
			scriptData = {
				scriptType : "condition",
				scriptId: scriptId,
				conditionActionToState : conditionActionToState,
				scriptName : gaNamesMap.get(scriptId)
			}
			break;
		}
	}
	var isActive = Hex.decodeBoolHex(hex.substring(0,2));
	hex = hex.substring(2,hex.length);
	var id = parseInt(hex.substring(0,2),16);
	hex = hex.substring(2,hex.length);
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = SCHEDULES_TYPES.SUNSET+"_"+id;
	}
	var schedule = new Schedule(id,scheduleName,isActive,SCHEDULES_TYPES.SUNSET);
	schedule.timeShifting = timeShifting;
	schedule.scriptData = scriptData;
	return schedule;
}
function createSunriseSchedule(schedulesNamesMap,data,gaNamesMap) {
	let hex = data;
	hex = hex.substring(2,hex.length);
	let timeShifting = parseInt(hex.substring(0,8),16);
	if (timeShifting > (4294967296/2)-1) {
		timeShifting = (4294967296 - timeShifting) * -1;
	}
	hex = hex.substring(8,hex.length);
	let scriptData = {};
	let scriptTypeHex = hex.substring(0,2);
	hex = hex.substring(2,hex.length);
	switch (scriptTypeHex) {
		case ("54") : { // text
			scriptData = {
				scriptType : "text",
				scriptName : Hex.decodeStrHex(hex.substring(0,32))
			}
			hex = hex.substring(32,hex.length);
			break;
		}
		case ("47") : { // manual ga
			let scriptId = parseInt(hex.substring(0,2),16)
			hex = hex.substring(2,hex.length);
			scriptData = {
				scriptType : "manual",
				scriptId: scriptId,
				scriptName : gaNamesMap.get(scriptId)
			}
			break;
		}
		case ("48") : { // condition ga
			let scriptId = parseInt(hex.substring(0,2),16)
			hex = hex.substring(2,hex.length);
			let conditionActionToState = Hex.decodeBoolHex(hex.substring(0,2));
			hex = hex.substring(2,hex.length);
			scriptData = {
				scriptType : "condition",
				scriptId: scriptId,
				conditionActionToState : conditionActionToState,
				scriptName : gaNamesMap.get(scriptId)
			}
			break;
		}
	}
	var isActive = Hex.decodeBoolHex(hex.substring(0,2));
	hex = hex.substring(2,hex.length);
	var id = parseInt(hex.substring(0,2),16);
	hex = hex.substring(2,hex.length);
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = SCHEDULES_TYPES.SUNRISE+"_"+id;
	}
	var schedule = new Schedule(id,scheduleName,isActive,SCHEDULES_TYPES.SUNRISE);
	schedule.timeShifting = timeShifting;
	schedule.scriptData = scriptData;
	return schedule;
}
export function getNextScheduleId(schedulesMap) {
	let freeIds = [];
	for (let i = 0; i < 256; i++) {
		if (!schedulesMap.get(i)) {
			freeIds.push(i);
		}
	}
	let id = 0;
	id =  Math.trunc(Math.random() * freeIds.length);
	return id;
}
