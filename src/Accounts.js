import React from 'react';
import * as CommonComponents from './CommonComponents';

class AccountElement extends React.Component {
  constructor(props){
    super(props);
    this.handleChangeSelect = this.handleChangeSelect.bind(this);
    this.handleChangeChecks = this.handleChangeChecks.bind(this);
    this.onSaveButtonPressed = this.onSaveButtonPressed.bind(this);
    this.state = {
      account: props.account,
      isChanged: false,
    };
  }

  handleChangeSelect(event){
    let key = event.target.name;
    if(this.state.account.privilegies.hasOwnProperty(key)) {
      let newstate = Object.assign({}, this.state.account);
      newstate.privilegies[key] = event.target.value;
      this.setState(newstate);
    }
    this.onInputsChanged();
  }
  
  handleChangeChecks(event){
    let key = event.target.name;
    if(this.state.account.privilegies.hasOwnProperty(key)) {
      let newstate = Object.assign({}, this.state.account);
      newstate.privilegies[key] = event.target.checked;
      this.setState(newstate);
    }
    this.onInputsChanged();
  }

  onInputsChanged(){
    this.setState({"isChanged": true});
  }

  onSaveButtonPressed(){
    this.setState({"isChanged": false})
  }

  render(){
    console.log(this.state.account);
    if(this.state.account){
      let disabledInput = false;
      let settingsButtonName = "Settings";
      if(this.props.permission != "write") {
        disabledInput = true;
        settingsButtonName = "Settings (readonly)";
      }

      return <div>
        <div className="container-sm mb-3 w-auto border rounded border-primary">
          <div className="row p-2">
            <div className="col-sm-2">
              <span>Login: {this.state.account.login}</span>
            </div>
            <div className="col-sm-2">
              <span>Uid: {this.state.account.uid}</span>
            </div>
            <div className="col-sm-2">
              <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target={"#accounts-collapse-"+this.state.account.uid} aria-expanded="false" aria-controls={"#accounts-collapse-"+this.state.account.uid}>
              {settingsButtonName}
              </button>
            </div>

          </div>
          
          <div className="row  collapse" id={"accounts-collapse-"+this.state.account.uid}>
            <div className="col-4 container m-3 border border-success rounded-3">
              
              <div className="row p-2 border-bottom border-success">
                <div className="col text-center">
                  Privilegies
                </div>
              </div>  

              <div className="row">
                <div className="col">
                  <div className="container">
                    <div className="row p-2 border-bottom ">
                      <div class="col-sm">
                        Accounts
                      </div>
                      <div className="col-sm">
                        <select class="form-select" name="accounts" onChange={this.handleChangeSelect} value={this.state.account.privilegies.accounts} disabled={disabledInput} >
                          <option value="none">No</option>
                          <option value="read">View</option>
                          <option value="write">Changing</option>
                        </select>
                      </div>
                    </div>

                    <div className="row p-2 border-bottom">
                      <div class="col-sm">
                        Graphical actions
                      </div>
                      <div className="col-sm">
                        <select class="form-select" name="int_scr" onChange={this.handleChangeSelect} value={this.state.account.privilegies.int_scr} disabled={disabledInput}>
                          <option value="none">No</option>
                          <option value="read">View</option>
                          <option value="write">Changing</option>
                        </select>
                      </div>
                    </div>

                    <div className="row p-2 border-bottom">
                      <div class="col-sm">
                        Schedule
                      </div>
                      <div className="col-sm">
                        <select class="form-select" name="schedule" onChange={this.handleChangeSelect} value={this.state.account.privilegies.schedule} disabled={disabledInput}>
                          <option value="none">No</option>
                          <option value="read">View</option>
                          <option value="write">Changing</option>
                        </select>
                      </div>
                    </div>

                    <div className="row p-2 border-bottom">
                      <div class="col-sm">
                        Script actions
                      </div>
                      <div className="col-sm">
                        <select class="form-select" name="text_scr" onChange={this.handleChangeSelect} value={this.state.account.privilegies.text_scr} disabled={disabledInput}>
                          <option value="none">No</option>
                          <option value="read">View</option>
                          <option value="write">Changing</option>
                        </select>
                      </div>
                    </div>

                    <div className="row p-2 border-bottom">
                      <div class="col-sm">
                        Settings
                      </div>
                      <div className="col-sm">
                        <select class="form-select" name="settings" onChange={this.handleChangeSelect} value={this.state.account.privilegies.settings} disabled={disabledInput}>
                          <option value="none">No</option>
                          <option value="read">View</option>
                          <option value="write">Changing</option>
                        </select>
                      </div>
                    </div>
                    
                    <div className="row p-2 border-bottom">
                      <div class="col-sm">
                        Logs
                      </div>
                      <div className="col-sm">
                        <input class="form-check-input" type="checkbox" name="logs" onChange={this.handleChangeChecks} checked={this.state.account.privilegies.logs} id={"account-perms-logs-" + this.state.account.uid} disabled={disabledInput}/>
                      </div>
                    </div>

                    <div className="row p-2 border-bottom">
                      <div class="col-sm">
                        Status
                      </div>
                      <div className="col-sm">
                        <input class="form-check-input" type="checkbox" name="status" onChange={this.handleChangeChecks} checked={this.state.account.privilegies.status} id={"account-perms-status-" + this.state.account.uid} disabled={disabledInput}/>
                      </div>
                    </div>

                    <div className="row p-2 border-bottom">
                      <div class="col-sm">
                        Control
                      </div>
                      <div className="col-sm">
                        <input class="form-check-input" type="checkbox" name="device_control" onChange={this.handleChangeChecks} checked={this.state.account.privilegies.device_control} id={"account-perms-device-control-" + this.state.account.uid} disabled={disabledInput}/>
                      </div>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div className="row-sm p-2 border-bottom">
              <button class={this.state.isChanged ? "btn btn-primary" : "btn btn-outline-secondary"} type="button" onClick={this.onSaveButtonPressed} disabled={disabledInput}>
                Save
              </button>
            </div>
          </div>
        </div>

      </div>
    }
    else {
      return <div></div>;
    }
  }

}

class Accounts extends React.Component {
  constructor(props){
    super(props);
   // this.reload = this.reload.bind(this);
    this.userPermission = props.permissions.accounts;
    this.state = {
      inputObj: null,
    };
  }
  reload(){
    //fetch getAccounts
    let accountsData = JSON.parse('{"result":"ok","accounts":[{"login":"Nicolas","uid":25,"privilegies":{"accounts":"write","int_scr":"write","schedule":"none","text_scr":"read","settings":"read","logs":true,"status":true,"device_control":true},"last_action":123},{"login":"Andry","uid":12,"privilegies":{"accounts":"read","int_scr":"write","schedule":"none","text_scr":"read","settings":"read","logs":true,"status":true,"device_control":true},"last_action":20}]}');
    console.log(accountsData);
    console.log("asdasd123");
    this.setState({
      inputObj: accountsData,
    });
  }
  componentWillMount() {
    this.reload();
  }
    
  render() {
    if(this.userPermission != "none"){
      let accounts = [];
      for(let i = 0; i < this.state.inputObj.accounts.length; i++){
        let el = <AccountElement account={this.state.inputObj.accounts[i]} permission={this.userPermission}></AccountElement>
        accounts.push(el);
      }
      return <div className = "container">
        <form class="mw-120">
            Accounts: 
            <hr/>
            {accounts}
        </form>
      </div>;
    }
    else {
      return <CommonComponents.AccessDeniedMessage />
    }
  }
}

export default Accounts;