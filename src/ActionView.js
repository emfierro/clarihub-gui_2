import React from 'react';
import { Script } from './ScriptActionsData';
import {ScriptActionSettings, ScriptActionsList, ScriptActionView} from './ScriptActionsView';
import {GraphicalActionsList, GraphicalActionsView, CreateNewGraphicalAction} from './GraphicalActionsView'
import * as CommonComponents from './CommonComponents';
import * as Hub from './Network';
import {updateDevicesMap,updateDevicesNamesMap} from './DevicesData';
import * as GraphicalActionsData from './GraphicalActionsData';

const SORT_TYPES = {
    AZ : 1,
    ZA : 2,
    MANUAL : 3,
    AUTO : 4
}

class ActionSortSelect extends React.Component {
    constructor(props) {
        super(props);
        this.handlerChange = this.handlerChange.bind(this);
        this.state = {
            value : this.props.sort
        }
    }
    handlerChange(element){
        this.setState({
            value : element.target.value
        });
        this.props.onChange(["actionsSort",element.target.value]);
    }
    render() {
        return <select className = "form-select my-2" onChange = {this.handlerChange} value = {this.state.value}>
            <option value = {SORT_TYPES.AZ}>Sort alphabetically (a-z)</option>
            <option value = {SORT_TYPES.ZA}>Sort alphabetically (z-a)</option>
            <option value = {SORT_TYPES.MANUAL}>Sort by type (manual start type first)</option>
            <option value = {SORT_TYPES.AUTO}>Sort by type (auto start type first)</option>
        </select>;
    }
}

class ActionTypeSelect extends React.Component {
    constructor(props) {
        super(props);
        this.handlerChange = this.handlerChange.bind(this);
        this.state = {
            value : this.props.select 
        }
    }
    handlerChange(element){
        this.setState({
            value : element.target.value
        });
        this.props.onChange(["actionsType",element.target.value]);
    }
    render() {
        return <select className = "form-select my-2" onChange = {this.handlerChange} value = {this.state.value}>
            <option value = "ga">Graphical actions</option>
            <option value = "txt">Script actions</option>
        </select>;
    }
}

class ActionsScreen extends React.Component {

    constructor(props){
        super(props);
        this.onChange = this.onChange.bind(this);
        this.reload = this.reload.bind(this);
        this.userPermissionSA = props.permissions.text_scr; //script actions
        this.userPermissionGA = props.permissions.int_scr; //graphical actions
        console.log(this.userPermissionGA);
        console.log(this.userPermissionSA);
        this.STATES = {
            TXT_SCRIPTS_LIST : 0,
            GA_SCRIPTS_LIST : 1,
            TXT_SCRIPT_SETTINGS: 2,
            TXT_SCRIPT : 3,
            GA_SCRIPT_SETTINGS : 4,
            GA_SCRIPT : 5
        }
        this.state = {
            view : this.STATES.GA_SCRIPTS_LIST,
            select : "ga",
            header : "Graphical actions list",
            gaSortType : SORT_TYPES.AZ,
            loading : true,
            error : false
        };
        this.statesHistory = [];
    }
    componentDidMount() {
        this.reload();
    }
    reload() {
        this.setState({
            loading : true,
            error : false
        });
        let devicesMap = new Map();
        updateDevicesNamesMap()
        .then(namesMap => {
            Hub.getDevices()
            .then(devicesJSON => {
                try {
                    devicesMap = updateDevicesMap(devicesMap,namesMap,devicesJSON);
                    Hub.getGraphicalActions()
                    .then(gasHex => {
                        let graphicalActionsMap = GraphicalActionsData.initGraphicalActionsMap(gasHex,devicesMap);
                        Hub.getGraphicalActionsData()
                        .then(gasData => {
                            Hub.getScriptActionsList()
                            .then(scriptActions => {
                                graphicalActionsMap = GraphicalActionsData.initGaViewData(gasData,graphicalActionsMap);
                                this.setState({
                                    loading : false,
                                    error : false,
                                    devicesMap : devicesMap,
                                    graphicalActionsMap : graphicalActionsMap,
                                    scriptActions : scriptActions
                                })
                            })
                        })
                    })
                } catch (error) {
                    this.setState ({
                        error : "Data loading error (details in the console)",
                        loading : false
                    });
                    throw(error);
                }
            })
        })
        .catch(error => {
            this.setState ({
                error : "Data loading error (details in the console)",
                loading : false
            });
            throw(error);
        })
    }
    onChange(params) {
        if (params === CommonComponents.APPLYING_SUCCESSFUL) {
            this.setState({
                view : this.STATES.GA_SCRIPTS_LIST,
                select : "ga",
                header : "Graphical actions list"
            });
            this.statesHistory = [];
        } else {
        if (params[0] instanceof GraphicalActionsData.GraphicalAction) {
            this.statesHistory.push(this.state);
            this.setState({
                view : this.STATES.GA_SCRIPT,
                ga : params[0]
            });
        } else {
            switch (params[0]) {
                case (CommonComponents.BTNS_RETURNS.BACK) : {
                    this.setState(this.statesHistory.pop());
                    this.reload();
                    break;
                }
                case (CommonComponents.BTNS_RETURNS.ADD_NEW) : {
                    this.statesHistory.push(this.state);
                    if (this.state.view === this.STATES.TXT_SCRIPTS_LIST) {
                        this.setState({
                            view : this.STATES.TXT_SCRIPT_SETTINGS,
                            header : "New script action"
                        });
                    } else if (this.state.view === this.STATES.GA_SCRIPTS_LIST) {
                        this.setState({
                            view : this.STATES.GA_SCRIPT_SETTINGS,
                            ga : null,
                            header : "New graphical action"
                        });
                    }
                    break;
                }

                    case (CommonComponents.BTNS_RETURNS.SETTINGS) : {
                        this.statesHistory.push(this.state);
                        this.setState({
                            view : this.STATES.TXT_SCRIPT_SETTINGS
                        });
                        break;
                    }
                case (CommonComponents.BTNS_RETURNS.APPLY_FORM) : {
                    if (this.state.view === this.STATES.TXT_SCRIPT_SETTINGS) {
                        let lowerCaseActionsList = [];
                        for (let e of this.state.scriptActions) {
                            lowerCaseActionsList.push(e.toLowerCase())
                        }
                        if (lowerCaseActionsList.includes(params[1].name.toLowerCase())) {
                            alert ("A action with the same name already exists");
                        } else {
                            this.setState({
                                view : this.STATES.TXT_SCRIPT,
                                currentScriptAction : params[1],
                                isNewTextScript : true
                            });
                        }  
                    } else if (this.state.view === this.STATES.GA_SCRIPT_SETTINGS) {
                        this.setState({
                            ga : params[1],
                            view : this.STATES.GA_SCRIPT
                        });
                    }
                    break;
                }
                case ("scriptAction") : {
                    this.statesHistory.push(this.state);
                    this.setState({
                        view : this.STATES.TXT_SCRIPT,
                        currentScriptAction : params[1],
                        isNewTextScript : false
                    });
                    break;
                }
                case ("actionsType") : {
                    switch(params[1]) {
                        case ("ga") : {
                            this.statesHistory.push(this.state);
                            this.setState({
                                view : this.STATES.GA_SCRIPTS_LIST,
                                select : "ga",
                                header : "Graphical actions list"
                            });
                            break;
                        }
                        case ("txt") : {
                            this.statesHistory.push(this.state);
                            this.setState({
                                view : this.STATES.TXT_SCRIPTS_LIST,
                                select : "txt",
                                header : "Script actions list"
                            });
                            break;
                        }
                        default:{
                            console.error("error at ActionView.js switch (params[1])");break; 
                        }  
                    }
                    break;
                }
                case ("actionsSort") : {
                    this.setState({
                        gaSortType : params[1]
                    });break;  
                }

                default:{
                    console.error("error at ActionView.js switch (params[0])");break; 
                }  
            }
        }
    }
    }
    render() {
        if (this.state.loading) {
            return <CommonComponents.LoadingField />
        } else if (this.state.error) {
            return <CommonComponents.ReloadErrorField onChange = {this.reload} error = {this.state.error}/>
        } else {
            switch (this.state.view) {
                case (this.STATES.TXT_SCRIPTS_LIST) : {
                    if(this.userPermissionSA == "write"){
                        return <div>
                            <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                            <ActionTypeSelect select = {this.state.select} onChange = {this.onChange}/>
                            <ScriptActionsList  scriptActions = {this.state.scriptActions} onChange = {this.onChange}/>
                            <CommonComponents.AddNewBtn onChange = {this.onChange}/>
                        </div>;
                    }
                    else if(this.userPermissionSA == "read"){
                        console.log("reeeaadd");
                        return <div>
                            <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                            <ActionTypeSelect select = {this.state.select} onChange = {this.onChange}/>
                            <ScriptActionsList readonly = {true} scriptActions = {this.state.scriptActions} onChange = {this.onChange}/>
                        </div>;
                    }
                    else {
                        return <div>
                            <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                            <ActionTypeSelect select = {this.state.select} onChange = {this.onChange}/>
                            <CommonComponents.AccessDeniedMessage />
                        </div>
                    }
                }
                case (this.STATES.GA_SCRIPTS_LIST) : {
                    if(this.userPermissionGA == "write"){
                        return <div>
                            <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                            <div className = "row mt-2">
                                <div className = "col-6">
                                    <ActionTypeSelect select = {this.state.select} onChange = {this.onChange}/>
                                </div>
                                <div className = "col-6">
                                    <ActionSortSelect sort = {this.state.gaSortType} onChange = {this.onChange} />
                                </div>
                            </div>
                            <GraphicalActionsList sortType = {this.state.gaSortType} scriptActions = {this.state.scriptActions} graphicalActionsMap = {this.state.graphicalActionsMap} onChange = {this.onChange}/>
                            <CommonComponents.AddNewBtn onChange = {this.onChange}/>
                        </div>;
                    }
                    else if(this.userPermissionGA == "read"){
                        return <div>
                            <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                            <div className = "row mt-2">
                                <div className = "col-6">
                                    <ActionTypeSelect select = {this.state.select} onChange = {this.onChange}/>
                                </div>
                                <div className = "col-6">
                                    <ActionSortSelect sort = {this.state.gaSortType} onChange = {this.onChange} />
                                </div>
                            </div>
                            <GraphicalActionsList readonly = {true} sortType = {this.state.gaSortType} scriptActions = {this.state.scriptActions} graphicalActionsMap = {this.state.graphicalActionsMap} onChange = {this.onChange}/>
                        </div>;
                    }
                    else {
                        return <div>
                            <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                            <ActionTypeSelect select = {this.state.select} onChange = {this.onChange}/>
                            <CommonComponents.AccessDeniedMessage />
                        </div>
                    }                    
                }
                case (this.STATES.TXT_SCRIPT_SETTINGS) : {
                    return <div>
                         <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                        <ScriptActionSettings scriptAction = {new Script("NoName","")} onChange = {this.onChange} />
                    </div>;
                }
                case (this.STATES.TXT_SCRIPT) : {
                    return <div>
                        <ScriptActionView 
                            scriptActionsList = {this.state.scriptActions}
                            isNew = {this.state.isNewTextScript} 
                            scriptAction = {this.state.currentScriptAction} 
                            onChange = {this.onChange}
                        />
                    </div>;
                }
                case (this.STATES.GA_SCRIPT_SETTINGS) : {
                    return (<div>
                        <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                        <CreateNewGraphicalAction 
                            onChange = {this.onChange}
                            graphicalActionsMap = {this.state.graphicalActionsMap}
                        /> 
                    </div>);
                }
                case (this.STATES.GA_SCRIPT) : {
                    return (<GraphicalActionsView 
                        onChange = {this.onChange}
                        ga = {this.state.ga}
                        graphicalActionsMap = {this.state.graphicalActionsMap}
                        scriptActions = {this.state.scriptActions}
                    />);
                }
                
                default:{
                    console.error("error at ActionView.js  switch (this.state.view) reaches default");break; 
                }  
            }
        }
    }
}

export default ActionsScreen;