import React from 'react';


import SENSORS_IMAGES from './SensorImages';
import * as CommonComponents from './CommonComponents';
import CONSTANTS from './Constants';
import { AnalogInput, DeviceClock, DeviceTimer, MegaSensorChannel, DeviceManualControl, DeviceScriptAction, Outlet, PWM_Output, Solenoid24vac, DeviceDaily } from './DevicesData';
import {updateDevicesMap, updateDevicesNamesMap} from './DevicesData';
import * as GraphicalActionsData from './GraphicalActionsData';
import {DevicesScreen, DEVICES_VIEW_MODES} from './DevicesView';
import CALC_HH_MM_SS from './Seconds';

import * as Hub from './Network'

import * as TimeZone from './TimeZoneConvertations'

let CONDITION_TYPES = {
    TRIGGER : "trigger",
    TERMINATION : "termination"
}

const SORT_TYPES = {
    AZ : 1,
    ZA : 2,
    MANUAL : 3,
    AUTO : 4
}

class GraphicalActionsSettings extends React.Component { 
    constructor(props) {
        super(props);
        this.STATES = {
            MAIN_SCREEN : 0,
            IMG_SELECT_SCREEN : 1
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.colorChange = this.colorChange.bind(this);
        this.nameChange = this.nameChange.bind(this);
        this.imgChange = this.imgChange.bind(this);

        this.state = {  color :   this.props.ga.color,
                        name :   this.props.ga.name,
                        imgSrc : this.props.ga.imgSrc,
                        view : this.STATES.MAIN_SCREEN
        }
    }
    colorChange(e) {
        this.setState({color : e.target.value});
    }
    nameChange(e) {
        this.setState({name : e.target.value});
    }
    handleSubmit(e) {
        e.preventDefault();
        const resGa = this.props.ga;
        resGa.color = this.state.color;
        resGa.name = this.state.name;
        resGa.imgSrc = this.state.imgSrc;
        this.props.onChange([CommonComponents.BTNS_RETURNS.APPLY_FORM,resGa]);
    }
    imgChange(params){
        if (params[0] === "imgSrc") {
            this.setState({
                imgSrc : params[1],
                view : this.STATES.MAIN_SCREEN
            });
        }
    }
    render() {
        switch (this.state.view) {
            case (this.STATES.MAIN_SCREEN) : {
                const divStyle = {borderRadius : "5px",cursor:"pointer"};
                return <form onSubmit = {this.handleSubmit}>
                    <div className="mb-3">
                    <label htmlFor="dev-name-input" className="form-label">
                        Device name
                    </label>
                    <input type="text" className="form-control" id="dev-name-input"  defaultValue = {this.state.name} required pattern="[a-zA-Z0-9_-]{1,16}" onChange = {this.nameChange}/>
                    <div className="form-text">
                        Only letters , numbers and _ or - (from 1 to 16 characters)
                    </div>
                    <label htmlFor="device-color" className="form-label">
                        Color for <strong>Device</strong>
                    </label>
                    <input type="color" defaultValue = {this.state.color} className="form-control form-control-lg" onChange = {this.colorChange}/>
                </div>
                <div  className="text-center mb-3 p-1 border bg-light row" style= {divStyle} onClick = {() => this.setState({view : this.STATES.IMG_SELECT_SCREEN})} >
                    <div className="col-2">
                        <img width={CONSTANTS.SM_LIST_ELEM_IMG_WIDTH} src={this.state.imgSrc} className="img-fluid" alt="..."></img>
                    </div>
                    <div className="col-9 my-auto">
                        <h5>Select the icon</h5>
                    </div>
                </div>
                <CommonComponents.BACK_APPLY_BTNS onChange = {this.props.onChange}/>
            </form>;
            } 
            case (this.STATES.IMG_SELECT_SCREEN) : {
                return <CommonComponents.SelectImage onChange = {this.imgChange}/>
            }
        }
        
    }
}

class GraphicalActionsSelectDeviceType extends React.Component {
    render() {
        const divStyle = {
            backgroundColor : "#8FBC8F",
            borderRadius : "5px",
            cursor : "pointer"
        }
        return <div className = "row my-3 py-1" style={divStyle} onClick = {() => this.props.onChange([this.props.deviceType])}>
            <div className = "col-2" >
                <CommonComponents.ImageOnListElement width = {CONSTANTS.LIST_ELEM_IMG_WIDTH} src = {this.props.imgSrc} />
            </div>
            <div className = "col-10 my-auto text-center">
                <h5>{this.props.deviceType}</h5>
            </div>
        </div>;
    }
}

class GraphicalActionsActionSelect extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.DEVICES_TYPES = {
            PS : "PowerStrips list",
            SCRIPT : "Script action"
        };
        this.STATES = {
            MAIN_SCREEN : 0,
            SHOW_PS_LIST : 1
        }
        this.state = {
            view : this.STATES.MAIN_SCREEN
        }
        this.statesHistory = [];
    }
    onChange(params) {
        switch (params[0]) {
            case (CommonComponents.BTNS_RETURNS.BACK) : {
                this.setState(this.statesHistory.pop());
                break;
            }
            case (CommonComponents.BTNS_RETURNS.PLUS) : {
                this.props.onChange(params);
                break;
            } 
            case (this.DEVICES_TYPES.PS) : {
                this.statesHistory.push(this.state);
                this.setState({
                    view : this.STATES.SHOW_PS_LIST
                });
                break;
            }
            case (this.DEVICES_TYPES.SCRIPT) : {
                this.props.onChange([CommonComponents.BTNS_RETURNS.PLUS,new DeviceScriptAction()]);
                break;
            }
        }
    }
    render() {
        switch (this.state.view) {
            case (this.STATES.MAIN_SCREEN) : {
                return <div>
                    <CommonComponents.ContainerHeaderText headerText = "Select action device:" />
        
                    <GraphicalActionsSelectDeviceType onChange = {this.onChange} deviceType = {this.DEVICES_TYPES.PS} imgSrc = {SENSORS_IMAGES.PowerStrip}/>
                    <GraphicalActionsSelectDeviceType onChange = {this.onChange} deviceType = {this.DEVICES_TYPES.SCRIPT} imgSrc = {SENSORS_IMAGES.script}/>
        
                    <CommonComponents.ReturnBackBtn onChange = {this.props.onChange}/>
                </div>;
            }
            case (this.STATES.SHOW_PS_LIST) : {
                return <div>
                    <CommonComponents.ContainerHeaderText headerText = "Select action device:"/>
                    <DevicesScreen mode = {DEVICES_VIEW_MODES.ACTION} onChange = {this.onChange} />
                </div>
                
            }
        }
       
    }
}

class GraphicalActionsConditionSelect extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.DEVICES_TYPES = {
            DEVICES : "Sensor or analog input",
            SCRIPT : "Script action",
            ONE_TIME : "One time",
            TIMER : "Timer",
            MANUAL : "Manual control",
            DAILY : "Daily"
        };
        this.STATES = {
            MAIN_SCREEN : 0,
            SHOW_DEVICES : 1
        }
        this.state = {
            view : this.STATES.MAIN_SCREEN
        }
        this.statesHistory = [];
    }
    onChange(params) {
        switch (params[0]) {
            case (CommonComponents.BTNS_RETURNS.BACK) : {
                this.setState(this.statesHistory.pop());
                break;
            }
            case (CommonComponents.BTNS_RETURNS.PLUS) : {
                this.props.onChange(params);
                break;
            } 
            case (this.DEVICES_TYPES.DEVICES) : {
                this.statesHistory.push(this.state);
                this.setState({
                    view : this.STATES.SHOW_DEVICES
                });
                break;
            }
            case (this.DEVICES_TYPES.SCRIPT) : {
                this.props.onChange([CommonComponents.BTNS_RETURNS.PLUS,new DeviceScriptAction()]);
                break;
            }
            case (this.DEVICES_TYPES.TIMER) : {
                this.props.onChange([CommonComponents.BTNS_RETURNS.PLUS,new DeviceTimer()]);
                break;
            }
            case (this.DEVICES_TYPES.ONE_TIME) : {
                this.props.onChange([CommonComponents.BTNS_RETURNS.PLUS,new DeviceClock()]);
                break;
            }
            case (this.DEVICES_TYPES.DAILY) : {
                this.props.onChange([CommonComponents.BTNS_RETURNS.PLUS,new DeviceDaily()]);
                break;
            }
            case (this.DEVICES_TYPES.MANUAL) : {
                this.props.onChange([CommonComponents.BTNS_RETURNS.PLUS,new DeviceManualControl()]);
                break;
            }
        }
    }
    render() {
        const headerText = `Select ${this.props.conditionType} condition device:`;
        if (this.state.view === this.STATES.MAIN_SCREEN) {
            return <div>
                <CommonComponents.ContainerHeaderText headerText = {headerText} />

                {this.props.conditionType === CONDITION_TYPES.TRIGGER && 
                <GraphicalActionsSelectDeviceType onChange = {this.onChange} deviceType = "Manual control" imgSrc = {SENSORS_IMAGES.manual}/>}

                {this.props.conditionType === CONDITION_TYPES.TRIGGER && 
                <GraphicalActionsSelectDeviceType onChange = {this.onChange} deviceType = "Daily" imgSrc = {SENSORS_IMAGES.daily}/>}

                <GraphicalActionsSelectDeviceType onChange = {this.onChange} deviceType = {this.DEVICES_TYPES.DEVICES} imgSrc = {SENSORS_IMAGES.Mega}/>
                <GraphicalActionsSelectDeviceType onChange = {this.onChange} deviceType = "One time" imgSrc = {SENSORS_IMAGES.clock}/>
                
                <GraphicalActionsSelectDeviceType onChange = {this.onChange} deviceType = "Timer" imgSrc = {SENSORS_IMAGES.timer}/>
                <GraphicalActionsSelectDeviceType onChange = {this.onChange} deviceType = "Script action" imgSrc = {SENSORS_IMAGES.script}/>

                <CommonComponents.ReturnBackBtn onChange = {this.props.onChange}/>
            </div>;
        } else if (this.state.view === this.STATES.SHOW_DEVICES) {
            return <div>
                <CommonComponents.ContainerHeaderText headerText = {headerText} />
                <DevicesScreen mode = {DEVICES_VIEW_MODES.CONDITION} onChange = {this.onChange} />
            </div>
        }
        
    }
}
class GraphicalActionsHeader extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const headerStyle = {
            borderRadius : "5px",
            backgroundColor : this.props.ga.color
        }
        return <div className = "my-2 gy-3">
            <div className="text-center p-2 row border" style = {headerStyle}>
                <div className="col-2">
                    <CommonComponents.ImageOnListElement width = {CONSTANTS.SM_LIST_ELEM_IMG_WIDTH} src={this.props.ga.imgSrc} />
                </div>
                <div className="col-8 my-auto">
                    <h5>{this.props.ga.name}</h5>
                </div>
                <div className="col-2 my-auto">
                    <button type="button" className="btn btn-secondary" onClick = {() => this.props.onChange(["editBtn",this.props.ga])}>
                        <i className="bi bi-pen"></i>
                    </button>
                </div>
            </div>
            <hr/>
        </div>;
    }
}
class GraphicalActionsBlockHeader extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return <div className="col-12">
            <div className="text-center p-3 border bg-light row" style={{borderRadius : "5px"}}>
                <div className="col-10">
                    <h5>{this.props.headerText}</h5>
                </div>
                <div className="col-1">
                <button type="button" className="btn btn-secondary" onClick = {() => this.props.onChange(["editBtn",this.props.device])}>
                        <i className="bi bi-pen"></i>
                    </button>
                </div>
            </div>
        </div>;
    }
}

class GraphicalActionsBlockImage extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return <div className ="text-center p-3 ">
            <figure className="figure">
                <img width={CONSTANTS.LIST_ELEM_IMG_WIDTH} src={this.props.device.imgSrc} className="figure-img img-fluid rounded" alt="..."></img>
                <figcaption className="figure-caption">{this.props.device.specifedName}</figcaption>
            </figure>
        </div>;
    }
}

class DeviceTimerCondition extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        let secondsData = CALC_HH_MM_SS(this.props.conditionDevice.value);
        this.state = {
            conditionDevice : this.props.conditionDevice,
            hh : secondsData.hh,
            mm : secondsData.mm,
            ss : secondsData.ss
        }
        this.state.conditionDevice.comparison = ">";
    }
    handleChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.value;

        switch (name) {
            case ("hh") : {
                this.state.conditionDevice.value = Number.parseInt(this.state.mm)*60 + Number.parseInt(this.state.ss) + Number.parseInt(value)*3600;
                break;
            }
            case ("mm") : {
                this.state.conditionDevice.value = Number.parseInt(value)*60 + Number.parseInt(this.state.ss) + Number.parseInt(this.state.hh)*3600;
                break;
            }
            case ("ss") : {
                this.state.conditionDevice.value = Number.parseInt(this.state.mm)*60 + Number.parseInt(value) + Number.parseInt(this.state.hh)*3600;
                break;
            }
        }
        this.setState({
            [name] : value
        });

        this.props.onChange([this.state.conditionDevice]);
    }
    render() {
        return <div className = "row my-2 gy-3">
            <GraphicalActionsBlockHeader onChange = {this.props.onChange} device = {this.props.conditionDevice} headerText = {this.props.headerText} />
            <div className = "col-3 my-auto">
                <GraphicalActionsBlockImage device = {this.props.conditionDevice.device}/>
            </div>
            <div className = "col-9 my-auto">
                <div className="row text-center p-3">
                    <div className="col-4">
                        <input type="number" className="form-control" name="hh" value={this.state.hh} onChange = {this.handleChange} required/>
                        <label className="form-label" htmlFor="hh">hh</label>
                    </div>
                    <div className="col-4">
                        <input type="number" className="form-control" name="mm" value={this.state.mm} onChange = {this.handleChange} required/>
                        <label className="form-label" htmlFor="mm">mm</label>
                    </div>
                    <div className="col-4">
                        <input type="number" className="form-control" name="ss" value={this.state.ss} onChange = {this.handleChange} required/>
                        <label className="form-label" htmlFor="ss">ss</label>
                    </div>
                </div>
            </div>
        </div>;
    }
}
class DeviceDailyCondition extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        let seconds = this.props.conditionDevice.value;
        let hh = Math.trunc(seconds / 3600);
        let mm = Math.trunc((seconds - hh*3600) / 60);
        hh = hh < 10 ? "0"+hh : hh;
        mm = mm < 10 ? "0"+mm : mm;
        this.state = {
            conditionDevice : this.props.conditionDevice,
            time : hh+":"+mm
        }
        this.state.conditionDevice.comparison = "=";
    }
    handleChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.value;

        this.setState({
            [name] : value
        });
        let timeData = value.split(":");
        this.state.conditionDevice.value = timeData[0]*3600 + timeData[1]*60;
        this.props.onChange([this.state.conditionDevice]);
    }
    render() {
        return <div className = "row my-2 gy-3">
            <GraphicalActionsBlockHeader onChange = {this.props.onChange}  device = {this.props.conditionDevice} headerText = {this.props.headerText} />
            <div className = "col-3 my-auto">
                <GraphicalActionsBlockImage device = {this.props.conditionDevice.device}/>
            </div>
            <div className = "col-9 my-auto">
                <div className="text-center p-3">
                    <div className="mb-3">
                        <label htmlFor="time" className="form-label">Perform daily at time</label>
                        <input type="time" className="form-control text-center" value = {this.state.time} name="time"  onChange = {this.handleChange} required/>
                    </div>
                </div>
            </div>
        </div>;
    }
}

class DeviceClockCondition extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            loading : true,
            error : false
        }
    }
    componentDidMount() {
        Hub.getSystemTimeZone()
        .then(systemTimeZone => {
            if(this.props.conditionDevice.value<5){
                this.props.conditionDevice.value=Math.floor(Number(new Date))/1000;
            }
            const dateToViewMS = (this.props.conditionDevice.value + new Date().getTimezoneOffset()*60 + systemTimeZone*60) * 1000;
             

            const dateToView = new Date(dateToViewMS);

            
            
            this.setState({
                conditionDevice : this.props.conditionDevice,
                date : dateToView.YYYYMMDD(),
                time : dateToView.HHmm(),
                systemTimeZone : systemTimeZone,
                error : false,
                loading : false
            });

            this.state.conditionDevice.comparison = ">";
        })
        .catch(error => {
            this.setState({
                loading : false,
                error : error
            });
        })
    }
    reload() {
        this.setState({
            loading : true,
            error : false
        });
        Hub.getSystemTimeZone()
        .then(systemTimeZone => {
            const dateToViewMS = (this.props.conditionDevice.value + new Date().getTimezoneOffset()*60 + systemTimeZone*60) * 1000; 
            const dateToView = new Date(dateToViewMS);
            
            this.setState({
                conditionDevice : this.props.conditionDevice,
                date : dateToView.YYYYMMDD(),
                time : dateToView.HHmm(),
                systemTimeZone : systemTimeZone,
                error : false,
                loading : false
            });

            this.state.conditionDevice.comparison = ">";
        })
        .catch(error => {
            this.setState({
                loading : false,
                error : error
            });
        })
    }
    handleChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.value;

        switch (name) {
            case ("date") : {
                let dateString = value+"T"+this.state.time + TimeZone.convertTimeZoneToViewFormat(this.state.systemTimeZone);
                this.state.conditionDevice.value = Math.trunc(new Date(dateString)/1000) ; 
                break;
            }
            case ("time") : {
                let dateString = this.state.date+"T"+value;
                this.state.conditionDevice.value = Math.trunc(new Date(dateString)/1000) ;  
                break;
            }
        }

        this.setState({
            [name] : value
        });

        this.props.onChange([this.state.conditionDevice]);
    }
    render() {
        if (this.state.loading) {
            return <CommonComponents.LoadingField />;
        } else if (this.state.error) {
            return <CommonComponents.ReloadErrorField onChange = {this.reload} error = {this.props.error}/>
        } else {
            return <div className = "row my-2 gy-3">
                <GraphicalActionsBlockHeader onChange = {this.props.onChange}  device = {this.props.conditionDevice} headerText = {this.props.headerText} />
                <div className = "col-3 my-auto">
                    <GraphicalActionsBlockImage device = {this.props.conditionDevice.device}/>
                </div>
                <div className = "col-9 my-auto">
                    <div className="text-center p-3">
                        <div className="mb-3">
                            <label htmlFor="date" className="form-label">Date</label>
                            <input type="date" className="form-control" value = {this.state.date} name="date" onChange = {this.handleChange} required/>
                            
                        </div>
                        <div className="mb-3">
                            <label htmlFor="time" className="form-label">Time</label>
                            <input type="time" className="form-control" value = {this.state.time} name="time"  onChange = {this.handleChange} required/>
                        </div>
                    </div>
                </div>
            </div>;
        }
        
    }
}

class MegaSensorChannelCondition extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            conditionDevice : this.props.conditionDevice,
        }
    }
    handleChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.value;

        if (name === "comparison") {
            this.state.conditionDevice.comparison = value;
        } else if (name === "value") {
            this.state.conditionDevice.value = value;
        }     

        this.props.onChange([this.state.conditionDevice]);
    }
    render() {
        return <div className = "row my-2 gy-3">
            <GraphicalActionsBlockHeader onChange = {this.props.onChange}  device = {this.props.conditionDevice} headerText = {this.props.headerText} />
            <div className = "col-3 my-auto">
                <GraphicalActionsBlockImage device = {this.props.conditionDevice.device}/>
            </div>
            <div className = "col-9 my-auto">
                <div className="row text-center p-3">
                    <div className="col-6">
                        <select name = "comparison" className = "form-select" value = {this.state.conditionDevice.comparison} onChange = {this.handleChange}>
                            <option value = "=">=</option>
                            <option value = ">">&gt;</option>
                            <option value = "<">&lt;</option>
                        </select>
                    </div>
                    <div className="col-6">
                        <input type="number" className="form-control" name="value"  value = {this.state.conditionDevice.value} onChange = {this.handleChange} required/>
                        <label className="form-label" htmlFor="value">value</label>
                    </div>
                </div>
            </div>
        </div>;
    }
}

class AnalogInputCondition extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            conditionDevice : this.props.conditionDevice,
        }
    }
    handleChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.value;

        if (name === "comparison") {
            this.state.conditionDevice.comparison = value;
        } else if (name === "value") {
            this.state.conditionDevice.value = value;
        }     

        this.props.onChange([this.state.conditionDevice]);
    }
    render() {
        return <div className = "row my-2 gy-3">
            <GraphicalActionsBlockHeader onChange = {this.props.onChange}  device = {this.props.conditionDevice} headerText = {this.props.headerText} />
            <div className = "col-3 my-auto">
                <GraphicalActionsBlockImage device = {this.props.conditionDevice.device}/>
            </div>
            <div className = "col-9 my-auto">
                <div className="row text-center p-3">
                    <div className="col-6">
                        <select name = "comparison" className = "form-select" value = {this.state.conditionDevice.comparison} onChange = {this.handleChange}>
                            <option value = "=">=</option>
                            <option value = ">">&gt;</option>
                            <option value = "<">&lt;</option>
                        </select>
                    </div>
                    <div className="col-6">
                        <input type="number" className="form-control" name="value"  value = {this.state.conditionDevice.value} onChange = {this.handleChange} required/>
                        <label className="form-label" htmlFor="value">value</label>
                    </div>
                </div>
            </div>
        </div>;
    }
}

class ScriptActionItem extends React.Component {
    render() {
        return  <option value = {this.props.value}>{this.props.value}</option>;
    }
}

class ScriptActionsList extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.state = {
            value : this.props.value
        }
        
    }
    onChange(event) {
        const target = event.target;
        const value = target.value;
        this.setState({
            value : value
        });
        this.props.onChange(event);
    }
    render() {
        let i = Math.trunc(Math.random()*1000);
        const listItems = this.props.scriptActions.map(data => 
            <ScriptActionItem key = {++i} value = {data} />
        )
        return <select name = "scriptName" className = "form-select" onChange = {this.onChange} value = {this.state.value} required>
            <option value = "null">Select the action</option>
            {listItems}
        </select>;
    }
}

class DeviceScriptActionCondition extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            conditionDevice : this.props.conditionDevice,
            scriptActions :  this.props.scriptActions
        };
    }
    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        if (name === "value") {
            this.state.conditionDevice.value = value;
        } else if (name === "scriptName") {
            this.state.conditionDevice.device.name = value;
            this.state.conditionDevice.scriptName = value;
        }
        this.props.onChange([this.state.conditionDevice]);
    }
    render() {
        if (this.state.loading) {
            return <CommonComponents.LoadingField />
        } else if (this.state.error) {
            return <CommonComponents.ReloadErrorField onChange = {this.reload} error = {this.state.error}/>
        } else {
            return <div className = "row my-2 gy-3">
                <GraphicalActionsBlockHeader onChange = {this.props.onChange}  device = {this.props.conditionDevice} headerText = {this.props.headerText} />
                <div className = "col-3 my-auto">
                    <GraphicalActionsBlockImage device = {this.props.conditionDevice.device}/>
                </div>
                <div className = "col-9 my-auto">
                    <div className="row text-center p-3">
                        <div className="col-6">
                            <ScriptActionsList onChange = {this.handleChange} scriptActions = {this.props.scriptActions} value = {this.state.conditionDevice.device.name}/>
                        </div>
                        <div className="col-6">
                            <select name = "value" value = {this.state.conditionDevice.value} className = "form-select" onChange = {this.handleChange}>
                                <option value = "1">True</option>
                                <option value = "0">False</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>;
        }
        
    }
}

class DeviceManualControlCondition extends React.Component {
    constructor(props) {
        super(props);
    }
    handleChange() {

    }
    render() {
        return <div className = "row my-2 gy-3">
            <GraphicalActionsBlockHeader onChange = {this.props.onChange}  device = {this.props.conditionDevice} headerText = {this.props.headerText} />
            <div className = "col-3 my-auto">
                <GraphicalActionsBlockImage device = {this.props.conditionDevice.device}/>
            </div>
            <div className = "col-9 my-auto">
                <div className="alert alert-secondary text-center" role="alert">
                    <h5>
                        This action  is run manually
                    </h5>
                </div>
            </div>
        </div>;
    }
}

class GraphicalActionsCondition extends React.Component {
    constructor(props) {
        super(props);
    }
    handleChange() {

    }
    render() {
        if (this.props.conditionDevice.device instanceof DeviceTimer) {
            return <DeviceTimerCondition 
                    onChange = {this.props.onChange} 
                    conditionDevice = {this.props.conditionDevice} 
                    headerText = {this.props.headerText}
            />;
        } else if (this.props.conditionDevice.device instanceof DeviceClock) {
            return <DeviceClockCondition 
                    onChange = {this.props.onChange} 
                    conditionDevice = {this.props.conditionDevice} 
                    headerText = {this.props.headerText}
            />;
        } else if (this.props.conditionDevice.device instanceof MegaSensorChannel) {
            return <MegaSensorChannelCondition 
                onChange = {this.props.onChange} 
                conditionDevice = {this.props.conditionDevice} 
                headerText = {this.props.headerText}
            />;
        } else if (this.props.conditionDevice.device instanceof AnalogInput) { 
            return <AnalogInputCondition 
                onChange = {this.props.onChange} 
                conditionDevice = {this.props.conditionDevice} 
                headerText = {this.props.headerText}
            />;
        } else if (this.props.conditionDevice.device instanceof DeviceScriptAction) { 
            return <DeviceScriptActionCondition 
                onChange = {this.props.onChange} 
                conditionDevice = {this.props.conditionDevice} 
                headerText = {this.props.headerText}
                scriptActions = {this.props.scriptActions}
            />;
        } else if (this.props.conditionDevice.device instanceof DeviceManualControl) { 
            return <DeviceManualControlCondition 
                onChange = {this.props.onChange} 
                conditionDevice = {this.props.conditionDevice} 
                headerText = {this.props.headerText}
            />;
        } else if (this.props.conditionDevice.device instanceof DeviceDaily) { 
            return <DeviceDailyCondition 
                onChange = {this.props.onChange} 
                conditionDevice = {this.props.conditionDevice} 
                headerText = {this.props.headerText}
            />;
        }
        else {
            return <div className = "row my-2 gy-3 text-center">
                <GraphicalActionsBlockHeader 
                    onChange = {this.props.onChange} 
                    device = {this.props.conditionDevice} 
                    headerText = {this.props.headerText} 
                />
                <div className = "col-12">
                    <div 
                        className="alert alert-danger d-grid gap-2 col-8 mx-auto" 
                        role="alert" 
                        style = {{cursor : "pointer"}} 
                        onClick = {() => this.props.onChange(["editBtn",this.props.conditionDevice])}
                    >
                        <h5>Condition not selected</h5>
                    </div> 
                </div>
            </div>;
        }
    }
}
 
class OutletAction extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            actionDevice : this.props.actionDevice
        };
    }
    handleChange(event) {
        const target = event.target;
        const value = target.value;
        this.state.actionDevice.value = value;
        this.props.onChange([this.state.actionDevice]);
    }
    render() {
        return <div className = "row my-2 gy-3">
            <GraphicalActionsBlockHeader onChange = {this.props.onChange} device = {this.props.actionDevice} headerText = {this.props.headerText} />
            <div className = "col-3 my-auto">
                <GraphicalActionsBlockImage device = {this.props.actionDevice.device}/>
            </div>
            <div className = "col-9 my-auto">
                <div className="row text-center p-3">
                    <div className="col-12">
                        <select name = "value" value = {this.state.actionDevice.value} className = "form-select" onChange = {this.handleChange}>
                            <option value = "1">Turn ON</option>
                            <option value = "0">Turn OFF</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>;
    }
}

class PWM_OutputAction extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            actionDevice : this.props.actionDevice
        };
    }
    handleChange(event) {
        const target = event.target;
        const value = target.value;
        this.state.actionDevice.value = value;
        this.props.onChange([this.state.actionDevice]);
    }
    render() {
        return <div className = "row my-2 gy-3">
            <GraphicalActionsBlockHeader onChange = {this.props.onChange} device = {this.props.actionDevice} headerText = {this.props.headerText} />
            <div className = "col-3 my-auto">
                <GraphicalActionsBlockImage device = {this.props.actionDevice.device}/>
            </div>
            <div className = "col-9 my-auto">
                <div className="row text-center p-3">
                    <div className="col-6">
                        <h5>set value</h5>
                    </div>
                    <div className="col-6">
                        <input type="number" className="form-control" name="value" value = {this.state.actionDevice.value} onChange = {this.handleChange} required/>
                        <label className="form-label" htmlFor="value">0-100%</label>
                    </div>
                </div>
            </div>
        </div>;
    }
}

class DeviceScriptActionAction extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            actionDevice : this.props.actionDevice,
            scriptActions : this.props.scriptActions,
        };
        
    }
    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        if (name === "scriptName") {
            this.state.actionDevice.device.name = value;
            this.state.actionDevice.scriptName = value;
        }
        this.props.onChange([this.state.actionDevice]);
    }
    render() {
        return <div className = "row my-2 gy-3">
            <GraphicalActionsBlockHeader onChange = {this.props.onChange} device = {this.props.actionDevice} headerText = {this.props.headerText} />
            <div className = "col-3 my-auto">
                <GraphicalActionsBlockImage device = {this.props.actionDevice.device}/>
            </div>
            <div className = "col-9 my-auto">
                <div className="row text-center p-3">
                    <div className="col-6">
                        <h5>execute script</h5>
                    </div>
                    <div className="col-6">
                        <ScriptActionsList onChange = {this.handleChange} scriptActions = {this.props.scriptActions} value = {this.state.actionDevice.device.name}/>
                    </div>
                </div>
            </div>
        </div>;
    }
}

class SolenoidAction extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            actionDevice : this.props.actionDevice
        };
    }
    handleChange(event) {
        const target = event.target;
        const value = target.value;
        this.state.actionDevice.value = value;
        this.props.onChange([this.state.actionDevice]);
    }
    render() {
        return <div className = "row my-2 gy-3">
            <GraphicalActionsBlockHeader onChange = {this.props.onChange} device = {this.props.actionDevice} headerText = {this.props.headerText} />
            <div className = "col-3 my-auto">
                <GraphicalActionsBlockImage device = {this.props.actionDevice.device}/>
            </div>
            <div className = "col-9 my-auto">
                <div className="row text-center p-3">
                    <div className="col-12">
                        <select name = "value" value = {this.state.actionDevice.value} className = "form-select" onChange = {this.handleChange}>
                            <option value = "1">Turn ON</option>
                            <option value = "0">Turn OFF</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>;
    }
}

class GraphicalActionsAction extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        if (this.props.actionDevice.device instanceof Outlet) {
            return <OutletAction 
                onChange = {this.props.onChange} 
                actionDevice = {this.props.actionDevice} 
                headerText = {this.props.headerText}
            />;
        } else if (this.props.actionDevice.device instanceof PWM_Output) {
            return <PWM_OutputAction  
                onChange = {this.props.onChange} 
                actionDevice = {this.props.actionDevice} 
                headerText = {this.props.headerText}
            />;
        } else if (this.props.actionDevice.device instanceof Solenoid24vac) {
            return <SolenoidAction  
                onChange = {this.props.onChange} 
                actionDevice = {this.props.actionDevice} 
                headerText = {this.props.headerText}
            />;
        } else if (this.props.actionDevice.device instanceof DeviceScriptAction) {
            return <DeviceScriptActionAction  
                onChange = {this.props.onChange} 
                actionDevice = {this.props.actionDevice} 
                headerText = {this.props.headerText}
                scriptActions = {this.props.scriptActions}
            />;
        } else {
            return <div className = "row my-2 gy-3 text-center">
                <GraphicalActionsBlockHeader 
                    onChange = {this.props.onChange}  
                    device = {this.props.actionDevice} 
                    headerText = {this.props.headerText} 
                />
                <div className = "col-12">
                    <div 
                        className="alert alert-danger d-grid gap-2 col-8 mx-auto" 
                        role="alert" 
                        style = {{cursor : "pointer"}} 
                        onClick = {() => this.props.onChange(["editBtn",this.props.actionDevice])}
                    >
                        <h5>Action not selected</h5>
                    </div>
                </div>
            </div>;
        }
    }
}

export class CreateNewGraphicalAction extends React.Component {
    constructor(props) {
        super(props);
        this.ga = new GraphicalActionsData.GraphicalAction({
            graphicalActionsMap : this.props.graphicalActionsMap
        });
    }
    render() {
        return <GraphicalActionsSettings ga = {this.ga} onChange = {this.props.onChange}/>;
    }
}

export class GraphicalActionsView extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.STATES = {
            GA_SETTINGS_SCREEN : 0,
            MAIN_SCREEN : 1,
            TRIGGER_SELECT_SCREEN : 2,
            ACTION_SELECT_SCREEN : 3,
            TERMINATION_SELECT_SCREEN : 4
        };
        this.statesHistory = [];
        this.state = {
            view : this.STATES.MAIN_SCREEN,
            ga : this.props.ga,
            applyBtnApplying : false,
            removing : false

        }
    }
    handleSubmit(e) {
        e.preventDefault();
        this.onChange([CommonComponents.BTNS_RETURNS.APPLY_FORM]);
    }
    onChange(params) {
        if (params.target) {
            if (params.target.name === "gaIsActive") {
                let tempGa =  this.state.ga;
                tempGa.status.active = params.target.checked;
                this.setState({
                    ga : tempGa
                })
            }
        } else if (params[0] instanceof GraphicalActionsData.ConditionDeviceTrigger) {
            let tempGa = this.state.ga;
            tempGa.conditionDeviceTrigger = params[0];
            this.setState({
                ga : tempGa
            });
        } else if (params[0] instanceof GraphicalActionsData.ConditionDeviceTermination) {
            let tempGa = this.state.ga;
            tempGa.conditionDeviceTermination = params[0];
            this.setState({
                ga : tempGa
            });
        } else if (params[0] instanceof GraphicalActionsData.ActionDevice) {
            let tempGa = this.state.ga;
            tempGa.actionDevice = params[0];
            this.setState({
                ga : tempGa
            });
        } else {
            switch (params[0]) {
                case (CommonComponents.BTNS_RETURNS.BACK) : {
                    this.setState(this.statesHistory.pop());
                    break;
                }
                case (CommonComponents.BTNS_RETURNS.REMOVE) : {
                    this.setState({
                        removing : true
                    })
                    let tempMap = new Map();
                    for(let [k,v] of this.state.ga.graphicalActionsMap) {
                        tempMap.set(k,v);
                    }
                    tempMap.delete(this.state.ga.id);
                    Hub.setGraphicalActions(tempMap)
                    .then(res => {
                        Hub.setGraphicalActionsData(tempMap)
                        .then(res => {
                            this.props.onChange([CommonComponents.BTNS_RETURNS.BACK]);
                        })
                    })
                    .catch(err => {
                        alert("Error on removing graphical action (details in the console)");
                        this.setState({
                            removing : false
                        });
                        throw(err);
                    })
                    break;
                }
                case ("editBtn") : {
                    this.statesHistory.push(this.state);
                    if (params[1] instanceof GraphicalActionsData.GraphicalAction) {
                        this.setState({
                            view : this.STATES.GA_SETTINGS_SCREEN
                        });
    
                    } else if (params[1] instanceof GraphicalActionsData.ConditionDeviceTrigger) {
                        this.setState({
                            view : this.STATES.TRIGGER_SELECT_SCREEN
                        });
                    } else if (params[1] instanceof GraphicalActionsData.ConditionDeviceTermination) {
                        this.setState({
                            view : this.STATES.TERMINATION_SELECT_SCREEN
                        });
                    } else if (params[1] instanceof GraphicalActionsData.ActionDevice) {
                        this.setState({
                            view : this.STATES.ACTION_SELECT_SCREEN
                        });
                    }
                    break;
                }
                case (CommonComponents.BTNS_RETURNS.PLUS) : {
                    switch (this.state.view) {
                        case (this.STATES.TRIGGER_SELECT_SCREEN) : {
                            let tempGA = this.state.ga;
                            tempGA.conditionDeviceTrigger = new GraphicalActionsData.ConditionDeviceTrigger(params[1]);
                            this.setState({
                                view : this.STATES.MAIN_SCREEN,
                                ga : tempGA
                            });
                            break;
                        }
                        case (this.STATES.ACTION_SELECT_SCREEN) : {
                            let tempGA = this.state.ga;
                            tempGA.actionDevice = new GraphicalActionsData.ActionDevice(params[1]);
                            this.setState({
                                view : this.STATES.MAIN_SCREEN,
                                ga : tempGA
                            });
                            break;
                        }
                        case (this.STATES.TERMINATION_SELECT_SCREEN) : {
                            let tempGA = this.state.ga;
                            tempGA.conditionDeviceTermination = new GraphicalActionsData.ConditionDeviceTermination(params[1]);
                            this.setState({
                                view : this.STATES.MAIN_SCREEN,
                                ga : tempGA
                            });
                            break;
                        }
                    }
                }
                case (CommonComponents.BTNS_RETURNS.APPLY_FORM) : {
                    switch (this.state.view) {
                        case (this.STATES.GA_SETTINGS_SCREEN) : {
                            let tempGA = this.state.ga;
                            tempGA.name = params[1].name;
                            tempGA.imgSrc = params[1].imgSrc;
                            tempGA.color = params[1].color;
                            this.setState({
                                ga : tempGA,
                                view : this.STATES.MAIN_SCREEN
                            });
                            break;
                        }
                        case (this.STATES.MAIN_SCREEN) : {
                            try {
                                if (this.state.ga.conditionDeviceTermination.device instanceof DeviceScriptAction) {
                                    if (!this.props.scriptActions.includes(this.state.ga.conditionDeviceTermination.device.name)) {
                                        throw new Error("Action not selected");
                                    }
                                }
                                if (this.state.ga.actionDevice.device instanceof DeviceScriptAction) {
                                    if (!this.props.scriptActions.includes(this.state.ga.actionDevice.device.name)) {
                                        throw new Error("Action not selected");
                                    }
                                }
                                if (this.state.ga.conditionDeviceTrigger.device instanceof DeviceScriptAction) {
                                    if (!this.props.scriptActions.includes(this.state.ga.conditionDeviceTrigger.device.name)) {
                                        throw new Error("Action not selected");
                                    }
                                }
                                this.setState({
                                    applyBtnApplying : true
                                });
                                
                            

                                let tempMap = new Map();
                                for(let [k,v] of this.state.ga.graphicalActionsMap) {
                                    let vClone = Object.assign({},v);

                                    let conditionDeviceTriggerClone = Object.assign({}, v.conditionDeviceTrigger)
                                    Object.setPrototypeOf(conditionDeviceTriggerClone,GraphicalActionsData.ConditionDeviceTrigger.prototype);
                                    
                                    let conditionDeviceTerminationClone = Object.assign({}, v.conditionDeviceTermination);
                                    Object.setPrototypeOf(conditionDeviceTerminationClone,GraphicalActionsData.ConditionDeviceTermination.prototype);

                                    let actionDeviceClone = Object.assign({}, v.actionDevice)
                                    Object.setPrototypeOf(actionDeviceClone,GraphicalActionsData.ActionDevice.prototype);

                                    vClone.conditionDeviceTermination = conditionDeviceTerminationClone;
                                    vClone.conditionDeviceTrigger = conditionDeviceTriggerClone;
                                    vClone.actionDevice = actionDeviceClone;

                                    Object.setPrototypeOf(vClone,GraphicalActionsData.GraphicalAction.prototype);

                                    tempMap.set(k,vClone);
                                }
                                

                                // temp ga
                                let tempGA = GraphicalActionsData.copyGraphicalAction(this.state.ga);
                                
                                Object.assign({} , this.state.ga)

                                Object.setPrototypeOf( tempGA, GraphicalActionsData.GraphicalAction.prototype );

                                let _conditionDeviceTriggerClone = Object.assign({}, this.state.ga.conditionDeviceTrigger)
                                Object.setPrototypeOf(_conditionDeviceTriggerClone,GraphicalActionsData.ConditionDeviceTrigger.prototype);

                                let _conditionDeviceTerminationClone = Object.assign({}, this.state.ga.conditionDeviceTermination);
                                Object.setPrototypeOf(_conditionDeviceTerminationClone,GraphicalActionsData.ConditionDeviceTermination.prototype);

                                let _actionDeviceClone = Object.assign({}, this.state.ga.actionDevice);
                                Object.setPrototypeOf(_actionDeviceClone,GraphicalActionsData.ActionDevice.prototype);

                                tempGA.conditionDeviceTermination = _conditionDeviceTerminationClone;
                                tempGA.conditionDeviceTrigger = _conditionDeviceTriggerClone;
                                tempGA.actionDevice = _actionDeviceClone;
                                // temp ga

                                tempMap.set(tempGA.id,tempGA);

                                // check ga is valid
                                let gaClone = Object.assign({} , this.state.ga)

                                Object.setPrototypeOf( gaClone, GraphicalActionsData.GraphicalAction.prototype );

                                let conditionDeviceTriggerClone = Object.assign({}, this.state.ga.conditionDeviceTrigger)
                                Object.setPrototypeOf(conditionDeviceTriggerClone,GraphicalActionsData.ConditionDeviceTrigger.prototype);

                                let conditionDeviceTerminationClone = Object.assign({}, this.state.ga.conditionDeviceTermination);
                                Object.setPrototypeOf(conditionDeviceTerminationClone,GraphicalActionsData.ConditionDeviceTermination.prototype);

                                let actionDeviceClone = Object.assign({}, this.state.ga.actionDevice);
                                Object.setPrototypeOf(actionDeviceClone,GraphicalActionsData.ActionDevice.prototype);

                                gaClone.conditionDeviceTermination = conditionDeviceTerminationClone;
                                gaClone.conditionDeviceTrigger = conditionDeviceTriggerClone;
                                gaClone.actionDevice = actionDeviceClone;

                                let hex = gaClone.createHexContext();
                                // end check 

                                Hub.setGraphicalActions(tempMap)
                                .then(res => {
                                    Hub.setGraphicalActionsData(tempMap)
                                    .then(res => {
                                        this.props.onChange([CommonComponents.BTNS_RETURNS.BACK]);
                                    })
                                })
                                .catch(err => {
                                    console.error(err);
                                    alert("Error on applying graphical action (details in the console)");
                                    this.setState({
                                        applyBtnApplying : false
                                    });
                                })
                            } catch (err) {
                                console.error(err);
                                alert("Graphical action not valid (details in the console)");
                                this.setState({
                                    applyBtnApplying : false
                                });
                            }
                            break;
                        }
                    }
                }
            }
        }
    }
    render() {
        switch (this.state.view) {
            case (this.STATES.MAIN_SCREEN) : {
                return <form onSubmit = {this.handleSubmit}>
                    <GraphicalActionsHeader ga = {this.state.ga} onChange = {this.onChange}/>
        
                    <GraphicalActionsCondition 
                        onChange = {this.onChange} 
                        headerText = "Trigger condition" 
                        conditionType = {CONDITION_TYPES.TRIGGER} 
                        conditionDevice = {this.state.ga.conditionDeviceTrigger}
                        scriptActions = {this.props.scriptActions}
                    />
                    <GraphicalActionsAction 
                        onChange = {this.onChange} 
                        headerText = "Executable action" 
                        actionDevice = {this.state.ga.actionDevice}
                        scriptActions = {this.props.scriptActions}
                    />
                    <GraphicalActionsCondition 
                        onChange = {this.onChange} 
                        headerText = "Termination condition" 
                        conditionType = {CONDITION_TYPES.TERMINATION} 
                        conditionDevice = {this.state.ga.conditionDeviceTermination}
                        scriptActions = {this.props.scriptActions}
                    />
                    {
                        (this.state.ga.conditionDeviceTrigger.device instanceof DeviceManualControl) ||
                            <div className="form-check form-switch my-2">
                                <input 
                                    className="form-check-input" 
                                    type="checkbox" 
                                    name="gaIsActive" 
                                    checked = {this.state.ga.status.active} 
                                    onChange = {this.onChange}
                                /> 
                                <label className="form-check-label" htmlFor="gaIsActive">
                                    Action is active
                                </label>
                            </div>
                    }
                    <div className = "row my-2">
                        <div className = "col-6">
                            <CommonComponents.ReturnBackBtn onChange = {this.props.onChange} />
                            <CommonComponents.ApplyFormBtn applying = {this.state.applyBtnApplying} onChange = {this.onChange} />
                        </div>
                        <div className = "col-2">

                        </div>
                        <div className = "col-4">
                            {
                                this.props.graphicalActionsMap.get(this.props.ga.id) &&
                                    <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                                        <CommonComponents.RemoveBtn removing = {this.state.removing} onChange = {this.onChange}/>
                                    </div>
                            }
                        </div>
                    </div>
                </form>;
            }
            case (this.STATES.GA_SETTINGS_SCREEN) : {
                return <GraphicalActionsSettings  ga = {this.state.ga} onChange = {this.onChange}/>;
            }
            case (this.STATES.TRIGGER_SELECT_SCREEN) : {
                return <GraphicalActionsConditionSelect onChange = {this.onChange} conditionType = {CONDITION_TYPES.TRIGGER}/>
            }
            case (this.STATES.ACTION_SELECT_SCREEN) : {
                return <GraphicalActionsActionSelect onChange = {this.onChange} />
            }
            case (this.STATES.TERMINATION_SELECT_SCREEN) : {
                return <GraphicalActionsConditionSelect onChange = {this.onChange} conditionType = {CONDITION_TYPES.TERMINATION}/>
            }
        }
       
    }
}

class GraphicalActionListElem extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = this.props.onChange.bind(this);
        this.state = {disable: false}
    }
    render() {
        const gaStyle = {
            backgroundColor : this.props.ga.color,
            borderRadius : "5px",
            cursor : "pointer"
        }
        const cursorPointer = {
            cursor : "pointer"
        }
        let _ga = this.props.ga;
        _ga.scriptActions = this.scriptActions;
        _ga.graphicalActionsMap = this.props.graphicalActionsMap;

        // substitute query address if you need
        let query = ''

        let handleClick = null
        if(this.props.readonly != true) handleClick = () => this.props.onChange([_ga]);

        
        // send query on click  Button
        const fetchSendId = () => {
            //disable button
            this.setState({disable: true})
            // send GET query
            Hub.sendGET(/startGA/+parseInt(this.props.ga.id)) 
                .then(response => {
                     
                        response.json().then(json => {
                            if (json.result === "ok") {
                                // show window if everything is ok
                                alert('Started successfuly');
                                //change button color
                                document.getElementById('btn-start-'+this.props.ga.id).classList.remove('btn-primary')
                                document.getElementById('btn-start-'+this.props.ga.id).classList.add('btn-success')
                                setTimeout(() => {
                                    // change button class and replace color
                                    document.getElementById('btn-start-'+this.props.ga.id).classList.remove('btn-success')
                                    document.getElementById('btn-start-'+this.props.ga.id).classList.add('btn-primary')
                                    this.setState({disable: false})
                                }, 3000 )
                            } else {
                                // show window if request failed
                                switch (json.result){
                                    case "NOT_FOUND":{alert('Failed. This task has been deleted or unavailable.');}break;
                                    case "ALREADY_RUN":{alert('Failed. This task already has been started.');}break;
                                    case "ON_DEV_WAIT":{alert('Failed. One of involved device is disconected or unavaileble.');}break;
                                    default:{alert('Failed. Internal error #6997752.');console.log("unknown json.result: "+json.result);}break;//
                                }
                                
                                document.getElementById('btn-start-'+this.props.ga.id).classList.remove('btn-primary')
                                document.getElementById('btn-start-'+this.props.ga.id).classList.add('btn-danger')
                                setTimeout(() => {
                                    document.getElementById('btn-start-'+this.props.ga.id).classList.remove('btn-danger')
                                    document.getElementById('btn-start-'+this.props.ga.id).classList.add('btn-primary')
                                    this.setState({disable: false})
                                }, 3000 )
                            }
                        })
                     
                    })
                    .catch(err => { 

                        document.getElementById('btn-start-'+this.props.ga.id).classList.remove('btn-primary')
                        document.getElementById('btn-start-'+this.props.ga.id).classList.add('btn-danger')
                        setTimeout(() => {
                            document.getElementById('btn-start-'+this.props.ga.id).classList.remove('btn-danger')
                            document.getElementById('btn-start-'+this.props.ga.id).classList.add('btn-primary')
                            this.setState({disable: false})
                        }, 3000 )

                        alert(err + "");
                    });
        }

console.log(this.props.ga)

        if ( this.props.ga.conditionDeviceTrigger.device instanceof DeviceManualControl) {
            return  <div className = "row my-3 py-2" style={gaStyle}>
                <div style = {cursorPointer} className = "col-4" onClick = {handleClick}>
                    <CommonComponents.ImageOnListElement width = {CONSTANTS.LIST_ELEM_IMG_WIDTH} src = {this.props.ga.imgSrc} />
                </div>
                <div style = {cursorPointer} className = "col-6 my-auto" onClick = {handleClick}>
                <h5>{this.props.ga.name}</h5>
               
           
            
                </div>
                <div style = {cursorPointer} className = "col-1 my-auto" >
                    <button id={'btn-start-'+this.props.ga.id } type="button" className= "btn btn-primary"  disabled={this.state.disable || this.props.readonly}
                            onClick={fetchSendId}
                    >
                        start
                    </button>
                </div>
            </div>;
        } else {
            return  <div className = "row my-3 py-2" style={gaStyle} >
                <div className = "col-4" onClick = {handleClick}>
                    <CommonComponents.ImageOnListElement width = {CONSTANTS.LIST_ELEM_IMG_WIDTH} src = {this.props.ga.imgSrc} />
                </div>
                <div className = "col-6 my-auto" onClick = {handleClick} >
                    <h5>{this.props.ga.name}</h5>
                </div>
                <div style = {cursorPointer} className = "col-2 my-auto">
                    <button id={'btn-start-'+this.props.ga.id } type="button" className= "btn btn-primary"  disabled={this.state.disable || this.props.readonly} style={{margin: '0 10px 5px 0'}}
                            onClick={fetchSendId}>
                        start
                    </button>
                    {
                            this.props.ga.status.active &&
                                <i className="bi bi-check-lg"></i>
                    }
                    {
                            this.props.ga.status.active ||
                                <i className="bi bi-x-lg"></i>
                    }
                </div>
            </div>;
        }
    }
}

export class GraphicalActionsList extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        let gasArray = this.props.graphicalActionsMap.toArray();
        switch(Number.parseInt(this.props.sortType)) {
            case (SORT_TYPES.AZ) : {
                gasArray.sort((a, b) => a.value.name > b.value.name ? 1 : -1);
                break;
            }
            case (SORT_TYPES.ZA) : {
                gasArray.sort((a, b) => a.value.name > b.value.name ? -1 : 1);
                break;
            }
            case (SORT_TYPES.MANUAL) : {
                gasArray.sort((a, b) => {
                    if (a.value.conditionDeviceTrigger.device.type === "DeviceManualControl" && b.value.conditionDeviceTrigger.device.type !== "DeviceManualControl" ) {
                        return -1;
                    }
                    if (a.value.conditionDeviceTrigger.device.type !== "DeviceManualControl" && b.value.conditionDeviceTrigger.device.type === "DeviceManualControl" ) {
                        return 1;
                    }
                    return 0;
                })
                break;
            }
            case (SORT_TYPES.AUTO) : {
                gasArray.sort((a, b) => {
                    if (a.value.conditionDeviceTrigger.device.type === "DeviceManualControl" && b.value.conditionDeviceTrigger.device.type !== "DeviceManualControl" ) {
                        return 1;
                    }
                    if (a.value.conditionDeviceTrigger.device.type !== "DeviceManualControl" && b.value.conditionDeviceTrigger.device.type === "DeviceManualControl" ) {
                        return -1;
                    }
                    return 0;
                })
                break;
            }
        }
        let listItems =  gasArray.map((data) => 
            <GraphicalActionListElem 
                readonly = {this.props.readonly}
                onChange = {this.props.onChange} 
                key = {data.key} 
                ga = {data.value} 
                graphicalActionsMap = {this.props.graphicalActionsMap}
                scriptActions = {this.props.scriptActions} 
            />
        )
        return <div>
            {listItems}
        </div>;
    }
}