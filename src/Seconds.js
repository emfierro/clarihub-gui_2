export function CALC_HH_MM_SS(seconds) {
	var hh = parseInt(seconds/3600);
	var mm = parseInt((seconds % 3600)/60);
	var ss = parseInt(seconds % (3600/60));
	return {
		hh : hh,
		mm : mm,
		ss : ss
	}
}

export default CALC_HH_MM_SS;