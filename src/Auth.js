import React from 'react';
import * as Hub from './Network';
import * as CommonComponents from './CommonComponents';

class Auth extends React.Component {
  constructor(props){
	super(props);
	this.handleSubmit = this.handleSubmit.bind(this);
  this.handleChange = this.handleChange.bind(this);
	this.state = {
    isValid: false,
    isLoading: false,
    isLoginValid: false,
    isLoginInvalid: false,
    isPasswordValid: false,
    isPasswordInvalid: false,
    login: "",
    password: "",
    alertError: false,
    alertWrong: false,
  };
  }
  handleSubmit(event){
    event.preventDefault();
    this.setState({isLoading: true});
    Hub.sendPOST("/checkUser/", "login=" + this.state.login + "&password=" + this.state.password)
    .then(res => {
      res.json()
      .then(data => {
        if(data.result == "SUCCESS"){
          this.props.onLogin();
        }
        else{
          this.setState({alertWrong: true});
        }
      })
      .catch(err => {
        this.setState({alertError: true});
      });
    })
    .catch(err => {
      this.setState({alertError: true});
    })
    .finally(() => {
      this.setState({isLoading: false});
    });
  }

  handleChange(event){

    if(event.target.id == "auth-login"){
      let l = event.target.value;
      let re = new RegExp("^[a-zA-Z0-9_\-]{3,64}$");
      let v = false; //valid
      let iv = false;//invalid
      let tv = false;//total valid
      if(re.test(l)) v = true;
      else iv = true;
      if(this.state.isPasswordValid && v) tv = true;
      this.setState({alertWrong: false, alertError: false, isLoginValid: v, isLoginInvalid: iv, login: l, isValid: tv});
    }
    else if(event.target.id == "auth-password"){
      let p = event.target.value;
      let re = new RegExp("^[a-zA-Z0-9_\-]{4,64}$");
      let v = false; //valid
      let iv = false;//invalid
      let tv = false;//total valid
      if(re.test(p)) v = true;
      else iv = true;
      if(this.state.isLoginValid && v) tv = true;
      this.setState({alertWrong: false, alertError: false, isPasswordValid: v, isPasswordInvalid: iv, password: p, isValid: tv});
    }
  }
  render() {
    let loginVadidCls = "";
    if(this.state.isLoginValid) loginVadidCls = "is-valid";
    if(this.state.isLoginInvalid) loginVadidCls = "is-invalid";

    let passwordValidCls = "";
    if(this.state.isPasswordValid) passwordValidCls = "is-valid";
    if(this.state.isPasswordInvalid) passwordValidCls = "is-invalid";


    return <div>
      <div className="container mt-5" style={{"max-width": "540px"}}>
        <div className="col">
        <form className="form-signin">  
        <p class="h3 mb-3 font-weight-normal">
          Please login to your account
        </p>
        <div className="form-floating mb-3">
          <input id="auth-login" type="email" class={"form-control " + loginVadidCls} placeholder="" onChange = {this.handleChange} required/>
          <label className="form-label" for="auth-login">Username</label>
          <div class="invalid-feedback">
            Invalid username, min length = 3, max = 64; Characters: a-z, A-Z, 0-9, _, -
          </div>
        </div>
        <div className="form-floating mb-3">
          <input id="auth-password" type="password" class={"form-control " + passwordValidCls} placeholder="password" onChange = {this.handleChange} required/>
          <label className="form-label" for="auth-password">password</label>
          <div class="invalid-feedback">
            Invalid password, min length = 4, max = 64; Characters: a-z, A-Z, 0-9, _, -
          </div>
        </div>
        
        <button class={this.state.isLoading ? "btn btn-outline-primary mb-3 w-100 disabled" : this.state.isValid ? "btn btn-primary mb-3 w-100" : "btn btn-secondary mb-3 w-100 disabled"} type="submit" onClick = {this.handleSubmit}>
          Log in
        </button>
        <div class="alert alert-danger" role="alert" hidden={!this.state.alertWrong}> 
          Wrong username or password.
        </div>
        <div class="alert alert-danger" role="alert" hidden={!this.state.alertError}>
          Error (timeout). Please try again later, or contact support.
        </div>
        
        </form>
        </div>
      </div>
    </div>;
  }
}

export default Auth;