import React from 'react';
import * as CommonComponents from './CommonComponents';
import {DevicesScreen, DEVICES_VIEW_MODES} from './DevicesView';

class Status extends React.Component {
    render() {
      if(this.props.permissions.status == "true") {
        return <div className = "container">
          <DevicesScreen mode = {DEVICES_VIEW_MODES.DEFAULT}/>
        </div>;
      }
      else{
        return <CommonComponents.AccessDeniedMessage />
      }
    }
  }
  export default Status;