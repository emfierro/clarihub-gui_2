import React from 'react';

import {
    Script
} from './ScriptActionsData';

import SENSORS_IMAGES from './SensorImages';
import * as CommonComponents from './CommonComponents';
import CodeMirror from '@uiw/react-codemirror';
import 'codemirror/keymap/sublime';
import 'codemirror/theme/eclipse.css';
import * as Hub from './Network';

class ScriptActionCodeArea extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        
    }
    onChange(e) {
        this.props.onChange(["codeChanged",e.getValue()]);
    }
    render() {
        const divStyle = {
            height : "40em"
        };
        return <div className = "col-12">
            <div className = "border" style = {divStyle}>
                <CodeMirror
                    value={this.props.value}
                    options={{
                       theme: 'eclipse',
                        keyMap: 'sublime',
                        mode: 'jsx',
                    }}
                    onChange = {this.onChange}
                />
            </div>
            
            </div>;
    }
}

class ScriptActionHeaderName extends React.Component {
    render(){
        return  <div className = "col-6 my-3 text-center">
            <h4>{this.props.actionName}</h4>
        </div>
    }
    
}

class ScriptActionCodeVariablesSelect extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            value : this.props.value
        }
    }
    handleChange(element) {
        this.setState({
            value : element.target.value
        });
        this.props.onChange([element.target.value]);
    }
    render() {
        return <div className = "col-3 my-3">
            <select name = "varsOrCodeSelect" className = "form-select" value={this.state.value} onChange={this.handleChange}>
                <option value = "vars" >variables</option>
                <option value = "code" >code</option>
            </select>
        </div>;
    }
}

class ScriptActionSavedStatus extends React.Component {
    render() {
        if (this.props.notSaved === true) {
            return <figcaption className="blockquote-footer">
                Not saved
            </figcaption>;
        } else {
            return <p></p>;
        }
        
    }
}

export class ScriptActionView extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.reload = this.reload.bind(this);
        this.statesHistory = [];
        this.VIEW_STATES = {
            MAIN_SCREEN : 0,
            SETTINGS : 1
        } 
        this.CODE_VARS = {
            CODE : 'code',
            VARS : 'vars'
        }
        this.state = {
            loading : true,
            error : false,
            running : false
        };
    }
    onChange(params) {
        switch (params[0]) {
            case (CommonComponents.BTNS_RETURNS.BACK) : {
                if (this.statesHistory.length === 0) {
                    this.props.onChange([CommonComponents.BTNS_RETURNS.BACK]);
                } else {
                   this.setState(this.statesHistory.pop()); 
                }
                
                break;
            }
            case (CommonComponents.BTNS_RETURNS.SETTINGS) : {
                this.statesHistory.push(this.state);
                this.setState({
                    view : this.VIEW_STATES.SETTINGS
                });
                break;
            }
            case (this.CODE_VARS.CODE) : {
                this.setState({
                    code_vars : this.CODE_VARS.CODE
                });
                break;
            }
            case (this.CODE_VARS.VARS) : {
                this.setState({
                    code_vars : this.CODE_VARS.VARS
                });
                break;
            }
            case ("codeChanged") : {
                if (this.state.code_vars === this.CODE_VARS.VARS) {
                    if (this.state.scriptAction.vars !== params[1]) {
                        this.state.scriptAction.vars = params[1];
                        this.setState({
                            notSaved : true
                        })
                    }
                } else if (this.state.code_vars === this.CODE_VARS.CODE) {
                    if (this.state.scriptAction.code !== params[1]) {
                        this.state.scriptAction.code = params[1];
                        this.setState({
                            notSaved : true
                        })
                    }
                }
                break;
            }
            case (CommonComponents.BTNS_RETURNS.SAVE) : {
                this.setState({
                    saving : true
                })
                Hub.setScriptActionData(this.state.scriptAction)
                .then(response => {
                    this.setState({
                        notSaved : false,
                        saving : false
                    })
                })
                .catch(err => {
                    this.setState({
                        saving : false
                    })
                    alert(err + "")
                })
                break;
            }
            case (CommonComponents.BTNS_RETURNS.RUN) : {
                this.setState({
                    running : true,
                    saving : true
                })
                Hub.setScriptActionData(this.state.scriptAction)
                .then(response => {
                    Hub.runScriptAction(this.state.scriptAction)
                    .then(response => {
                        this.setState({
                            notSaved : false,
                            running : false,
                            saving : false
                        })
                    })
                })
                .catch(err => {
                    this.setState({
                        running : false,
                        saving : false
                    })
                    alert(err + "");
                })
                break;
            }
            case (CommonComponents.BTNS_RETURNS.CONSOLE) : {
                window.open("/log.htm",'targetWindow',
                               `toolbar=no,
                                location=no,
                                status=no,
                                menubar=no,
                                scrollbars=no,
                                resizable=yes,
                                width=700,
                                height=500`);
                break;
            }
            case (CommonComponents.BTNS_RETURNS.APPLY_FORM) : {
                if (this.state.scriptAction.name === params[1].name) {
                    this.setState({
                        applying : true
                    })
                    Hub.setScriptActionData(params[1])
                    .then(response => {
                        this.setState(this.statesHistory.pop());
                        this.setState({
                            notSaved : false,
                            scriptAction : params[1],
                            applying : false
                        })
                    })
                    .catch(err => {
                        this.setState({
                            applying : false
                        })
                        alert(err + "")
                    })
                } else {
                    let lowerCaseActionsList = [];
                    for (let e of this.props.scriptActionsList) {
                        lowerCaseActionsList.push(e.toLowerCase())
                    }
                    if (lowerCaseActionsList.includes(params[1].name.toLowerCase())) {
                        alert ("A action with the same name already exists");
                    } else {
                        this.setState({
                            applying : true
                        })
                        Hub.renameSciptAction(this.state.scriptAction.name,params[1].name)
                        .then(response => {
                            Hub.setScriptActionData(params[1])
                            .then(response => {
                                this.setState(this.statesHistory.pop());
                                this.setState({
                                    notSaved : false,
                                    scriptAction : params[1],
                                    applying : false
                                })
                            })
                        })
                        .catch(err => {
                            this.setState({
                                applying : false
                            })
                            alert(err + "")
                        })
                    }
                }
                break;
            }
            case (CommonComponents.BTNS_RETURNS.REMOVE) : {
                let remove = window.confirm("Realy remove action?");
                if (remove) {
                    this.setState({
                        removing : true
                    })
                    Hub.removeScriptAction(this.state.scriptAction)
                    .then(response => {
                        this.props.onChange([CommonComponents.BTNS_RETURNS.BACK]);
                    })
                    .catch(err => {
                        alert(err+ "");
                        this.setState({
                            removing : false
                        })
                    })
                }
            }
        }
    }
    componentDidMount() {
        if(this.props.isNew === true) {
            this.setState({
                loading : false,
                error : false,
                scriptAction : this.props.scriptAction,
                view : this.VIEW_STATES.MAIN_SCREEN,
                code_vars : this.CODE_VARS.CODE,
                notSaved : false,
                applying : false,
                saving : false,
                removing : false
            })
        } else {
            this.reload();
        }
    }
    reload() {
        Hub.getScriptAction(this.props.scriptAction)
        .then(scriptAction => {
            this.setState({
                loading : false,
                error : false,
                scriptAction : scriptAction,
                view : this.VIEW_STATES.MAIN_SCREEN,
                code_vars : this.CODE_VARS.CODE,
                notSaved : false,
                applying : false,
                saving : false,
                removing : false
            })
        })
        .catch(err => {
            this.setState({
                loading : false,
                error : err + "",
            })
        })
    }
    render() {
        if (this.state.loading) {
            return <CommonComponents.LoadingField />
        } else if (this.state.error) {
            return <CommonComponents.ReloadErrorField onChange = {this.reload} error = {this.state.error}/>
        } else {
            if (this.state.view === this.VIEW_STATES.MAIN_SCREEN) {
                return <div>
                    <div className = "row my-3">
                        <ScriptActionCodeVariablesSelect value = {this.state.code_vars} onChange = {this.onChange}/>
                        <ScriptActionHeaderName actionName = {this.state.scriptAction.name}/>
                        <div className = "col-1 my-3">
                            <CommonComponents.ConsoleBtn onChange = {this.onChange}/>
                        </div>
                        <div className = "col-1 my-3">
                            <CommonComponents.RunBtn running = {this.state.running} onChange = {this.onChange}/>
                        </div>
                        <div className = "col-1 my-3">
                            <CommonComponents.SettingsBtn onChange = {this.onChange} scriptAction  = {this.state.scriptAction} />
                        </div>
                        {this.state.code_vars === this.CODE_VARS.CODE &&
                            <ScriptActionCodeArea onChange = {this.onChange} value = {this.state.scriptAction.code}/>
                        }
                        {this.state.code_vars === this.CODE_VARS.VARS &&
                            <ScriptActionCodeArea onChange = {this.onChange} value = {this.state.scriptAction.vars}/>
                        }
                    </div>
                    <ScriptActionSavedStatus notSaved = {this.state.notSaved} />
                    <CommonComponents.Back_Save_Remove_Btns saving = {this.state.saving} removing = {this.state.removing} onChange = {this.onChange} />
                </div>;
            } else if (this.state.view === this.VIEW_STATES.SETTINGS) {
                return <ScriptActionSettings applying = {this.state.applying} scriptAction = {this.state.scriptAction} onChange = {this.onChange}/>
            }
            
        }
    }
}

export class ScriptActionSettings extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.state = {  
            name : this.props.scriptAction.name,
            execution : this.props.scriptAction.execution,
            applying : false
        }
    }
    handleChange(e) {
        const value = e.target.value;
        const name = e.target.name;
        this.setState({
            [name]: value
        });
    }
    handleSubmit(e) {
        e.preventDefault();

        let scriptAction = new Script(this.state.name,'');
        scriptAction.execution = this.state.execution;
        scriptAction.code = this.props.scriptAction.code;
        scriptAction.vars = this.props.scriptAction.vars;

        this.props.onChange([CommonComponents.BTNS_RETURNS.APPLY_FORM,scriptAction]);
    }
    render() {
        return  <form onSubmit = {this.handleSubmit}>
            <div className="mb-3">
                <label htmlFor="actionNameInput" className="form-label">Action name</label>
                <input value = {this.state.name} type="text" className="form-control" name="name" onChange = {this.handleChange}/>
            </div>
            <div className="input-group mb-3">
                <select value = {[this.state.execution]} name = "execution" className="form-select" multiple aria-label="multiple select example" onChange = {this.handleChange}>
                    <option disabled>Select the execution type</option>
                    <option value = "-" >once</option>
                    <option value = "+" >repeatedly</option>
                </select>
            </div>
            <CommonComponents.BACK_APPLY_BTNS applying = {this.props.applying} onChange = {this.props.onChange}/>
        </form>;
    }
    
}

class ScriptActionImage extends React.Component {
    render() {
        return <img width={this.props.imgWidth} src = {SENSORS_IMAGES.script} className ='img-fluid' alt={this.props.scriptName}></img>
    }
}


class ScriptActionListElement extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = this.props.onChange.bind(this);
    }
    render() {
        const elementStyle = {
            backgroundColor : "#8FBC8F",
            borderRadius : "1em",
            cursor : "pointer"
        }
        let handleClick = null
        if(this.props.readonly != true) handleClick = () => this.props.onChange(["scriptAction",this.props.name]);
        return  <div className = "row my-3 py-1" style={elementStyle} onClick = {handleClick}>
                    <div className = "col-2" >
                        <ScriptActionImage imgWidth = "80" scriptName = {this.props.name}/>
                    </div>
                    <div className = "col-9 my-auto text-center">
                        <h5>{this.props.name}</h5>
                    </div>
                </div>;
    }
}

export class ScriptActionsList extends React.Component {
    render() {
        const listItems = this.props.scriptActions.map((scriptName) => 
            <ScriptActionListElement readonly = {this.props.readonly} key = {scriptName} name = {scriptName} onChange = {this.props.onChange}/>
        )
        return <div>
            {listItems}
        </div>;
    }
}