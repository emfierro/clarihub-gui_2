import React from 'react';
import SchedulesScreen from './SchedulesView';
import * as CommonComponents from './CommonComponents';

class Schedules extends React.Component {
    render() {
      if(this.props.permissions.schedule != "none") {
        return <div className = "container">
          <SchedulesScreen permissions={this.props.permissions}/>
        </div>;
      }
      else{
        return <CommonComponents.AccessDeniedMessage />
      }
    }
  }
  export default Schedules;