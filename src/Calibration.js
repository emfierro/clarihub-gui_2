import React from 'react';
import * as CommonComponents from './CommonComponents';
import * as Hub from './Network';

const CALIBRATION_POINTS_LIMIT = {
    NO_DATA : 0,
    LINEAR : 2,
    PARABOLIC : 3,
    PIECEWISE : 3
}

const CALIBRATION_STR_TYPES = ["No data","No calibration","Linear","Parabolic", "Piecewise"];

const CALIBRATION_INT_TYPES = {
    NO_DATA : 0,
    NO_CALIBRATION : 1,
    LINEAR : 2,
    PARABOLIC : 3,
    PIECEWISE : 4
}

class CalibrationOld extends React.Component { // old calibration points
    constructor(props){
        super(props);
        this.LOADING_STATE = {
            LOADING : 0,
            LOADED : 1,
            ERROR : 2
        }
    }
    render () {
        switch (this.props.loadingState) {
            case (this.LOADING_STATE.LOADING) : {
                return <CommonComponents.LoadingField />
            }
            case (this.LOADING_STATE.LOADED) : {
                let i = 0;
                let tempCalibrationPoints = [];
                switch (Number.parseInt(this.props.calibrationType)) {
                    case (CALIBRATION_INT_TYPES.LINEAR) :
                        for (let i = 0; i < CALIBRATION_POINTS_LIMIT.LINEAR; i++) {
                            tempCalibrationPoints.push(this.props.calibrationData[i]);
                        }
                        break;
                    case (CALIBRATION_INT_TYPES.PARABOLIC) :
                        for (let i = 0; i < CALIBRATION_POINTS_LIMIT.PARABOLIC; i++) {
                            tempCalibrationPoints.push(this.props.calibrationData[i]);
                        }
                        break;
                    case (CALIBRATION_INT_TYPES.PIECEWISE) :
                        for (let i = 0; i < CALIBRATION_POINTS_LIMIT.PIECEWISE; i++) {
                            tempCalibrationPoints.push(this.props.calibrationData[i]);
                        }
                        break;
                    
                    default:{
                        console.error("error at Calibration.js  switch (this.LOADING_STATE.LOADED) reaches default");break; 
                    }  
                }
                const listItems = tempCalibrationPoints.map(value => 
                        <li key = {i+1731}>Constant {++i} : [ <strong>{Number.parseFloat(value)}</strong> ]</li>
                    )
                return <div>
                    Current calibration type : <strong>{CALIBRATION_STR_TYPES[this.props.calibrationType]}</strong>
                    {
                        this.props.calibrationType > 1 &&
                            <ul>
                                {listItems}
                            </ul>
                    }
                </div>;
            }
            case (this.LOADING_STATE.ERROR) : {
                return <div className = "col-12">
                    <div className="alert alert-danger d-grid gap-2 col-8 mx-auto text-center" role="alert" >
                        <h5>{this.props.calibrationError}</h5>
                    </div> 
                </div>
                
            }
            default:{
                console.error("error at Calibration.js  switch (this.props.loadingState) reaches default"); 
                return <div></div>;
            }  

        }
    }
}

class CalibrationProcedure extends React.Component {
    constructor(props) {
        super(props);
        this.changeCalibrationProcedureType = this.changeCalibrationProcedureType.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.calibrationChange = this.calibrationChange.bind(this);
        this.VIEW_STATES = {
            CHANGE_BTNS : 0,
            MANUAL : 1,
            AUTOMATIC : 2
        }
        let tempArraySize = 0;
        switch (Number.parseInt(this.props.calibrationType)) {
            case (CALIBRATION_INT_TYPES.LINEAR) :
                tempArraySize =  CALIBRATION_POINTS_LIMIT.LINEAR
                break;
            case (CALIBRATION_INT_TYPES.PARABOLIC) :
                tempArraySize =  CALIBRATION_POINTS_LIMIT.PARABOLIC
                break;
            case (CALIBRATION_INT_TYPES.PIECEWISE) :
                tempArraySize =  CALIBRATION_POINTS_LIMIT.PIECEWISE
                break;
                
                default:{
                    console.error("error at Calibration.js  switch (Number.parseInt(this.props.calibrationType)) reaches default");break; 
                }  
        }
        let tempPoints = new Array(tempArraySize).fill(0);
        this.state = {
            view : this.VIEW_STATES.CHANGE_BTNS,
            calibrationPointArraySize : tempArraySize,
            calibrationChanged : this.props.calibrationType,
            capturingPoints : [],
            calibrationConstants : tempPoints
        }
    }

    changeCalibrationProcedureType(calType) {
        if (calType === "manually") {
            this.setState({
                view : this.VIEW_STATES.MANUAL
            })
            this.props.onChange([calType]);
            this.setState({
                calibrationConstants : new Array(this.state.calibrationPointArraySize).fill(0)
            })
            this.props.onChange(["manualArray", this.state.calibrationConstants]);
        } else if (calType === "automatic") {
            this.setState({
                view : this.VIEW_STATES.AUTOMATIC
            })
        }
    }

    handleChange(e) {
        if (this.state.view === this.VIEW_STATES.MANUAL) {
            let name = Number.parseInt(e.target.name);
            let value = e.target.value;
            this.state.calibrationConstants[name].setState(value);
            this.props.onChange(["manualArray", this.state.calibrationConstants]);
        } else if (this.state.view === this.VIEW_STATES.AUTOMATIC) {
            let tempPoints = this.state.capturingPoints;
            tempPoints.push(e);
            if (tempPoints.length === this.state.calibrationPointArraySize) { 
                this.props.onChange(["automatic"]);
            }
            this.setState({
                capturingPoints : tempPoints
            })
            this.props.onChange(["capturingPoints", tempPoints]);
        }
    }

    calibrationChange(e) {
        let tempArraySize = 0;
        let calibrationTypeChanged = Number.parseInt(e.target.value);
        switch (calibrationTypeChanged) {
            case (CALIBRATION_INT_TYPES.NO_CALIBRATION) : 
                this.props.onChange([CALIBRATION_STR_TYPES[calibrationTypeChanged]]);
                break;
            case (CALIBRATION_INT_TYPES.LINEAR) :
                tempArraySize =  CALIBRATION_POINTS_LIMIT.LINEAR
                this.props.onChange([CALIBRATION_STR_TYPES[calibrationTypeChanged]]);
                break;
            case (CALIBRATION_INT_TYPES.PARABOLIC) :
                tempArraySize =  CALIBRATION_POINTS_LIMIT.PARABOLIC
                this.props.onChange([CALIBRATION_STR_TYPES[calibrationTypeChanged]]);
                break;
            case (CALIBRATION_INT_TYPES.PIECEWISE) :
                tempArraySize =  CALIBRATION_POINTS_LIMIT.PIECEWISE
                this.props.onChange([CALIBRATION_STR_TYPES[calibrationTypeChanged]]);
                break;
                
                default:{
                    console.error("error at Calibration.js  switch (calibrationTypeChanged) reaches default");break; 
                }  
        }
        
        this.setState({
            [e.target.name] : Number.parseInt(e.target.value),
            calibrationPointArraySize : tempArraySize
        })
    }

    render() {
        switch(this.state.view) {
            case (this.VIEW_STATES.CHANGE_BTNS) : {
                return <div>
                    <select className = "form-control my-2" value = {this.state.calibrationChanged} name = "calibrationChanged" onChange = {this.calibrationChange}>  
                        <option value = {CALIBRATION_INT_TYPES.NO_CALIBRATION}>No calibration</option>
                        <option value = {CALIBRATION_INT_TYPES.LINEAR}>Linear</option>
                        <option value = {CALIBRATION_INT_TYPES.PARABOLIC}>Parabolic</option>
                        <option value = {CALIBRATION_INT_TYPES.PIECEWISE}>Piecewise</option>
                    </select>
                    {
                        this.state.calibrationChanged > 1 &&
                        <div className = "my-2">
                            <button type = "button" className = "btn btn-light mx-1 my-1 border" onClick = {() => this.changeCalibrationProcedureType("manually")}>
                                <i className="bi bi-hand-index"></i>
                                &nbsp;
                                Enter calibration constants
                            </button>
                            <button type = "button" className = "btn btn-light mx-1 my-1 border" onClick = {() => this.changeCalibrationProcedureType("automatic")}>
                                <i className="bi bi-play"></i>
                                &nbsp;
                                Start calibration procedure
                            </button>
                        </div>
                    }
                   
                </div>;
            }
            case (this.VIEW_STATES.MANUAL) : { // состояние калибровочных констант
                let i = 0;
                let arr = new Array(this.state.calibrationPointArraySize).fill(0);
                const listItems = arr.map(value => 
                        <li key = {i+900}>
                            Constant {++i} : 
                            <input type = "number" className="form-control" value = {this.state.calibrationConstants[i-1]} name = {i-1} onChange = {this.handleChange} required />
                        </li>
                    )
                return <div>
                    <hr />
                    <CommonComponents.ContainerHeaderText headerText = {`Set calibration constants [${CALIBRATION_STR_TYPES[this.state.calibrationChanged]}]`} />
                    <ul>
                        {listItems}
                    </ul>
                </div>;
            }
            case (this.VIEW_STATES.AUTOMATIC) : {
                const tableStyle = {
                    borderCollapse : "separate",
                    borderSpacing:" 0px 10px"
                }
                return <div>
                    <hr />
                    <CommonComponents.ContainerHeaderText headerText = {`Calibration procedure [${CALIBRATION_STR_TYPES[this.state.calibrationChanged]}]`} />
                    <table className ="table" style = {tableStyle}>
                        <thead > 
                            <tr>
                                <th scope="col">Current value</th>
                                <th scope="col">Actual value</th>
                                <th scope="col">Capture</th>
                            </tr>
                        </thead>
                        <CapturingPoints 
                            device = {this.props.device}
                            capturingPoints = {this.state.capturingPoints}
                            limit = {this.state.calibrationPointArraySize}
                            onChange = {this.handleChange}
                        />
                    </table>
                </div>;
            }
 
            
            default:{
                console.error("error at Calibration.js  switch (calibrationTypeChanged) reaches default");
                return <div>[see error in console]</div>;
            }  
        }
        
    }
}

class CapturingPoints extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            actualValue : 0
        }
    }

    handleChange(e) {
        this.setState({
            actualValue : e.target.value
        })
    }

    handleClick(e) {
        this.props.onChange({
            current : this.props.device.raw,
            actual : this.state.actualValue
        })
    }

    render() {
        let i = 90;
        const listItems = this.props.capturingPoints.map((data) => 
            <tr key = {++i+800} className = "table-success">
                <td>{data.current}</td>
                <td>{data.actual}</td>
                <td><i className = "bi bi-check"></i></td>
            </tr>
        )
        return <tbody>
            {listItems}
            {
                this.props.capturingPoints.length < this.props.limit &&

                    <tr className = "table-primary">
                        <td>{this.props.device.raw}</td>
                        <td><input type = "number" className = "form-control" value = {this.state.actualValue} onChange = {this.handleChange} required/></td>
                        <td>
                            <button type = "button" className = "btn btn-light border" onClick = {this.handleClick}>
                                <i className="bi bi-hand-index"></i>
                                &nbsp;
                                Capture
                            </button>
                        </td>
                    </tr>
            }
        </tbody>;
    }
}

class CalibrationScreen extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.channel = this.props.device;
        this.sensor = this.channel.parent;
        this.mega = this.sensor.parent;
        this.LOADING_STATE = {
            LOADING : 0,
            LOADED : 1,
            ERROR : 2
        }
        this.state = {
            disabledApply : true,
            applying : false,
            calibrationOldLoadingError : "",
            calibrationOldLoading : this.LOADING_STATE.LOADING
        };
    }
    componentDidMount(){
        Hub.sendGET(`/gCalData/${this.sensor.id}/${this.channel.arrayIndex}`)
        .then(response => {
            response.json().then(json => {
                if (json.result === "dnf") {
                    this.setState({
                        disabledApply : true,
                        calibrationOldLoading : this.LOADING_STATE.ERROR,
                        calibrationOldLoadingError : "Device not found!"
                    })
                } else if (json.result === "cioor") {
                    this.setState({
                        disabledApply : true,
                        calibrationOldLoading : this.LOADING_STATE.ERROR,
                        calibrationOldLoadingError : "Channel index out of range"
                    })
                } else {
                    this.setState({
                        disabledApply : true,
                        calibrationOldLoading : this.LOADING_STATE.LOADED,
                        calibrationOldType : json.result.calType,
                        calibrationOldData : json.result.calData,
                        calibrationMethod : Number.parseInt(json.result.calType)
                    })
                }
            })
            
        })
        .catch(err => {
            this.setState({
                calibrationOldLoading : this.LOADING_STATE.ERROR,
                calibrationOldLoadingError : err + ""
            })
        })
    }
    onChange(params){
        switch (params[0]) {
            case ("manually") : {
                this.setState({
                    calibrationNewType : params[0],
                    disabledApply : false
                })
                break;
            }
            case ("automatic") : {
                this.setState({
                    calibrationNewType : params[0],
                    disabledApply : false
                })
                break;
            }
            case ("manualArray") : {
                this.setState({
                    manualArray : params[1]
                })
                break;
            }
            case ("capturingPoints") : {
                this.setState({
                    capturingPoints : params[1]
                })
                break;
            }
            case (CALIBRATION_STR_TYPES[CALIBRATION_INT_TYPES.NO_CALIBRATION]) : {
                this.setState({
                    calibrationMethod :CALIBRATION_INT_TYPES.NO_CALIBRATION,
                    disabledApply : false
                })
                break;
            }
            case (CALIBRATION_STR_TYPES[CALIBRATION_INT_TYPES.LINEAR]) : {
                this.setState({
                    calibrationMethod : CALIBRATION_INT_TYPES.LINEAR,
                    disabledApply : true
                })
                break;
            }
            case (CALIBRATION_STR_TYPES[CALIBRATION_INT_TYPES.PARABOLIC]) : {
                this.setState({
                    calibrationMethod : CALIBRATION_INT_TYPES.PARABOLIC,
                    disabledApply : true
                })
                break;
            }
            case (CALIBRATION_STR_TYPES[CALIBRATION_INT_TYPES.PIECEWISE]) : {
                this.setState({
                    calibrationMethod : CALIBRATION_INT_TYPES.PIECEWISE,
                    disabledApply : true
                })
                break; 
            }
            default:{
                console.error("error at Calibration.js  switch (params[0]) reaches default");break;
            }  
        }
    }
    handleSubmit(e) {
        e.preventDefault();
        let body = "::SCA:";
        let queryName = ""
        if (this.state.calibrationMethod === CALIBRATION_INT_TYPES.NO_CALIBRATION) {
            body  +=  CALIBRATION_INT_TYPES.NO_CALIBRATION + ":";
            queryName = "calibrate";
        } else if (this.state.calibrationNewType === "manually") {
            body  +=  this.state.calibrationMethod + ":";
            queryName = "calibrate";
            for (let point of this.state.manualArray) {
                body += point + ":";
                // --- trash --- (repeat count check)
                //let repeatCountCheck = 0;
                //for (let _point of this.state.manualArray) { // repeat count check
                //    if (_point === point) {
                //        repeatCountCheck++;
                //    }
                //}
                //if (repeatCountCheck > 1) {
                //    alert("dots cannot be repeated");
                //    return;
                //}
                // --- trash ---
            }
        } else if (this.state.calibrationNewType === "automatic") {
            body  +=  this.state.calibrationMethod + ":";
            queryName = "calibrate";
            for (let point of this.state.capturingPoints) {
                body += point.current + ":";
            }
            for (let point of this.state.capturingPoints) {
                body += point.actual + ":";
                // --- trash --- (repeat count check)
                //let actualRepeatCountCheck = 0;
                //let currentRepeatCountCheck = 0;
                //for (let _point of this.state.capturingPoints) { // repeat count check
                //    if (_point.actual === point.actual) {
                //        actualRepeatCountCheck++;
                //    }
                //    if (_point.current === point.current) {
                //        currentRepeatCountCheck++;
                //    }
                //}
                //if (currentRepeatCountCheck > 1 || actualRepeatCountCheck > 1) {
                //    alert("dots cannot be repeated");
                //    return;
                //}
                // --- trash ---
            }
        }
        body += ":";
        let query = `/${queryName}/${this.sensor.id}/${this.channel.arrayIndex}`;
        this.setState({
            applying : true
        })
        Hub.sendPOST(query,body)
        .then(response => {
            response.json().then(json => {
                switch (json.result) {
                    case ("dnf") : {
                        alert("Device not found");
                        break;
                    } 
                    case ("cioor") : {
                        alert("Channel index out of range");
                        break;
                    } 
                    case ("mdna") : {
                        alert("Mega does not answer");
                        break;
                    } 
                    case ("pe") : {
                        alert("Parse error");
                        break;
                    } 
                    case ("swe") : {
                        alert("Sensor write error");
                        break;
                    } 
                    case ("OK") : {
                        this.props.onChange([CommonComponents.BTNS_RETURNS.BACK]);
                        break;
                    }
                        
                    default:{
                        console.error("error at Calibration.js  switch (json.result) reaches default");break; 
                    }  
                }
            })
            this.setState({applying : false});
        })
        .catch(err => {
            this.setState({applying : false});
            alert(`No calibrated ; error : ${err+""}`);
        })
    }
    render() {
        const headerText = `Calibration channel #[${this.channel.arrayIndex}] of sensor [${this.sensor.specifedName}] on [${this.mega.specifedName}]`;
        return <form onSubmit = {this.handleSubmit}>
            <CommonComponents.ContainerHeaderText headerText = {headerText} />
            <CalibrationOld 
                loadingState = {this.state.calibrationOldLoading}
                calibrationType = {this.state.calibrationOldType}
                calibrationData = {this.state.calibrationOldData}
                calibrationError = {this.state.calibrationOldLoadingError}
            />
            {
                this.state.calibrationOldLoading === this.LOADING_STATE.LOADED &&
                    <CalibrationProcedure device = {this.props.device} calibrationType = {this.state.calibrationOldType} onChange = {this.onChange} />
            }
            <CommonComponents.BACK_APPLY_BTNS applying = {this.state.applying} disabled = {this.state.disabledApply} onChange = {this.props.onChange}/>
        </form>;
    }
}

export default CalibrationScreen;