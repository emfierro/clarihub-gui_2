import React from 'react';
import SettingsScreen from './SettingsView';
import * as CommonComponents from './CommonComponents';

class Settings extends React.Component {
    constructor(props) {
      super(props);
    }
    render() {
      console.log("Settings permissions:");
      console.log(this.props.permissions);
      if(this.props.permissions.settings != "none") {
        return <div className = "container">
          <SettingsScreen appUpdate = {this.props.appUpdate} permissions = {this.props.permissions}/>
        </div>;
      }
      else {
        return <CommonComponents.AccessDeniedMessage />
      }
    }
  }
  export default Settings;