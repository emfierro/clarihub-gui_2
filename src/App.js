import React from 'react';

  import {
    Route,
    Switch,
    Redirect,
    BrowserRouter,
    Link
  } from "react-router-dom"

import Actions from './Actions'; 
import Status from './Status';  
import Settings from './Settings';  
import Schedules from './Schedules'; 
import Accounts from './Accounts'; 
import Auth from './Auth'; 

import * as Hub from './Network';

class NavBrand extends React.Component {
  render() { 
    return  <Link to="/status" className="navbar-brand" onClick = {this.props.onChange}>
              clarify
              <img  type="image/png" src="../SD/app/images/favicon.png" alt="" width="30" height="30" />
              team
            </Link>

  }
}

class NavBarSystemTime extends React.Component {
  constructor(props){
    super(props);
    this.update = this.update.bind(this);
    this.oldTime = this.props.hubTime;
    this.oldName = this.props.hubName;
    this.state = {
      hubTime : this.props.hubTime,
      hubName : this.props.hubName
    }
  }
  componentDidMount() {
    Hub.getHubName()
    .then(name => {
      Hub.getShiftedTimeByUserTimeZone()
      .then(time => {
        let date = new Date(time);
        let hh = date.getHours();
			  let mm = date.getMinutes();
			  let timeToView = [
          (hh>9 ? '' : '0') + hh,
					(mm>9 ? ':' : ':0') + mm
				].join('');
        this.setState({
          hubName : name,
          hubTime : timeToView
        });
        this.updateDevicesTimer = setInterval(
          () => this.update(),
          20000
      );
      })
    })
    .catch(err => {
      throw(err);
    })
  }
  componentWillUnmount() {
    clearInterval(this.updateDevicesTimer);
  }
  update() {
      Hub.getShiftedTimeByUserTimeZone()
      .then(time => {
        let date = new Date(time);
        let hh = date.getHours();
        let mm = date.getMinutes();
        let timeToView = [
          (hh>9 ? '' : '0') + hh,
          (mm>9 ? ':' : ':0') + mm
        ].join('');
        this.setState({
          hubTime : timeToView
        });
      })
  }
  render() {
    if (this.oldTime !== this.props.hubTime) {
      this.oldTime = this.props.hubTime;
      this.state.hubTime = this.props.hubTime;
      return <span className="navbar-text">
        {this.props.hubTime} &nbsp; [{this.state.hubName}]  
      </span>;
    } else if (this.oldName !== this.props.hubName) {
      this.oldName = this.props.hubName;
      this.state.hubName = this.props.hubName; 
      return <span className="navbar-text">
        {this.state.hubTime} &nbsp; [{this.props.hubName}]  
      </span>;
    } else {
      return  <span className="navbar-text">
        {this.state.hubTime} &nbsp; [{this.state.hubName}]  
      </span>;
    }
    
  }
}

class NavBarLink extends React.Component {
  render() {
    return <li className="nav-item">
              <Link to = {this.props.linkPath} className="nav-link" onClick = {this.props.onChange}>
                <i className={this.props.linkIcon}></i>
                &nbsp;
                {this.props.linkTitle}
              </Link>
            </li>;
  }
}

class NavBarToggler extends React.Component {
  render() {
    return <button className="navbar-toggler collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>;
  }
}

class NavBar extends React.Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.state = {
      key : 1,
    }
  }
  onChange() {
    this.setState({
      key : this.state.key * -1
    });
  }
  render() {
      return <nav key = {this.state.key} className="navbar navbar-expand-lg navbar-light bg-light" >
            <div className="container-fluid">
              <NavBrand onChange = {this.onChange}/>
              <NavBarToggler  onChange = {this.onChange} ariaExpanded = {this.state.ariaExpanded} btnClass = {this.state.btnClass}/>
              <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                  <NavBarLink onChange = {this.onChange} linkPath = "/status" linkTitle = "Status" linkIcon = "bi bi-info-circle"/>
                  <NavBarLink onChange = {this.onChange} linkPath = "/actions" linkTitle = "Actions" linkIcon = "bi bi-code-slash"/>
                  <NavBarLink onChange = {this.onChange} linkPath = "/schedules" linkTitle = "Schedules" linkIcon = "bi bi-calendar-date"/>
                  <NavBarLink onChange = {this.onChange} linkPath = "/settings" linkTitle = "Settings" linkIcon = "bi bi-gear"/>
                  <NavBarLink onChange = {this.onChange} linkPath = "/login" linkTitle = "Sign out" linkIcon = "bi bi-person-x"/>
                  <NavBarLink onChange = {this.onChange} linkPath = "/getAccounts" linkTitle = "Accounts" linkIcon = "bi bi-person-x"/>
                </ul>
                <NavBarSystemTime hubTime = {this.props.hubTime} hubName = {this.props.hubName}/>
              </div>
            </div>
          </nav>;
  }
}

class App extends React.Component {
  constructor(props){
    super(props);
    this.update = this.update.bind(this);
    this.onLogin = this.onLogin.bind(this);
    this.state = {
      hubTime : "hub",
      hubName : "00:00",
      loaded: false,
      logined: false,
      userPermissions: {},
    }
    Hub.getUserPermissions().then(perms => {
      this.setState({userPermissions : perms, loaded : true});
      console.log("App permissions updated");
    });
    
  }
  update(param) {
    console.log("will update " + param);
      if (param) {
        if (param === "name") {
            Hub.getHubName()
            .then(name => {
              this.setState({
                hubName : name,
              });
            })
        }
        if (param === "time") {
          Hub.getShiftedTimeByUserTimeZone()
          .then(time => {
            let date = new Date(time);
            let hh = date.getHours();
            let mm = date.getMinutes();
            let timeToView = [
              (hh>9 ? '' : '0') + hh,
              (mm>9 ? ':' : ':0') + mm
            ].join('');
            this.setState({
              hubTime : timeToView
            });
          })
        }
      }
  }
  componentWillMount() {
    this.update("param"); 
  }

  onLogin(){
    this.setState({logined: true});
  }

  render() {
    if(this.state.logined){
      if(this.state.loaded){
        return  (
          <BrowserRouter>
          <NavBar hubTime = {this.state.hubTime} hubName = {this.state.hubName}/>
            <Switch>
              <Route path='/status'>
                <Status permissions={this.state.userPermissions}/>
              </Route>
              <Route path='/actions'>
                <Actions permissions={this.state.userPermissions}/>
              </Route>
              <Route path='/schedules'>
                <Schedules permissions={this.state.userPermissions}/>
              </Route>
              <Route path='/getAccounts'>
                <Accounts permissions={this.state.userPermissions}/>
              </Route>
              <Route path='/settings'>
                <Settings appUpdate = {this.update} permissions={this.state.userPermissions}/>
              </Route>
              <Redirect from='/' to='/status'/>
            </Switch>
          </BrowserRouter>
        );
      }
      else {
        return <div>
          <div style={{position: "absolute", top: "40%", left: "40%", "font-size": "50pt"}}>
            Loading
          </div>
        </div>
      }
    }
    else {
      return <Auth onLogin = {this.onLogin} />
    }
  }
}


export default App;
