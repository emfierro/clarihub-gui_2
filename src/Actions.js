import React from 'react';
import ActionsScreen from './ActionView';

class Actions extends React.Component {
    render() {
      return <div className = "container">
        <ActionsScreen permissions = {this.props.permissions}/>
      </div>;
    }
  }

  export default Actions;