import React from 'react';
import * as CommonComponents from './CommonComponents';
import {DevicesScreen, DEVICES_VIEW_MODES} from './DevicesView';
import { convertTimeZoneToViewFormat } from './TimeZoneConvertations';
import * as Hub from './Network';
import * as VersionInfo from './VersionInfo';

const SETTINGS_NAMES = {
    GENERAL_SETTINGS    : "General settings"    ,
    NETWORK_SETTINGS    : "Network settings"    ,
    WIFI_SETTINGS       : "WiFi settings"       ,
    AP_SETTINGS         : "AP settings"         ,
    CLOCK_SETTINGS      : "Clock settings"      ,
    GEO_SETTINGS        : "Geolocation settings",
    UPDATE_FIRMWARE     : "Update firmware"     ,
    DEVICES_SETTINGS    : "Devices settings"    ,
    HUB_NAME_SETTINGS   : "Hub settings",
    DUMP_SETTINGS       : "Dump settings",
}

class SettingsButton extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = this.props.onChange.bind(this);
    }
    render() {
        const btnStyle = {
            cursor : "pointer"
        }
        return (
            <tr  onClick={() => this.props.onChange([this.props.btnName])} style = {btnStyle}>
                <td>&nbsp;</td>
                <td><i className={this.props.iconClass}></i></td>
                <td>{this.props.btnName}</td>
            </tr>
        );
    }
}

class SettingsButtonsList extends React.Component {
    render() {
        const tableStyle = {
            borderCollapse : "separate",
            borderSpacing : "0px 22px"
        }
        return (
            <table className="table table-primary table-striped" style = {tableStyle} >
                <tbody>
                    <SettingsButton btnName = {SETTINGS_NAMES.GENERAL_SETTINGS}     iconClass = "bi bi-gear"        onChange = {this.props.onChange}/>
                    <SettingsButton btnName = {SETTINGS_NAMES.NETWORK_SETTINGS}     iconClass = "bi bi-globe"       onChange = {this.props.onChange}/>
                    <SettingsButton btnName = {SETTINGS_NAMES.WIFI_SETTINGS}        iconClass = "bi bi-wifi"        onChange = {this.props.onChange}/>
                    <SettingsButton btnName = {SETTINGS_NAMES.AP_SETTINGS}          iconClass = "bi bi-wifi"        onChange = {this.props.onChange}/>
                    <SettingsButton btnName = {SETTINGS_NAMES.CLOCK_SETTINGS}       iconClass = "bi bi-clock"       onChange = {this.props.onChange}/>
                    <SettingsButton btnName = {SETTINGS_NAMES.GEO_SETTINGS}         iconClass = "bi bi-geo-alt"     onChange = {this.props.onChange}/>
                    <SettingsButton btnName = {SETTINGS_NAMES.UPDATE_FIRMWARE}      iconClass = "bi bi-code-slash"  onChange = {this.props.onChange}/>
                    <SettingsButton btnName = {SETTINGS_NAMES.DEVICES_SETTINGS}     iconClass = "bi bi-cpu"         onChange = {this.props.onChange}/>
                    <SettingsButton btnName = {SETTINGS_NAMES.HUB_NAME_SETTINGS}    iconClass = "bi bi-cpu"         onChange = {this.props.onChange}/>
                    <SettingsButton btnName = {SETTINGS_NAMES.DUMP_SETTINGS}        iconClass = "bi bi-file-earmark-arrow-down" onChange = {this.props.onChange}/>
                </tbody>
            </table>
        );
    }
}

class GeneralSettings extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.state = {  oldPassword : "",
                        newPassword : "",
                        confirmPassword : ""
        }
    }
    handleChange(e) {
        const value = e.target.value;
        const name = e.target.name;
        this.setState({
            [name]: value
        });
    }
    handleSubmit(e) {
        alert (this.state);
        e.preventDefault();
    }
    render() {
        if(this.props.permission == "write"){
            return <form onSubmit = {this.handleSubmit}>
                <div className="mb-3">
                    <label htmlFor="oldPassword" className="form-label">
                        Old password
                    </label>
                    <input type="password" className="form-control" name = "oldPassword" onChange = {this.handleChange}/>
                    <label htmlFor="newPassword" className="form-label">
                        New password
                    </label>
                    <input type="password" className="form-control" name = "newPassword" onChange = {this.handleChange}/>
                    <label htmlFor="confirmPassword" className="form-label">
                        Confirm password
                    </label>
                    <input type="password" className="form-control" name = "confirmPassword" onChange = {this.handleChange}/>
                </div>
                <CommonComponents.BACK_APPLY_BTNS disabled = {true} onChange = {this.props.onChange}/>
            </form>;
        }
        else {
            return <div className="container">
                <CommonComponents.AccessDeniedMessage />
                <CommonComponents.ReturnBackBtn onChange = {this.props.onChange} />
            </div>
        }
    }
}

class NetworkSettings extends React.Component { 
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {};
    }

    componentDidMount() {
        this.reload();
    }

    reload() {
        this.setState({
            applying : false,
            loading : true,
            error : false,
        })
        Hub.getNetworkSettings()
        .then(netData => {
            this.setState({
                ip : netData.netClient.ip,
                dhcp : netData.netConfig.dhcp === "1" ? true : false,
                dns : netData.netConfig.dns,
                subnet : netData.netClient.mask,
                gateway : netData.netClient.gateway,
                loading : false
            })
        })
        .catch(err => {
          console.error(err);
          this.setState({
              error : err + "",
              loading : false
          })
        })
    }

    handleChange(e) {
        let value = e.target.value;
        const name = e.target.name;
        if ( value === "true" || value === "false" ) {
            value = JSON.parse(value);
        }
        this.setState({
            [name]: value
        });
    }
    handleSubmit(e) {
        e.preventDefault();
        const netConfig = {
            dhcp : this.state.dhcp,
            ip : this.state.ip,
            subnet : this.state.subnet,
            gateway : this.state.gateway,
            dns : this.state.dns
        }
        this.setState({
            applying : true
        })
        Hub.setNetworkSettings(netConfig)
        .then(response => {
            this.props.onChange([CommonComponents.BTNS_RETURNS.BACK])
        })
        .catch(err => {
            this.setState({
                applying : false
            })
            console.error(err);
            alert(err + "");
        })
    }
    render() {
        if(this.props.permission == "write"){
            if (this.state.loading) {
                return <CommonComponents.LoadingField />
            } else if (this.state.error) {
                return <CommonComponents.ReloadErrorField onChange = {this.reload} error = {this.state.error}/>
            } else {
                const IPPattern = "((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
                return <form onSubmit = {this.handleSubmit}>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="dhcp" value = "true" checked = {this.state.dhcp} onChange = {this.handleChange}/>
                        <label className="form-check-label" htmlFor="dhcp">
                            Get ip address automatically
                        </label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="dhcp" value = "false" checked = {!this.state.dhcp} onChange = {this.handleChange}/>
                        <label className="form-check-label" htmlFor="dhcp">
                            Use the following ip address
                        </label>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="ip" className="form-label">IP</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            name="ip" 
                            value = {this.state.ip} 
                            onChange = {this.handleChange}
                            pattern = {IPPattern}
                            required = {!this.state.dhcp}
                            disabled = {this.state.dhcp}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="subnet" className="form-label">Subnet mask</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            name="subnet" 
                            value = {this.state.subnet} 
                            onChange = {this.handleChange}
                            pattern = {IPPattern}
                            required = {!this.state.dhcp}
                            disabled = {this.state.dhcp}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="gateway" className="form-label">Gateway</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            name="gateway" 
                            value = {this.state.gateway} 
                            onChange = {this.handleChange} 
                            pattern = {IPPattern}
                            required = {!this.state.dhcp}
                            disabled = {this.state.dhcp}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="dns" className="form-label">DNS:</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            name="dns" 
                            value = {this.state.dns} 
                            onChange = {this.handleChange} 
                            pattern = {IPPattern}
                            required = {!this.state.dhcp}
                            disabled = {this.state.dhcp}
                        />
                    </div>
                    <CommonComponents.BACK_APPLY_BTNS applying = {this.state.applying} onChange = {this.props.onChange}/>
                </form>;
            }
        }
        else{
            return <div className="container">
                <CommonComponents.AccessDeniedMessage />
                <CommonComponents.ReturnBackBtn onChange = {this.props.onChange} />
            </div>
        }
    }
}

class WiFi_Settings extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.reload = this.reload.bind(this);
        this.changeWifiPoint = this.changeWifiPoint.bind(this);
        this.state = {  
            applying : false,
            ssid : "",
            password : "",
            network : [],
            loading : true,
            error : false,
            networkLoading : true,
            avaliableWiFiDisabled : true,
            passwordRequired : true
        }
    }
    getSignalLevelIcon(RSSI) {
        RSSI = Number.parseInt(RSSI)
        if (RSSI >= -50) {
            return <i className="bi bi-reception-4 mx-2"></i>;
        }
        if (RSSI >= -65) {
            return <i className="bi bi-reception-3 mx-2"></i>;
        }
        if (RSSI >= -75) {
            return <i className="bi bi-reception-2 mx-2"></i>;
        }
        if (RSSI >= -85) {
            return <i className="bi bi-reception-1 mx-2"></i>;
        }
        if (RSSI >= -100) {
            return <i className="bi bi-reception-0 mx-2"></i>;
        }
    }
    getEncryptionIcon(encryption) {
        if (encryption == 1) {
            return <i className="bi bi-lock mx-2"></i>
        } else {
            return <i className="bi bi-unlock mx-2"></i>
        }
    }
    handleChange(e) {
        const value = e.target.value;
        const name = e.target.name;
        this.setState({
            [name]: value
        });
    }
    handleSubmit(e) {
        e.preventDefault();
        this.setState({
            applying : true
        })
        Hub.setWiFiSettings(encodeURIComponent(this.state.ssid),encodeURIComponent(this.state.password))
        .then(response => {
            this.props.onChange([CommonComponents.BTNS_RETURNS.BACK])
        })
        .catch(err => {
            console.error(err);
            this.setState({
                applying : false
            })
            alert(err+"");
        })
    }
    changeWifiPoint(e) {
        let ssid = e.target.name;
        let tempPassRequired = false;
        for (let point of this.state.network) {
            if (point.SSID === ssid) {
                tempPassRequired = point.encr == 1;
            }
        }
        this.setState({
            ssid : ssid,
            passwordRequired : tempPassRequired
        })
    }
    componentDidMount() {
        Hub.getWiFiSettings()
        .then(ssid => {
            this.setState({
                ssid : ssid,
                loading : false,
                password : ""
            })
            Hub.getAvaliableWiFiList()
            .then(network => {
                
                this.setState({
                    network : network,
                    networkLoading : false,
                    avaliableWiFiDisabled : false
                })
            })
        })
    }
    reload() {
        this.setState({
            loading : true,
            error : false,
            networkLoading : true,
            avaliableWiFiDisabled : true
        })
        Hub.getWiFiSettings()
        .then(ssid => {
            this.setState({
                ssid : ssid,
                loading : false,
                password : ""
            })
            Hub.getAvaliableWiFiList()
            .then(network => {
                this.setState({
                    network : network,
                    networkLoading : false,
                    avaliableWiFiDisabled : false
                })
            })
        })
    }
    render() {
        let disabledInput = false;
        if(this.props.permission != "write" || this.state.networkLoading) {
          disabledInput = true;
        }
        if (this.state.loading) {
            return <CommonComponents.LoadingField />;
        } else if (this.state.error) {
            return <CommonComponents.ReloadErrorField onChange = {this.reload} error = {this.props.error}/>
        } else {
            let i = 10400;
            let wifiList = [];
            if ( !this.state.networkLoading ) {
                
                wifiList = this.state.network.map(data => 
                    <li key = {i++}>
                        <a 
                        className="dropdown-item" 
                        href="#" 
                        name =  {data.SSID} 
                        onClick = {this.changeWifiPoint}>
                            {this.getEncryptionIcon(data.encr)}
                            &nbsp;
                            {this.getSignalLevelIcon(data.RSSI)}
                            &nbsp;
                            {data.SSID}
                        </a>
                    </li>  
                )
               
            }
            return <form onSubmit = {this.handleSubmit}>
                <div className="mb-3">
                    <div className="dropdown d-grid gap-2 my-2">
                        <button className="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false" disabled = {this.state.avaliableWiFiDisabled}> 
                            {
                                this.state.networkLoading && 
                                    <span>
                                    <div id = "loadingView" className = "spinner-border spinner-border-sm" role="status">
                                    </div>  
                                    &nbsp;
                                    Avaliable wifi loading
                                    </span>
                            }
                            {
                                this.state.networkLoading ||
                                    <span>Сhoose wi-fi from the list</span>
                            }
                        </button>
                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            {wifiList}
                        </ul>
                    </div>
                    <label htmlFor="ssid" className="form-label">
                        WiFi SSID
                    </label>
                    <input type="text" className="form-control" name = "ssid" value = {this.state.ssid} onChange = {this.handleChange} disabled/>
                    <label htmlFor="password" className="form-label">
                        Password
                    </label>
                    <input type="password" className="form-control" value = {this.state.password} name = "password" onChange = {this.handleChange} required = {this.state.passwordRequired} disabled={disabledInput}/>
                </div>
                <CommonComponents.BACK_APPLY_BTNS disabled={disabledInput} applying = {this.state.applying} onChange = {this.props.onChange}/>
            </form>;
        }
    }
}

class AP_Settings extends React.Component {
    render() {
        return <CommonComponents.ReturnBackBtn onChange = {this.props.onChange} />
    }
}

class ClockSettings extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.state = {};
    }
    componentDidMount() {
        this.reload();
    }

    reload() {
        this.setState({
            applying : false,
            loading : true,
            error : false,
        })
        Hub.getSystemTimeZone()
        .then(tz => {
            Hub.getShiftedTimeByUserTimeZone()
            .then(time => {
                let date = new Date(time);
                let dateInfo = this.getDateInfo(date);
                let tzToView = convertTimeZoneToViewFormat(tz);

                this.setState({
                    hubDate : date,
                    hubTz : tz,
                    timeToView : dateInfo.timeToView,
                    yyyymmdd : dateInfo.yyyymmdd,
                    tzToView : tzToView,
                    isHUB_Checked : true,
                    isPC_Checked : false,
                    loading : false
                });
            })
        })
        .catch(err => {
          throw(err);
        })
    }

    getDateInfo(date) {
        let hh = date.getHours();
        let mm = date.getMinutes();
        let yyyymmdd = date.YYYYMMDD();

        let timeToView = [
            (hh>9 ? '' : '0') + hh,
            (mm>9 ? ':' : ':0') + mm
        ].join('');
        return {
            yyyymmdd : yyyymmdd,
            timeToView : timeToView
        };
    }

    handleChange(e) {
        const value = e.target.value;
        const name = e.target.name;
        switch(name) {
            case ("timeRadio") : {
                if (value === "pc") {
                    let date = new Date();
                    let dateInfo = this.getDateInfo(date);
                    let tzToView = convertTimeZoneToViewFormat(date.getTimezoneOffset() * -1);

                    this.setState({
                        timeToView : dateInfo.timeToView,
                        yyyymmdd : dateInfo.yyyymmdd,
                        tzToView : tzToView,
                        isHUB_Checked : false,
                        isPC_Checked : true
                    })
                } else {
                    let date = this.state.hubDate;
                    let dateInfo = this.getDateInfo(date);
                    let tzToView = convertTimeZoneToViewFormat(this.state.hubTz);
                    this.setState({
                        timeToView : dateInfo.timeToView,
                        yyyymmdd : dateInfo.yyyymmdd,
                        tzToView : tzToView,
                        isHUB_Checked : true,
                        isPC_Checked : false
                    })
                }
                break;
            }
            case ("date") : {
                this.setState({
                    yyyymmdd : value
                })
                break;
            }
            case ("time") : {
                this.setState({
                    timeToView : value
                })
                break;
            }
            case ("timeZone") : {
                this.setState({
                    tzToView : value
                })
                break;
            }
        }
    }
    handleSubmit(e) {
        e.preventDefault();
        let timeZoneData = this.state.tzToView.split(":");
        if (timeZoneData[0].match(/^\d{2}/)) {
            timeZoneData[0] = "+" + timeZoneData[0];
        }
        let dateString = this.state.yyyymmdd+
                        "T"+
                        this.state.timeToView+
                        timeZoneData[0]+":"+timeZoneData[1];
        let  date  = new Date(dateString);
        let dateTime = Math.trunc((date.getTime())/1000);

        let time1 = Number.parseInt(timeZoneData[0])
        let time2 = Number.parseInt(timeZoneData[1])
        let timeZone = 0;
        if (time1 < 0) {
            timeZone = time1*60 - time2;
        } else {
            timeZone = time1*60 + time2;   
        }
        Hub.setClockSettings(dateTime,timeZone)
        .then(response => {
            this.props.appUpdate("time");
            this.props.onChange([CommonComponents.BTNS_RETURNS.BACK])
        })
        .catch(err => {
            console.error(err);
            alert(err+"");
        })
    }
    
    render() {
        let disabledInput = false;
        if(this.props.permission != "write" || this.state.networkLoading) {
          disabledInput = true;
        }
        if (this.state.loading) {
            return <CommonComponents.LoadingField />
        } else if (this.state.error) {
            return <CommonComponents.ReloadErrorField onChange = {this.reload} error = {this.state.error}/>
        } else {
            return <form onSubmit = {this.handleSubmit}>

                <div className="form-check">
                    <input className="form-check-input" type="radio" name="timeRadio"  value = "hub" onChange = {this.handleChange} checked = {this.state.isHUB_Checked} disabled={disabledInput}/>
                    <label className="form-check-label" htmlFor="timeRadio">
                        Show time on hub
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" type="radio" name="timeRadio" value = "pc" onChange = {this.handleChange} checked = {this.state.isPC_Checked} disabled={disabledInput}/>
                    <label className="form-check-label" htmlFor="timeRadio">
                        Show time on computer
                    </label>
                </div>

                
                <div className="mb-3">
                    <label htmlFor="date" className="form-label">Date</label>
                    <input type="date" className="form-control" name="date" value = {this.state.yyyymmdd} onChange = {this.handleChange} required disabled={disabledInput}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="time" className="form-label">Time</label>
                    <input type="time" className="form-control" name="time" value = {this.state.timeToView} onChange = {this.handleChange} required disabled={disabledInput}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="timeZone" className="form-label">Time-zone</label>
                    <input type="text" className="form-control" name ="timeZone" required pattern = "[+-]?[\d]{2}:[\d]{2}" value = {this.state.tzToView} onChange = {this.handleChange} disabled={disabledInput}/>
                    <div className="form-text">Example -03:15</div>
                </div>
                <CommonComponents.BACK_APPLY_BTNS onChange = {this.props.onChange} disabled={disabledInput}/>
            </form>;
        }
    }
}

class GeoSettings extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.state = {  oldPassword : "",
                        newPassword : "",
                        confirmPassword : ""
        }
    }
    handleChange(e) {
        const value = e.target.value;
        const name = e.target.name;
        this.setState({
            [name]: value
        });
    }
    handleSubmit(e) {
        alert (this.state);
        e.preventDefault();
    }
    render() {
        return <form onSubmit = {this.handleSubmit}>
            <div className="input-group mb-3">
                <select name="state" className="form-select" onChange = {this.handleChange} disabled>
                    <option value = "null" >states loading...</option>
                </select>
            </div>
            <div className="input-group mb-3">
                <select name="city" className="form-select" onChange = {this.handleChange} disabled>
                    <option value = "null" >cities loading...</option>
                </select>
            </div>
            <div className="mb-3">
                <label htmlFor="latitude" className="form-label">Latitude</label>
                <input type="text" className="form-control" name="latitude" onChange = {this.handleChange}/>
            </div>
            <div className="mb-3">
                <label htmlFor="latitude" className="form-label">Longitude</label>
                <input type="text" className="form-control" name="longitude" onChange = {this.handleChange}/>
            </div>
            <CommonComponents.BACK_APPLY_BTNS disabled = {true} onChange = {this.props.onChange}/>
        </form>;
    }
}

class UpdateFirmware extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.state = {  firmware : null,
                        updDev : "Mega",
                        applying : false
        }
    }
    handleChange(e) {
        const value = e.target.value;
        const name = e.target.name;
        switch(name) {
            case ("firmware") : {
                this.setState({
                    firmware : e.target.files[0]
                });
                break;
            }
            case ("updDev") : {
                this.setState({
                    updDev : e.target.value
                });
            }
        }
    }
    handleSubmit(e) {
        e.preventDefault();
        this.setState({
            applying : true
        })
        Hub.updateFirmware(this.state.firmware,this.state.updDev)
        .then(response => {
            this.props.onChange([CommonComponents.BTNS_RETURNS.BACK])
        })
        .catch(err => {
            this.setState({
                applying : false
            })
            console.error(err);
            alert("uploading firmaware error (detail in console)");
        })
    }
    render() {
        if(this.props.permission == "write"){
            return <form onSubmit = {this.handleSubmit}>
                <div className="form-group mb-3">
                    <select name = 'updDev' className="form-select">
                        <option value = "Mega">Mega</option>
                    </select>
                    <div className="form-text">Select the device</div>
                </div>
                <div className="input-group mb-3">
                    <input type="file" className="form-control" name = "firmware" onChange = {this.handleChange} accept=".hex" required />
                    <label className="input-group-text" htmlFor="firmware">Upload</label>
                </div>
                <span>Applying may take up to 2 minutes.</span>
                <CommonComponents.BACK_APPLY_BTNS applying = {this.state.applying} onChange = {this.props.onChange}/>
            </form>;
        }
        else{
            return <div className="container">
                <CommonComponents.AccessDeniedMessage />
                <CommonComponents.ReturnBackBtn onChange = {this.props.onChange} />
            </div>
        }
    }
}

class ChangeHubName extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.nameChange = this.nameChange.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {   
            loading : true,
            error : false
        }
    }
    componentDidMount() {
        this.reload();
    }
    reload() {
        Hub.getHubName()
        .then(hubName => {
            this.setState({
                hubName : hubName,
                loading : false,
                error : false,
                applying : false
            })
        })
        .catch(error => {
            this.setState({
                error : "Error on loading hub name (details in the console)",
                loading : false
            })
            throw(error);
        })
    }
    nameChange(e) {
        this.setState({hubName : e.target.value});
    }
    handleSubmit(e) {
        e.preventDefault();
        this.setState({
            applying : true
        })
        Hub.setHubName(this.state.hubName)
        .then(res => {
            this.props.appUpdate("name");
            this.props.onChange([CommonComponents.BTNS_RETURNS.BACK]);
        })
        .catch(error => {
            this.setState({
                applying : false
            })
            alert("Error on applying hubName (details in the console)");
            throw(error);
        })
    }
    render() {
        if(this.props.permission == "write"){
            if (this.state.loading) {
                return <CommonComponents.LoadingField />;
            } else if (this.state.error) {
                return <CommonComponents.ReloadErrorField onChange = {this.reload} error = {this.state.error}/>
            } else {
                return <form onSubmit = {this.handleSubmit}>
                        <div className="mb-3">
                        <label htmlFor="dev-name-input" className="form-label">
                            Device name
                        </label>
                        <input type="text" className="form-control" id="dev-name-input" defaultValue = {this.state.hubName} required pattern="[a-zA-Z0-9_-]{1,16}"  onChange = {this.nameChange}/>
                        <div className="form-text">
                            Only letters , numbers and _ or - (from 1 to 16 characters)
                        </div>
                        <hr/>
                    </div>
                    <CommonComponents.BACK_APPLY_BTNS applying = {this.state.applying} onChange = {this.props.onChange}/>
                </form>;
            }
        }
        else{
            return <div className="container">
                <CommonComponents.AccessDeniedMessage /> 
                <CommonComponents.ReturnBackBtn onChange = {this.props.onChange} />
            </div>
        }
    }
}

class DumpDownload extends React.Component {
    constructor(props) {
        super(props);
        this.onDownload = this.onDownload.bind(this);
        this.onUpload = this.onUpload.bind(this);
        this.onFileSelected = this.onFileSelected.bind(this);    
        this.state = {
            isDownloading: false,  
            isUploading: false,
            file: null,
        }
    }
    onDownload(e) {
        try{
            this.setState({isDownloading: true});
            Promise.all([
                new Promise(function(resolve, reject) {
                    Hub.downloadFile("gs.dat")
                    .then(response => {
                        resolve(response);
                    })
                    .catch(error => {
                        reject(error);
                    })
                }),
                new Promise(function(resolve, reject) {
                    Hub.downloadFile("GA_ViewData.json")
                    .then(response => {
                        resolve(response);
                    })
                    .catch((error) => {
                        reject(error);
                    })
                })
            ])
            .then(results => {
                let obj = {};
                obj.datfile = results[0];
                obj.jsonfile = results[1];
                Hub.saveFile(JSON.stringify(obj),"dump.dump");
            })
            .catch(error => {
                console.log("error downloading dump:");
                console.log(error);
            })
            .finally(() => {
                this.setState({isDownloading: false});
            })
        }
        catch(error) {

        }
    }
    onUpload(e) {
        if(this.state.file){
            this.setState({isUploading: true});
            Hub.getFirmwareFileData(this.state.file, "Mega")
            .then(data => {
                const obj = JSON.parse(data);
                return Promise.all([
                    new Promise(function(resolve, reject) {
                        Hub.uploadFile(obj.datfile,"gs.dat")
                        .then(response => {
                            resolve(response);
                        })
                        .catch(error => {
                            reject(error);
                        })
                    }),
                    new Promise(function(resolve, reject) {
                        Hub.uploadFile(obj.jsonfile,"GA_ViewData.json")
                        .then(response => {
                            resolve(response);
                        })
                        .catch((error) => {
                            reject(error);
                        })
                    })  
                ]);
            })
            .catch(error => {
                alert("error uploading file");
                console.log(error);
            })
            .finally(() => {
                this.setState({isUploading: false});
            })
        } 
        else{
            alert("please select file");
        }
    }
    onFileSelected(e){
        this.setState({file: e.target.files[0]});
    }
    render() {
        let disabledInput = false;
        if(this.props.permission != "write") {
          disabledInput = true;
        }
        return <div>
            <button className="btn btn-primary" onClick={this.onDownload} disabled={this.state.isDownloading} type="button">
                Download dump
            </button> 
            <div className="input-group my-3">
                <input type="file" className="form-control" name = "dumpFile" onChange={this.onFileSelected} accept=".dump" required disabled={disabledInput}/>
                <label className="input-group-text" htmlFor="firmware">Upload</label>
            </div>
            <CommonComponents.ReturnBackBtn onChange = {this.props.onChange} />
            <button className="btn btn-primary" onClick={this.onUpload} disabled={this.state.isUploading} type="button" disabled={disabledInput}>
                Upload Dump
            </button> 
        </div>       
    }
}

class SettingsScreen extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.statesHistory = [];
        this.STATES = {
            MAIN_SCREEN : 0,
            GENERAL_SETTINGS : 1,
            NETWORK_SETTINGS : 2,
            WIFI_SETTINGS : 3,
            AP_SETTINGS : 4,
            CLOCK_SETTINGS : 5,
            GEO_SETTINGS : 6,
            UPDATE_FIRMWARE : 7,
            DEVICES_SETTINGS : 8,
            HUB_NAME_SETTINGS : 9,
            DUMP_SETTINGS : 10
        }
        this.state = {
            view : this.STATES.MAIN_SCREEN,
            header : "Settings:",
            //permissions: props.permissions,
        }
    }
    onChange(params) {
        switch (params[0]) {
            case (CommonComponents.BTNS_RETURNS.BACK) : {
                this.setState(this.statesHistory.pop());
                break;
            }
            case (SETTINGS_NAMES.GENERAL_SETTINGS) : {
                this.statesHistory.push(this.state);
                this.setState({header : `${SETTINGS_NAMES.GENERAL_SETTINGS}:`});
                this.setState({view : this.STATES.GENERAL_SETTINGS});
                break;
            }
            case (SETTINGS_NAMES.NETWORK_SETTINGS) : {
                this.statesHistory.push(this.state);
                this.setState({header : `${SETTINGS_NAMES.NETWORK_SETTINGS}:`});
                this.setState({view : this.STATES.NETWORK_SETTINGS});
                break;
            }
            case (SETTINGS_NAMES.WIFI_SETTINGS) : {
                this.statesHistory.push(this.state);
                this.setState({header : `${SETTINGS_NAMES.WIFI_SETTINGS}:`});
                this.setState({view : this.STATES.WIFI_SETTINGS});
                break;
            }
            case (SETTINGS_NAMES.AP_SETTINGS) : {
                this.statesHistory.push(this.state);
                this.setState({header : `${SETTINGS_NAMES.AP_SETTINGS}:`});
                this.setState({view : this.STATES.AP_SETTINGS});
                break;
            }
            case (SETTINGS_NAMES.CLOCK_SETTINGS) : {
                this.statesHistory.push(this.state);
                this.setState({header : `${SETTINGS_NAMES.CLOCK_SETTINGS}:`});
                this.setState({view : this.STATES.CLOCK_SETTINGS});
                break;
            }
            case (SETTINGS_NAMES.GEO_SETTINGS) : {
                this.statesHistory.push(this.state);
                this.setState({header : `${SETTINGS_NAMES.GEO_SETTINGS}:`});
                this.setState({view : this.STATES.GEO_SETTINGS});
                break;
            }
            case (SETTINGS_NAMES.UPDATE_FIRMWARE) : {
                this.statesHistory.push(this.state);
                this.setState({header : `${SETTINGS_NAMES.UPDATE_FIRMWARE}:`});
                this.setState({view : this.STATES.UPDATE_FIRMWARE});
                break;
            }
            case (SETTINGS_NAMES.DEVICES_SETTINGS) : {
                this.statesHistory.push(this.state);
                this.setState({header : `${SETTINGS_NAMES.DEVICES_SETTINGS}:`});
                this.setState({view : this.STATES.DEVICES_SETTINGS});
                break;
            }
            case (SETTINGS_NAMES.HUB_NAME_SETTINGS) : {
                this.statesHistory.push(this.state);
                this.setState({header : `${SETTINGS_NAMES.HUB_NAME_SETTINGS}:`});
                this.setState({view : this.STATES.HUB_NAME_SETTINGS});
                break;
            }
            case (SETTINGS_NAMES.DUMP_SETTINGS) : {
                this.statesHistory.push(this.state);
                this.setState({header : `${SETTINGS_NAMES.DUMP_SETTINGS}:`});
                this.setState({view : this.STATES.DUMP_SETTINGS});
                break;
            }
        }
    }
    render() {
        switch(this.state.view) {
            case (this.STATES.MAIN_SCREEN) : {
                let footerText = `Version [${VersionInfo.GUI_VERSION}] from [${VersionInfo.GUI_DATE.toLocaleDateString()}]`;
                return  <div >
                            <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                            <SettingsButtonsList onChange = {this.onChange}/>

                            <footer className="page-footer font-small blue">
                                <div className="footer-copyright text-center py-3">
                                    {footerText}
                                </div>
                            </footer>

                        </div>;
            }
            case (this.STATES.GENERAL_SETTINGS) : {
                return  <div >
                            <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                            <GeneralSettings onChange = {this.onChange} permission={this.props.permissions.settings}/>
                        </div>;
            }
            case (this.STATES.NETWORK_SETTINGS) : {
                return  <div >
                            <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                            <NetworkSettings onChange = {this.onChange} permission={this.props.permissions.settings}/>
                        </div>;
            }
            case (this.STATES.WIFI_SETTINGS) : {
                return  <div >
                            <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                            <WiFi_Settings onChange = {this.onChange} permission={this.props.permissions.settings}/>
                        </div>;
            }
            case (this.STATES.AP_SETTINGS) : {
                return  <div >
                            <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                            <AP_Settings onChange = {this.onChange} permission={this.props.permissions.settings}/>
                        </div>;
            }
            case (this.STATES.CLOCK_SETTINGS) : {
                return  <div >
                            <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                            <ClockSettings appUpdate = {this.props.appUpdate} onChange = {this.onChange} permission={this.props.permissions.settings}/>
                        </div>;
            }
            case (this.STATES.GEO_SETTINGS) : {
                return  <div >
                            <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                            <GeoSettings onChange = {this.onChange} permission={this.props.permissions.settings}/>
                        </div>;
            }
            case (this.STATES.UPDATE_FIRMWARE) : {
                return  <div >
                            <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                            <UpdateFirmware onChange = {this.onChange} permission={this.props.permissions.settings}/>
                        </div>;
            }
            case (this.STATES.DEVICES_SETTINGS) : {
                if(this.props.permissions.device_control == "true"){
                    return <div >
                        <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                        <DevicesScreen showVersion={true} mode = {DEVICES_VIEW_MODES.SETTINGS} onChange = {this.onChange}/>
                    </div>;
                }
                else {
                    return <div>
                        <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                        <CommonComponents.AccessDeniedMessage /> 
                        <CommonComponents.ReturnBackBtn onChange = {this.onChange} />
                    </div>
                }
            }
            case (this.STATES.HUB_NAME_SETTINGS) : {
                return  <div >
                            <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                            <ChangeHubName appUpdate = {this.props.appUpdate}  onChange = {this.onChange} permission={this.props.permissions.settings}/>
                        </div>;
            }
            case (this.STATES.DUMP_SETTINGS) : {
                return  <div >
                            <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                            <DumpDownload appUpdate = {this.props.appUpdate}  onChange = {this.onChange} permission={this.props.permissions.settings}/>
                        </div>;
            }
        }
    }
}

export default SettingsScreen;