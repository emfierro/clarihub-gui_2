import { Mega,PowerStrip } from "./DevicesData";
import {Script} from './ScriptActionsData';
import MemoryMap from './intel-hex'

const IS_TEST = true;
const HUB_ADDR = "http://178.158.234.158:200";

export function sendGET(query) {
    return new Promise((resolve,reject) => {
        if (IS_TEST) {
            query = HUB_ADDR + query;
        }
        fetch(query)
        .then(response => {
            if (response.status === 200) {
                resolve(response);
            } else {
                reject(`Request failed [${query}]. Error code : [${response.status}]`);
            }
        })
        .catch(error => {
            reject(`Request failed [${query}]. Error : [${error}]`);
        })
    })
}
export function sendPOST(query,body) {
    return new Promise((resolve,reject) => {
        if (IS_TEST) {
            query = HUB_ADDR + query;
        }
        fetch(query,{
            method : "POST",
            body : body
        })
        .then(response => {
            if (response.status === 200) {
                resolve(response);
            } else {
                reject(`Request failed [${query}]. Error code : [${response.status}]`);
            }
        })
        .catch(error => {
            reject(`Request failed [${query}]. Error : [${error}]`);
        })
    })
}

export function uploadFile(file,filePath) {
    return new Promise((resolve,reject) => {
        let formData = new FormData();
        formData.append("file",file);
        let query = "/rdrok/updMP/"+filePath;
        sendPOST(query,formData)
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error);
        })
    })
}

export function downloadFile(filePath) {
    return new Promise((resolve,reject) => {
        sendGET("/"+filePath)
        .then(response => {
            response.text().then(fileData =>{
                resolve(fileData);
            })
        })
        .catch(error => {
            reject(error);
        })
    })
}

export function saveFile(fileData, fileName) {
    const file = new File([fileData], fileName, {
        type: 'text/plain',
    })

    const link = document.createElement('a')
    const url = URL.createObjectURL(file)
    
    link.href = url
    link.download = file.name
    document.body.appendChild(link)
    link.click()
    
    document.body.removeChild(link)
    window.URL.revokeObjectURL(url)
}

export function getSystemTimeZone() {
    return new Promise((resolve,reject) => {
        sendGET('/sysinfo/?timezone')
        .then(response => {
            response.json().then(json => {
                resolve(json.timezone)
            })
        })
        .catch(error => {
            reject(error)
        })
    })
}

export function getSystemTime() {
    return new Promise((resolve,reject) => {
        sendGET('/sysinfo/?hub_time')
        .then(response => {
            response.json().then(json => {
                resolve(json.hub_time)
            })
        })
        .catch(error => {
            reject(error)
        })
    })
}

export function getShiftedTimeByUserTimeZone() {
    return new Promise((resolve,reject) => {
        getSystemTime()
        .then(hub_time => {
            let timeToView = (Number.parseInt(hub_time) ) * 1000;
            resolve(timeToView);
        })
        .catch(error => {
            reject(error)
        })
    })
}

export function getScriptActionsList() {
    return new Promise((resolve, reject) => {
        sendGET('/getScripts/')
	    .then(response => {
            let scriptsNamesList = [];
            response.json().then(data => {
                if (data.scripts) {
                    for (let i = 0; i < data.scripts.length; i++) {
                        scriptsNamesList.push(data.scripts[i].replace(/^.*[\\\/]/, '').split('.')[0]);
                    }
                   
                }
                scriptsNamesList.sort();
                resolve(scriptsNamesList);
            })
        })
        .catch(error => {
            reject(error);
        })
    })
}

export function getScriptAction(actionName) {
    return new Promise((resolve, reject) => {
        sendGET("/scs/"+actionName+".wws")
        .then(response => {
            response.text().then(actionData => {
                if (!actionData) {
                    reject("404 error (scriptActionNotLoaded)");
                } else {
                    resolve(new Script(actionName,actionData));
                }
            })
        })
        .catch(err => {
            reject(err);
        })
    })
}

export function setScriptActionData(action) {
    return new Promise((resolve,reject) => {
        let data = 	action.execution+
                action.vars+
                String.fromCharCode(7)+
                action.code;
        let file =  new File([data], "file", {
                    type: "text/plain",
            });
        uploadFile(file,'scs/'+action.name+'.wws')
        .then(response => {
            resolve(response);
        })
        .catch(err => {
            reject(err);
        })
    })
}

export function renameSciptAction(oldName,newName) {
    return new Promise((resolve,reject) => {
        let query = "/rnSc/";
        let body = "rename:/scs/"+oldName+".wws:/scs/"+newName+".wws::";
        sendPOST(query,body)
        .then(response => {
            resolve(response);
        })
        .catch(err => {
            reject(err);
        })
    })
}

export function runScriptAction(action) {
    return new Promise((resolve,reject) => {
        sendGET('/startScript/'+action.name)
        .then(response => {
            resolve(response);
        })
        .catch(err => {
            reject(err);
        })
    })
}

export function removeScriptAction(action) {
    return new Promise((resolve,reject) => {
        sendGET('/rm/scs/'+action.name+'.wws')
        .then(response => {
            resolve(response);
        })
        .catch(err => {
            reject(err);
        })
    })
}

export function getDevices() {
    return new Promise((resolve, reject) => {
        sendGET('/getDevices/')
        .then(response => {
            response.text().then(data => {
                resolve(data)
            })
        })
        .catch(error => {
            reject(error);
        })
    });
}

export function getDevicesColors() { 
    return new Promise((resolve,reject) => {
        downloadFile("DevicesColors.json")
        .then(file => {
            resolve(file);
        })
        .catch(error => {
            reject(error);
        })
    })
}

export function addNewGlobalDevice(id,type) {
    return new Promise((resolve,reject) => {
        if (type === "Mega") {
            addNewMega(id)
            .then(response => {
                response.json().then(json => {
                    if (json.result === "OK") {
                        resolve(true);
                    } else if (json.result === "error1") {
                        reject("Mega limit exceeded ("+json.hubMaxMEGAS+")")
                    } else {
                        reject("Unknown error on adding new device");
                    }
                })
                .catch(err => {
                    reject (err);
                })
            })
            .catch(err => {
                reject (err);
            })
        } else if (type === "PowerStrip") {
            addNewPowerStrip(id)
            .then(response => {
                response.json().then(json => {
                    if (json.result === "OK") {
                        resolve(true);
                    } else if (json.result === "error2") {
                        reject("Power Strips limit exceeded ("+json.hubMaxSPS+")")
                    } else {
                        reject("Unknown error on adding new device");
                    }
                })
                .catch(err => {
                    reject (err);
                })
            })
            .catch(err => {
                reject (err);
            })
        } else {
            reject("Unknown device type")
        }
    })
}

function addNewMega(id) {
    return new Promise((resolve,reject) => {
        sendGET('/addMega/'+id)
        .then(response => {
            resolve(response);
        })
        .catch(err => {
            reject (err);
        })
    });
}

function addNewPowerStrip(id) {
    return new Promise((resolve,reject) => {
        sendGET('/addSPS/'+id)
        .then(response => {
            resolve(response);
        })
        .catch(err => {
            reject (err);
        })
    });
}

export function setDevicesColors(devicesMap) {
    return new Promise((resolve,reject) => {
        let data = {
            DevicesColors : []
        };
        for (let [id,dev] of devicesMap) {
            if (dev instanceof Mega || dev instanceof PowerStrip) {
                data.DevicesColors.push({
                    deviceID : id,
                    deviceColor : dev.color
                });
            }
        }
        let fileData = JSON.stringify(data);
        let file = new File([fileData],"DevicesColors", {
            type: "text/plain"
        });
        uploadFile(file,"DevicesColors.json")
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error);
        })
    })
}

export function getHubName() {
    return new Promise((resolve,reject) => {
        downloadFile("hubName.txt")
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error);
        })
    })
}

export function setHubName(name) {
    return new Promise((resolve,reject) => {
        const file = new File([name], "ga", {
                            type: "text/plain",
                    });
        uploadFile(file,"hubName.txt")
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error);
        })
    });
}

export function getDevicesNames() {
    return new Promise((resolve,reject) => {
        downloadFile("names.txt")
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error);
        })
    })
}

export function setDevicesNames(devicesMap) {
    return new Promise((resolve,reject) => {
        
        var data = "";
        for (let [k,v] of devicesMap) {
            data += k + " = " + v.specifedName + "\r\n";
        }
        const file = new File([data], "ga", {
                            type: "text/plain",
                    });
        uploadFile(file,"names.txt")
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error);
        })
    });
}

export function setGraphicalActions(graphicalActionsMap) {
    return new Promise((resolve,reject) => {
        
        var data = "";
        for (let [id,ga] of graphicalActionsMap) {
            try {
                data += ga.createHexContext() + "\r\n";
            } catch (e) {
                if (ga.hexContext) {
                    data += ga.hexContext + "\r\n";
                }
            }
        }
        const file = new File([data], "ga", {
                            type: "text/plain",
                    });
        uploadFile(file,"gs.dat")
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error);
        })
    });
}

export function getGraphicalActions() {
    return new Promise((resolve,reject) => {
        downloadFile("gs.dat")
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error);
        })
    });
}


export function getGraphicalActionsData() {
    return new Promise((resolve,reject) => {
        downloadFile("GA_ViewData.json")
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error);
        })
    });
}


export function setGraphicalActionsData(graphicalActionsMap) {
    return new Promise((resolve,reject) => {
        let data = {
            gaViewData : []
        };
        for (let [id,ga] of graphicalActionsMap) {
            data.gaViewData.push({
                gaID : id,
                gaColor : ga.color,
                gaName : ga.name,
                gaImgSrc :  ga.imgSrc
            });
        }
        const gaData = JSON.stringify(data);
        const file = new File([gaData],"gaViewData", {
            type: "text/plain"
        });
        uploadFile(file,"GA_ViewData.json")
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error);
        })
    });
}


export function getSchedules() {
    return new Promise((resolve,reject) => {
        downloadFile("schedules.txt")
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error);
        })
        
    })
}

export function getSchedulesNames() {
    return new Promise((resolve,reject) => {
        downloadFile("schNames.txt")
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error);
        })
    })
}

export function setSchedules(schedulesMap) {
    return new Promise((resolve,reject) => {
        var data = "";
        for (let [k,v] of schedulesMap) {
            data += v.getHex() + "\n";
        }
        const file = new File([data], "sch", {
                            type: "text/plain",
                    });
        uploadFile(file,"schedules.txt")
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error);
        })
    });
}

export function setSchedulesNames(schedulesMap) {
    return new Promise((resolve,reject) => {
        var data = "";
        for (let [k,v] of schedulesMap) {
            data += k + " = " + v.schName + "\n";
        }
        const file = new File([data], "sch", {
                            type: "text/plain",
                    });
        uploadFile(file,"schNames.txt")
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error);
        })
    });
}

export function setGeneralSettings() {

}

export function setNetworkSettings(netConfig) {
    // netconfig = {
    //    dhcp : true | false,
    //    ip : ip.
    //    subnet : subnet,
    //    gateway : gateway,
    //    dns : dns
    //}
    return new Promise((resolve, reject) => {
        let body = `||dhcp|${netConfig.dhcp ? 1 : 0}|ip|${netConfig.ip}|gateway|${netConfig.gateway}|subnet|${netConfig.subnet}|dns|${netConfig.dns}||`;
        sendPOST('/setnetCFG/',body)
        .then(response => {
            resolve(response)
        })
        .catch(err => {
            reject(err);
        })
    })
}

export function getNetworkSettings() {
    return new Promise((resolve,reject) => {
        sendGET('/sysinfo/?netcfg,netclient')
        .then(response => {
            response.json().then(json => {
                resolve(json)
            })
        })
        .catch(error => {
            reject(error)
        })
    })
}

export function getAvaliableWiFiList() {
    return new Promise((resolve,reject) => {
        sendGET('/sysinfo/?awail_APs')
        .then(response => {
            response.json().then(json => {
                resolve(json.networks)
            })
        })
        .catch(err => {
            reject(err);
        })
    })
}

export function getWiFiSettings() {
    return new Promise((resolve,reject) => {
        sendGET('/sysinfo/?getExtSSID')
        .then(response => {
            response.json().then(json => {
                resolve(json.externalSSID);
            })
        })
        .catch(err => {
            reject(err);
        })
    })
}

export function setWiFiSettings(ssid,password) {
    return new Promise((resolve,reject) => {
        sendGET('/setWifiLogin/'+ssid)
        .then(response => {
            sendGET('/setWifiPass/'+password)
            .then(response => {
                resolve(true);
            })
            .catch(error => {
                reject(error);
            })
        })
        .catch(error => {
            reject(error);
        })
    });
}

export function getApSettings() {

}

export function setApSettings() {
    
}

export function setClockSettings(date, timeZone) {
    return new Promise((resolve, reject) => {
        let bodyData =  ":nt:"+date+":"+timeZone+"::";
        sendPOST("/setTime/",bodyData)
        .then(response => {
            response.json().then(json => {
                if (json.result !== "OK") {
                    switch (json.result) {
                        case ("badcm") : {
                            reject ("error : not nt at start request");
                            break;
                        }
                        case ("badtime") : {
                            reject ("error : time year must be at [2000-2060]");
                            break;
                        }
                        case ("badtz") : {
                            reject ("time zone error");
                            break;
                        }
                        default : {
                            reject ("unknown error");
                            break;
                        }
                    }
                } else {
                    resolve(true);
                }
            })
        })
        .catch(err => {
            reject(err)
        });
    })
}

export function getGeoSettings() {

}

export function setGeoSettings() {
    
}

export function getUserPermissions() {
    return new Promise((resolve, reject) => {
        sendGET("/sysinfo/?user_permissions").then(response =>
            {       
                response.json().then(json => {
                    resolve(json.user_permissions);
                })
                .catch(err => {
                    console.log(err);
                    reject(err);
                })

            }
        )
    })
}

export function getDeviceVersion(device) {
    return new Promise((resolve, reject) => {
        sendGET("/getDevInfo/" + device).then(response =>
            {
                response.json().then(json => {
                    console.log(json);
                    if(typeof json.result === "string")
                        resolve(json.result);
                    else
                        reject(new Error("wrong result type"));
                })
                .catch(err => {
                    console.log(err);
                    reject(err);
                })
            }
        )
    })
}

// ------------------------------------------------
//                  firmvare
// ------------------------------------------------

function getFirmwareSize(firmware_len) {
	var temp = (firmware_len).toString(16);
	var res = ['0','0','0','0','0','0','0','0'];
	for (var i = 7, tempIdx = temp.length-1; tempIdx >= 0; i--, tempIdx--) {
		res[i] = temp[tempIdx];
	}
	return res.join('').toUpperCase();
}

function convertFirmwareToHexString(firmware) {
	var res = '';
	for (var i = 0; i < firmware.length; i++) {
		var temp = firmware[i].toString(16);
		if (temp.length < 2) temp = '0'+temp;
		res += temp;
	}
	return res.toUpperCase();
}

function hexFirmwareParse(fileData) {
	let memMap = MemoryMap.fromHex(fileData);
	var resultDataArrays = [];
	var lastAddr = 0;
	var lastBlockSize = 0;
	for (let [address, dataBlock] of memMap) {
		if (lastBlockSize) {
			var length = address - (lastAddr+lastBlockSize);
			var emptyBlock = new Uint8Array(length);
			resultDataArrays.push(emptyBlock);
		} else {
			if (address) {
				var length = address;
				var emptyBlock = new Uint8Array(length);
				resultDataArrays.push(emptyBlock);
			}
		}
		lastAddr = address;
		lastBlockSize = dataBlock.length;
		resultDataArrays.push(dataBlock);
	}
	return resultDataArrays;
}

export function getFirmwareFileData(file,updDev) {
	return 	new Promise((resolve, reject) => {
        let fileReader = new FileReader();
        if (updDev === "Mega") {
            fileReader.readAsText(file);
        } else {
            fileReader.readAsArrayBuffer(file);
        }
        fileReader.onload = function() {
            if (updDev === "Mega") {
                resolve(fileReader.result)
            } else {
                resolve(new Uint8Array(fileReader.result))
            }
        }
	});
}

export function updateFirmware(firmware,updDev) { // temp function
    return new Promise((resolve, reject) => {
        getFirmwareFileData(firmware,updDev)
        .then(fileData => {
            if (updDev === "Mega") {
                fileData = hexFirmwareParse(fileData);
                var megaID = '1E9801';
                var crc = '00000000';
                var data =  megaID + crc + getFirmwareSize(fileData[0].length);
                fileData = [data + convertFirmwareToHexString(fileData[0])];
                firmware = new File(fileData, "firmware.hex", {
                        type: "text/plain",
                });
                var form_data = new FormData()
                form_data.append('file', firmware)
                uploadFile(firmware,"flashm.hex")
                .then(response => {
                    sendGET("/flashAVR/flashm.hex")
                    .then(useFirmwareResponse => {
                        useFirmwareResponse.json().then(jsonRes => {
                            if (jsonRes.result == "0") {
                                resolve (true);
                            } else if (jsonRes.result == "1") {
                                reject ("ferror : bad filename");
                            } else if (jsonRes.result == "2") {
                                reject ("ferror :  can't open the file");
                            } else if (jsonRes.result == "3") {
                                reject ("ferror :  file is too short");
                            } else if (jsonRes.result == "4") {
                                reject ("ferror :   chip signature mismatch");
                            } else if (jsonRes.result == "5") {
                                reject ("ferror :  flash file is too short or corrupted");
                            }
                        })
                    })
                })
                .catch(err => {
                    reject(err);
                })
            } else {
                reject ("Device ["+updDev+"] cannot be flashed!");
            } 
        })
        .catch(err => {
            reject(err);
        })
    });
}

function getStelPowerStripDeviceErrorDescription(errorCode) {
    switch (errorCode) {
        case ('sl_TO') : {
            return  'slave timeout (not answer from PowerStrip)'
        }
        case ('ID_nf') : {
            return  'ID is not found'
        }
        case ('s_err') : {
            return  'syntax error'
        }
        case ('el_or') : {
            return  'element out of range'
        }
        case ('pa_or') : {
            return  'paramenter out of range'
        }
        default : {
            return  'unknown error'
        }
    }
}

export function onPowerStripDevice(device) { // ### outlet/out12v/out5v/solenoid
    return new Promise ((resolve, reject) => {
        let body = '::set:powerStrip:'+device.parent.id+':'+device.type+':E:'+device.arrayIndex+':1::';
        sendPOST('/stel/',body)
        .then(response => {
            response.json().then(json => {
                if (json.result === "ok") {
                    resolve(true);
                } else {
                    reject ( getStelPowerStripDeviceErrorDescription(json.result) );
                }
            })
        })
        .catch(err => {
            reject(err);
        })
    })
}
export function onMegaSensor(device) {
    return new Promise ((resolve, reject) => {
        sendGET(`/enSens/${device.id}`)
        .then(response => {
            resolve(response);
        })
        .catch(err => {
            reject(err);
        })
    })
}
export function offMegaSensor(device) {
    return new Promise ((resolve, reject) => {
        sendGET(`/disSens/${device.id}`)
        .then(response => {
            resolve(response);
        })
        .catch(err => {
            reject(err);
        })
    })
}

export function offPowerStripDevice(device) { // ### outlet/out12v/out5v/solenoid
    return new Promise ((resolve, reject) => {
        let body = '::set:powerStrip:'+device.parent.id+':'+device.type+':E:'+device.arrayIndex+':0::';
        sendPOST('/stel/',body)
        .then(response => {
            response.json().then(json => {
                if (json.result === "ok") {
                    resolve(true);
                } else {
                    reject ( getStelPowerStripDeviceErrorDescription(json.result) );
                }
            })
        })
        .catch(err => {
            reject(err);
        })
    })
}

export function setPowerStripPower(ps,value) {
    return new Promise ((resolve, reject) => {
        let body = '::set:powerStrip:'+ps.parent.id+':'+ps.type+':V:'+ps.arrayIndex+':'+value+'::';
        sendPOST('/stel/',body)
        .then(response => {
            response.json().then(json => {
                if (json.result === "ok") {
                    resolve(true);
                } else {
                    reject ( getStelPowerStripDeviceErrorDescription(json.result) );
                }
            })
        })
        .catch(err => {
            reject(err);
        })
    })
}

export function removeGlovalDevice(deviceId) { // mega or ps
    return new Promise ((resolve, reject) => {
        sendGET('/dropDevice/'+deviceId)
        .then(response => {
            resolve(response)
        })
        .catch(err => {
            reject(err);
        })
    })
}

export function getReportLog(year,month) {
    return new Promise ((resolve, reject) => {
        month=parseInt(month);
        if(isNaN(month)){month=1;}
        year=parseInt(year);
        if(isNaN(year)){year=2020;}

        month = month < 10 ? '0'+month : month;
        let localStorageKey = `${year}${month}log`;
        let log = window.localStorage.getItem(localStorageKey);

            
        if (log&&false) {//do not cache
            let jsonLog = getJsonReportLog(log);
            let lastLogNoteDate = jsonLog[jsonLog.length-1].timestamp;
            getSystemTime()
            .then(userTime => {
                let timeDifferense = userTime - lastLogNoteDate;
                if (timeDifferense > 30*60) {
                    sendGET('/SD/sdump/'+year+month+'.log')
                    .then(response => {
                        response.text().then(data => {
                            try{
                                if(data.length<3e6){
                                    window.localStorage.setItem(localStorageKey,data);
                                } 
                            }catch(e){
                                console.error(e);
                            } 
                            let jsonData = getJsonReportLog(data);
                            resolve(jsonData)
                        })
                        .catch(err => {
                            reject(err);
                        })
                        
                    })
                    .catch(err => {
                        reject(err);
                    })
                } else {
                    resolve(jsonLog);
                }
            })
        } else {
            console.log ( "loading date: "+ '/SD/sdump/'+year+month+'.log')
            sendGET('/SD/sdump/'+year+month+'.log')
            .then(response => {
                response.text().then(data => { 
                    try{
                        if(data.length<3e6){
                            window.localStorage.setItem(localStorageKey,data);
                        }

                    }catch(e){
                        console.error(e);
                    } 
                    let jsonData = getJsonReportLog(data);
                    resolve(jsonData)
                })
                .catch(err => {
                    reject(err);
                })
                
            })
            .catch(err => {
                reject(err);
            })
        }
    })
}

function getJsonReportLog(log) {
    let counter = 0;
    let temp = "";
    let res = "[";
    let isOpen = false;
    let isClose = false;
    for (let i = 0; i < log.length; i++) {
        if (log[i] === '{') {
            counter++;
            if (isClose) {
                res += ",";
            }
            isClose = false;
            isOpen = true;
        }
        if (isOpen) {
            if (log[i] === '}') {
                counter--;
            }
            temp = temp + log[i];
            if(counter === 0) {
                isClose = true;
                res += temp;
                temp = "";
                isOpen = false;
            }
        }
    }
    res += "]";
    return JSON.parse(res);
}

/*
async function _updateFirmwareSettings() { // real function
	var updDev = document.getElementById('firmware_select').value;
	var firmware = document.getElementById('firmware_input').files[0];
	if (!firmware){
		alert("firmware is not selected");
		return;
	}
	var devType;
	var devUid = '000000000000';
	var flqueryBody;
	getFileData(firmware,updDev)
	.then(function(fileData){
			var isBytesData = true;
			if (updDev == "Mega") {
				devType = 1;
				fileData = hexFirmwareParse(fileData);
				firmware = new File(fileData, "firmware.hex", {
						type: "text/plain",
				});
			//	download(firmware);
				for (var [key,value] of devicesMap) {
					if (value instanceof Mega) {
						devUid = value.id;
					}
				}
			} else if (updDev == "hub") {
			//	download(firmware);
				devType = 0;
			} else if (updDev == "all_ps") {
				return;
			} else {
				// power strip
				devType = 2;
				devUid = updDev;
			}
		flqueryBody = [devType,devUid,firmware.size,crc16_BUYPASS(fileData,isBytesData)].join(";");
		fetch("/flquery/",{
			method: 'POST',
			body: flqueryBody
		})
		.then(function(response){
			if (response.status == 200) {
				response.json().then(function(jsonResp){
					if (jsonResp.result == "ready") {
						formData = new FormData();
						formData.append('file', firmware);
						fetch("/pflash/", {
							method: 'POST',
							body: formData
						})
						.then(function(response_pflash){
							if (response_pflash.status == 200) {
								response_pflash.json().then(function(jsonResp_pflash){
									if (jsonResp_pflash.result = "prfm") {
										allowUpdateDevices = false;
										var intervalID = setInterval(() => {
											fetch()
											.then(function(uploadResponse){
												if(uploadResponse.status == 200) {
													uploadResponse.json().then(function(uploadResult){
														if (uploadResult.result == "ready") {
															clearInterval(intervalID);
															allowUpdateDevices = true;
														}
													})
												}
											})
										},10000)
										
									} else {
										alert("error : "+jsonResp_pflash.result);
										return;
									}
								})
							} else {
								alert(response_pflash.status);
								return;
							}
						})
					} else {
						alert("error : "+jsonResp.result);
						return;
					}
				})
			} else {
				alert(response.status);
				return;
			}
		})
		.catch(function(err){
			alert(err);
			return;
		})
	})
}
*/

// ------------------------------------------------
//                  end firmvare
// ------------------------------------------------
