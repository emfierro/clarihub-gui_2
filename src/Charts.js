import React from 'react';
import * as Hub from './Network';
import { Line } from 'react-chartjs-2';
import * as DevicesData from './DevicesData';
import * as CommonComponents from './CommonComponents'
import CONSTANTS from './Constants';
import {getShiftedDateByUserTimeZone} from "./TimeOperations";
//import {ReloadBtn} from "./CommonComponents";//commented because is unused

class ChartHeader extends React.Component {
    render() {
        let label = ``;
        switch (this.props.device.type) {
            case (DevicesData.DEVICES_TXT_TYPES.MEGA_SENSOR_CHANNEL) : {
                label = `Channel # ${this.props.device.arrayIndex+1} at sensor : [${this.props.device.parent.specifedName}]`;
                break;
            }
            case (DevicesData.DEVICES_TXT_TYPES.OUTLET) : {
                label = `Outlet # ${this.props.device.arrayIndex+1} at Power Strip : [${this.props.device.parent.specifedName}]`;
                break;
            }
            case (DevicesData.DEVICES_TXT_TYPES.PWM_OUTPUT) : {
                label = `PWM output # ${this.props.device.arrayIndex+1} at Power Strip : [${this.props.device.parent.specifedName}]`;
                break;
            }
            case (DevicesData.DEVICES_TXT_TYPES.ANALOG_INPUT) : {
                label = `Analog input # ${this.props.device.arrayIndex+1} at Power Strip : [${this.props.device.parent.specifedName}]`;
                break;
            }
            case  (DevicesData.DEVICES_TXT_TYPES.SOLENOID) : {
                label = `Solenoid # ${this.props.device.arrayIndex+1} at Power Strip : [${this.props.device.parent.specifedName}]`;
                break;
            }
            
            default:{
                label = `[uncknown node] # ${this.props.device.arrayIndex+1} at Power Strip : [${this.props.device.parent.specifedName}]`;
                break; 
            }  
        }
        return <div  className="text-center mb-3 p-1 border bg-light row">
            <div className="col-2">
                <img width={CONSTANTS.LIST_ELEM_IMG_WIDTH} src={this.props.device.imgSrc} className="img-fluid" alt="..."/>
            </div>
            <div className="col-9 my-auto">
                <h5>{label}</h5>
            </div>
        </div>;
    }
}

class ChartDate extends React.Component {
    render() {
        console.log("class ChartDate render");
        let YYYYMMDD = (new Date()).YYYYMMDD();
//        let minData = YYYYMMDD.split('-');
//        let min = minData[0]+'-'+minData[1]+'-01';
        let max = YYYYMMDD;
        return <div className = "my-3">
          <h5>Chart date</h5>
          <input
              className = "form-control my-3"
              type = "date"
              value = { this.props.selectedDate }
            //  min = {min}
              max = {max}
              onChange = {this.props.onChange}
          />
        </div>
    }
}

class Chart extends React.Component {
    constructor(props) {
        super(props);
        this.getChartInfo = this.getChartInfo.bind(this);
    }

    getChartInfo() {
        switch (this.props.device.type) {
            case (DevicesData.DEVICES_TXT_TYPES.MEGA_SENSOR_CHANNEL) : {
                return this.getChannelChartInfo();
            }
            case (DevicesData.DEVICES_TXT_TYPES.OUTLET) : {
                return this.getOutletChartInfo();
            }
            case (DevicesData.DEVICES_TXT_TYPES.PWM_OUTPUT) : {
                return this.getPWMChartInfo();
            }
            case (DevicesData.DEVICES_TXT_TYPES.ANALOG_INPUT) : {
                return this.getAnalogInputChartInfo();
            }
            case  (DevicesData.DEVICES_TXT_TYPES.SOLENOID) : {
                return this.getSolenoidChartInfo();
            }
            
            default:{
                console.error("error at Charts.js  switch (this.props.device.type) reaches default");
                return 0;
            }  
        }
    }

    getChannelChartInfo() {
        let data = [];
        let labels = [];

        let chartLabel = this.props.device.description;

        let log = this.props.logObject.log;

        for(let i = 0; i < log.length; i++) {
            data.push(log[i].device.value);
            let HHmm = getShiftedDateByUserTimeZone(log[i].timestamp).HHmm();
            labels.push(HHmm);
        }

        let chartData = {
            labels: labels,
            datasets: [{
                label: chartLabel,
                tension : 0.2,
                data: data
            }]
        }
        let chartOptions = {
            borderColor : "orange"
        }
        return {
            data : chartData,
            options : chartOptions
        }
    }

    getOutletChartInfo() {
        let onOffDataArr = [];
        let currentSensorDataArr = [];
        let errorLevelDaraArr = [];

        let labels = [];

        let log = this.props.logObject.log;

        for(let i = 0; i < log.length; i++) {
            currentSensorDataArr.push(log[i].device.currentSensor);
            if (log[i].device.isEnable === 1) {
                onOffDataArr.push(1);
            } else {
                onOffDataArr.push(0);
            }
            errorLevelDaraArr.push(log[i].device.errorLevel);
            let HHmm = getShiftedDateByUserTimeZone(log[i].timestamp).HHmm();
            labels.push(HHmm);
        }

        let onOffData = {
            label : "enabled / disabled",
            data : onOffDataArr,
            tension : 0.2
        }
        let currentSensorData = {
            label : "currentSensor",
            data : currentSensorDataArr,
            tension : 0.2
        }
        let errorLevelData = {
            label : "errorLevel",
            data : errorLevelDaraArr,
            tension : 0.2
        }
        let outletData = {
            labels : labels,
            datasets : [onOffData,currentSensorData,errorLevelData]
        }

        return {
            data : outletData,
            options : {}
        }
    }

    getPWMChartInfo() {
        let isEnableDataArr = [];
        let valueDataArr = [];

        let labels = [];

        let log = this.props.logObject.log;

        for(let i = 0; i < log.length; i++) {
            valueDataArr.push(log[i].device.value);
            if (log[i].device.isEnable === 1) {
                isEnableDataArr.push(1);
            } else {
                isEnableDataArr.push(0);
            }
            let HHmm = getShiftedDateByUserTimeZone(log[i].timestamp).HHmm();
            labels.push(HHmm);
        }

        let isEnableData = {
            label : "enabled / disabled",
            data : isEnableDataArr,
            tension : 0.2
        }
        let valueData = {
            label : "power",
            data : valueDataArr,
            tension : 0.2
        }

        let pwmData = {
            labels : labels,
            datasets : [isEnableData,valueData]
        }

        return {
            data : pwmData,
            options : {}
        }
    }

    getSolenoidChartInfo() {
        let isEnableDataArr = [];

        let labels = [];

        let log = this.props.logObject.log;

        for(let i = 0; i < log.length; i++) {
            if (log[i].device.isEnable === 1) {
                isEnableDataArr.push(1);
            } else {
                isEnableDataArr.push(0);
            }
            let HHmm = getShiftedDateByUserTimeZone(log[i].timestamp).HHmm();
            labels.push(HHmm);
        }

        let isEnableData = {
            label : "enabled / disabled",
            data : isEnableDataArr,
            tension : 0.2
        }


        let solenoidData = {
            labels : labels,
            datasets : [isEnableData]
        }

        return {
            data : solenoidData,
            options : {}
        }
    }

    getAnalogInputChartInfo() {
        let valueDataArr = [];

        let labels = [];

        let log = this.props.logObject.log;

        for(let i = 0; i < log.length; i++) {
            valueDataArr.push(log[i].device)
            let HHmm = getShiftedDateByUserTimeZone(log[i].timestamp).HHmm();
            labels.push(HHmm);
        }

        let valueData = {
            label : "value",
            data : valueDataArr,
            tension : 0.2
        }


        let analogInputData = {
            labels : labels,
            datasets : [valueData]
        }

        return {
            data : analogInputData,
            options : {}
        }
    }

    render() {
        if (this.props.logObject) {
            let chartInfo = this.getChartInfo();
            return <div className = "my-3">
                <Line data={chartInfo.data} options={chartInfo.options} />
            </div>
        } else {
            return <div className = "col-12">
                <div className="alert alert-danger  mx-auto text-center" role="alert" >
                    <h5>No data for this period</h5>
                </div>
            </div>
        }
    }
}


class ChartScreen extends React.Component {
    constructor(props){
      super(props);
      this.getDeviceLog = this.getDeviceLog.bind(this);
      this.onChange = this.onChange.bind(this);
      this.reload = this.reload.bind(this)
      this.state = {
        loading : true,
        error : false
      }
    }

    onChange(element) { 
        let value = element.target.value;
        
        console.log("date: "+value);
        var selectedDateElements=value.split('-');
        var loadedDateElements=this.state.selectedDate.split('-');

        if(selectedDateElements.length>2 && loadedDateElements.length>2 &&( selectedDateElements[0]!==loadedDateElements[0] || selectedDateElements[1]!==loadedDateElements[1] )){//if month or day missmatch
            this.setState({
                loading : true, 
            })
            this.componentDidMount([selectedDateElements[0],selectedDateElements[1]]);
            
            
        } 
        this.setState({ 
            selectedDate : value
        })
    }

    getDeviceLog(log) {
        let deviceLog = [];
        for (let logElem of log) {
            for (let logData of logElem.data) {
                logData.device = logData.device.toLowerCase();
                switch (logData.device) {
                    case (DevicesData.DEVICES_TXT_TYPES.MEGA) :
                        let mega = logData;
                        if (this.props.device.type === DevicesData.DEVICES_TXT_TYPES.MEGA_SENSOR_CHANNEL) {
                            let channel = this.props.device;
                            for (let snr of mega.sensors) {
                                if (snr.hwUid === channel.parent.id) {
                                    if (snr.channels[channel.arrayIndex]) {
                                        deviceLog.push({
                                            timestamp : logElem.timestamp,
                                            device : snr.channels[channel.arrayIndex],
                                            type : DevicesData.DEVICES_TXT_TYPES.MEGA_SENSOR_CHANNEL
                                        })
                                    }
                                }
                            }
                        }
                        break;
                        case (DevicesData.DEVICES_TXT_TYPES.SYSTEM) : break;
                    case (DevicesData.DEVICES_TXT_TYPES.POWER_STRIP) :
                        let ps = logData;
                        if (this.props.device.parent.id === ps.hwUid) {
                            switch (this.props.device.type) {
                                case (DevicesData.DEVICES_TXT_TYPES.OUTLET) :
                                    deviceLog.push({
                                        timestamp : logElem.timestamp,
                                        device : ps.outlets[this.props.device.arrayIndex],
                                        type : DevicesData.DEVICES_TXT_TYPES.OUTLET
                                    })
                                    break;
                                case (DevicesData.DEVICES_TXT_TYPES.PWM_OUTPUT) :
                                    deviceLog.push({
                                        timestamp : logElem.timestamp,
                                        device : ps.pwmOutput12V[this.props.device.arrayIndex],
                                        type : DevicesData.DEVICES_TXT_TYPES.OUTLET
                                    })
                                    break;
                                case (DevicesData.DEVICES_TXT_TYPES.SOLENOID) :
                                    deviceLog.push({
                                        timestamp : logElem.timestamp,
                                        device : ps.solenoid24vac[this.props.device.arrayIndex],
                                        type : DevicesData.DEVICES_TXT_TYPES.OUTLET
                                    })
                                    break;
                                case (DevicesData.DEVICES_TXT_TYPES.ANALOG_INPUT) :
                                    deviceLog.push({
                                        timestamp : logElem.timestamp,
                                        device : ps.analogInputs[this.props.device.arrayIndex],
                                        type : DevicesData.DEVICES_TXT_TYPES.OUTLET
                                    })
                                    break;
                                    
                                default:{
                                    console.error("error at Charts.js  switch (this.props.device.type) reaches default");
                                    break;
                                } 
                            }
                        }
                        break;
                        
            
                    default:{
                        console.error("error at Charts.js  switch (logData.device) reaches default with value: "+logData.device);
                        break;
                    }  
                }
            }
        }
        return deviceLog;
    }
    getMonthLogMap(log) {

        let monthMap = new Map();
        for (let logObject of log) {
            let currentYYYYMMDD = getShiftedDateByUserTimeZone(logObject.timestamp).YYYYMMDD();
            let mapElement = monthMap.get(currentYYYYMMDD);
            if (mapElement) {
                mapElement.log.push(logObject);
            } else {
                monthMap.set(currentYYYYMMDD, {
                    log : [logObject]
                })
            }
        } 
        return monthMap;
    }

    componentDidMount( requidedDate) {//requidedDate - array of [year, month]


        console.log("componentDidMount");

         var year,month; 
        if(!requidedDate){
            let date= new Date();
            year = date.getFullYear();
            month = date.getMonth() + 1;
        }else{
            year=requidedDate[0];
            month = requidedDate[1];

        }

        console.log("class ChartScreen load with date ["+year+" "+month+"]");
        
        Hub.getReportLog(year,month)

        .then(log => {
        //    console.log(log);
            let deviceLog = this.getDeviceLog(log);
            let monthMap = this.getMonthLogMap(deviceLog);
        //    console.log(deviceLog);
        //    console.log(monthMap)
          
            this.setState({
                selectedDate : ((this.state.selectedDate===undefined)? (new Date()).YYYYMMDD():this.state.selectedDate ),
                monthMap : monthMap,
                loading : false,
                error : false
            })

            console.log( "selected date: "+this.state.selectedDate);
        })
        .catch(error => {
            console.log(error);
            this.setState({
                error : error,
                loading : false
            })
        })
    }

    componentWillUnmount() {
    }

    reload() {
        let date = new Date();
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        
        console.log("class ChartScreen REload with date ["+year+" "+month+"]");
        Hub.getReportLog(year,month)
        .then(log => {
            let deviceLog = this.getDeviceLog(log);
            let monthMap = this.getMonthLogMap(deviceLog);
        //    console.log(deviceLog);
        //    console.log(monthMap)
            this.setState({
                selectedDate : (new Date()).YYYYMMDD(),
                monthMap : monthMap,
                loading : false,
                error : false
            })
        })
        .catch(error => {
            console.log(error);
            this.setState({
                error : error,
                loading : false
            })
        })
    }

    render() {
        console.log ("class ChartScreen render");
      if (this.state.loading) {
        return <CommonComponents.LoadingField />
      } else if (this.state.error) {
          return <>
                <CommonComponents.ReloadErrorField onChange = {this.reload} error = {this.state.error}/>
                <CommonComponents.ReturnBackBtn onChange = {this.props.onChange}/>
          </>
      } else {
        return <>
            <ChartDate selectedDate = {this.state.selectedDate} onChange = {this.onChange} />
            <div className = "my-3">
                <ChartHeader device = {this.props.device} />
               
                <Chart device = {this.props.device}  logObject = {this.state.monthMap.get(this.state.selectedDate)} />
            </div>
          <CommonComponents.ReturnBackBtn onChange = {this.props.onChange}/>
        </>
      }
    }
}
  
export default ChartScreen;