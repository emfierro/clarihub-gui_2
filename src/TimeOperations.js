 export function calcHH_MM_SS(seconds) {
	var hh = parseInt(seconds/3600);
	var mm = parseInt((seconds % 3600)/60);
	var ss = parseInt(seconds % (3600/60));
	return [hh,mm,ss];
}

export function getShiftedDateByUserTimeZone(hub_time) {
	return new Date((Number.parseInt(hub_time) + new Date().getTimezoneOffset()*60) * 1000);
}