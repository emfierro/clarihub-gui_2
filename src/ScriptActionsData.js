export class Script {
	constructor(name,fullCode) {
		let data = fullCode.split(String.fromCharCode(7));
		if (data.length != 2) {
			data = ['',''];
		}
		this.name = name;
		this.vars = data[0] ? data[0].substring(1,data[0].length) : '';
		this.code = data[1] ? data[1] : '';

		this.execution = data[0] ? data[0].charAt(0) : '-';
	}
}