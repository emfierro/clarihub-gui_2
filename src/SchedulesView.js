import React from 'react';
import * as CommonComponents from './CommonComponents';
import { getManualGraphicalActionsList, getConditionGraphicalActionsList } from './GraphicalActionsData';
import {
    Schedule,
    initSchedulesMap,
    SCHEDULES_TYPES,
    initSchedulesNamesMap,
    getNextScheduleId
} from './SchedulesData';
import * as Hub from './Network';
import {updateDevicesMap, updateDevicesNamesMap} from './DevicesData';
import * as GraphicalActionsData from './GraphicalActionsData';
import * as TimeZone from './TimeZoneConvertations';
import {calcHH_MM_SS} from './TimeOperations'

class SchedulesTable extends React.Component {
    render() {
        const tableStyle = {
            borderCollapse : "separate",
            borderSpacing:" 0px 10px"
        }
        return <table className ="table" style = {tableStyle}>
            <thead > 
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Type</th>
                    <th scope="col">Action</th>
                    <th scope="col">Active</th>
                </tr>
            </thead>
            <SchedulesTableElements schedulesMap = {this.props.schedulesMap} onChange = {this.props.onChange} />
        </table>;
    }
}

class SchedulesTableElements extends React.Component {
    render() {
        const schedulesArray = this.props.schedulesMap.toArray();
        const listItems = schedulesArray.map((data) => 
            <ScheduleTableElement key = {data.key} schedule = {data.value} onChange = {this.props.onChange}/>
        )
        return <tbody>
                {listItems}
            </tbody>;
    }
}

class ScheduleTableElement extends React.Component {
    render() {
        const imgClass = this.props.schedule.isActive ? 'bi bi-check' : 'bi bi-x';
        const trClass = this.props.schedule.isActive ? 'table-success' : 'table-danger';
        const trStyle = {
            cursor : "pointer"
        }

        return <tr className = {trClass} style = {trStyle} onClick = {() => this.props.onChange([this.props.schedule])}>
            <td>{this.props.schedule.schName}</td>
            <td><i className = {this.props.schedule.getIconClass()}></i> &nbsp; {this.props.schedule.type}</td>
            <td>{this.props.schedule.scriptData.scriptName}</td>
            <td><i className = {imgClass}></i></td>
        </tr>;
    }
}

class ScriptActionItem extends React.Component {
    render() {
        return  <option value = {this.props.value}>{this.props.value}</option>;
    }
}

class ScriptAndGraphicalActionsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value : this.props.value
        }
        this.onChange = this.onChange.bind(this);
        this.render = this.render.bind(this);
    }
    onChange(event) {
        const target = event.target;
        const value = target.value;
        this.setState({
            value : value
        });
        this.props.onChange(event);
    }
    render() {
        let scriptActions = this.props.scriptActions;
        let manualGraphicalActions = this.props.manualGraphicalActions;
        let conditionGraphicalActions = this.props.conditionGraphicalActions;
        let actionsList = [];
        let i = 256;

        for (let action of scriptActions) {
            actionsList.push({
                name : "(text) : " + action,
                key : i
            });
            i += 1;
        }
        for (let action of manualGraphicalActions) {
            actionsList.push({
                name : "(manual) : " + action.name,
                key : action.id
            });
        }
        for (let action of conditionGraphicalActions) {
            actionsList.push({
                name : "(condition) : " + action.name,
                key : action.id
            });
        }
        const listItems = actionsList.map(data => 
            <ScriptActionItem key = {data.key} value = {data.name} />
        )
        return <div className="form-group mb-3">
            <select name = {this.props.name} className = "form-select" onChange = {this.onChange} value = {this.state.value} disabled={this.props.disabled}>
                <option value = "null">Select the action</option>
                {listItems}
            </select>
            <div className="form-text">
                Select the action
            </div>
        </div>;
    }
}

class ScheduleSettingsTypeSelect extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value : this.props.scheduleType
        }
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(element) {
        this.setState({
            value : element.target.value
        });
        this.props.onChange(element.target.value);
    }
    render() {
        let disabledInput = false;
        if(this.props.permission != "write") {
          disabledInput = true;
        }
        return <div className="form-group mb-3">
            <select className = "form-select" value={this.state.value} onChange={this.handleChange} disabled = {disabledInput}>
                <option value = {SCHEDULES_TYPES.ONE_TIME}>One time</option>
                <option value = {SCHEDULES_TYPES.PERIODICALLY}>Periodically</option>
                <option value = {SCHEDULES_TYPES.DAILY}>Daily</option>
                <option value = {SCHEDULES_TYPES.SUNSET}>Sunset</option>
                <option value = {SCHEDULES_TYPES.SUNRISE}>Sunrise</option>
            </select>
        </div>;
    }
}

class ScheduleNameSettings extends React.Component {
    render() {
        return <div className="mb-3">
            <label htmlFor="sch-name-input" className="form-label">Schedule name</label>
            <input 
                type="text" 
                className="form-control" 
                name="sch-name-input" 
                value = {this.props.schName}
                onChange = {this.props.onChange}
                required pattern="[a-zA-Z0-9_]{1,16}"
                disabled = {this.props.disabled}
            />
            <div className="form-text">
                Only letters , numbers and _ (from 1 to 16 characters)
            </div>
        </div>;
    }
}

class ScheduleSettings extends React.Component {
    constructor(props) {
        super(props);
        this.STATES = {
            PERIODICALLY    : SCHEDULES_TYPES.PERIODICALLY,
            ONE_TIME        : SCHEDULES_TYPES.ONE_TIME,
            DAILY           : SCHEDULES_TYPES.DAILY,
            BY_VALUE        : SCHEDULES_TYPES.BY_VALUE,
            SUNSET          : SCHEDULES_TYPES.SUNSET,
            SUNRISE         : SCHEDULES_TYPES.SUNRISE
        };
        this.state = {
            loading : true,
            error : false
        }
        this.onChange = this.onChange.bind(this);
        this.reload = this.reload.bind(this);
    }
    componentDidMount() {
        this.reload();
    }
    reload() {
        let devicesMap = new Map();
        updateDevicesNamesMap()
        .then(namesMap => {
            Hub.getDevices()
            .then(devicesJSON => {
                devicesMap = updateDevicesMap(devicesMap,namesMap,devicesJSON);
                Hub.getGraphicalActions()
                .then(gasHex => {
                    let graphicalActionsMap = GraphicalActionsData.initGraphicalActionsMap(gasHex,devicesMap);
                    Hub.getGraphicalActionsData()
                    .then(gasData => {
                        graphicalActionsMap = GraphicalActionsData.initGaViewData(gasData,graphicalActionsMap);
                        const manualGraphicalActions = getManualGraphicalActionsList(graphicalActionsMap);
                        const conditionGraphicalActions = getConditionGraphicalActionsList(graphicalActionsMap);
                        Hub.getScriptActionsList()
                        .then(scriptActions => {
                            this.setState({
                                loading : false,
                                error : false,
                                manualGraphicalActions : manualGraphicalActions,
                                conditionGraphicalActions : conditionGraphicalActions,
                                scriptActions : scriptActions,
                                view : this.props.schedule.type || this.STATES.ONE_TIME
                            })
                        })            
                    })
                })
            })
        })
        .catch(error => {
            this.setState ({
                error : error,
                loading : false
            });
        })
    }
    onChange(menuName) {
        this.setState({
            view : menuName
        });
    }
    render() {
        if (this.state.loading) {
            return <CommonComponents.LoadingField />
        } else if (this.state.error) {
            return <CommonComponents.ReloadErrorField onChange = {this.reload} error = {this.state.error}/>
        } else {
            switch (this.state.view) {
                case (this.STATES.PERIODICALLY) : {
                    return <div>
                        <ScheduleSettingsTypeSelect onChange = {this.onChange} scheduleType = {this.state.view} permission={this.props.permission}/>
                        <SchedulePeriodicallySettings schedulesMap = {this.props.schedulesMap} schedule = {this.props.schedule} conditionGraphicalActions = {this.state.conditionGraphicalActions} manualGraphicalActions = {this.state.manualGraphicalActions} scriptActions = {this.state.scriptActions} onChange = {this.props.onChange} permission={this.props.permission}/>
                    </div>;
                }
                case (this.STATES.ONE_TIME) : {
                    return <div>
                        <ScheduleSettingsTypeSelect onChange = {this.onChange} scheduleType = {this.state.view} permission={this.props.permission}/>
                        <ScheduleOneTimeSettings schedulesMap = {this.props.schedulesMap} schedule = {this.props.schedule} conditionGraphicalActions = {this.state.conditionGraphicalActions} manualGraphicalActions = {this.state.manualGraphicalActions} scriptActions = {this.state.scriptActions} onChange = {this.props.onChange} permission={this.props.permission}/>
                    </div>;
                }
                case (this.STATES.DAILY) : {
                    return <div>
                        <ScheduleSettingsTypeSelect onChange = {this.onChange} scheduleType = {this.state.view} permission={this.props.permission}/>
                        <ScheduleDailySettings schedulesMap = {this.props.schedulesMap} schedule = {this.props.schedule} conditionGraphicalActions = {this.state.conditionGraphicalActions} manualGraphicalActions = {this.state.manualGraphicalActions} scriptActions = {this.state.scriptActions} onChange = {this.props.onChange} permission={this.props.permission}/>
                    </div>;
                }
                case (this.STATES.SUNRISE) : {
                    return <div>
                        <ScheduleSettingsTypeSelect onChange = {this.onChange} scheduleType = {this.state.view} permission={this.props.permission}/>
                        <ScheduleSunriseSettings schedulesMap = {this.props.schedulesMap} schedule = {this.props.schedule} conditionGraphicalActions = {this.state.conditionGraphicalActions} manualGraphicalActions = {this.state.manualGraphicalActions} scriptActions = {this.state.scriptActions} onChange = {this.props.onChange} permission={this.props.permission}/>
                    </div>;
                }
                case (this.STATES.SUNSET) : {
                    return <div>
                        <ScheduleSettingsTypeSelect onChange = {this.onChange} scheduleType = {this.state.view} permission={this.props.permission}/>
                        <ScheduleSunsetSettings schedulesMap = {this.props.schedulesMap} schedule = {this.props.schedule} conditionGraphicalActions = {this.state.conditionGraphicalActions} manualGraphicalActions = {this.state.manualGraphicalActions} scriptActions = {this.state.scriptActions} onChange = {this.props.onChange} permission={this.props.permission}/>
                    </div>;
                }
            }
        }
    }
}

class ScheduleOneTimeSettings extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.removeSchedule = this.removeSchedule.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            removing : false,
            applying : false,
            id : this.props.schedule.id || getNextScheduleId(),
            schName :  this.props.schedule.schName || "",
            isActive :  this.props.schedule.isActive || true,
            scriptName : this.props.schedule.scriptData.scriptName,
            type : SCHEDULES_TYPES.ONE_TIME,
            isConditionAction : this.props.schedule.scriptData.scriptType === "condition",
            conditionActionToState : this.props.schedule.scriptData.conditionActionToState,
            scriptId : this.props.schedule.scriptData.scriptId,
            scriptType : this.props.schedule.scriptData.scriptType
        };
    }

    componentDidMount() {
       this.reload();
    }
    reload() {
        this.setState({
            loading: true,
            error : false
        })
        Hub.getSystemTimeZone()
        .then(systemTimeZone => {
            const dateToViewMS = (this.props.schedule.dateTime || Math.trunc (new Date()/1000) + new Date().getTimezoneOffset()*60 + systemTimeZone*60) * 1000;

            const dateToView = new Date(dateToViewMS);

            this.setState({
                date : dateToView.YYYYMMDD(),
                time : dateToView.HHmm(),
                dateTime : this.props.schedule.dateTime,
                systemTimeZone : systemTimeZone,
                error : false,
                loading : false
            });

        })
        .catch(error => {
            this.setState({
                loading : false,
                error : error + ""
            });
        })
    }
    handleSubmit(e) {
        e.preventDefault();
        let schedule = new Schedule(
            this.state.id,
            this.state.schName,
            this.state.isActive,
            this.state.type
        );
        if (!this.state.scriptType || !this.state.scriptName) {
            alert("action not selected");
            return;
        }
        this.setState({
            applying : true
        })
        schedule.dateTime = this.state.dateTime;
        if (this.state.conditionActionToState) {
            schedule.scriptData.conditionActionToState = this.state.conditionActionToState;
        }
        schedule.scriptData.scriptType = this.state.scriptType;
        schedule.scriptData.scriptName = this.state.scriptName;
        if (this.state.scriptId) {
            schedule.scriptData.scriptId = this.state.scriptId;
        }
        
        let schedulesMap = new Map ()
        for (let [k,v] of this.props.schedulesMap) {
            schedulesMap.set(k,v);
        }
        schedulesMap.set(schedule.id,schedule);
        Hub.setSchedules(schedulesMap)
        .then(response => {
            Hub.setSchedulesNames(schedulesMap)
            .then(response => {
                this.props.onChange([CommonComponents.BTNS_RETURNS.BACK])
            })
        })
        .catch(err => {
            console.error(err);
            alert ("applying error (details in the console)");
        })
    }
    handleChange(e) {
        const target = e.target;
        const value = target.value;
        const name = target.name;
        if (name === "sch-name-input") {
            this.setState({
                schName : value
            });
        } else if (name === "sch-date-input") {
            let dateString = value+"T"+this.state.time + TimeZone.convertTimeZoneToViewFormat(this.state.systemTimeZone);
            this.setState({
                dateTime : Math.trunc(new Date(dateString)/1000),
                date : value
            });
        } else if (name === "sch-time-input") {
            let dateString = this.state.date+"T"+value + TimeZone.convertTimeZoneToViewFormat(this.state.systemTimeZone);
            this.setState({
                dateTime : Math.trunc(new Date(dateString)/1000),
                time : value
            });
        } else if (name === "sch-active-input") {
            this.setState({
                isActive : target.checked
            });
        } else if (name === "scriptName") { 
            if (value === "null") {
                this.setState({
                    scriptName : null,
                    isConditionAction : false,
                    scriptType : null,
                });
            } else {
                let scriptData = value.split(":");
                let scriptName = scriptData[1].trim();
                let scriptType = scriptData[0].trim();
            if (scriptType === "(condition)") {
                this.setState({
                    scriptName : scriptName,
                    isConditionAction : true,
                    scriptType : "condition",
                    scriptId : GraphicalActionsData.getGraphicalActionIdByName(scriptName,this.props.conditionGraphicalActions)
                });
            } else if (scriptType === "(manual)") {
                this.setState({
                    scriptName : scriptName,
                    isConditionAction : false,
                    scriptType : "manual",
                    scriptId : GraphicalActionsData.getGraphicalActionIdByName(scriptName,this.props.manualGraphicalActions)
                });
            } else if (scriptType === "(text)") {
                this.setState({
                    scriptName : scriptName,
                    isConditionAction : false,
                    scriptType : "text",
                });
            } 
        }
        } else if (name === "conditionGa-active-input") {
            this.setState({
                conditionActionToState : target.checked
            })
        }
    }
    removeSchedule() {
        this.setState({
            removing : true
        })
        let schedulesMap = new Map()
        for (let [k,v] of this.props.schedulesMap) {
            schedulesMap.set(k,v);
        }
        schedulesMap.delete(this.state.id);
        Hub.setSchedules(schedulesMap)
        .then(response => {
            Hub.setSchedulesNames(schedulesMap)
            .then(response => {
                this.props.onChange([CommonComponents.BTNS_RETURNS.BACK])
            })
        })
        .catch(err => {
            this.setState({
                removing : false
            })
            console.error(err);
            alert (err + "");
        })
    }
    render() {
        let disabledInput = false;
        if(this.props.permission != "write") {
          disabledInput = true;
        }
        if (this.state.loading) {
            return <CommonComponents.LoadingField />;
        } else if (this.state.error) {
            return <CommonComponents.ReloadErrorField onChange = {this.reload} error = {this.props.error}/>
        } else {
            let valueForScriptsList = "("+this.state.scriptType+") : "+this.state.scriptName;
            return <form onSubmit = {this.handleSubmit}>
                <ScheduleNameSettings schName = {this.state.schName} onChange = {this.handleChange} disabled = {disabledInput}/>
                <ScriptAndGraphicalActionsList 
                    value = {valueForScriptsList} 
                    name = "scriptName" 
                    onChange = {this.handleChange} 
                    conditionGraphicalActions = {this.props.conditionGraphicalActions} 
                    manualGraphicalActions = {this.props.manualGraphicalActions} 
                    scriptActions = {this.props.scriptActions} 
                    disabled = {disabledInput}
                />
                {
                    this.state.isConditionAction &&
                        <div className="form-check form-switch">
                            <input className="form-check-input" type="checkbox"  name="conditionGa-active-input" onChange = {this.handleChange} checked = {this.state.conditionActionToState} disabled = {disabledInput}/> 
                            <label className="form-check-label" htmlFor="sch-active-input">
                                On / Off [ <strong>{this.state.scriptName}</strong> ]
                            </label>
                        </div>
                }
                <div className="mb-3">
                    <label htmlFor="sch-date-input" className="form-label">Date</label>
                    <input type="date" value = {this.state.date} className="form-control" name="sch-date-input" onChange = {this.handleChange} required disabled = {disabledInput}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="sch-time-input" className="form-label">Time</label>
                    <input type="time" value = {this.state.time} className="form-control" name="sch-time-input" onChange = {this.handleChange} required disabled = {disabledInput}/>
                </div>
                <div className="form-check form-switch">
                    <input className="form-check-input" type="checkbox" checked = {this.state.isActive} name="sch-active-input" onChange = {this.handleChange} disabled = {disabledInput}/> 
                    <label className="form-check-label" htmlFor="sch-active-input">
                        Schedule is active
                    </label>
                </div>
                <div className = "row my-2">
                    <div className = "col-6">
                        <CommonComponents.ReturnBackBtn onChange = {this.props.onChange} />
                        <CommonComponents.ApplyFormBtn applying = {this.state.applying} disabled = {disabledInput}/>
                    </div>
                    <div className = "col-2">

                    </div>
                    <div className = "col-4">
                        {
                            this.props.schedulesMap.get(this.state.id) &&
                                <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <CommonComponents.RemoveBtn removing = {this.state.removing} onChange = {this.removeSchedule} disabled = {disabledInput}/>
                                </div>
                        }
                    </div>
                </div>
            </form>;
        }
    }
}

class SchedulePeriodicallySettings extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.removeSchedule = this.removeSchedule.bind(this);
        let timeData = calcHH_MM_SS(this.props.schedule.period);
        this.state = {
            removing : false,
            applying : false,
            id : this.props.schedule.id || getNextScheduleId(),
            schName :  this.props.schedule.schName || "",
            isActive :  this.props.schedule.isActive || true,
            scriptName : this.props.schedule.scriptData.scriptName,
            type : SCHEDULES_TYPES.PERIODICALLY,
            isConditionAction : this.props.schedule.scriptData.scriptType === "condition",
            conditionActionToState : this.props.schedule.scriptData.conditionActionToState,
            scriptId : this.props.schedule.scriptData.scriptId,
            scriptType : this.props.schedule.scriptData.scriptType,
            hh : timeData[0],
            mm : timeData[1],
            ss : timeData [2]
        };
    }
    handleSubmit(e) {
        e.preventDefault();
        let schedule = new Schedule(
            this.state.id,
            this.state.schName,
            this.state.isActive,
            this.state.type
        );
        if (!this.state.scriptType || !this.state.scriptName) {
            alert("action not selected");
            return;
        }
        this.setState({
            applying : true
        })
        schedule.period = this.state.hh * 3600 + this.state.mm*60 + this.state.ss * 1;

        if (this.state.conditionActionToState) {
            schedule.scriptData.conditionActionToState = this.state.conditionActionToState;
        }
        schedule.scriptData.scriptType = this.state.scriptType;
        schedule.scriptData.scriptName = this.state.scriptName;
        if (this.state.scriptId) {
            schedule.scriptData.scriptId = this.state.scriptId;
        }
        
        let schedulesMap = new Map ()
        for (let [k,v] of this.props.schedulesMap) {
            schedulesMap.set(k,v);
        }
        schedulesMap.set(schedule.id,schedule);
        Hub.setSchedules(schedulesMap)
        .then(response => {
            Hub.setSchedulesNames(schedulesMap)
            .then(response => {
                this.props.onChange([CommonComponents.BTNS_RETURNS.BACK])
            })
        })
        .catch(err => {
            console.error(err);
            alert (err + "");
        })
    }
    handleChange(e) {
        const target = e.target;
        const value = target.value;
        const name = target.name;
        if (name === "sch-name-input") {
            this.setState({
                schName : value
            });
        }  else if (name === "hh") {
            this.setState({
                hh : value
            });
        } else if (name === "mm") {
            this.setState({
                mm : value
            });
        } else if (name === "ss") {
            this.setState({
                ss : value
            });
        } else if (name === "sch-active-input") {
            this.setState({
                isActive : target.checked
            });
        } else if (name === "scriptName") { 
            if (value === "null") {
                this.setState({
                    scriptName : null,
                    isConditionAction : false,
                    scriptType : null,
                });
            } else {
                let scriptData = value.split(":");
                let scriptName = scriptData[1].trim();
                let scriptType = scriptData[0].trim();
                if (scriptType === "(condition)") {
                    this.setState({
                        scriptName : scriptName,
                        isConditionAction : true,
                        scriptType : "condition",
                        scriptId : GraphicalActionsData.getGraphicalActionIdByName(scriptName,this.props.conditionGraphicalActions)
                    });
                } else if (scriptType === "(manual)") {
                    this.setState({
                        scriptName : scriptName,
                        isConditionAction : false,
                        scriptType : "manual",
                        scriptId : GraphicalActionsData.getGraphicalActionIdByName(scriptName,this.props.manualGraphicalActions)
                    });
                } else if (scriptType === "(text)") {
                    this.setState({
                        scriptName : scriptName,
                        isConditionAction : false,
                        scriptType : "text",
                    });
                } 
            }
        } else if (name === "conditionGa-active-input") {
            this.setState({
                conditionActionToState : target.checked
            })
        }
    }
    removeSchedule() {
        this.setState({
            removing : true
        })
        let schedulesMap = new Map()
        for (let [k,v] of this.props.schedulesMap) {
            schedulesMap.set(k,v);
        }
        schedulesMap.delete(this.state.id);
        Hub.setSchedules(schedulesMap)
        .then(response => {
            Hub.setSchedulesNames(schedulesMap)
            .then(response => {
                this.props.onChange([CommonComponents.BTNS_RETURNS.BACK])
            })
        })
        .catch(err => {
            this.setState({
                removing : false
            })
            console.error(err);
            alert (err + "");
        })
    }
    render() {
        let disabledInput = false;
        if(this.props.permission != "write") {
          disabledInput = true;
        }
        let valueForScriptsList = "("+this.state.scriptType+") : "+this.state.scriptName;
        return <form onSubmit = {this.handleSubmit}>
            <ScheduleNameSettings schName = {this.state.schName} onChange = {this.handleChange} disabled = {disabledInput}/>
            <ScriptAndGraphicalActionsList 
                value = {valueForScriptsList} 
                name = "scriptName" 
                onChange = {this.handleChange} 
                conditionGraphicalActions = {this.props.conditionGraphicalActions} 
                manualGraphicalActions = {this.props.manualGraphicalActions} 
                scriptActions = {this.props.scriptActions}
                disabled = {disabledInput} 
            />
            {
                this.state.isConditionAction &&
                    <div className="form-check form-switch">
                        <input className="form-check-input" type="checkbox"  name="conditionGa-active-input" onChange = {this.handleChange} checked = {this.state.conditionActionToState} disabled = {disabledInput}/> 
                        <label className="form-check-label" htmlFor="sch-active-input">
                            On / Off [ <strong>{this.state.scriptName}</strong> ]
                        </label>
                    </div>
            }
            <div className="mb-3">
                <label htmlFor="hh" className="form-label">Period(hours)</label>
                <input type="number" className="form-control" value = {this.state.hh} name="hh" onChange = {this.handleChange} required disabled = {disabledInput}/>
            </div>
            <div className="mb-3">
                <label htmlFor="mm" className="form-label">Period(minutes)</label>
                <input type="number" className="form-control" name="mm" value = {this.state.mm} onChange = {this.handleChange} required disabled = {disabledInput}/>
            </div>
            <div className="mb-3">
                <label htmlFor="ss" className="form-label">Period(seconds)</label>
                <input type="number" className="form-control" name="ss" value = {this.state.ss} onChange = {this.handleChange} required disabled = {disabledInput}/>
            </div>
            <div className="form-check form-switch">
                <input className="form-check-input" type="checkbox" checked = {this.state.isActive} name="sch-active-input" onChange = {this.handleChange} disabled = {disabledInput}/> 
                <label className="form-check-label" htmlFor="sch-active-input">
                    Schedule is active
                </label>
            </div>
            <div className = "row my-2">
                <div className = "col-6">
                    <CommonComponents.ReturnBackBtn onChange = {this.props.onChange} />
                    <CommonComponents.ApplyFormBtn applying = {this.state.applying} disabled = {disabledInput}/>
                </div>
                <div className = "col-2">

                </div>
                <div className = "col-4">
                    {
                        this.props.schedulesMap.get(this.state.id) &&
                            <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                                <CommonComponents.RemoveBtn removing = {this.state.removing} onChange = {this.removeSchedule} disabled = {disabledInput}/>
                            </div>
                    }
                </div>
            </div>
        </form>;
    }
}

class ScheduleDailySettings extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.removeSchedule = this.removeSchedule.bind(this);
        let dailyTime = this.props.schedule.dailyTime;
        let mins = (dailyTime%3600)/60;
        if (mins < 10) {
            mins = '0'+mins;
        }
        let hrs = parseInt(dailyTime/3600);
        if (hrs < 10) {
            hrs = '0'+hrs;
        }
	    dailyTime = hrs+":"+mins;
        this.state = {
            removing : false,
            applying : false,
            id : this.props.schedule.id || getNextScheduleId(),
            schName :  this.props.schedule.schName || "",
            isActive :  this.props.schedule.isActive || true,
            scriptName : this.props.schedule.scriptData.scriptName,
            type : SCHEDULES_TYPES.DAILY,
            isConditionAction : this.props.schedule.scriptData.scriptType === "condition",
            conditionActionToState : this.props.schedule.scriptData.conditionActionToState,
            scriptId : this.props.schedule.scriptData.scriptId,
            scriptType : this.props.schedule.scriptData.scriptType,
            dailyTime : dailyTime
        };
    }
    handleSubmit(e) {
        e.preventDefault();
        let schedule = new Schedule(
            this.state.id,
            this.state.schName,
            this.state.isActive,
            this.state.type
        );
        if (!this.state.scriptType || !this.state.scriptName) {
            alert("action not selected");
            return;
        }
        this.setState({
            applying : true
        })
        let dailyTimeData = this.state.dailyTime.split(":");
        schedule.dailyTime = dailyTimeData[0] * 3600 + dailyTimeData[1]*60;

        if (this.state.conditionActionToState) {
            schedule.scriptData.conditionActionToState = this.state.conditionActionToState;
        }
        schedule.scriptData.scriptType = this.state.scriptType;
        schedule.scriptData.scriptName = this.state.scriptName;
        if (this.state.scriptId) {
            schedule.scriptData.scriptId = this.state.scriptId;
        }
        
        let schedulesMap = new Map ()
        for (let [k,v] of this.props.schedulesMap) {
            schedulesMap.set(k,v);
        }
        schedulesMap.set(schedule.id,schedule);
        Hub.setSchedules(schedulesMap)
        .then(response => {
            Hub.setSchedulesNames(schedulesMap)
            .then(response => {
                this.props.onChange([CommonComponents.BTNS_RETURNS.BACK])
            })
        })
        .catch(err => {
            console.error(err);
            alert (err + "");
        })
    }
    handleChange(e) {
        const target = e.target;
        const value = target.value;
        const name = target.name;
        if (name === "sch-name-input") {
            this.setState({
                schName : value
            });
        }  else if (name === "sch-time-input") {
            this.setState({
                dailyTime : value
            });
        } else if (name === "sch-active-input") {
            this.setState({
                isActive : target.checked
            });
        } else if (name === "scriptName") { 
            if (value === "null") {
                this.setState({
                    scriptName : null,
                    isConditionAction : false,
                    scriptType : null,
                });
            } else {
                let scriptData = value.split(":");
                let scriptName = scriptData[1].trim();
                let scriptType = scriptData[0].trim();
                if (scriptType === "(condition)") {
                    this.setState({
                        scriptName : scriptName,
                        isConditionAction : true,
                        scriptType : "condition",
                        scriptId : GraphicalActionsData.getGraphicalActionIdByName(scriptName,this.props.conditionGraphicalActions)
                    });
                } else if (scriptType === "(manual)") {
                    this.setState({
                        scriptName : scriptName,
                        isConditionAction : false,
                        scriptType : "manual",
                        scriptId : GraphicalActionsData.getGraphicalActionIdByName(scriptName,this.props.manualGraphicalActions)
                    });
                } else if (scriptType === "(text)") {
                    this.setState({
                        scriptName : scriptName,
                        isConditionAction : false,
                        scriptType : "text",
                    });
                } 
            }
        } else if (name === "conditionGa-active-input") {
            this.setState({
                conditionActionToState : target.checked
            })
        }
    }
    removeSchedule() {
        this.setState({
            removing : true
        })
        let schedulesMap = new Map()
        for (let [k,v] of this.props.schedulesMap) {
            schedulesMap.set(k,v);
        }
        schedulesMap.delete(this.state.id);
        Hub.setSchedules(schedulesMap)
        .then(response => {
            Hub.setSchedulesNames(schedulesMap)
            .then(response => {
                this.props.onChange([CommonComponents.BTNS_RETURNS.BACK])
            })
        })
        .catch(err => {
            this.setState({
                removing : false
            })
            console.error(err);
            alert (err + "");
        })
    }
    render() {
        let disabledInput = false;
        if(this.props.permission != "write") {
          disabledInput = true;
        }
        let valueForScriptsList = "("+this.state.scriptType+") : "+this.state.scriptName;
         return <form onSubmit = {this.handleSubmit}>
            <ScheduleNameSettings schName = {this.state.schName} onChange = {this.handleChange} disabled = {disabledInput}/>
            <ScriptAndGraphicalActionsList 
                value = {valueForScriptsList} 
                name = "scriptName" 
                onChange = {this.handleChange} 
                conditionGraphicalActions = {this.props.conditionGraphicalActions} 
                manualGraphicalActions = {this.props.manualGraphicalActions} 
                scriptActions = {this.props.scriptActions} 
                disabled = {disabledInput}
            />
            {
                this.state.isConditionAction &&
                    <div className="form-check form-switch">
                        <input className="form-check-input" type="checkbox"  name="conditionGa-active-input" onChange = {this.handleChange} checked = {this.state.conditionActionToState} disabled = {disabledInput}/> 
                        <label className="form-check-label" htmlFor="sch-active-input">
                            On / Off [ <strong>{this.state.scriptName}</strong> ]
                        </label>
                    </div>
            }
            <div className="mb-3">
                <label htmlFor="sch-time-input" className="form-label">Time</label>
                <input type="time" value = {this.state.dailyTime} className="form-control" name="sch-time-input" onChange = {this.handleChange} required disabled = {disabledInput}/>
            </div>
            <div className="form-check form-switch">
                <input className="form-check-input" type="checkbox" checked = {this.state.isActive} name="sch-active-input" onChange = {this.handleChange} disabled = {disabledInput}/> 
                <label className="form-check-label" htmlFor="sch-active-input">
                    Schedule is active
                </label>
            </div>
            <div className = "row my-2">
                <div className = "col-6">
                    <CommonComponents.ReturnBackBtn onChange = {this.props.onChange} />
                    <CommonComponents.ApplyFormBtn applying = {this.state.applying} disabled = {disabledInput}/>
                </div>
                <div className = "col-2">

                </div>
                <div className = "col-4">
                    {
                        this.props.schedulesMap.get(this.state.id) &&
                            <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                                <CommonComponents.RemoveBtn removing = {this.state.removing} onChange = {this.removeSchedule} disabled = {disabledInput}/>
                            </div>
                    }
                </div>
            </div>
        </form>;
    }
}

class BeforeAfterSelect extends React.Component {
    render() {
        return <div className="form-group mb-3">
            <select name="sch-timeShifting-select" value = {this.props.timeShiftingState} className="form-select" onChange = {this.props.onChange} disabled = {this.props.disabled}>
                <option value = "0" >No time shifting</option>
                <option value = "+" >After {this.props.selectType}</option>
                <option value = "-" >Before {this.props.selectType}</option>
            </select>
        </div>
    }
}

class ScheduleSunsetSettings extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.removeSchedule = this.removeSchedule.bind(this);
        let timeShifting = this.props.schedule.timeShifting;
        let timeShiftingState = "0";
        if (timeShifting === 0) {
            timeShiftingState = "0"
        } else if (timeShifting > 0) {
            timeShiftingState = "+";
        } else if (timeShifting < 0) {
            timeShifting *= -1;
            timeShiftingState = "-";
        }
        let mins = (timeShifting%3600)/60;
        if (mins < 10) {
            mins = '0'+mins;
        }
        let hrs = parseInt(timeShifting/3600);
        if (hrs < 10) {
            hrs = '0'+hrs;
        }
	    timeShifting = hrs+":"+mins;
        this.state = {
            removing : false,
            applying : false,
            id : this.props.schedule.id || getNextScheduleId(),
            schName :  this.props.schedule.schName || "",
            isActive :  this.props.schedule.isActive || true,
            scriptName : this.props.schedule.scriptData.scriptName,
            type : SCHEDULES_TYPES.SUNSET,
            isConditionAction : this.props.schedule.scriptData.scriptType === "condition",
            conditionActionToState : this.props.schedule.scriptData.conditionActionToState,
            scriptId : this.props.schedule.scriptData.scriptId,
            scriptType : this.props.schedule.scriptData.scriptType,
            timeShifting : timeShifting,
            timeShiftingState : timeShiftingState
        };
    }
    handleSubmit(e) {
        e.preventDefault();
        let schedule = new Schedule(
            this.state.id,
            this.state.schName,
            this.state.isActive,
            this.state.type
        );
        if (!this.state.scriptType || !this.state.scriptName) {
            alert("action not selected");
            return;
        }
        this.setState({
            applying : true
        })
        let timeShiftingData = this.state.timeShifting.split(":");
        schedule.timeShifting = timeShiftingData[0] * 3600 + timeShiftingData[1]*60;

        if (this.state.timeShiftingState === "-") {
            schedule.timeShifting *= -1;
        }

        if (this.state.conditionActionToState) {
            schedule.scriptData.conditionActionToState = this.state.conditionActionToState;
        }

        schedule.scriptData.scriptType = this.state.scriptType;
        schedule.scriptData.scriptName = this.state.scriptName;
        if (this.state.scriptId) {
            schedule.scriptData.scriptId = this.state.scriptId;
        }
        
        let schedulesMap = new Map ()
        for (let [k,v] of this.props.schedulesMap) {
            schedulesMap.set(k,v);
        }
        schedulesMap.set(schedule.id,schedule);
        Hub.setSchedules(schedulesMap)
        .then(response => {
            Hub.setSchedulesNames(schedulesMap)
            .then(response => {
                this.props.onChange([CommonComponents.BTNS_RETURNS.BACK])
            })
        })
        .catch(err => {
            console.error(err);
            alert (err + "");
        })
    }
    handleChange(e) {
        const target = e.target;
        const value = target.value;
        const name = target.name;
        if (name === "sch-name-input") {
            this.setState({
                schName : value
            });
        }  else if (name === "sch-time-input") {
            if (value === "00:00") {
                this.setState({
                    timeShiftingState : "0"
                })
            } else {
                if (this.state.timeShiftingState === "0") {
                    this.setState ({
                        timeShiftingState : "+"
                    });
                }
            }
            this.setState({
                timeShifting : value
            });
        } else if (name === "sch-active-input") {
            this.setState({
                isActive : target.checked
            });
        } else if (name === "sch-timeShifting-select") {
            this.setState({
                timeShiftingState : value
            })
        } else if (name === "scriptName") { 
            if (value === "null") {
                this.setState({
                    scriptName : null,
                    isConditionAction : false,
                    scriptType : null,
                });
            } else {
                let scriptData = value.split(":");
                let scriptName = scriptData[1].trim();
                let scriptType = scriptData[0].trim();
                if (scriptType === "(condition)") {
                    this.setState({
                        scriptName : scriptName,
                        isConditionAction : true,
                        scriptType : "condition",
                        scriptId : GraphicalActionsData.getGraphicalActionIdByName(scriptName,this.props.conditionGraphicalActions)
                    });
                } else if (scriptType === "(manual)") {
                    this.setState({
                        scriptName : scriptName,
                        isConditionAction : false,
                        scriptType : "manual",
                        scriptId : GraphicalActionsData.getGraphicalActionIdByName(scriptName,this.props.manualGraphicalActions)
                    });
                } else if (scriptType === "(text)") {
                    this.setState({
                        scriptName : scriptName,
                        isConditionAction : false,
                        scriptType : "text",
                    });
                } 
            }
        } else if (name === "conditionGa-active-input") {
            this.setState({
                conditionActionToState : target.checked
            })
        }
    }
    removeSchedule() {
        this.setState({
            removing : true
        })
        let schedulesMap = new Map()
        for (let [k,v] of this.props.schedulesMap) {
            schedulesMap.set(k,v);
        }
        schedulesMap.delete(this.state.id);
        Hub.setSchedules(schedulesMap)
        .then(response => {
            Hub.setSchedulesNames(schedulesMap)
            .then(response => {
                this.props.onChange([CommonComponents.BTNS_RETURNS.BACK])
            })
        })
        .catch(err => {
            this.setState({
                removing : false
            })
            console.error(err);
            alert (err + "");
        })
    }
    render() {
        let disabledInput = false;
        if(this.props.permission != "write") {
          disabledInput = true;
        }
        let valueForScriptsList = "("+this.state.scriptType+") : "+this.state.scriptName;
         return <form onSubmit = {this.handleSubmit}>
            <ScheduleNameSettings schName = {this.state.schName} onChange = {this.handleChange} disabled = {disabledInput}/>
            <ScriptAndGraphicalActionsList 
                value = {valueForScriptsList} 
                name = "scriptName" 
                onChange = {this.handleChange} 
                conditionGraphicalActions = {this.props.conditionGraphicalActions} 
                manualGraphicalActions = {this.props.manualGraphicalActions} 
                scriptActions = {this.props.scriptActions} 
                disabled = {disabledInput}
            />
            {
                this.state.isConditionAction &&
                    <div className="form-check form-switch">
                        <input className="form-check-input" type="checkbox"  name="conditionGa-active-input" onChange = {this.handleChange} checked = {this.state.conditionActionToState} disabled = {disabledInput}/> 
                        <label className="form-check-label" htmlFor="sch-active-input">
                            On / Off [ <strong>{this.state.scriptName}</strong> ]
                        </label>
                    </div>
            }
            <BeforeAfterSelect selectType = "sunset" onChange = {this.handleChange} timeShiftingState = {this.state.timeShiftingState} disabled = {disabledInput}/>
            <div className="mb-3">
                <label htmlFor="sch-time-input" className="form-label">Time shifting</label>
                <input type="time" value = {this.state.timeShifting} className="form-control" name="sch-time-input" onChange = {this.handleChange} required disabled = {disabledInput}/>
            </div>
            <div className="form-check form-switch">
                <input className="form-check-input" type="checkbox" checked = {this.state.isActive} name="sch-active-input" onChange = {this.handleChange} disabled = {disabledInput}/> 
                <label className="form-check-label" htmlFor="sch-active-input">
                    Schedule is active
                </label>
            </div>
            <div className = "row my-2">
                <div className = "col-6">
                    <CommonComponents.ReturnBackBtn onChange = {this.props.onChange} />
                    <CommonComponents.ApplyFormBtn applying = {this.state.applying} disabled = {disabledInput}/>
                </div>
                <div className = "col-2">

                </div>
                <div className = "col-4">
                    {
                        this.props.schedulesMap.get(this.state.id) &&
                            <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                                <CommonComponents.RemoveBtn removing = {this.state.removing} onChange = {this.removeSchedule} disabled = {disabledInput}/>
                            </div>
                    }
                </div>
            </div>
        </form>;
    }
}

class ScheduleSunriseSettings extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.removeSchedule = this.removeSchedule.bind(this);
        let timeShifting = this.props.schedule.timeShifting;
        let timeShiftingState = "0";
        if (timeShifting === 0) {
            timeShiftingState = "0"
        } else if (timeShifting > 0) {
            timeShiftingState = "+";
        } else if (timeShifting < 0) {
            timeShifting *= -1;
            timeShiftingState = "-";
        }
        let mins = (timeShifting%3600)/60;
        if (mins < 10) {
            mins = '0'+mins;
        }
        let hrs = parseInt(timeShifting/3600);
        if (hrs < 10) {
            hrs = '0'+hrs;
        }
	    timeShifting = hrs+":"+mins;
        this.state = {
            removing : false,
            applying : false,
            id : this.props.schedule.id || getNextScheduleId(),
            schName :  this.props.schedule.schName || "",
            isActive :  this.props.schedule.isActive || true,
            scriptName : this.props.schedule.scriptData.scriptName,
            type : SCHEDULES_TYPES.SUNRISE,
            isConditionAction : this.props.schedule.scriptData.scriptType === "condition",
            conditionActionToState : this.props.schedule.scriptData.conditionActionToState,
            scriptId : this.props.schedule.scriptData.scriptId,
            scriptType : this.props.schedule.scriptData.scriptType,
            timeShifting : timeShifting,
            timeShiftingState : timeShiftingState
        };
    }
    handleSubmit(e) {
        e.preventDefault();
        let schedule = new Schedule(
            this.state.id,
            this.state.schName,
            this.state.isActive,
            this.state.type
        );
        if (!this.state.scriptType || !this.state.scriptName) {
            alert("action not selected");
            return;
        }
        this.setState({
            applying : true
        })
        let timeShiftingData = this.state.timeShifting.split(":");
        schedule.timeShifting = timeShiftingData[0] * 3600 + timeShiftingData[1]*60;

        if (this.state.timeShiftingState === "-") {
            schedule.timeShifting *= -1;
        }

        if (this.state.conditionActionToState) {
            schedule.scriptData.conditionActionToState = this.state.conditionActionToState;
        }

        schedule.scriptData.scriptType = this.state.scriptType;
        schedule.scriptData.scriptName = this.state.scriptName;
        if (this.state.scriptId) {
            schedule.scriptData.scriptId = this.state.scriptId;
        }
        
        let schedulesMap = new Map ()
        for (let [k,v] of this.props.schedulesMap) {
            schedulesMap.set(k,v);
        }
        schedulesMap.set(schedule.id,schedule);
        Hub.setSchedules(schedulesMap)
        .then(response => {
            Hub.setSchedulesNames(schedulesMap)
            .then(response => {
                this.props.onChange([CommonComponents.BTNS_RETURNS.BACK])
            })
        })
        .catch(err => {
            console.error(err);
            alert (err + "");
        })
    }
    handleChange(e) {
        const target = e.target;
        const value = target.value;
        const name = target.name;
        if (name === "sch-name-input") {
            this.setState({
                schName : value
            });
        }  else if (name === "sch-time-input") {
            if (value === "00:00") {
                this.setState({
                    timeShiftingState : "0"
                })
            } else {
                if (this.state.timeShiftingState === "0") {
                    this.setState ({
                        timeShiftingState : "+"
                    });
                }
            }
            this.setState({
                timeShifting : value
            });
        } else if (name === "sch-active-input") {
            this.setState({
                isActive : target.checked
            });
        } else if (name === "sch-timeShifting-select") {
            this.setState({
                timeShiftingState : value
            })
        } else if (name === "scriptName") { 
            if (value === "null") {
                this.setState({
                    scriptName : null,
                    isConditionAction : false,
                    scriptType : null,
                });
            } else {
                let scriptData = value.split(":");
                let scriptName = scriptData[1].trim();
                let scriptType = scriptData[0].trim();
                if (scriptType === "(condition)") {
                    this.setState({
                        scriptName : scriptName,
                        isConditionAction : true,
                        scriptType : "condition",
                        scriptId : GraphicalActionsData.getGraphicalActionIdByName(scriptName,this.props.conditionGraphicalActions)
                    });
                } else if (scriptType === "(manual)") {
                    this.setState({
                        scriptName : scriptName,
                        isConditionAction : false,
                        scriptType : "manual",
                        scriptId : GraphicalActionsData.getGraphicalActionIdByName(scriptName,this.props.manualGraphicalActions)
                    });
                } else if (scriptType === "(text)") {
                    this.setState({
                        scriptName : scriptName,
                        isConditionAction : false,
                        scriptType : "text",
                    });
                } 
            }
        } else if (name === "conditionGa-active-input") {
            this.setState({
                conditionActionToState : target.checked
            })
        }
    }
    removeSchedule() {
        this.setState({
            removing : true
        })
        let schedulesMap = new Map()
        for (let [k,v] of this.props.schedulesMap) {
            schedulesMap.set(k,v);
        }
        schedulesMap.delete(this.state.id);
        Hub.setSchedules(schedulesMap)
        .then(response => {
            Hub.setSchedulesNames(schedulesMap)
            .then(response => {
                this.props.onChange([CommonComponents.BTNS_RETURNS.BACK])
            })
        })
        .catch(err => {
            this.setState({
                removing : false
            })
            console.error(err);
            alert (err + "");
        })
    }
    render() {
        let disabledInput = false;
        if(this.props.permission != "write") {
          disabledInput = true;
        }
        let valueForScriptsList = "("+this.state.scriptType+") : "+this.state.scriptName;
         return <form onSubmit = {this.handleSubmit}>
            <ScheduleNameSettings schName = {this.state.schName} onChange = {this.handleChange} disabled = {disabledInput}/>
            <ScriptAndGraphicalActionsList 
                value = {valueForScriptsList} 
                name = "scriptName" 
                onChange = {this.handleChange} 
                conditionGraphicalActions = {this.props.conditionGraphicalActions} 
                manualGraphicalActions = {this.props.manualGraphicalActions} 
                scriptActions = {this.props.scriptActions} 
                disabled = {disabledInput}
            />
            {
                this.state.isConditionAction &&
                    <div className="form-check form-switch">
                        <input className="form-check-input" type="checkbox"  name="conditionGa-active-input" onChange = {this.handleChange} checked = {this.state.conditionActionToState} disabled = {disabledInput}/> 
                        <label className="form-check-label" htmlFor="sch-active-input">
                            On / Off [ <strong>{this.state.scriptName}</strong> ]
                        </label>
                    </div>
            }
            <BeforeAfterSelect selectType = "sunrise" onChange = {this.handleChange} timeShiftingState = {this.state.timeShiftingState} disabled = {disabledInput}/>
            <div className="mb-3">
                <label htmlFor="sch-time-input" className="form-label">Time shifting</label>
                <input type="time" value = {this.state.timeShifting} className="form-control" name="sch-time-input" onChange = {this.handleChange} required disabled = {disabledInput}/>
            </div>
            <div className="form-check form-switch">
                <input className="form-check-input" type="checkbox" checked = {this.state.isActive} name="sch-active-input" onChange = {this.handleChange} disabled = {disabledInput}/> 
                <label className="form-check-label" htmlFor="sch-active-input">
                    Schedule is active
                </label>
            </div>
            <div className = "row my-2">
                <div className = "col-6">
                    <CommonComponents.ReturnBackBtn onChange = {this.props.onChange} />
                    <CommonComponents.ApplyFormBtn applying = {this.state.applying} disabled = {disabledInput}/>
                </div>
                <div className = "col-2">

                </div>
                <div className = "col-4">
                    {
                        this.props.schedulesMap.get(this.state.id) &&
                            <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                                <CommonComponents.RemoveBtn removing = {this.state.removing} onChange = {this.removeSchedule} disabled = {disabledInput}/>
                            </div>
                    }
                </div>
            </div>
        </form>;
    }
}

class SchedulesScreen extends React.Component {
    constructor(props){
        super(props);
        this.onChange = this.onChange.bind(this);
        this.reload = this.reload.bind(this);
        this.userPermission = props.permissions.schedule || "read";
        this.STATES = {
            MAIN_SCREEN : 0,
            SETTINGS : 1
        };
        this.state = {
            loading : true,
            error : false,
            header : "Schedules"
        }
        this.statesHistory = [];

    }
    componentDidMount() {
        this.reload()
    }
    reload() {
        this.setState({
            loading : true,
            error : false
        })
        Hub.getGraphicalActionsData()
        .then(gasData => {
            let gaNamesMap = new Map();
            try {
                let json = JSON.parse(gasData);
                for (let viewObj of json.gaViewData) {
                    gaNamesMap.set(viewObj.gaID,viewObj.gaName)
                }
            } catch (err) {
                console.error(err);
            }
            
            Hub.getSchedulesNames()
            .then(schedulesNames => {
                let schedulesNamesMap = initSchedulesNamesMap(schedulesNames);
                Hub.getSchedules()
                .then(response => {
                    let schedulesMap = initSchedulesMap(response,schedulesNamesMap,gaNamesMap);
                    this.setState({
                        schedulesMap : schedulesMap,
                        view : this.STATES.MAIN_SCREEN,
                        loading : false,
                        error : false
                    })
                })
            })
        })
        .catch(err => {
            this.setState({
                loading : false,
                error : err + ""
            })
            console.error(err);
        })
    }
    onChange(params) {
        switch (params[0]) {
            case (CommonComponents.BTNS_RETURNS.ADD_NEW) : {
                this.statesHistory.push(this.state);
                this.setState({
                    header : "New schedule",
                    schedule : new Schedule(getNextScheduleId(this.state.schedulesMap)),
                    view : this.STATES.SETTINGS
                });
                break;
            }
            case (CommonComponents.BTNS_RETURNS.BACK) : {
                this.reload();
            //    this.setState(this.statesHistory.pop());
                break;
            }
            case (CommonComponents.BTNS_RETURNS.APPLY_FORM) : {
            //    this.setState({
            //        schedule 
            //    })
                break;
            }
            default : {
                if (params[0] instanceof Schedule) {
                    this.statesHistory.push(this.state);
                    this.setState({
                        header : `Schedule [${params[0].schName}]`,
                        schedule : params[0],
                        view : this.STATES.SETTINGS
                    });
                }
            }
        }
    }
    render() {
        let disabledInput = false;
        if(this.userPermission != "write") {
          disabledInput = true;
        }
        if (this.state.loading) {
            return <CommonComponents.LoadingField />
        } else if (this.state.error) {
            return <CommonComponents.ReloadErrorField onChange = {this.reload} error = {this.state.error}/>
        } else {
            switch (this.state.view) {
                case (this.STATES.MAIN_SCREEN) : {
                    return  <div>
                        <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                        <SchedulesTable schedulesMap = {this.state.schedulesMap} onChange = {this.onChange}/>
                        <CommonComponents.AddNewBtn onChange = {this.onChange} disabled = {disabledInput}/>
                    </div>;
                }
                case (this.STATES.SETTINGS) : {
                    return  <div>
                        <CommonComponents.ContainerHeaderText headerText = {this.state.header}/>
                        <ScheduleSettings  schedulesMap = {this.state.schedulesMap} schedule = {this.state.schedule} onChange = {this.onChange} permission={this.userPermission}/>
                    </div>;
                }
            }
        }
    }
}

export default SchedulesScreen;