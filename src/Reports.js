import React from 'react';
import * as Hub from './Network';
import { Line } from 'react-chartjs-2';

const data = {
    labels: ["0s", "10s", "20s", "30s", "40s", "50s", "60s"],
    datasets: [{
      label: "Car Speed",
      tension : 0.2,
      data: [0, 59, 75, 20, 20, 55, 40],
    }]
};

const options = {
};

const LineChart = () => (
  <>
    <div className='header'>
      <h1 className='title'>Line Chart</h1>
      <div className='links'>
      </div>
    </div>
    <Line data={data} options={options} />
  </>
);



class Reports extends React.Component {
    constructor(props){
      super(props);
    }
    componentDidMount() {
        Hub.getReportLog(2021,7)
        .then(log => {
            let data = Hub.getJsonReportLog(log);
            console.log(data)
        //    this.setState({
        //    log : log
        //    })
        })
    }
  
    componentWillUnmount() {
    }

    render() {
      return <div className = "container">
        <h5>Reports</h5>
        <LineChart />
      </div>;
    }
  }
  
  export default Reports;