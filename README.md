# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Test work on `localhost`

1. Download entire repository
2. in file HUB_module\clarihub_GUI\src\Network.js 
    1. replace `IS_TEST = false` on `IS_TEST = true`
    2. replace address at `const HUB_ADDR = "http://192.168.2.105";` (address of the HUB)
3. install NodeJS if not installed  
    * go to https://nodejs.org/en/download/ and install NodeJS
    * open console in project directory
    * open the project catalog, for example `cd C:/2/clarihub-gui_2`
    * install npm: `npm install`
    * build the project: `npm run-script build`
4. run `npm start` in your console
 

