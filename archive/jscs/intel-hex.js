const hexLineRegexp = /:([0-9A-Fa-f]{8,})([0-9A-Fa-f]{2})(?:\r\n|\r|\n|)/g;
function checksum(bytes) {
    return (-bytes.reduce((sum, v)=>sum + v, 0)) & 0xFF;
}
function checksumTwo(array1, array2) {
    const partial1 = array1.reduce((sum, v)=>sum + v, 0);
    const partial2 = array2.reduce((sum, v)=>sum + v, 0);
    return -( partial1 + partial2 ) & 0xFF;
}
function hexpad(number) {
    return number.toString(16).toUpperCase().padStart(2, '0');
}
Number.isInteger = Number.isInteger || function(value) {
    return typeof value === 'number' &&
    isFinite(value) &&
    Math.floor(value) === value;
};
class MemoryMap {
    constructor(blocks) {
        this._blocks = new Map();
        if (blocks && typeof blocks[Symbol.iterator] === 'function') {
            for (const tuple of blocks) {
                if (!(tuple instanceof Array) || tuple.length !== 2) {
                    throw new Error('First parameter to MemoryMap constructor must be an iterable of [addr, bytes] or undefined');
                }
                this.set(tuple[0], tuple[1]);
            }
        } else if (typeof blocks === 'object') {

            const addrs = Object.keys(blocks);
            for (const addr of addrs) {
                this.set(parseInt(addr), blocks[addr]);
            }
        } else if (blocks !== undefined && blocks !== null) {
            throw new Error('First parameter to MemoryMap constructor must be an iterable of [addr, bytes] or undefined');
        }
    }
    set(addr, value) {
        if (!Number.isInteger(addr)) {
            throw new Error('Address passed to MemoryMap is not an integer');
        }
        if (addr < 0) {
            throw new Error('Address passed to MemoryMap is negative');
        }
        if (!(value instanceof Uint8Array)) {
            throw new Error('Bytes passed to MemoryMap are not an Uint8Array');
        }
        return this._blocks.set(addr, value);
    }
    get(addr)    { return this._blocks.get(addr);    }
    clear()      { return this._blocks.clear();      }
    delete(addr) { return this._blocks.delete(addr); }
    entries()    { return this._blocks.entries();    }
    forEach(callback, that) { return this._blocks.forEach(callback, that); }
    has(addr)    { return this._blocks.has(addr);    }
    keys()       { return this._blocks.keys();       }
    values()     { return this._blocks.values();     }
    get size()   { return this._blocks.size;         }
    [Symbol.iterator]() { return this._blocks[Symbol.iterator](); }
    static fromHex(hexText, maxBlockSize = Infinity) {
        const blocks = new MemoryMap();
        let lastCharacterParsed = 0;
        let matchResult;
        let recordCount = 0;
        let ulba = 0;
        hexLineRegexp.lastIndex = 0; 
        while ((matchResult = hexLineRegexp.exec(hexText)) !== null) {
            recordCount++;
            if (lastCharacterParsed !== matchResult.index) {
                throw new Error(
                    'Malformed hex file: Could not parse between characters ' +
                    lastCharacterParsed +
                    ' and ' +
                    matchResult.index +
                    ' ("' +
                    hexText.substring(lastCharacterParsed, Math.min(matchResult.index, lastCharacterParsed + 16)).trim() +
                    '")');
            }
            lastCharacterParsed = hexLineRegexp.lastIndex;
            const [, recordStr, recordChecksum] = matchResult;
            const recordBytes = new Uint8Array(recordStr.match(/[\da-f]{2}/gi).map((h)=>parseInt(h, 16)));
            const recordLength = recordBytes[0];
            if (recordLength + 4 !== recordBytes.length) {
                throw new Error('Mismatched record length at record ' + recordCount + ' (' + matchResult[0].trim() + '), expected ' + (recordLength) + ' data bytes but actual length is ' + (recordBytes.length - 4));
            }
            const cs = checksum(recordBytes);
            if (parseInt(recordChecksum, 16) !== cs) {
                throw new Error('Checksum failed at record ' + recordCount + ' (' + matchResult[0].trim() + '), should be ' + cs.toString(16) );
            }
            const offset = (recordBytes[1] << 8) + recordBytes[2];
            const recordType = recordBytes[3];
            const data = recordBytes.subarray(4);
            if (recordType === 0) {

                if (blocks.has(ulba + offset)) {
                    throw new Error('Duplicated data at record ' + recordCount + ' (' + matchResult[0].trim() + ')');
                }
                if (offset + data.length > 0x10000) {
                    throw new Error(
                        'Data at record ' +
                        recordCount +
                        ' (' +
                        matchResult[0].trim() +
                        ') wraps over 0xFFFF. This would trigger ambiguous behaviour. Please restructure your data so that for every record the data offset plus the data length do not exceed 0xFFFF.');
                }
                blocks.set( ulba + offset, data );
            } else {

                if (offset !== 0) {
                    throw new Error('Record ' + recordCount + ' (' + matchResult[0].trim() + ') must have 0000 as data offset.');
                }
                switch (recordType) {
                case 1:
                    if (lastCharacterParsed !== hexText.length) {

                        throw new Error('There is data after an EOF record at record ' + recordCount);
                    }
                    return blocks.join(maxBlockSize);
                case 2: 

                    ulba = ((data[0] << 8) + data[1]) << 4;
                    break;
                case 3:

                    break;
                case 4: 

                    ulba = ((data[0] << 8) + data[1]) << 16;
                    break;
                case 5: 

                    break;
                default:
                    throw new Error('Invalid record type 0x' + hexpad(recordType) + ' at record ' + recordCount + ' (should be between 0x00 and 0x05)');
                }
            }
        }
        if (recordCount) {
            throw new Error('No EOF record at end of file');
        } else {
            throw new Error('Malformed .hex file, could not parse any registers');
        }
    }
    join(maxBlockSize = Infinity) {

        const sortedKeys = Array.from(this.keys()).sort((a,b)=>a-b);
        const blockSizes = new Map();
        let lastBlockAddr = -1;
        let lastBlockEndAddr = -1;
        for (let i=0,l=sortedKeys.length; i<l; i++) {
            const blockAddr = sortedKeys[i];
            const blockLength = this.get(sortedKeys[i]).length;
            if (lastBlockEndAddr === blockAddr && (lastBlockEndAddr - lastBlockAddr) < maxBlockSize) {

                blockSizes.set(lastBlockAddr, blockSizes.get(lastBlockAddr) + blockLength);
                lastBlockEndAddr += blockLength;
            } else if (lastBlockEndAddr <= blockAddr) {

                blockSizes.set(blockAddr, blockLength);
                lastBlockAddr = blockAddr;
                lastBlockEndAddr = blockAddr + blockLength;
            } else {
                throw new Error('Overlapping data around address 0x' + blockAddr.toString(16));
            }
        }

        const mergedBlocks = new MemoryMap();
        let mergingBlock;
        let mergingBlockAddr = -1;
        for (let i=0,l=sortedKeys.length; i<l; i++) {
            const blockAddr = sortedKeys[i];
            if (blockSizes.has(blockAddr)) {
                mergingBlock = new Uint8Array(blockSizes.get(blockAddr));
                mergedBlocks.set(blockAddr, mergingBlock);
                mergingBlockAddr = blockAddr;
            }
            mergingBlock.set(this.get(blockAddr), blockAddr - mergingBlockAddr);
        }
        return mergedBlocks;
    }
    static overlapMemoryMaps(memoryMaps) {

        const cuts = new Set();
        for (const [, blocks] of memoryMaps) {
            for (const [address, block] of blocks) {
                cuts.add(address);
                cuts.add(address + block.length);
            }
        }
        const orderedCuts = Array.from(cuts.values()).sort((a,b)=>a-b);
        const overlaps = new Map();

        for (let i=0, l=orderedCuts.length-1; i<l; i++) {
            const cut = orderedCuts[i];
            const nextCut = orderedCuts[i+1];
            const tuples = [];
            for (const [setId, blocks] of memoryMaps) {

                const blockAddr = Array.from(blocks.keys()).reduce((acc, val)=>{
                    if (val > cut) {
                        return acc;
                    }
                    return Math.max( acc, val );
                }, -1);
                if (blockAddr !== -1) {
                    const block = blocks.get(blockAddr);
                    const subBlockStart = cut - blockAddr;
                    const subBlockEnd = nextCut - blockAddr;
                    if (subBlockStart < block.length) {
                        tuples.push([ setId, block.subarray(subBlockStart, subBlockEnd) ]);
                    }
                }
            }
            if (tuples.length) {
                overlaps.set(cut, tuples);
            }
        }
        return overlaps;
    }
    static flattenOverlaps(overlaps) {
        return new MemoryMap(
            Array.from(overlaps.entries()).map(([address, tuples]) => {
                return [address, tuples[tuples.length - 1][1] ];
            })
        );
    }

    paginate( pageSize=1024, pad=0xFF) {
        if (pageSize <= 0) {
            throw new Error('Page size must be greater than zero');
        }
        const outPages = new MemoryMap();
        let page;
        const sortedKeys = Array.from(this.keys()).sort((a,b)=>a-b);
        for (let i=0,l=sortedKeys.length; i<l; i++) {
            const blockAddr = sortedKeys[i];
            const block = this.get(blockAddr);
            const blockLength = block.length;
            const blockEnd = blockAddr + blockLength;
            for (let pageAddr = blockAddr - (blockAddr % pageSize); pageAddr < blockEnd; pageAddr += pageSize) {
                page = outPages.get(pageAddr);
                if (!page) {
                    page = new Uint8Array(pageSize);
                    page.fill(pad);
                    outPages.set(pageAddr, page);
                }
                const offset = pageAddr - blockAddr;
                let subBlock;
                if (offset <= 0) {

                    subBlock = block.subarray(0, Math.min(pageSize + offset, blockLength));
                    page.set(subBlock, -offset);
                } else {

                    subBlock = block.subarray(offset, offset + Math.min(pageSize, blockLength - offset));
                    page.set(subBlock, 0);
                }
            }
        }
        return outPages;
    }

    getUint32(offset, littleEndian) {
        const keys = Array.from(this.keys());
        for (let i=0,l=keys.length; i<l; i++) {
            const blockAddr = keys[i];
            const block = this.get(blockAddr);
            const blockLength = block.length;
            const blockEnd = blockAddr + blockLength;
            if (blockAddr <= offset && (offset+4) <= blockEnd) {
                return (new DataView(block.buffer, offset - blockAddr, 4)).getUint32(0, littleEndian);
            }
        }
        return;
    }

    asHexString(lineSize = 16) {
        let lowAddress  = 0;   
        let highAddress = -1 << 16; 
        const records = [];
        if (lineSize <=0) {
            throw new Error('Size of record must be greater than zero');
        } else if (lineSize > 255) {
            throw new Error('Size of record must be less than 256');
        }
        // Placeholders
        const offsetRecord = new Uint8Array(6);
        const recordHeader = new Uint8Array(4);
        const sortedKeys = Array.from(this.keys()).sort((a,b)=>a-b);
        for (let i=0,l=sortedKeys.length; i<l; i++) {
            const blockAddr = sortedKeys[i];
            const block = this.get(blockAddr);
            // Sanity checks
            if (!(block instanceof Uint8Array)) {
                throw new Error('Block at offset ' + blockAddr + ' is not an Uint8Array');
            }
            if (blockAddr < 0) {
                throw new Error('Block at offset ' + blockAddr + ' has a negative thus invalid address');
            }
            const blockSize = block.length;
            if (!blockSize) { continue; }  
            if (blockAddr > (highAddress + 0xFFFF)) {
                highAddress = blockAddr - blockAddr % 0x10000;
                lowAddress = 0;
                offsetRecord[0] = 2;    
                offsetRecord[1] = 0;    
                offsetRecord[2] = 0;    
                offsetRecord[3] = 4;    
                offsetRecord[4] = highAddress >> 24;  
                offsetRecord[5] = highAddress >> 16;  
                records.push(
                    ':' +
                    Array.prototype.map.call(offsetRecord, hexpad).join('') +
                    hexpad(checksum(offsetRecord))
                );
            }
            if (blockAddr < (highAddress + lowAddress)) {
                throw new Error(
                    'Block starting at 0x' +
                    blockAddr.toString(16) +
                    ' overlaps with a previous block.');
            }
            lowAddress = blockAddr % 0x10000;
            let blockOffset = 0;
            const blockEnd = blockAddr + blockSize;
            if (blockEnd > 0xFFFFFFFF) {
                throw new Error('Data cannot be over 0xFFFFFFFF');
            }

            while (highAddress + lowAddress < blockEnd) {
                if (lowAddress > 0xFFFF) {

                    highAddress += 1 << 16; 
                    lowAddress = 0;
                    offsetRecord[0] = 2;    
                    offsetRecord[1] = 0;    
                    offsetRecord[2] = 0;    
                    offsetRecord[3] = 4;    
                    offsetRecord[4] = highAddress >> 24;   
                    offsetRecord[5] = highAddress >> 16;   
                    records.push(
                        ':' +
                        Array.prototype.map.call(offsetRecord, hexpad).join('') +
                        hexpad(checksum(offsetRecord))
                    );
                }
                let recordSize = -1;

                while (lowAddress < 0x10000 && recordSize) {
                    recordSize = Math.min(
                        lineSize,                           
                        blockEnd - highAddress - lowAddress,
                        0x10000 - lowAddress                
                    );
                    if (recordSize) {
                        recordHeader[0] = recordSize;   
                        recordHeader[1] = lowAddress >> 8;    
                        recordHeader[2] = lowAddress;    
                        recordHeader[3] = 0;    
                        const subBlock = block.subarray(blockOffset, blockOffset + recordSize);  
                        records.push(
                            ':' +
                            Array.prototype.map.call(recordHeader, hexpad).join('') +
                            Array.prototype.map.call(subBlock, hexpad).join('') +
                            hexpad(checksumTwo(recordHeader, subBlock))
                        );
                        blockOffset += recordSize;
                        lowAddress += recordSize;
                    }
                }
            }
        }
        records.push(':00000001FF');   
        return records.join('\n');
    }

    clone() {
        const cloned = new MemoryMap();
        for (let [addr, value] of this) {
            cloned.set(addr, new Uint8Array(value));
        }
        return cloned;
    }

    static fromPaddedUint8Array(bytes, padByte=0xFF, minPadLength=64) {
        if (!(bytes instanceof Uint8Array)) {
            throw new Error('Bytes passed to fromPaddedUint8Array are not an Uint8Array');
        }
 
        const memMap = new MemoryMap();
        let consecutivePads = 0;
        let lastNonPad = -1;
        let firstNonPad = 0;
        let skippingBytes = false;
        const l = bytes.length;
        for (let addr = 0; addr < l; addr++) {
            const byte = bytes[addr];
            if (byte === padByte) {
                consecutivePads++;
                if (consecutivePads >= minPadLength) {

                    if (lastNonPad !== -1) {

                        memMap.set(firstNonPad, bytes.subarray(firstNonPad, lastNonPad+1));
                    }
                    skippingBytes = true;
                }
            } else {
                if (skippingBytes) {
                    skippingBytes = false;
                    firstNonPad = addr;
                }
                lastNonPad = addr;
                consecutivePads = 0;
            }
        }

        if (!skippingBytes && lastNonPad !== -1) {
            memMap.set(firstNonPad, bytes.subarray(firstNonPad, l));
        }
        return memMap;
    }

    slice(address, length = Infinity){
        if (length < 0) {
            throw new Error('Length of the slice cannot be negative');
        }
        const sliced = new MemoryMap();
        for (let [blockAddr, block] of this) {
            const blockLength = block.length;
            if ((blockAddr + blockLength) >= address && blockAddr < (address + length)) {
                const sliceStart = Math.max(address, blockAddr);
                const sliceEnd = Math.min(address + length, blockAddr + blockLength);
                const sliceLength = sliceEnd - sliceStart;
                const relativeSliceStart = sliceStart - blockAddr;
                if (sliceLength > 0) {
                    sliced.set(sliceStart, block.subarray(relativeSliceStart, relativeSliceStart + sliceLength));
                }
            }
        }
        return sliced;
    }

    slicePad(address, length, padByte=0xFF){
        if (length < 0) {
            throw new Error('Length of the slice cannot be negative');
        }
        
        const out = (new Uint8Array(length)).fill(padByte);
        for (let [blockAddr, block] of this) {
            const blockLength = block.length;
            if ((blockAddr + blockLength) >= address && blockAddr < (address + length)) {
                const sliceStart = Math.max(address, blockAddr);
                const sliceEnd = Math.min(address + length, blockAddr + blockLength);
                const sliceLength = sliceEnd - sliceStart;
                const relativeSliceStart = sliceStart - blockAddr;
                if (sliceLength > 0) {
                    out.set(block.subarray(relativeSliceStart, relativeSliceStart + sliceLength), sliceStart - address);
                }
            }
        }
        return out;
    }

    contains(memMap) {
        for (let [blockAddr, block] of memMap) {
            const blockLength = block.length;
            const slice = this.slice(blockAddr, blockLength).join().get(blockAddr);
            if ((!slice) || slice.length !== blockLength ) {
                return false;
            }
            for (const i in block) {
                if (block[i] !== slice[i]) {
                    return false;
                }
            }
        }
        return true;
    }
}
