function changeCalendarDateFormat(select_el) {
	calendarDateFormat = select_el.value;
}
function showSettingsList() {
	var settingsList = ["SendTime","DateFormat","WiFi","WebPanel","Coords","DateTime","UpdateFirmware","Logout"];
	if (userDeviceType == 'desktop') {
		showSettingsListDesktop(settingsList)
	}
	if (userDeviceType == 'mobile') {
		showSettingsListMob(settingsList)
	}
}
function showSettingsData(settingsName,caller) {
	selectedSettings = settingsName;
	var settingsData = "<p align = 'center'><strong>"+settingsName+"</strong></p>";
	switch (settingsName) {
		case ("SendTime") : {
			sendRealTimeToHub(caller);
			break;
		}
		case ("DateFormat") : {
			settingsData += getDateFormatSettingsData();
			break;
		}
		case ("WebPanel") : {
			settingsData +=  getWebPanelSettingsData();
			break;
		}
		case ("WiFi") : {
			settingsData += getWiFiSettingsData();
			break;
		}
		case ("Coords") : {
			settingsData +=	getCordsSettingsData();
			break;
		}
		case ("DateTime") : {
			settingsData +=	getDateTimeSettingsData();
			break;
		}
		case ("UpdateFirmware") : {
			settingsData +=	getUpdateFirmwareSettings();
			break;
		}
		case ("Logout") : {
			result = confirm("Log out ?");
			if (result) {
				document.cookie = "session=0";
				window.location.href = '/login/';
			}
			break;
		}
	}
	if (settingsName != "SendTime" && settingsName != "Logout") {
		if (userDeviceType == 'desktop') {
			document.getElementById("settingsDataBlock").innerHTML = settingsData;
		}
		if (userDeviceType == 'mobile') {
			document.getElementById("Settings").innerHTML = "<div class = 'settingsMobBlock'>"+settingsData+"</div>";
		}
		if(document.querySelector('form')) {
			document.querySelector('form').onsubmit = function(e) {
				e.preventDefault();
				return applySettings();
			};
		}
	}
}
function getDateFormatSettingsData() {
	return	"<ul>"+
			"	<li>"+
			"		<label>date format :</label>"+
			"		<select onchange='if (this.selectedIndex || this.selectedIndex == 0) changeCalendarDateFormat(this);'>"+
			"			<option value = '0'>default</option>"+
			"			<option value = 't_mdy'>MM-DD-YYYY</option>"+
			"			<option value = 't_ymd'>YYYY-MM-DD</option>"+
			"			<option value = 'p_dmy'>DD.MM.YYYY</option>"+
			"			<option value = 't_dmy'>DD-MM-YYYY</option>"+
			"			<option value = 's_dmy'>DD/MM/YYYY</option>"+
			"		</select>"+
			"	</li>"+
			"</ul>";
}
function getWiFiSettingsData() {
	var settingsBtns = 	"<button type = 'submit'>OK</button></td>";
	if (userDeviceType == "mobile") {
		settingsBtns += "<button onclick = 'showSettingsList();'>Cansel</button>";
	}
	return	"<form>"+
			"  <table>"+
			"	<tr>"+
			"		<td>WiFi SSID :</td>"+
			"		<td><input type = 'text'/></td>"+
			"	</tr>"+
			"	<tr>"+
			"		<td>WiFi password :</td>"+
			"		<td><input type = 'password'/></td>"+
			"	</tr>"+
			"  </table>"+
				settingsBtns+
			"</form>";
}
function getWebPanelSettingsData() {
	var settingsBtns = 	"<button type = 'submit'>OK</button></td>";
	if (userDeviceType == "mobile") {
		settingsBtns += "<button onclick = 'showSettingsList();'>Cansel</button>";
	}
	return	"<form>"+
			"	<table>"+
			"		<tr>"+
			"			<td>old password:</td>"+
			"			<td><input id = 'old_password' type = 'password'></td>"+
			"		</tr>"+
			"		<tr>"+
			"			<td>new password:</td>"+
			"			<td><input id = 'new_password' type = 'password'></td>"+
			"		</tr>"+
			"		<tr>"+
			"			<td>confirm password :</td>"+
			"			<td><input id = 'confirm_password' type = 'password'></td>"+
			"		</tr>"+
			"		<tr id = 'loadingtr'>"+
		//	"			<td>&nbsp</td>"+
		//	"			<td><img src = 'images/loading.gif' style = 'margin-left:150px';/></td>"+
			"		</tr>"+
			"	</table>"+
				settingsBtns+
			"</form>";
}
function getCordsSettingsData() {
	var settingsBtns = 	"<button type = 'submit'>OK</button></td>";
	if (userDeviceType == "mobile") {
		settingsBtns += "<button onclick = 'showSettingsList();'>Cansel</button>";
	}
	return	"<form>"+
			"  <table>"+
			"	<tr>"+
			"		<td>N: </td>"+
			"		<td><input type = 'text'/></td>"+
			"	</tr>"+
			"	<tr>"+
			"		<td>E: </td>"+
			"		<td><input type = 'text'/></td>"+
			"	</tr>"+
			"  </table>"+
				settingsBtns+
			"</form>";
}
function getDateTimeSettingsData() {
	var settingsBtns = 	"<button type = 'submit'>OK</button>";
	if (userDeviceType == "mobile") {
		settingsBtns += "<button onclick = 'showSettingsList();'>Cansel</button>";
	}
	var dateInput;
	if (calendarDateFormat == 0) {
		dateInput = " <input type='date' id='date'>";
	} else {
		dateInput = " <input id = 'myDateInput1' class='calendar_input' onclick = 'showCalendar(this)'/ readonly>";
	}
	return	"<form>"+
			"  <table>"+
			"	<tr>"+
			"		<td>date: </td>"+
			"		<td>"+dateInput+"</td>"+
			"	</tr>"+
			"	<tr>"+
			"		<td>time: </td>"+
			"		<td><input type = 'time'/></td>"+
			"	</tr>"+
			"	<tr>"+
			"		<td>timeZone: </td>"+
			"		<td><input type = 'text'/></td>"+
			"	</tr>"+
			"  </table>"+
				settingsBtns+
			"</form>";
}
function getUpdateFirmwareSettings() {
	var settingsBtns = 	"<button id = 'updateFirmwareBtn' type = 'submit'>OK</button>";
	if (userDeviceType == "mobile") {
		settingsBtns += "<button onclick = 'showSettingsList();'>Cansel</button>";
	}
	var devOptionsList = //	'<option value = "hub">Main HUB</option>'+
							'<option value = "mega">Mega</option>';
	//						'<option value = "all_ps">All PowerStrips</option>';
//	for ( var ps of getPowerStripsList() ) {
//		devOptionsList += '<option value = "'+ps.id+'">ps: '+ps.specifedName+'</option>'
//	}
	return 	"<form>"+
			"  <table>"+
			"	<tr>"+
			"		<td>device: </td>"+
			"		<td><select id = 'firmware_select' onchange='if (this.selectedIndex || this.selectedIndex == 0) changeDeviceForFirmware(this);'>"+
					devOptionsList+
			"		</select></td>"+
			"	</tr>"+
			"	<tr>"+
			"		<td>firmware: </td>"+
			"		<td><input id = 'firmware_input' type = 'file' accept = '.hex' style = 'border : none; width : 300px;'></td>"+
			"	</tr>"+
			"  </table>"+
				settingsBtns+
			"</form>";
}
async function sendRealTimeToHub(caller) {
	var oldCallerVal = caller.value;
	caller.value = 'time_sending...';
	var bodyData =  ":nt:"+Number((new Date())/1000).toFixed()+":2::";
	fetch("/setTime/",{
		method: 'POST',
		body: bodyData
	})
	.then(function(response){
		if (response.status !== 200) {
			alert(response.status);
			caller.disabled = false;
			caller.value = oldCallerVal;
			return;
		}
		response.json().then(function(jsonResp){
			caller.disabled = false;
			caller.value = oldCallerVal;
			if (jsonResp.result != "OK") {
				alert(function(){
					switch (jsonResp.result) {
						case ("badcm") : {
							return "error : not nt at start request";
						}
						case ("badtime") : {
							return "error : time year must be at [2000-2060]";
						}
						case ("badtz") : {
							return "time zone error";
						}
						default : {
							return "unknown error";
						}
					}
				});
			}
		})
	})
	.catch(function(err){alert(err)});
}
function changeDeviceForFirmware(sel) {
	var input = document.getElementById("firmware_input");
	if (sel.value == 'mega') {
		input.accept = '.hex';
	} else {
		input.accept = '.bin';
	}
}
function getPowerStripsList() {
	var res = [];
	for (var[key,value] of devicesMap) {
		if (value instanceof PowerStrip) {
			res.push(value);
		}
	}
	return res;
}
function applySettings() {
	switch (selectedSettings) {
		case ('DateFormat') : {
			break;
		}
		case ('WiFi') : {
			break;
		}
		case ('WebPanel') : {
			applyWebPanelSettings();
			break;
		}
		case ('Coords') : {
			break;
		}
		case ('DateTime') : {
			break;
		}
		case ('UpdateFirmware') : {
			updateFirmwareSettings();
			break;
		}
	}
}
async function applyWebPanelSettings() {
	document.getElementById("loadingLI").innerHTML = "<img src = 'images/loading.gif' style = 'margin-left:150px'/>";
	document.getElementById("formOk").disabled = true;
	var oldPass = document.getElementById("old_password").value;
	var newPass = document.getElementById("new_password").value;
	var confirmPass = document.getElementById("confirm_password").value;
	if (newPass == confirmPass) {
		fetch("/configs/srvPass.dat")
		.then(function(response){
			if (response.status == 200) {
				response.text().then(function(pass){
					if (pass != oldPass) {
						alert("wrong old password");
						document.getElementById("loadingLI").innerHTML = "";
						document.getElementById("formOk").disabled = false;
					} else {
						var file =  new File([newPass], "file", {
										type: "text/plain",
								});
						var data = new FormData()
						data.append('file', file)
						fetch('/updMP/configs/srvPass.dat', {
							method: 'POST',
							body: data
						})
						.then(function(resultPass){
							if (resultPass.status == 200) {
								alert("password change");
								document.getElementById("loadingLI").innerHTML = "";
								document.getElementById("formOk").disabled = false;
							} else {
								alert ("fetch error; response.status = "+response.status);
								document.getElementById("loadingLI").innerHTML = "";
								document.getElementById("formOk").disabled = false;
							}
						})
					}
				})
			} else {
				alert ("fetch error; response.status = "+response.status);
				document.getElementById("loadingLI").innerHTML = "";
				document.getElementById("formOk").disabled = false;
			}
		})
		.catch(function (err){
			alert ("fetch error; error : "+err);
			document.getElementById("loadingLI").innerHTML = "";
			document.getElementById("formOk").disabled = false;
		})
	} else {
		alert("passwords mismatch (new and confirm)");
		document.getElementById("loadingLI").innerHTML = "";
		document.getElementById("formOk").disabled = false;
	}
}
function getFirmwareSize(firmware_len) {
	var temp = (firmware_len).toString(16);
	var res = ['0','0','0','0','0','0','0','0'];
	for (var i = 7, tempIdx = temp.length-1; tempIdx >= 0; i--, tempIdx--) {
		res[i] = temp[tempIdx];
	}
	return res.join('').toUpperCase();
}
function convertFirmwareToHexString(firmware) {
	var res = '';
	for (var i = 0; i < firmware.length; i++) {
		var temp = firmware[i].toString(16);
		if (temp.length < 2) temp = '0'+temp;
		res += temp;
	}
	return res.toUpperCase();
}
async function updateFirmwareSettings() { // temp function
	document.getElementById('updateFirmwareBtn').disabled = true;
	var updDev = document.getElementById('firmware_select').value;
	var firmware = document.getElementById('firmware_input').files[0];
	if (!firmware){
		alert("firmware is not selected");
		return;
	}
	getFileData(firmware,updDev)
	.then(function(fileData){
			if (updDev == "mega") {
				fileData = hexFirmwareParse(fileData);
				var megaID = '1E9801';
				var crc = '00000000';
				var data =  megaID + crc + getFirmwareSize(fileData[0].length);
				fileData = [data + convertFirmwareToHexString(fileData[0])];
				firmware = new File(fileData, "firmware.hex", {
						type: "text/plain",
				});
				var form_data = new FormData()
				form_data.append('file', firmware)
			} else {
				document.getElementById('updateFirmwareBtn').disabled = false;
				return;
			} 
		fetch("/updMP/flashm.hex",{
			method: 'POST',
			body: form_data
		})
		.then(function(response){
			if (response.status == 200) {
				fetch("/flashAVR/flashm.hex")
				.then(function(useFirmwareResponse){
					if(useFirmwareResponse.status == 200) {
						useFirmwareResponse.json().then(function(jsonRes){
							if (jsonRes.result == "0") {
								alert ("firmware applied successfully");
							} else if (jsonRes.result == "1") {
								alert ("ferror : bad filename");
							} else if (jsonRes.result == "2") {
								alert ("ferror :  can't open the file");
							} else if (jsonRes.result == "3") {
								alert ("ferror :  file is too short");
							} else if (jsonRes.result == "4") {
								alert ("ferror :   chip signature mismatch");
							} else if (jsonRes.result == "5") {
								alert ("ferror :  flash file is too short or corrupted");
							}
						})
					} else {
						alert ("(firmware not applied) err code : "+response.status)
					}
				})
			} else {
				alert ("(firmware not upload) err code : "+response.status)
			}
			document.getElementById('updateFirmwareBtn').disabled = false;
		})
		.catch(function(err){
			alert(err);
			document.getElementById('updateFirmwareBtn').disabled = false;
			return;
		})
	})
	
}
async function _updateFirmwareSettings() { // real function
	var updDev = document.getElementById('firmware_select').value;
	var firmware = document.getElementById('firmware_input').files[0];
	if (!firmware){
		alert("firmware is not selected");
		return;
	}
	var devType;
	var devUid = '000000000000';
	var flqueryBody;
	getFileData(firmware,updDev)
	.then(function(fileData){
			var isBytesData = true;
			if (updDev == "mega") {
				devType = 1;
				fileData = hexFirmwareParse(fileData);
				firmware = new File(fileData, "firmware.hex", {
						type: "text/plain",
				});
			//	download(firmware);
				for (var [key,value] of devicesMap) {
					if (value instanceof Mega) {
						devUid = value.id;
					}
				}
			} else if (updDev == "hub") {
			//	download(firmware);
				devType = 0;
			} else if (updDev == "all_ps") {
				return;
			} else {
				// power strip
				devType = 2;
				devUid = updDev;
			}
		flqueryBody = [devType,devUid,firmware.size,crc16_BUYPASS(fileData,isBytesData)].join(";");
		fetch("/flquery/",{
			method: 'POST',
			body: flqueryBody
		})
		.then(function(response){
			if (response.status == 200) {
				response.json().then(function(jsonResp){
					if (jsonResp.result == "ready") {
						formData = new FormData();
						formData.append('file', firmware);
						fetch("/pflash/", {
							method: 'POST',
							body: formData
						})
						.then(function(response_pflash){
							if (response_pflash.status == 200) {
								response_pflash.json().then(function(jsonResp_pflash){
									if (jsonResp_pflash.result = "prfm") {
										allowUpdateDevices = false;
										var intervalID = setInterval(() => {
											fetch()
											.then(function(uploadResponse){
												if(uploadResponse.status == 200) {
													uploadResponse.json().then(function(uploadResult){
														if (uploadResult.result == "ready") {
															clearInterval(intervalID);
															allowUpdateDevices = true;
														}
													})
												}
											})
										},10000)
										
									} else {
										alert("error : "+jsonResp_pflash.result);
										return;
									}
								})
							} else {
								alert(response_pflash.status);
								return;
							}
						})
					} else {
						alert("error : "+jsonResp.result);
						return;
					}
				})
			} else {
				alert(response.status);
				return;
			}
		})
		.catch(function(err){
			alert(err);
			return;
		})
	})
}
function getFileData(file,updDev) {
	var fileReader = new FileReader();
	if (updDev == "mega") {
		fileReader.readAsText(file);
	} else {
		fileReader.readAsArrayBuffer(file);
	}
	
	return 	new Promise((resolve, reject) => {
			fileReader.onload = function() {
				if (updDev == "mega") {
					resolve(fileReader.result)
				} else {
					resolve(new Uint8Array(fileReader.result))
				}
				
			}
		});
}
function hexFirmwareParse(fileData) {
	let memMap = MemoryMap.fromHex(fileData);
	var resultDataArrays = [];
	var lastAddr = 0;
	var lastBlockSize = 0;
	for (let [address, dataBlock] of memMap) {
		if (lastBlockSize) {
			var length = address - (lastAddr+lastBlockSize);
			var emptyBlock = new Uint8Array(length);
			resultDataArrays.push(emptyBlock);
		} else {
			if (address) {
				var length = address;
				var emptyBlock = new Uint8Array(length);
				resultDataArrays.push(emptyBlock);
			}
		}
		lastAddr = address;
		lastBlockSize = dataBlock.length;
		resultDataArrays.push(dataBlock);
	}
	return resultDataArrays;
}
function downloadFile(file) {
    var a = document.createElement("a"),
                url = URL.createObjectURL(file);
        a.href = url;
        a.download = file.name;
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);  
        }, 0); 
}
