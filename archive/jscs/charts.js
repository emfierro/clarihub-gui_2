function showDevicesToChartSelecData() {
    var selectElem = document.getElementById("chartDevSelect");
    var selData = "";
    for(var [deviceId, device] of devicesMap) {
        if (device instanceof MegaSensorChannel) {
            selData += "<option value = \""+deviceId+"\">"+device.specifedName+"</option>";
        }
    }
    selectElem.innerHTML = selData;
}
function getChartData(selectedDeviceName) {
    var label = selectedDeviceName;
    var dataArr = [];
    var labsArr = [];
    for (var i = 0; i < 24; i++) {
        labsArr.push(i+"");
        dataArr.push(Math.random()*50);
    }
    return {
        labels : labsArr,
        datasets : [{
            label: label,
            data: dataArr,
            fill : false,
            borderColor: 'orange'
        }],
    }
}
function showChart(selectElem) {
    restoreChartTimeDiapasonBlock();
    var dev = devicesMap.get(selectElem.value);
    if (!dev) return; 
    var dateNow = new Date();
    var dateView = "0"+Number(dateNow.getMonth()+1)+"/"+ dateNow.getDate() + "/" + dateNow.getFullYear();
    document.getElementById("chartDateP").innerHTML = "Chart for channel ["+dev.specifedName+"] by ["+dateView+"]";
    chartData = getChartData(dev.specifedName);
    document.getElementById('devChart').remove();
    document.getElementById('chartP').innerHTML = '<canvas id="devChart"></canvas>';
    var ctx = document.getElementById('devChart');
    var options =  {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    };


    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: chartData,
        options: options
    });
}

function showChartTimeDiapason(selEl) {
    switch (selEl.value) {
        case ("today") : {
            document.getElementById("chartTimeDiapason").innerHTML = "";
            break;
        }
        case ("date") : {
            document.getElementById("chartTimeDiapason").innerHTML = "<input type = 'date' onchange = ''>";
            break;
        }
        case ("diapason") : {
            document.getElementById("chartTimeDiapason").innerHTML = "start : <input type = 'date'> finish : <input type = 'date'>";
            break;
        }
    }
}

function showChartBlock() {
    showDevicesToChartSelecData();
    showChart(document.getElementById("chartDevSelect"));
}

function restoreChartTimeDiapasonBlock() {
    document.getElementById("chartTimeSelect").value = 'today';
    document.getElementById("chartTimeDiapason").innerHTML = "";
}