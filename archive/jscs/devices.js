class Mega {
	constructor(id,lastUpdate, specifedName, isEnable, isOnline) {
		this.type= "Mega";
		this.id = id;
		this.lastUpdate = lastUpdate;
		this.specifedName = specifedName;
		this.isEnable = isEnable;
		this.isOnline = isOnline;
		this.sensors = [];
	}
	showData(id) {
		window.redrawCallback = Mega.prototype.showData;
		window.redrawObjId=id;
		var device = devicesMap.get(id);
		var data = "<p align = 'center'><strong>"+device.type+" ["+device.specifedName+"]</strong></p>";
		if (device.isEnable) {
			data = data +
				"<hr/>"+
				"<table align = 'center'>"+
					"<tr>"+
						"<td align = 'left'><strong>device ID:</strong></td>"+
						"<td align = 'left'>"+device.id+"</td>"+
					"</tr>"+
					"<tr>"+
						"<td align = 'left'><strong>device type:</strong></td>"+
						"<td align = 'left'>"+device.type+"</td>"+
					"</tr>"+
					"<tr>"+
						"<td align = 'left'><strong>lastUpdate:</strong></td>"+
						"<td align = 'left'>"+device.lastUpdate+"</td>"+
					"</tr>"+
					"<tr>"+
						"<td align = 'left'><strong>isOnline:</strong></td>"+
						"<td align = 'left'>"+device.isOnline+"</td>"+
					"</tr>"+
				"<table>";
				
		} else {
			data = data +
				"<hr/>"+
				"<table align = 'center'>"+
					"<tr>"+
						"<td align = 'left'><strong>device ID:</strong></td>"+
						"<td align = 'left'>"+device.id+"</td>"+
					"</tr>"+
					"<tr>"+
						"<td align = 'left'><strong>device type:</strong></td>"+
						"<td align = 'left'>"+device.type+"</td>"+
					"</tr>"+
					"<tr>"+
						"<td align = 'left'><strong>lastUpdate:</strong></td>"+
						"<td align = 'left'>"+device.lastUpdate+"</td>"+
					"</tr>"+
				"<table>";
		}
		if (userDeviceType == 'desktop') {
			data = data + 
				"<ul>"+
					"<li class='dialogLiButton'>"+
						"<button onclick = 'removeDevice(\""+id+"\")'><img src = 'images/remove.png'></button>"+
						"<button onclick = 'showGraph()'><img src = 'images/graph.png'></button>"+
						"<button onclick = 'showDeviceSettings(\""+id+"\")'><img src='images/settings.png'></button>"+
					"</li>"+
				"</ul>";
			var e = document.getElementById("devicesDataBlock");
			if(e) e.innerHTML = data;
		}
		if (userDeviceType == 'mobile') {
			oldViewDevices.push(document.getElementById("devicesBlock").innerHTML);
			data = data + 
				"<div class = 'footer'>"+
					"<button onclick = 'removeDevice(\""+id+"\")'>Remove</button>"+
					"<button onclick = 'showDeviceSettings(\""+id+"\")'>Settings</button>"+
					"<button onclick = 'showDeviceChildsMob(\""+id+"\")'>Sensors</button>"+
					"<button onclick = 'returnBack()'>&lt;&lt;</button>"+
				"</div>";
			data = "<div id = 'devicesDataBlockMob'>"+data+"</div>";
			document.getElementById("devicesBlock").innerHTML = data;
			document.getElementById("devicesDataBlockMob").setAttribute("style", "padding-top:50%;padding-bottom:90%;");
		}
		currentShowData = id;
	}
}

class MegaSensor {
	constructor(id,isEnable, specifedName, sensorType, parent) {
		this.parent = parent;
		this.type = "MegaSensor";
		this.sensorType = sensorType;
		this.id = id;
		this.specifedName = specifedName;
		this.isEnable = isEnable;
		this.channels = [];
	}
	showData(id) {
		window.redrawCallback = MegaSensor.prototype.showData;
		window.redrawObjId=id;
		var device = devicesMap.get(id);
		// ------------
		var data = "<p align = 'center'><strong>"+device.type+" ["+device.specifedName+"]</strong></p>";
		data = data +
			"<hr/>"+
			"<table align = 'center'>"+
				"<tr>"+
					"<td align = 'left'><strong>device ID:</strong></td>"+
					"<td align = 'left'>"+device.id+"</td>"+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>device type:</strong></td>"+
					"<td align = 'left'>"+device.type+"</td>"+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>sensorType:</strong></td>"+
					"<td align = 'left'>"+device.sensorType+"</td>"+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>isEnable:</strong></td>"+
					"<td align = 'left'>"+device.isEnable+"</td>"+
				"</tr>"+
			"<table>";
				
		if (device.isEnable) {
			for (var channel of device.channels) {
				data += "<p align = 'center'><strong>"+channel.specifedName+"</strong></p>"+
					"<table><tr>"+
						"<td><strong>type: </strong></td>"+
						"<td><span style = 'margin-left : 10px'>"+channel.type+"</span></td>"+
					"</tr>"+
					"<tr>"+
						"<td><strong>value: </strong></td>"+
						"<td><span style = 'margin-left : 10px'>"+channel.value+"</span></td>"+
					"</tr>"+
			//		"<tr>"+
			//			"<td><strong>corrector1: </strong></td>"+
			//			"<td><span style = 'margin-left : 10px'>"+channel.corrector1+"</span></td>"+
			//		"</tr>"+
			//		"<tr>"+
			//			"<td><strong>corrector2: </strong></td>"+
			//			"<td><span style = 'margin-left : 10px'>"+channel.corrector2+"</span></td>"+
			//		"</tr>
					"</table>";
			}
		/*	data = data +
				"<hr/>"+
				"<table align = 'center'>"+
					"<tr>"+
						"<td align = 'left'><strong>device ID:</strong></td>"+
						"<td align = 'left'>"+device.id+"</td>"+
					"</tr>"+
					"<tr>"+
						"<td align = 'left'><strong>device type:</strong></td>"+
						"<td align = 'left'>"+device.type+"</td>"+
					"</tr>"+
					"<tr>"+
						"<td align = 'left'><strong>lastUpdate:</strong></td>"+
						"<td align = 'left'>"+device.lastUpdate+"</td>"+
					"</tr>"+
				"<table>";*/
		}
		if (userDeviceType == 'desktop') {
			data = data + 
				"<ul>"+
					"<li class='dialogLiButton'>"+
						"<button onclick = 'removeDevice(\""+id+"\")'><img src = 'images/remove.png'></button>"+
						"<button onclick = 'showGraph()'><img src = 'images/graph.png'></button>"+
						"<button onclick = 'showDeviceSettings(\""+id+"\")'><img src='images/settings.png'></button>"+
					"</li>"+
				"</ul>";
				var e = document.getElementById("devicesDataBlock");
				if(e) e.innerHTML = data;
		}
		if (userDeviceType == 'mobile') {
			oldViewDevices.push(document.getElementById("devicesBlock").innerHTML);
			data = data + 
				"<div class = 'footer'>"+
					"<button onclick = 'removeDevice(\""+id+"\")'>Remove</button>"+
					"<button onclick = 'returnBack()'>&lt;&lt;</button>"+
				"</div>";
			data = "<div id = 'devicesDataBlockMob'>"+data+"</div>";
			document.getElementById("devicesBlock").innerHTML = data;
			document.getElementById("devicesDataBlockMob").setAttribute("style", "padding-top:50%;padding-bottom:90%;");
		}
		// ------------
		
		var e = document.getElementById("devicesDataBlock");
			if (e) e.innerHTML = data;
		currentShowData = id;
	}
}

class MegaSensorChannel {
	constructor(id, type, value, corrector1, corrector2, specifedName, parent) {
		this.parent = parent;
		this.id = id;
		this.type = type;
		this.value = value;
		this.corrector1 = corrector1;
		this.corrector2 = corrector2;
		this.specifedName = specifedName;
		this.nestingLevel = 3;
		this.schEnable = true;
	}
}

class PowerStrip {
	constructor(id,lastUpdate,specifedName, isEnable, isOnline) {
		this.type= "PowerStrip";
		this.id = id;
		this.lastUpdate = lastUpdate;
		this.specifedName = specifedName;
		this.isEnable = isEnable;
		this.isOnline = isOnline;
		this.outlets = [];
		this.pwmsOutput12V = [];
		this.pwmsOutput5V = [];
		this.solenoids24vac = [];
		this.analogInputs = [];
		this.nestingLevel = 1;
	}
	showData(id) {
		window.redrawCallback = PowerStrip.prototype.showData;
		window.redrawObjId=id;
		var device = devicesMap.get(id);
		var data = "<p align = 'center'><strong>"+device.type+" ["+device.specifedName+"]</strong></p>";
		if (device.isEnable) {
			data = data +
				"<hr/>"+
				"<table align = 'center'>"+
					"<tr>"+
						"<td align = 'left'><strong>device ID:</strong></td>"+
						"<td align = 'left'>"+device.id+"</td>"+
					"</tr>"+
					"<tr>"+
						"<td align = 'left'><strong>device type:</strong></td>"+
						"<td align = 'left'>"+device.type+"</td>"+
					"</tr>"+
					"<tr>"+
						"<td align = 'left'><strong>lastUpdate:</strong></td>"+
						"<td align = 'left'>"+device.lastUpdate+"</td>"+
					"</tr>"+
					"<tr>"+
						"<td align = 'left'><strong>isOnline:</strong></td>"+
						"<td align = 'left'>"+device.isOnline+"</td>"+
					"</tr>"+
				"<table>";
				
		} else {
			data = data +
				"<hr/>"+
				"<table align = 'center'>"+
					"<tr>"+
						"<td align = 'left'><strong>device ID:</strong></td>"+
						"<td align = 'left'>"+device.id+"</td>"+
					"</tr>"+
					"<tr>"+
						"<td align = 'left'><strong>device type:</strong></td>"+
						"<td align = 'left'>"+device.type+"</td>"+
					"</tr>"+
					"<tr>"+
						"<td align = 'left'><strong>lastUpdate:</strong></td>"+
						"<td align = 'left'>"+device.lastUpdate+"</td>"+
					"</tr>"+
				"<table>";
		}
		if (userDeviceType == 'desktop') {
			data = data + 
				"<ul>"+
					"<li class='dialogLiButton'>"+
						"<button onclick = 'removeDevice(\""+id+"\")'><img src = 'images/remove.png'></button>"+
						"<button onclick = 'showGraph()'><img src = 'images/graph.png'></button>"+
						"<button onclick = 'showDeviceSettings(\""+id+"\")'><img src='images/settings.png'></button>"+
					"</li>"+
				"</ul>";
				var e = document.getElementById("devicesDataBlock");
				if(e) e.innerHTML = data;
		}
		if (userDeviceType == 'mobile') {
			oldViewDevices.push(document.getElementById("devicesBlock").innerHTML);
			data = data + 
				"<div class = 'footer'>"+
					"<button onclick = 'removeDevice(\""+id+"\")'>Remove</button>"+
					"<button onclick = 'showDeviceSettings(\""+id+"\")'>Settings</button>"+
					"<button onclick = 'showDeviceChildsMob(\""+id+"\")'>Sensors</button>"+
					"<button onclick = 'returnBack()'>&lt;&lt;</button>"+
				"</div>";
			data = "<div id = 'devicesDataBlockMob'>"+data+"</div>";
			document.getElementById("devicesBlock").innerHTML = data;
			document.getElementById("devicesDataBlockMob").setAttribute("style", "padding-top:50%;padding-bottom:90%;");
		}
		currentShowData = id;
	}
}

class Outlet {
	constructor(id,currentSensor, isEnable, overCurrentFlag, specifedName, parent, arrayIndex){
		this.arrayIndex = arrayIndex;
		this.parent = parent;
		this.id = id;
		this.type = "outlet";
		this.currentSensor = currentSensor;
		this.isEnable = isEnable;
		this.overCurrentFlag = overCurrentFlag;
		this.specifedName = specifedName;
		this.schEnable = true;
	}
	showData(id) {
		window.redrawCallback = Outlet.prototype.showData;
		window.redrawObjId=id;
		var device = devicesMap.get(id);
		var data = "<p align = 'center'><strong>"+device.type+" ["+device.specifedName+"]</strong></p>";
		data = data +
			"<hr/>"+
			"<table align = 'center'>"+
				"<tr>"+
					"<td align = 'left'><strong>type:</strong></td>"+
					"<td align = 'left'>"+device.type+"</td>"+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>currentSensor:</strong></td>"+
					"<td align = 'left'>"+device.currentSensor+"</td>"+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>isEnable:</strong></td>"+
					"<td align = 'left'>"+device.isEnable+"</td>"+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>overCurrentFlag:</strong></td>"+
					"<td align = 'left'>"+device.overCurrentFlag+"</td>"+
				"</tr>"+
			"<table>";
		if (userDeviceType == 'desktop') {
			data = data + 
				"<ul>"+
					"<li class='dialogLiButton'>"+
						"<button onclick = 'showGraph()'><img src = 'images/graph.png'></button>"+
						"<button onclick = 'showDeviceSettings(\""+id+"\")'><img src='images/settings.png'></button>"+
					"</li>"+
				"</ul>";
			var e = document.getElementById("devicesDataBlock");
			if (e) e.innerHTML = data;
		}
		if (userDeviceType == 'mobile') {
			oldViewDevices.push(document.getElementById("devicesBlock").innerHTML);
			data = data + 
				"<div class = 'footer'>"+
					"<button onclick = 'showDeviceSettings(\""+id+"\")'>Settings</button>"+
					"<button onclick = 'returnBack()'>&lt;&lt;</button>"+
				"</div>";
			data = "<div id = 'devicesDataBlockMob'>"+data+"</div>";
			document.getElementById("devicesBlock").innerHTML = data;
			document.getElementById("devicesDataBlockMob").setAttribute("style", "padding-top:50%;padding-bottom:90%;");
		}
		currentShowData = id;
	}
}

class PWM_Output12V {
	constructor(id, value, isEnable, specifedName, parent, arrayIndex) {
		this.parent = parent;
		this.arrayIndex = arrayIndex;
		this.id = id;
		this.type= "out12v";
		this.value = value;
		this.isEnable = isEnable;
		this.specifedName = specifedName;
	}
	showData(id) {
		window.redrawCallback = PWM_Output12V.prototype.showData;
		window.redrawObjId=id;
		var device = devicesMap.get(id);
		var data = "<p align = 'center'><strong>"+device.type+" ["+device.specifedName+"]</strong></p>";
		data = data +
			"<hr/>"+
			"<table align = 'center'>"+
				"<tr>"+
					"<td align = 'left'><strong>type:</strong></td>"+
					"<td align = 'left'>"+device.type+"</td>"+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>value:</strong></td>"+
					"<td align = 'left'>"+device.value+"</td>"+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>isEnable:</strong></td>"+
					"<td align = 'left'>"+device.isEnable+"</td>"+
				"</tr>"+
			"<table>";
		if (userDeviceType == 'desktop') {
			data = data + 
				"<ul>"+
					"<li class='dialogLiButton'>"+
						"<button onclick = 'showGraph()'><img src = 'images/graph.png'></button>"+
						"<button onclick = 'showDeviceSettings(\""+id+"\")'><img src='images/settings.png'></button>"+
					"</li>"+
				"</ul>";
			var e = document.getElementById("devicesDataBlock");
			if (e) e.innerHTML = data;
		}
		if (userDeviceType == 'mobile') {
			oldViewDevices.push(document.getElementById("devicesBlock").innerHTML);
			data = data + 
				"<div class = 'footer'>"+
					"<button onclick = 'showDeviceSettings(\""+id+"\")'>Settings</button>"+
					"<button onclick = 'returnBack()'>&lt;&lt;</button>"+
				"</div>";
			data = "<div id = 'devicesDataBlockMob'>"+data+"</div>";
			document.getElementById("devicesBlock").innerHTML = data;
			document.getElementById("devicesDataBlockMob").setAttribute("style", "padding-top:50%;padding-bottom:90%;");
		}
		currentShowData = id;
	}
}

class PWM_Output5V {
	constructor(id, value, isEnable, specifedName, parent, arrayIndex) {
		this.parent = parent;
		this.arrayIndex = arrayIndex;
		this.id = id;
		this.type= "out5v";
		this.value = value;
		this.isEnable = isEnable;
		this.specifedName = specifedName;
	}
	showData(id) {
		window.redrawCallback = PWM_Output5V.prototype.showData;
		window.redrawObjId=id;
		var device = devicesMap.get(id);
		var data = "<p align = 'center'><strong>"+device.type+" ["+device.specifedName+"]</strong></p>";
		data = data +
			"<hr/>"+
			"<table align = 'center'>"+
				"<tr>"+
					"<td align = 'left'><strong>type:</strong></td>"+
					"<td align = 'left'>"+device.type+"</td>"+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>value:</strong></td>"+
					"<td align = 'left'>"+device.value+"</td>"+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>isEnable:</strong></td>"+
					"<td align = 'left'>"+device.isEnable+"</td>"+
				"</tr>"+
			"<table>";
		if (userDeviceType == 'desktop') {
			data = data + 
				"<ul>"+
					"<li class='dialogLiButton'>"+
						"<button onclick = 'showGraph()'><img src = 'images/graph.png'></button>"+
						"<button onclick = 'showDeviceSettings(\""+id+"\")'><img src='images/settings.png'></button>"+
					"</li>"+
				"</ul>";
			var e = document.getElementById("devicesDataBlock");
			if (e) e.innerHTML = data;
		}
		if (userDeviceType == 'mobile') {
			oldViewDevices.push(document.getElementById("devicesBlock").innerHTML);
			data = data + 
				"<div class = 'footer'>"+
					"<button onclick = 'showDeviceSettings(\""+id+"\")'>Settings</button>"+
					"<button onclick = 'returnBack()'>&lt;&lt;</button>"+
				"</div>";
			data = "<div id = 'devicesDataBlockMob'>"+data+"</div>";
			document.getElementById("devicesBlock").innerHTML = data;
			document.getElementById("devicesDataBlockMob").setAttribute("style", "padding-top:50%;padding-bottom:90%;");
		}
		currentShowData = id;
	}
}

class Solenoid24vac {
	constructor(id ,isEnable, specifedName, parent, arrayIndex) {
		this.arrayIndex = arrayIndex;
		this.parent = parent;
		this.id = id;
		this.type= "solenoid";
		this.isEnable = isEnable;
		this.specifedName = specifedName;
	}
	showData(id) {
		window.redrawCallback = Solenoid24vac.prototype.showData;
		window.redrawObjId=id;
		var device = devicesMap.get(id);
		var data = "<p align = 'center'><strong>"+device.type+" ["+device.specifedName+"]</strong></p>";
		data = data +
			"<hr/>"+
			"<table align = 'center'>"+
				"<tr>"+
					"<td align = 'left'><strong>type:</strong></td>"+
					"<td align = 'left'>"+device.type+"</td>"+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>isEnable:</strong></td>"+
					"<td align = 'left'>"+device.isEnable+"</td>"+
				"</tr>"+
			"<table>";
		if (userDeviceType == 'desktop') {
			data = data + 
				"<ul>"+
					"<li class='dialogLiButton'>"+
						"<button onclick = 'showGraph()'><img src = 'images/graph.png'></button>"+
						"<button onclick = 'showDeviceSettings(\""+id+"\")'><img src='images/settings.png'></button>"+
					"</li>"+
				"</ul>";
			var e = document.getElementById("devicesDataBlock");
			if (e) e.innerHTML = data;
		}
		if (userDeviceType == 'mobile') {
			oldViewDevices.push(document.getElementById("devicesBlock").innerHTML);
			data = data + 
				"<div class = 'footer'>"+
					"<button onclick = 'showDeviceSettings(\""+id+"\")'>Settings</button>"+
					"<button onclick = 'returnBack()'>&lt;&lt;</button>"+
				"</div>";
			data = "<div id = 'devicesDataBlockMob'>"+data+"</div>";
			document.getElementById("devicesBlock").innerHTML = data;
			document.getElementById("devicesDataBlockMob").setAttribute("style", "padding-top:50%;padding-bottom:90%;");
		}
		currentShowData = id;
	}
}

class AnalogInput {
	constructor(id, value , specifedName, parent) {
		this.parent = parent;
		this.id = id;
		this.type= "AnalogInput";
		this.value = value;
		this.specifedName = specifedName;
		this.schEnable = true;
	}
	showData(id) {
		window.redrawCallback = AnalogInput.prototype.showData;
		window.redrawObjId=id;
		var device = devicesMap.get(id);
		var data = "<p align = 'center'><strong>"+device.type+" ["+device.specifedName+"]</strong></p>";
		data = data +
			"<hr/>"+
			"<table align = 'center'>"+
				"<tr>"+
					"<td align = 'left'><strong>type:</strong></td>"+
					"<td align = 'left'>"+device.type+"</td>"+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>value:</strong></td>"+
					"<td align = 'left'>"+device.value+"</td>"+
				"</tr>"+
			"<table>";
		if (userDeviceType == 'desktop') {
			data = data + 
				"<ul>"+
					"<li class='dialogLiButton'>"+
						"<button onclick = 'showGraph()'><img src = 'images/graph.png'></button>"+
						"<button onclick = 'showDeviceSettings(\""+id+"\")'><img src='images/settings.png'></button>"+
					"</li>"+
				"</ul>";
			var e = document.getElementById("devicesDataBlock");
			if (e) e.innerHTML = data;
		}
		if (userDeviceType == 'mobile') {
			oldViewDevices.push(document.getElementById("devicesBlock").innerHTML);
			data = data + 
				"<div class = 'footer'>"+
					"<button onclick = 'showDeviceSettings(\""+id+"\")'>Settings</button>"+
					"<button onclick = 'returnBack()'>&lt;&lt;</button>"+
				"</div>";
			data = "<div id = 'devicesDataBlockMob'>"+data+"</div>";
			document.getElementById("devicesBlock").innerHTML = data;
			document.getElementById("devicesDataBlockMob").setAttribute("style", "padding-top:50%;padding-bottom:90%;");
		}	
		currentShowData = id;
	}
}
// --------------------------------------------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------other devs functions------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------------------
function removeDevice(deviceId) {
	fetch('/dropDevice/'+deviceId)
	.then(function(response){
		if (response.status == 200) {
			devicesMap.delete(deviceId);
			showDevices();
		} else {
			alert ("error : ["+response.status+"]");
		}
	})
	.catch(function(err){
		alert(err);
	})
}
function updateDevicesMap(inData) {
	devData = JSON.parse(inData);
	for (var i = 0; i < devData.length; i++) {
		devData[i].device = devData[i].device.toLowerCase();
		switch (devData[i].device) {
			case ("system") : {
				controllerTime = devData[i].time;
				break;
			}
			case ("mega") : {
				var mega = devicesMap.get(devData[i].hwUid);
				if (mega) {
					mega.lastUpdate = devData[i].lastUpdate;
					mega.isEnable = devData[i].isEnable;
					mega.isOnline = devData[i].isOnline;
				} else {
					var specifedName = namesMap.get(devData[i].hwUid);
					if (!specifedName) {
						specifedName = devData[i].hwUid;
					}
					mega = new Mega (	devData[i].hwUid,
						devData[i].lastUpdate,
						specifedName,
						devData[i].isEnable,
						devData[i].isOnline
					) ;
					devicesMap.set(mega.id,mega);
				}
				for (var sind = 0; sind < devData[i].sensors.length; sind++) {
					
					//ignore sensors [id == 000000000000]
					if (devData[i].sensors[sind].hwUid === "000000000000") continue;
					//----------------------------------
					
					var sensor = devicesMap.get(devData[i].sensors[sind].hwUid);
					if (sensor) {
						sensor.isEnable = devData[i].sensors[sind].isEnable;
					} else {
						var specifedName = namesMap.get(devData[i].sensors[sind].hwUid);
						if (!specifedName) {
							specifedName = devData[i].sensors[sind].hwUid;
						}
						sensor = new MegaSensor	(	devData[i].sensors[sind].hwUid,
							devData[i].sensors[sind].isEnable,
							specifedName,
							devData[i].sensors[sind].type,
							mega
						);
						devicesMap.set(sensor.id,sensor);
						mega.sensors.push(sensor);
					}
					for (var chnum = 0; chnum < devData[i].sensors[sind].channels.length; chnum++) {
						var channel = devicesMap.get(sensor.id+":"+chnum);
						if (channel) {
							channel.value = devData[i].sensors[sind].channels[chnum].value;
						} else {
							var specifedName = namesMap.get(sensor.id+":"+chnum);
							if (!specifedName) {
								specifedName = sensor.id+":"+chnum;
							}
							var channel = new MegaSensorChannel	(	sensor.id+":"+chnum,
																	devData[i].sensors[sind].channels[chnum].type,
																	devData[i].sensors[sind].channels[chnum].value,
																	devData[i].sensors[sind].channels[chnum].corrector1,
																	devData[i].sensors[sind].channels[chnum].corrector2,
																	specifedName,
																	sensor
																);
							devicesMap.set(channel.id,channel);
							sensor.channels.push(channel);
						}
					}

				}
				break;
			}
			case ("powerstrip") : {
				var powerStrip = devicesMap.get(devData[i].hwUid);
				if (powerStrip) {
					powerStrip.lastUpdate = devData[i].lastUpdate;
					powerStrip.isEnable = devData[i].isEnable;
					powerStrip.isOnline = devData[i].isOnline;
				} else {
					var specifedName = namesMap.get(devData[i].hwUid);
					if (!specifedName) {
						specifedName = devData[i].hwUid;
					}
					powerStrip = new PowerStrip	(	devData[i].hwUid,
														devData[i].lastUpdate,
														specifedName,
														devData[i].isEnable,
														devData[i].isOnline
													) ;
					devicesMap.set(powerStrip.id,powerStrip);
				}
				for (var j = 0; j < devData[i].outlets.length; j++) {
					var outlet = devicesMap.get(powerStrip.id+":outlet:"+j);
					if (outlet) {
						outlet.overCurrentFlag = devData[i].outlets[j].overCurrentFlag;
						outlet.currentSensor = devData[i].outlets[j].currentSensor;
						outlet.isEnable = devData[i].outlets[j].isEnable;
					} else {
						var specifedName = namesMap.get(powerStrip.id+":outlet:"+j);
						if (!specifedName) {
							specifedName = powerStrip.id+":outlet:"+j;
						}
						var outlet = new Outlet	(	powerStrip.id+":outlet:"+j,
													devData[i].outlets[j].currentSensor,
													devData[i].outlets[j].isEnable,
													devData[i].outlets[j].overCurrentFlag,
													specifedName,
													powerStrip,
													j
												);
						devicesMap.set(outlet.id,outlet);
						powerStrip.outlets.push(outlet);
					}
				}
				for (var j = 0; j < devData[i].pwmOutput12V.length; j++) {
					var out12v = devicesMap.get(powerStrip.id+":out12v:"+j);
					if (out12v) {
						out12v.value = devData[i].pwmOutput12V[j].value;
						out12v.isEnable = devData[i].pwmOutput12V[j].isEnable;
					} else {
						var specifedName = namesMap.get(powerStrip.id+":out12v:"+j);
						if (!specifedName) {
							specifedName = powerStrip.id+":out12v:"+j;
						}
						var out12v = new PWM_Output12V	(	powerStrip.id+":out12v:"+j,
															devData[i].pwmOutput12V[j].value,
															devData[i].pwmOutput12V[j].isEnable,
															specifedName,
															powerStrip,
															j
														);
						devicesMap.set(out12v.id,out12v);
						powerStrip.pwmsOutput12V.push(out12v);
					}
				}
				for (var j = 0; j < devData[i].pwmOutput5V.length; j++) {
					var out5v = devicesMap.get(powerStrip.id+":out5v:"+j);
					if (out5v) {
						out5v.value = devData[i].pwmOutput5V[j].value;
						out5v.isEnable = devData[i].pwmOutput5V[j].isEnable;
					} else {
						var specifedName = namesMap.get(powerStrip.id+":out5v:"+j);
						if (!specifedName) {
							specifedName = powerStrip.id+":out5v:"+j;
						}
						var out5v = new PWM_Output5V	(	powerStrip.id+":out5v:"+j,
															devData[i].pwmOutput5V[j].value,
															devData[i].pwmOutput5V[j].isEnable,
															specifedName,
															powerStrip,
															j
														);
						devicesMap.set(out5v.id,out5v);
						powerStrip.pwmsOutput5V.push(out5v);
					}
					
				}
				for (var j = 0; j < devData[i].solenoid24vac.length; j++) {
					var solenoid = devicesMap.get(powerStrip.id+":solenoid:"+j);
					if (solenoid) {
						solenoid.isEnable = devData[i].solenoid24vac[j]; 
					} else {
						var specifedName = namesMap.get(powerStrip.id+":solenoid:"+j);
						if (!specifedName) {
							specifedName = powerStrip.id+":solenoid:"+j;
						}
						var solenoid = new Solenoid24vac	(	powerStrip.id+":solenoid:"+j,
																devData[i].solenoid24vac[j],
																specifedName,
																powerStrip,
																j
															);
						devicesMap.set(solenoid.id,solenoid);
						powerStrip.solenoids24vac.push(solenoid);
					}
				}
				for (var j = 0; j < devData[i].analogInputs.length; j++) {
					var analog = devicesMap.get(powerStrip.id+":analog:"+j);
					if (analog) {
						analog.value = devData[i].analogInputs[j];
					} else {
						var specifedName = namesMap.get(powerStrip.id+":analog:"+j);
						if (!specifedName) {
							specifedName = powerStrip.id+":analog:"+j;
						}
						var analog = new AnalogInput	(	powerStrip.id+":analog:"+j,
															devData[i].analogInputs[j],
															specifedName,
															powerStrip
														);
						devicesMap.set(analog.id,analog);
						powerStrip.analogInputs.push(analog);
					}
				}
				break;
			}
		}
	}
}
function updateDevices() {
	fetch('/getDevices/').then(function(devicesResponse) {
	if (devicesResponse.status !== 200) {
		console.log('Looks like there was a problem. Status Code: ' +
						devicesResponse.status);
		return;
	}
	devicesResponse.text().then(function(inData){
		updateDevicesMap(inData);
		showDevices(true)
		if(window.redrawCallback)
			window.redrawCallback(window.redrawObjId);
	})
	}).catch(function(err){
		console.log('fetch err : '+err);
	})
} 
function uploadNamesFile() {
	var data = new FormData()
	data.append('file', createNamesFile())
	fetch('/updMP/names.txt', {
		method: 'POST',
		body: data
	})
}
function createNamesFile() {
	var data = "";
	for (var [key,value] of devicesMap) {
		data += key+" = "+value.specifedName+"\n";
	}
	return new File([data], "names", {
						type: "text/plain",
				});
}
function showAddDeviceDialog() {
	if(userDeviceType == "mobile") {showAddDeviceDialogMob()}
	if(userDeviceType == "desktop") {showAddDeviceDialogDesktop()}
}

function getNewDeviceData() {
	var elems;
	if (userDeviceType == 'desktop') {
		elems = document.querySelectorAll('ul > li > input');
	}
	if (userDeviceType == 'mobile') {
		elems = document.querySelectorAll('td > input');
	}
	var id;
	var name;
	for (var i = 0; i < elems.length; i++) {
		switch (elems[i].id) {
			case ('newDeviceNameInput') : { 
				name = elems[i].value;
				break; 
			}
			case ('newDeviceIdInput') : {
				id = elems[i].value;
				break; 
			}
		}
	}
	var type;
	if (userDeviceType == 'desktop') {
		type = document.querySelector('ul > li > select').value;
	}
	if (userDeviceType == 'mobile') {
		type = document.querySelector('td > select').value;
	}
	return [type,id,name];
}
async function addDevice() {
	deviceData = getNewDeviceData();
	var id = deviceData[1];
	var name = deviceData[2];
	var type = deviceData[0];
	var url;
	switch (type) {
		case ('mega') : {
			url = '/addMega/'+id;
			break;
		}
		case ('powerStrip') : {
			url = '/addSPS/'+id;
			break;
		}
	}
	fetch(url).then(function(response) {
		if (response.status == 200) {
			response.json().then(function(answer){
				if (answer.result == "OK") {
					//Mega = function(id,lastUpdate, specifedName, isEnable, isOnline)
					if (type == 'mega') {
						var mega = new Mega (	id,
							null,
							name,
							null,
							null,
							null
						) ;
						devicesMap.set(id,mega);
					}
					if (type == 'powerStrip') {
						var ps = new PowerStrip (	id,
							null,
							name,
							null,
							null,
							null
						) ;
						devicesMap.set(id,ps);
					}
					showDevices();
					uploadNamesFile();
					dialogClose();
				} else {
					if (answer.result == "error1") {
						alert("Mega limit exceeded ("+answer.hubMaxSPS+")")
					}
					if (answer.result == "error2") {
						alert("PowerStrip limit exceeded ("+answer.hubMaxMEGAS+")")
					}
				}
			})
		} else {
			console.log('Looks like there was a problem. Status Code: ' +
          				response.status);
        return;
		}
	})
	.catch(function (err){
		console.log('fetch error : '+err);
	})
}
function getDisEnErrDescr(errorName) {
	switch (errorName) {
		case ('sl_TO') : {return  'slave timeout (not answer from PowerStrip)'}
		case ('ID_nf') : {return  'ID is not found'}
		case ('s_err') : {return  'syntax error'}
		case ('el_or') : {return  'element out of range'}
		case ('pa_or') : {return  'paramenter out of range'}
		default : {return  'unknown error'}
	}
}
function OnOffPSDev(devCheckbox,devId) {
	// ### outlet/out12v/out5v/solenoid
	devCheckbox.disabled = true;
	var device = devicesMap.get(devId);
	var isEnable = 0;
	if (devCheckbox.checked) {
		isEnable = 1;
	}
	let query = '::set:powerStrip:'+device.parent.id+':'+device.type+':E:'+device.arrayIndex+':'+isEnable+'::';
	fetch("/stel/",{
		method: 'POST',
		body: query
	})
	.then(function(response){
		if (response.status == 200) {
			response.json().then(function(answer){
				if (answer.result != "ok") {
					alert('error : ' +getDisEnErrDescr(answer.result));
				}	
			})
		} else {
			alert("error ["+response.status+"]");
		}
		devCheckbox.disabled = false;
	})
	.catch(function(error){
		devCheckbox.disabled = false;
		alert("error ["+error+"]");
		
	})
}
function sendIsEnable(device,enabled) { // megasensor
	var enabled = enabled === true ? 'enSens' : 'disSens'; 
	if (device instanceof MegaSensor) {
		console.log('fetch :: /'+enabled+'/'+device.id)
		fetch('/'+enabled+'/'+device.id);
	}
}
function applyDevSettings(deviceId) {
	var elems = document.querySelectorAll('ul > li > input');
	for (var i = 0; i < elems.length; i++){
		var elNameData = elems[i].id.split('::',2);
		var elId = elNameData [1];
		var elParam = elNameData[0];
		var device = devicesMap.get(elId);
		if (elems[i].type == "text" || elems[i].type == "number") {
			switch(elParam) {
				case ("specifedName") : {
					device.specifedName = elems[i].value;
					break;
				}
				case ("value") : {
					if (device.type == 'out12v' || device.type == 'out5v') {
						let query = '::set:powerStrip:'+device.parent.id+':'+device.type+':V:'+device.arrayIndex+':'+elems[i].value+'::';
						fetch("/stel/",{
							method: 'POST',
							body: query
						})
						.then(function(response){
							if (response.status == 200) {
								response.json().then(function(answer){
									if (answer.result != "ok") {
										alert('error : ' +getDisEnErrDescr(answer.result));
									}	
								})
							} else {
								alert("error ["+response.status+"]");
							}
							devCheckbox.disabled = false;
						})
						.catch(function(error){
							devCheckbox.disabled = false;
							alert("error ["+error+"]");
							
						})
					} else {
						device.value = elems[i].value;
					}
					break;
				}
				case ("corrector1") : {
					device.corrector1 = elems[i].value;
					break;
				}
				case ("corrector2") : {
					device.corrector2 = elems[i].value;
					break;
				}
			}
		}
	}
	showDeviceChilds(currentShowDevice,false);
	showDevices();
	var showDevice = devicesMap.get(currentShowData);
	showDevice.showData(currentShowData);
	dialogClose();
	uploadNamesFile();
}
function showDeviceSettings(deviceId) {
	if(userDeviceType == "mobile") {showDeviceSettingsMob(deviceId)}
	if(userDeviceType == "desktop") {showDeviceSettingsDesktop(deviceId)}
}
function showDevices(isDevsOnly) { // isDevsOnly - boolean (if true then redraw only devices btns)
	if(userDeviceType == "mobile") {showDevicesMob()}
	if(userDeviceType == "desktop") {showDevicesDesktop(isDevsOnly)}
}
function showDeviceChilds(deviceId) { 
	if(userDeviceType == "mobile") {showDeviceChildsMob(deviceId)}
	if(userDeviceType == "desktop") {showDeviceChildsDesktop(deviceId)}
}