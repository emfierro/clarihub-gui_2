class Schedule {
	constructor(id,schName,scriptName,isActive,type) {
		this.id = id;
		this.schName = schName;
		this.scriptName = scriptName;
		this.isActive = isActive;
		this.type = type;
		this.date = null;
		this.time = null;
		this.dailyTime = null;
		this.period = null;
		this.sensorId = null;
		this.sensorValue = null;
		this.valueComparison = null;
		this.sunsetSunrise = null;
		this.timeShifting = null;
		this.currentSensor = null;
		this.overCurrentFlag = null;
	}
	periodToSeconds() {
		return this.period;
	}
	getHex(schId) {
		var schedule;
		if (schId) {
			schedule = schedulesMap.get(schId);
		} else {
			schedule = this;
		}
		var result = "";
		switch (schedule.type) {
			case ("oneTime"): {
				var date = schedule.date.split("-");
				var time = schedule.time.split(":");
				var datetime = new Date(date[0],date[1]-1,date[2],time[0],time[1]).getTime()/1000;
				var result = "01";
				result += numToHex(datetime,4);
				break;
			}
			case ("daily"): {
				var secondsFromMidnight;
				var time = schedule.dailyTime.split(":");
				secondsFromMidnight = time[0] * 60 * 60 + time[1] * 60;
				var result = "02";
				result += numToHex(secondsFromMidnight,4);
				break;
			}
			case ("periodically"): {
				var periodicity = schedule.periodToSeconds();
				var result = "03";
				result += numToHex(periodicity,4);
				break;
			}
			case ("byValue"): {
				var valueComparison = "=";
				switch (schedule.valueComparison) {
					case ("more") : {
						valueComparison = ">";
						break;
					}
					case ("less") : {
						valueComparison = "<";
						break;
					}
				}
				var result = "";
				let device = devicesMap.get(schedule.sensorId);
				if (device instanceof MegaSensorChannel) {
					var channelData = schedule.sensorId.split(":");
					let sensorId = channelData[0];
					let megaId = devicesMap.get(sensorId).parent.id;
					let channelNumber = channelData[1];
					result += "04" + megaId.padStart(12,"0") + sensorId.padStart(12,"0") + numToHex(channelNumber,1) + strToHex(valueComparison,1) + numToHex(schedule.sensorValue,4);
					break;
				} else if (device instanceof Outlet) {
					var result = "05";
					let deviceData = schedule.sensorId.split(":");
					let psId = deviceData[0];
					let devNum = deviceData[2];
					result += psId.padStart(12,"0"); // powerStrip id
					result += "01"; // sensor type
					result += numToHex(devNum,1); // sensor number
					if (schedule.currentSensor) {
						result += "01"; // value type
						result += strToHex(valueComparison,1);
						result += numToHex(schedule.currentSensor,4); // value
					} else if (schedule.overCurrentFlag) {
						result += "02"; // value type
						result += strToHex(valueComparison,1);
						result += numToHex(schedule.overCurrentFlag,4); // value
					} else {
						return "";
					}
					break;
				} else if (device instanceof AnalogInput) {
					var result = "05";
					let deviceData = schedule.sensorId.split(":");
					let psId = deviceData[0];
					let devNum = deviceData[2];
					result += psId; // powerStrip id
					result += "02"; // sensor type = analog
					result += numToHex(devNum,1); // sensor number
					result += "01";
					result += strToHex(valueComparison,1);
					result += numToHex(schedule.sensorValue,4);
					break;
				}
			}
			case ("sunsetSunrise"): {
				var result = "";
				if (schedule.sunsetSunrise == "sunset") {
					result += "06";
				}
				if (schedule.sunsetSunrise == "sunrise") {
					result += "07";
				}
				result += numToHex(schedule.timeShifting,4,true);
				break;
			}
			default : return;
		}
		result += strToHex(schedule.scriptName,16);
		result += boolToHex(schedule.isActive,1);
		result += numToHex(schedule.id,1);
		result += crc16_BUYPASS(result);
		return result;
	}
}

async function initSchedules() {
	fetch('/schNames.txt')
	.then(
		function(response) {
			if (response.status !== 200) {
				console.log('Looks like there was a problem. Status Code: ' +
         					 response.status);
        		return;
			}
			response.text().then(
				function(fileData) {
					var idNames = fileData.split("\n");
					for ( var idName of idNames ) {
						var data = idName.trim();
						data = data.split("=");
						if (data.length != 2) continue;
						schedulesNamesMap.set(data[0].trim(),data[1].trim());
					}
				}
			)
			.then(function() {
				fetch("/schedules.txt")
				.then(function(schResponse){
					if (schResponse.status !== 200) {
						console.log('Looks like there was a problem. Status Code: ' +
									 devicesResponse.status);
						return;
					}
					schResponse.text().then( function(schData){
						initSchedulesMap(schData)
					})
				})
			})
		}
	)
	.catch(function(err) {
		console.log('Fetch Error :-S', err);
	});
}
function getTrTitle(sch) {
	switch (sch.type) {
		case ("oneTime") : {
			return "date : "+sch.date+"\n" +
					"time : "+sch.time+"\n";
		}
		case ("daily") : {
			return "time : "+sch.dailyTime+"\n";
		}
		case ("periodically") : {
			return "period : "+sch.period+"\n";
		}
		case ("byValue") : {
			let valComp = "=";
			if (sch.valueComparison == "more") {
				valComp = "&gt;"
			}
			if (sch.valueComparison == "less") {
				valComp = "&lt;"
			}
			if (sch.currentSensor || sch.currentSensor == 0) {
				return 	"sensor : "+devicesMap.get(sch.sensorId).specifedName+"\n"+
						"currentSensor "+valComp+" "+sch.currentSensor;
			} else if (sch.overCurrentFlag || sch.overCurrentFlag == 0) {
				return 	"sensor : "+devicesMap.get(sch.sensorId).specifedName+"\n"+
						"overCurrentFlag "+valComp+" "+sch.overCurrentFlag;
			} else {
				return 	"sensor : "+devicesMap.get(sch.sensorId).specifedName+"\n"+
						"value "+valComp+" "+sch.sensorValue;
			}
		}
		case ("sunsetSunrise") : {
			return 	sch.sunsetSunrise + "\n" +
					"timeShifting : " + sch.timeShifting;
		}
	}
}
function showSchedulesTable () {
	if(userDeviceType == "mobile") {showSchedulesTableMob()}
	if(userDeviceType == "desktop") {showSchedulesTableDesktop()}
}

function removeNotValidSchedules() {
	for (var [key,value] of schedulesMap) {
		if (!value.type) schedulesMap.delete(key);
	}
}
function getNextScheduleId() {
	var id;
	while(true) {
		id = Math.floor(Math.random() * Math.floor(256)).toString();
		if (!schedulesMap.get(id)) break;
	}
	return id;
}
function addSchedule() {
	if(userDeviceType == "mobile") {addScheduleMob()}
	if(userDeviceType == "desktop") {addScheduleDesktop()}
}

function showScheduleSettings(scheduleId) {
	if(userDeviceType == "mobile") {showScheduleSettingsMob(scheduleId)}
	if(userDeviceType == "desktop") {showScheduleSettingsDesktop(scheduleId)}
}
function getScriptsOptionList() {
	var result = "";
	for (var scrName of scriptsNamesList) {
		result += "<option value = '"+scrName+"'>"+scrName+"</option>";
	}
	return result;
}
function getSensorsOptionList() {
	var result = "";
	for (var [key,value] of devicesMap) {
		if (value.schEnable && value.parent.isEnable) {
			if (value instanceof AnalogInput) {
				result += "<option value = '"+key+"'>"+value.specifedName+"</option>";
			}
			if (value instanceof MegaSensorChannel) {
				if (value.parent.parent.isEnable) {
					result += "<option value = '"+key+"'>"+value.specifedName+"</option>";
				}
			}
			if (value instanceof Outlet) {
				if (value.isEnable) {
					result += "<option value = '"+key+"'>"+value.specifedName+"</option>";
				}
			}
		}
	}
	return result;
}


function fillOutletInput(scheduleId) {
	var schedule = schedulesMap.get(scheduleId);
	var text = "";
	if (schedule.currentSensor) {
		text = schedule.currentSensor;
	} else if (schedule.overCurrentFlag) {
		text = schedule.overCurrentFlag;
	}
	document.getElementById("sensorValue").value = text;
}
function applyScheduleSettings(schedule,schType) {
	if (!(schedule instanceof Schedule)) {
		schedule = schedulesMap.get(schedule);
	}
	schedule.timeShifting = 0;
	schedule.period = 0;
	schedule.type = schType;
	var elems = '';
	if(userDeviceType == "desktop") {
		elems = document.querySelectorAll('ul > li > input');
	} else if (userDeviceType == "mobile") {
		elems = document.querySelectorAll('td > input');
	}
	
	for (var i = 0; i < elems.length; i++){
		switch (elems[i].id) {
			case ("scheduleName") : {
				schedule.schName = elems[i].value;
				break;
			}
			case ("date") : {
				schedule.date = elems[i].value;
				break;
			}
			case ("my_date") : {
				switch (calendarDateFormat) {
					/* 
					  t_mdy - mm-dd-yyyy
					  t_ymd - yyyy-mm-dd
					  p_dmy - dd.mm.yyyy
					  t_dmy - dd-mm-yyyy
					  s_dmy - dd/mm/yyyy
					*/
					case ('t_mdy') : {
						var date = 	elems[i].value.split("-");
						schedule.date = [date[2],date[0],date[1]].join("-");
						break;
					}
					case ('t_ymd') : {
						schedule.date = elems[i].value;
						break;
					}
					case ('p_dmy') : {
						var date = 	elems[i].value.split(".");
						schedule.date = [date[2],date[1],date[0]].join("-");
					} 
					case ('t_dmy') : {
						var date =  	elems[i].value.split("-");
						schedule.date = [date[2],date[1],date[0]].join("-");
						break;
					} 
					case ('s_dmy') : {
						var date = 	elems[i].value.split("/");
						schedule.date = [date[2],date[1],date[0]].join("-");
						break;
					} 
				}
			}
			case ("time") : {
				schedule.time = elems[i].value;
				break;
			}
			case ("dailyTime") : {
				schedule.dailyTime = elems[i].value;
				break;
			}
			case ("period_hh") : {
				schedule.period += 3600 * elems[i].value;
				break;
			}
			case ("period_mm") : {
				schedule.period += 60 * elems[i].value;
				break;
			}
			case ("period_ss") : {
				schedule.period += 1 * elems[i].value;
				break;
			}
			case ("sensorValue") : {
				var selectOutlet = document.getElementById("outletValue");
				if (!selectOutlet) {
						schedule.sensorValue = elems[i].value;
						if (schedule.currentSensor || schedule.currentSensor == 0)
							schedule.currentSensor = null;
						if (schedule.overCurrentFlag || schedule.overCurrentFlag == 0)
						schedule.overCurrentFlag = null;
				} else {
					if (selectOutlet.value == "currentSensor") {
						schedule.currentSensor = elems[i].value;
						schedule.overCurrentFlag = null;
					} else if (selectOutlet.value == "overCurrentFlag") {
						schedule.overCurrentFlag = elems[i].value;
						schedule.currentSensor = null;
					}
				}
				break;
			}
			case ("timeShifting_hh") : {
				schedule.timeShifting += 3600 * elems[i].value;
				break;
			}
			case ("timeShifting_mm") : {
				schedule.timeShifting += 60 * elems[i].value;
				break;
			}
			case ("timeShifting_ss") : {
				schedule.timeShifting += 1 * elems[i].value;
				break;
			}
		}
	}
	
	var elems = document.querySelectorAll('ul > li > li > input');
	for (var i = 0; i < elems.length; i++) {
		switch(elems[i].id) {
			case ("timeShifting_hh") : {
				schedule.timeShifting += 3600 * elems[i].value;
				break;
			}
			case ("timeShifting_mm") : {
				schedule.timeShifting += 60 * elems[i].value;
				break;
			}
			case ("timeShifting_ss") : {
				schedule.timeShifting += 1 * elems[i].value;
				break;
			}
		}
	}
	if(userDeviceType == "desktop") {
		elems = document.querySelectorAll('ul > li > select');
	} else if (userDeviceType == "mobile") {
		elems = document.querySelectorAll('td > select');
	}
	for (var i = 0; i < elems.length; i++) {
		switch (elems[i].id) {
			case ("scriptName") : {
				schedule.scriptName = elems[i].value;
				break;
			}
			case ("sensorId") : {
				schedule.sensorId = elems[i].value;
				break;
			}
			case ("sunsetSunrise") : {
				schedule.sunsetSunrise = elems[i].value;
				break;
			}
			case ("sunTimeSettings") : {
				if (elems[i].value == '-') {
					if (schedule.timeShifting) {
						schedule.timeShifting  *= -1;
					} else {
						schedule.timeShifting = -1;
					}
				}
				break;
			}
		}
	}
	var valueComparisonElement = document.getElementById("valueComparison");
	if ( valueComparisonElement ) {
		schedule.valueComparison = valueComparisonElement.value;
	}
	dialogClose();
	if (userDeviceType == "mobile") {
		returnBack();
	}
	if(userDeviceType == "desktop") {
		showSchedulesTable();
	}
	uploadSchedulesFile();
	uploadSchedulesNamesFile();
}
function removeSchedule(schedule) {
	if (confirm("Remove schedule?")) {
		schedulesMap.delete(schedule);
		showSchedulesTable()
	}
	uploadSchedulesFile();
	uploadSchedulesNamesFile();
}
function OnOffSchedule(scheduleInput,schId) {
	var schedule = schedulesMap.get(schId);
	if (scheduleInput.checked) {
		if (confirm("ON schedule")) {
			scheduleInput.checked = true;
			schedule.isActive = true;
		} else {
			scheduleInput.checked = false;
			schedule.isActive = false;
		}
	} else {
		if (confirm("OFF schedule?")) {
			scheduleInput.checked = false;
			schedule.isActive = false;
		} else {
			scheduleInput.checked = true;
			schedule.isActive = true;
		}
	}
	uploadSchedulesFile();
}
function createSchedulesNamesFile() {
	var data = "";
	for (var [key,value] of schedulesMap) {
		data += key+" = "+value.schName+"\n";
	}
	return new File([data], "names", {
						type: "text/plain",
				});
}
function uploadSchedulesNamesFile() {
	var data = new FormData()
	data.append('file', createSchedulesNamesFile())
	fetch('/updMP/schNames.txt', {
		method: 'POST',
		body: data
	})
}
function decodeStrHex(hex) {
	var hex = hex.toString();
	var str = '';
	for (var i = 0; i < hex.length; i += 2) {
		var int = parseInt(hex.substr(i, 2), 16);
		if (int == 0) break;
		str += String.fromCharCode(int);
	}
	return str;
}
function decodeBoolHex(hex) {
	var data = Number(hex)
	return data == 0 || !data ? false : true ;
}
function boolToHex(data,bytes) {
	if (bytes) {
		var zeroStr = "";
		for (var i = 0; i < (bytes*2 - 1); i++) {
			zeroStr += "0";
		}
		return zeroStr+Number(data);
	} else {
			return "0"+Number(data);
	}
}

function numToHex(data,bytes,signed) {
	var numStr = Number(data) ;
	if (signed) {
		switch (bytes) {
			case (4): {
				if (numStr < 0) {
					numStr = 4294967296 + numStr;
				}
				break;
			}
		}
	}
	str = numStr.toString(16);
	var zeroStr = "";
	if (bytes) {
		for (var i = 0; i < (bytes*2 - str.length); i++) {
			zeroStr += "0";
		}
	}
	return zeroStr+str;
}

function strToHex(data,bytes) {
	var hex = '';
	for(var i=0;i<data.length;i++) {
		hex += ''+data.charCodeAt(i).toString(16);
	}
	var zeroStr = "";
	if (bytes) {
		for (var i = 0; i < (bytes*2 - hex.length); i++) {
			zeroStr += "0";
		}
	}
	return hex+zeroStr;
}
function crc16_BUYPASS(str,isBytesData){
	var arr = [];
	if (isBytesData) {
		for (var i = 0; i < str.length; i++) {
			if (str[i].length) {
				for (var j = 0; j < str[i].length; j++) {
				arr[i+j] = str[i][j];
			}
			} else {
				arr[i] = str[i];
			}
			
		}
	} else {
		for (let i = 0; i < str.length; i++) {
			arr[i] = str.charCodeAt(i);
		}
	}
	var ptr= Array.from(arr) ;
	var count = ptr.length;
	var  crc=0;
	var i=0;
	var ptri=0;
	while (count-- > 0)
	{
	  crc = crc ^ (ptr[ptri++] << 8);
	  i = 8;
	  while(i--)
	  {
		if ( crc  & 0x8000)
		  crc = (crc << 1) ^ 0x8005;
		else
		  crc = crc << 1;
	  };
	}
	return ((crc&0xffff).toString(16).padStart(4,"0").toLocaleUpperCase());
}
function createScheduleFile() {
	var data = "";
	for (var [key,value] of schedulesMap) {
		data += value.getHex()+"\n";
	}
	return new File([data], "schedules", {
		type: "text/plain",
	});
}
function uploadSchedulesFile() {
	var fileData = createScheduleFile();
	if (!fileData) return;
	var data = new FormData()
	data.append('file',fileData)
	fetch('/updMP/schedules.txt', {
		method: 'POST',
		body: data
	})
}
function checkScheduleCRC16(data) {
	var crc16 = data.substring(data.length - 4);
	var scheduleData = data.substring(0,data.length-4);
	return crc16_BUYPASS(scheduleData) == crc16;
}
function createOneTimeSchedule(data) {
	var unixTime = parseInt(data.substring(2,2+8),16);
	var date = new Date(unixTime*1000);
	var scriptName = decodeStrHex(data.substring(10,10+32));
	var isActive = decodeBoolHex(data.substring(42,44));
	var id = parseInt(data.substring(44,46),16).toString();
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = "oneTime:"+id;
	}
	var schedule = new Schedule(id,scheduleName,scriptName,isActive,"oneTime");
	schedule.date = date.YYYYMMDD();
	schedule.time = date.HHmm();
	return schedule;
}

function createdailySchedule(data) {
	var dailyTime = parseInt(data.substring(2,2+8),16);
	var scriptName = decodeStrHex(data.substring(10,10+32));
	var isActive = decodeBoolHex(data.substring(42,44));
	var id = parseInt(data.substring(44,46),16).toString();
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = "daily:"+id;
	}
	var schedule = new Schedule(id,scheduleName,scriptName,isActive,"daily")
	var mins = (dailyTime%3600)/60;
	if (mins < 10) {
		mins = '0'+mins;
	}
	var hrs = parseInt(dailyTime/3600);
	if (hrs < 10) {
		hrs = '0'+hrs;
	}
	schedule.dailyTime = hrs+":"+mins;
	return schedule;
}
function createPeriodicallySchedule(data) {
	var periodicity = parseInt(data.substring(2,2+8),16);
	var scriptName = decodeStrHex(data.substring(10,10+32));
	var isActive = decodeBoolHex(data.substring(42,44));
	var id = parseInt(data.substring(44,46),16).toString();
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = "periodically:"+id;
	}
	var schedule = new Schedule(id,scheduleName,scriptName,isActive,"periodically")
	schedule.period = periodicity;
	return schedule;
}
function createByValueMegaSchedule(data) {
	var sensorId = data.substring(14,14+12);
	var chNum = parseInt(data.substring(26,26+2),16);
	var valueComparison = decodeStrHex(data.substring(28,30));
	if (valueComparison == '>') {
		valueComparison = "more";
	}
	if (valueComparison == '<') {
		valueComparison = "less";
	}
	var sensorValue = parseInt(data.substring(30,38),16);
	var scriptName = decodeStrHex(data.substring(38,38+32));
	var isActive = decodeBoolHex(data.substring(70,72));
	var id = parseInt(data.substring(72,74),16).toString();
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = "byValue:"+id;
	}
	var schedule = new Schedule(id,scheduleName,scriptName,isActive,"byValue")
	schedule.sensorValue = sensorValue;
	schedule.valueComparison = valueComparison;
	schedule.sensorId = sensorId+":"+chNum;
	return schedule;
}
function createByValuePowerStripSchedule(data) {
	var psId = data.substring(2,2+12);
	var sensorType = data.substring(14,16);
	var sensorNumber =  parseInt(data.substring(16,18),16);
	var valueType = data.substring(18,20);
	var valueComparison = decodeStrHex(data.substring(20,22));
	if (valueComparison == '>') {
		valueComparison = "more";
	}
	if (valueComparison == '<') {
		valueComparison = "less";
	}
	var value = parseInt(data.substring(22,30),16);
	var scriptName = decodeStrHex(data.substring(30,62));
	var isActive = decodeBoolHex(data.substring(62,64));
	var id = parseInt(data.substring(64,66),16).toString();
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = "byValue:"+id;
	}
	var schedule = new Schedule(id,scheduleName,scriptName,isActive,"byValue")
	schedule.valueComparison = valueComparison;
	switch (sensorType) {
		case ("01") : {
			schedule.sensorId = psId+":outlet:"+sensorNumber;
			switch (valueType) {
				case ("01") : {
					schedule.currentSensor = value;
					break;}
				case ("02") : {
					schedule.overCurrentFlag = value;
					break;}
			}
			break;
		}
		case ("02") : {
			schedule.sensorId = psId+":analog:"+sensorNumber;
			schedule.sensorValue = value;
			break;
		}
	}
	return schedule;
}
function createSunsetSchedule(data) {
	var timeShifting = parseInt(data.substring(2,2+8),16);
	var scriptName = decodeStrHex(data.substring(10,10+32));
	var isActive = decodeBoolHex(data.substring(42,44));
	var id = parseInt(data.substring(44,46),16).toString();
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = "sunset:"+id;
	}
	var schedule = new Schedule(id,scheduleName,scriptName,isActive,"sunset");
	if (timeShifting > (4294967296/2)-1) {
		timeShifting = (4294967296 - timeShifting) * -1;
	}
	schedule.timeShifting = timeShifting;
	schedule.sunsetSunrise = "sunset";
	schedule.type = "sunsetSunrise";
	return schedule;
}
function createSunriseSchedule(data) {
	var timeShifting = parseInt(data.substring(2,2+8),16);
	var scriptName = decodeStrHex(data.substring(10,10+32));
	var isActive = decodeBoolHex(data.substring(42,44));
	var id = parseInt(data.substring(44,46),16).toString();
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = "sunrise:"+id;
	}
	var schedule = new Schedule(id,scheduleName,scriptName,isActive,"sunrise");
	if (timeShifting > (4294967296/2)-1) {
		timeShifting = (4294967296 - timeShifting) * -1;
	}
	schedule.timeShifting = timeShifting;
	schedule.sunsetSunrise = "sunrise";
	schedule.type = "sunsetSunrise";
	return schedule;
}

function initSchedulesMap(inData) {
	var schedulesArr = inData.split("\n");
	for (var i = 0; i < schedulesArr.length; i++) {
		if (!checkScheduleCRC16 (schedulesArr[i]) ) {
			continue;
		}
		var scheduleType = schedulesArr[i].substring(0,2);
		var schedule = null;
		switch (scheduleType) {
			case ("01") : {
				schedule = createOneTimeSchedule(schedulesArr[i]);
				break;
			}
			case ("02") : {
				schedule = createdailySchedule(schedulesArr[i])
				break;
			}
			case ("03") : {
				schedule = createPeriodicallySchedule(schedulesArr[i]);
				break;
			}
			case ("04") : {
				schedule = createByValueMegaSchedule(schedulesArr[i]);
				break;
			}
			case ("05") : {
				schedule = createByValuePowerStripSchedule(schedulesArr[i]);
				break;
			}
			case ("06") : {
				schedule = createSunsetSchedule(schedulesArr[i]);
				break;
			}
			case ("07") : {
				schedule = createSunriseSchedule(schedulesArr[i]);
				break;
			}
		}
		schedulesMap.set(schedule.id,schedule);
	}
}
