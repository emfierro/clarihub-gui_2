function c_fillMonth() {
	var calendar_month = ["January","February","March","April","May","June","July","August","September","October","November","December"];
	var monthSelect = document.getElementById('calendar_month');
	var data = '';
	for (var i = 0; i < calendar_month.length; i++) {
		data += "<option value = '"+i+"'>"+calendar_month[i]+"</option>"
	}
	monthSelect.innerHTML = data;
}
function c_fillWeeks() {
	var calendar_weeks = ["Mon.","Tue.","Wed.","Thu.","Fri.","Sat.","Sun."];
	var weeksTD = document.getElementById('calendar_weeks');
	var data = '';
	for (var i = 0; i < calendar_weeks.length; i++) {
		data += '<td>'+calendar_weeks[i]+'</td>';
	}
	weeksTD.innerHTML = data;
}
function Calendar(id, year, month,caller) {
	var callerId = caller.id;
	var Dlast = new Date(year,month+1,0).getDate(),
		D = new Date(year,month,Dlast),
		DNlast = D.getDay(),
		DNfirst = new Date(D.getFullYear(),D.getMonth(),1).getDay(),
		calendar = '<tr>',
		m = document.querySelector('#'+id+' option[value="' + D.getMonth() + '"]'),
		g = document.querySelector('#'+id+' input');
	if (DNfirst != 0) {
	  for(var  i = 1; i < DNfirst; i++) calendar += '<td>';
	}else{
	  for(var  i = 0; i < 6; i++) calendar += '<td>';
	}
	for(var  i = 1; i <= Dlast; i++) {
	  if (i == new Date().getDate() && D.getFullYear() == new Date().getFullYear() && D.getMonth() == new Date().getMonth()) {	  
		calendar += '<td class="clickedTD today" onclick = "c_selectDate("'+i+'","'+callerId+'")">' + i;
	  }else{
		calendar += '<td class = "clickedTD" onclick = "c_selectDate(\''+i+'\',\''+callerId+'\')">' + i;
	  }
	  if (new Date(D.getFullYear(),D.getMonth(),i).getDay() == 0) {
		calendar += '<tr>';
	  }
	}
	for(var  i = DNlast; i < 7; i++) calendar += '<td>&nbsp;';
	document.querySelector('#'+id+' tbody').innerHTML = calendar;
	g.value = D.getFullYear();
	m.selected = true;
	if (document.querySelectorAll('#'+id+' tbody tr').length < 6) {
		document.querySelector('#'+id+' tbody').innerHTML += '<tr><td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;';
	}
	document.querySelector('#'+id+' option[value="' + new Date().getMonth() + '"]').style.fontWeight = 'bold';
	document.querySelector('#'+id+' option[value="' + new Date().getMonth() + '"]').style.background = '#DCDCDC';
}
function c_selectDate(dd,callerId) {
	var mm = parseInt(document.getElementById('calendar_month').value) + 1;
	var year = document.getElementById('calendar_year').value;
	var date;
	switch (calendarDateFormat) {
		/* 
		  t_mdy - mm-dd-yyyy
		  t_ymd - yyyy-mm-dd
		  p_dmy - dd.mm.yyyy
		  t_dmy - dd-mm-yyyy
		  s_dmy - dd/mm/yyyy
		*/
		case ('t_mdy') : {
			date = 	[
					(mm>9 ? '' : '0') + mm,
					(dd>9 ? '' : '0') + dd,
					year
					].join('-');
			break;
		}
		case ('t_ymd') : {
			date = 	[
					year,
					(mm>9 ? '' : '0') + mm,
					(dd>9 ? '' : '0') + dd,
					].join('-');
			break;
		}
		case ('p_dmy') : {
			date = 	[
					(dd>9 ? '' : '0') + dd,
					(mm>9 ? '' : '0') + mm,
					year
					].join('.');
			break;
		} 
		case ('t_dmy') : {
			date = 	[
					(dd>9 ? '' : '0') + dd,
					(mm>9 ? '' : '0') + mm,
					year
					].join('-');
			break;
		} 
		case ('s_dmy') : {
			date = 	[
					(dd>9 ? '' : '0') + dd,
					(mm>9 ? '' : '0') + mm,
					year
					].join('/');
			break;
		} 
	}
	document.getElementById(callerId).value = date;
	document.getElementById('calendarContainer').remove();
}
function showCalendar(whoCalled) {
	var container = document.createElement('div');
	container.setAttribute('id','calendarContainer');
	whoCalled.insertAdjacentElement("afterEnd",container);
	container.innerHTML =	
							"<table id='calendarTable'>"+
								"<thead>"+
									"<tr><td colspan='4'><select id = 'calendar_month'>"+
									"</select></td><td colspan='3'><input id = 'calendar_year' type='number' min='0' max='9999' size='4'></td>"+
									"</tr>"+
									"<tr id = 'calendar_weeks'></tr>"+
								"</thead>"+
								"<tbody>"+
								"</tbody>"+
							"</table>";
	c_fillMonth();
	c_fillWeeks();
	Calendar("calendarTable",new Date().getFullYear(),new Date().getMonth(),whoCalled);
	document.querySelector('#calendarTable').onchange = function() {
		Calendar("calendarTable",document.querySelector('#calendarTable input').value,parseFloat(document.querySelector('#calendarTable select').options[document.querySelector('#calendarTable select').selectedIndex].value),whoCalled);
	}
}

document.addEventListener('click', function(event) {
	var c = document.getElementById('calendarContainer');
	if (event.target.className) {
		if ("calendar_input" == event.target.className) return;
	}
	if (c) {
		if (!c.contains(event.target)) c.remove();
	}
	
});