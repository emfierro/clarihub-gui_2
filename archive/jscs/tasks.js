class Script {
	constructor(name,fullCode) {
		let data = fullCode.split(String.fromCharCode(7));
		if (data.length != 2) {
			data = ['',''];
		}
		this.name = name;
		this.vars = data[0] ? data[0].substring(1,data[0].length) : '';
		this.code = data[1] ? data[1] : '';
		this.isEdited = false;
		this.execution = data[0] ? data[0].charAt(0) : '-';
	}
	save() {
		this.isEdited = false;
		var textarea = document.getElementById("taskTextArea");
		
		if (textarea) {
			if (textarea.className == 'ta_vars') {
				this.vars = textarea.value;
			}
			if (textarea.className == 'ta_code') {
				this.code = textarea.value;
			}
		}
		
		var data = 	this.execution+
					this.vars+
					String.fromCharCode(7)+
					this.code;

		var file =  new File([data], "file", {
						type: "text/plain",
				});
		data = new FormData()
		data.append('file', file)
		return fetch('/updMP/scs/'+this.name+'.wws', {
			method: 'POST',
			body: data
		})
	}
}
function changeTaskExecution(sel_el) {
	currentTask.execution = sel_el.value;
	currentTask.save();
	if (userDeviceType == 'mobile') {
		showTask(currentTask.name);
	}
}
function initScriptsList() {
	fetch('/getScripts/')
	.then(
		function (response) {
			if (response.status !== 200) {
				console.log('Looks like there was a problem. Status Code: ' +
         					 response.status);
        		return;
			}
			response.text().then(
				function (data) {
					data = JSON.parse(data);
					if(data.scripts){
						for (let i = 0; i < data.scripts.length; i++) {
							scriptsNamesList.push(data.scripts[i].replace(/^.*[\\\/]/, '').split('.')[0]);
						}
					}
				}
			)
		}
	)
	.catch(function(err){
		console.log('Fetch Error :-S', err);
	})
}
function addTask() {
	var elem = document.querySelector('ul > li > input');
	scriptsNamesList.push(elem.value);
	showTasksBlock();
	dialogClose();
}

function renameTask(taskName) {
	var scsData;
	if (taskName) {
		scsData = "rename:/scs/"+currentTask.name+".wws:/scs/"+taskName+".wws::";
	} else {
		var elem = document.querySelector('ul > li > input');
		taskName = elem.value;
		scsData = "rename:/scs/"+currentTask.name+".wws:/scs/"+elem.value+".wws::";
	}
	
	fetch("/rnSc/",{
		method: 'POST',
		body: scsData
	})
	.then(function(response){
		if (response.status == 200) {
			taskRenameInList(currentTask.name,taskName);
			showTasksBlock();
		} else {
			alert("error to rename task ["+response.status+"]");
			return;
		}
	})
	.catch(function(error){
		alert("error to rename task ["+error+"]");
	})
	dialogClose();
}
function showTaskSettings(isNew) {
	let taskName = '';
	if (!isNew) {
		taskName = currentTask.name;
	} else {
	}
	var dialog = document.createElement("dialog");
	dialog.setAttribute("class","settings");
	var data = 	"<form><ul>"+
					"<li>"+
						"<label>Script name : </label>"+
						"<input title = '[a-zA-Z0-9]{1,16}' type = 'text' value = '"+taskName+"' required pattern='[a-zA-Z0-9]{1,16}'>"+
					"</li>"+
					"<li class='dialogLiButton'>"+
						"<button type='submit'>Ok</button> <button id = 'canselBtn'>Cansel</button>"+
					"</li>"+
				"<ul></form>";	
	dialog.innerHTML = data;
	document.body.appendChild(dialog);
	document.querySelector('#canselBtn').onclick = function(e) {
		e.preventDefault();
		dialogClose();
	}
	document.querySelector('form').onsubmit = function(e) {
		e.preventDefault();
		return isNew ? addTask() : renameTask();
	};
	document.querySelector('dialog').showModal();
}
function removeTask() {
	fetch ('/rm/scs/'+currentTask.name+'.wws')
	.then(function (response){
		if (response.status !== 200) {
			console.log('fetch error : '+response.status);
			return;
		}
		// -- remone in lisst
		var pos = scriptsNamesList.findIndex(function(value) {return value == currentTask.name})
		scriptsNamesList.splice(pos,1);
		// - end remove
		
		currentTask = null;
		if (userDeviceType == 'desktop') {
			document.getElementById('taskTextArea').value = '';
			disableTaskBlock();
		}
		showTasksBlock();
	})
	.catch(function (err){
		console.log('fetch exception : '+ err);
	})
}
/*function showTasksBlock() {
	let listBlock = document.getElementById("tasksList");
	let data = '<p>&nbsp</p>';
	for (let scrName of scriptsNamesList) {
		data += "<button class = 'buttonDevice' onclick = 'highlightButton(this);showTask(\""+scrName+"\")'>"+scrName+"</button>";
	}
	data += "<button  class = 'buttonDevice' onclick = 'showTaskSettings(true)'><img src = 'images/plus.png'><strong>&nbsp Add task</strong></button>";
	listBlock.innerHTML = data;
}*/
function showTasksBlock() {
	if(userDeviceType == "mobile") {
		showTasksBlockMob();
	}
	if(userDeviceType == "desktop") {
		showTasksBlockDesktop();
	}
}
function showTaskVars() {
	if (document.getElementById('taskExecutionSelect'))
		document.getElementById('taskExecutionSelect').value = currentTask.execution;
	var textarea = document.getElementById("taskTextArea");

	if (textarea.value)
		currentTask.code = document.getElementById("taskTextArea").value;

	textarea.setAttribute('class','ta_vars')
	if (userDeviceType == 'desktop') {
		document.getElementById("taskVarsBtn").disabled = true;
		document.getElementById("taskCodeBtn").disabled = false;
	}
	
	textarea.value = currentTask.vars;
}
function disableTaskBlock() {
	var elems = document.querySelectorAll('#taskBlock > p > button')
	for(var i=0;i<elems.length;i++){
		if (elems[i].id != 'consoleBtn') 
			elems[i].disabled = true;
	}
	var elems = document.querySelectorAll('#taskBlock > button')
	for(var i=0;i<elems.length;i++){
		elems[i].disabled = true;
	}
	var elems = document.querySelectorAll('#taskBlock > textarea')
	for(var i=0;i<elems.length;i++){
		elems[i].disabled = true;
	}
	document.getElementById('taskExecutionSelect').disabled = true;
}
function enableTaskBlock() {
	var elems = document.querySelectorAll('#taskBlock > p > button')
	for(var i=0;i<elems.length;i++){
		if (elems[i].id != 'consoleBtn')
			elems[i].disabled = false;
		
	}
	var elems = document.querySelectorAll('#taskBlock > button')
	for(var i=0;i<elems.length;i++){
		elems[i].disabled = false;
	}
	var elems = document.querySelectorAll('#taskBlock > textarea')
	for(var i=0;i<elems.length;i++){
		elems[i].disabled = false;
	}
	document.getElementById('taskExecutionSelect').disabled = false;
}
function showTaskCode() {
	if (document.getElementById('taskExecutionSelect'))
		document.getElementById('taskExecutionSelect').value = currentTask.execution;
	var textarea = document.getElementById("taskTextArea");
	textarea.setAttribute('class','ta_code')
	if (textarea.value)
		currentTask.vars = textarea.value;
	if (userDeviceType == 'desktop') {
		document.getElementById("taskCodeBtn").disabled = true;
		document.getElementById("taskVarsBtn").disabled = false;
	}
	textarea.value = currentTask.code;
}
function taskRenameInList(oldName,newName) {
	for (let i = 0; i < scriptsNamesList.length; i++) {
		if (scriptsNamesList[i] == oldName) {
			scriptsNamesList[i] = newName;
			return;
		}
	}
}
function taskSave() {
	currentTask.save();
}

async function showTask(scriptName) {
	fetch("/scs/"+scriptName+".wws")
	.then(function(response){
		response.text().then(function(taskData){
			if (!taskData) taskData = '[404]';
			currentTask = null;
			currentTask = new Script(scriptName,taskData);
			if(userDeviceType == "mobile") {
				showTaskMob();
			}
			if(userDeviceType == "desktop") {
				document.getElementById("taskTextArea").value = '';
				enableTaskBlock();
				showTaskVars();
			}
		})
	})
	.catch(function(err){
		console.log("fetch err : "+err);
	})
}
function startScript() {
	if (currentTask) {
		if (!currentTask.isEdited) {
			fetch('/startScript/'+currentTask.name);
		} else {
			currentTask.save().then(function(){
				fetch('/startScript/'+currentTask.name);
			})
		}
	}
		
}