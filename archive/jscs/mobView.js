function returnBack() {
	switch (currentMenu) {
		case ('Devices') : {
			document.getElementById("devicesBlock").innerHTML = oldViewDevices[oldViewDevices.length-1];
			oldViewDevices.pop();
			break;
		}
		case ('Schedule') : {
			document.getElementById("Schedule").innerHTML = oldViewSchedule[oldViewSchedule.length-1];
			showSchedulesTableMob();
			oldViewSchedule.pop();
			break;
		}
		case ('TasksList') : {
			document.getElementById("tasksBlock").innerHTML = oldViewTasks[oldViewTasks.length-1];
			oldViewDevices.pop();
			break;
		}
	}
}

function showDevicesMob() {
		var devicesList= "";
		for (var [key,value] of devicesMap) {
			if (value.type == "Mega")
				devicesList += "<button id = 'button::"+key+"' class = 'buttonDeviceMob' onclick = 'Mega.prototype.showData(\""+key+"\")'>"+value.specifedName+"</button>";
			if (value.type == "PowerStrip")
				devicesList += "<button id = 'button::"+key+"' class = 'buttonDeviceMob' onclick = 'PowerStrip.prototype.showData(\""+key+"\")'>"+value.specifedName+"</button>";
		}
		devicesList += "<button  class = 'buttonDeviceMob' onclick = 'showAddDeviceDialog()'><img src = 'images/plus.png'><strong>&nbspAdd new device</strong></button>";
		document.getElementById("devicesBlock").innerHTML = devicesList;
		
}
function showDeviceChildsMob(deviceId) {
	oldViewDevices.push(document.getElementById("devicesBlock").innerHTML);
	currentShowDevice = deviceId;
	var device = devicesMap.get(deviceId);
	var sensorsList = "";
	if (device.type == "Mega") {
		for (sensor of device.sensors) {
			sensorsList += "<button id = 'button::"+sensor.id+"' class = 'buttonDeviceMob' onclick = 'sensor.showData(\""+sensor.id+"\")' >"+sensor.specifedName+"</button>";
		}
	} else if (device.type == "PowerStrip"){
		for (outlet of device.outlets) {
			sensorsList += "<button id = 'button::"+outlet.id+"' class = 'buttonDeviceMob'  onclick = 'outlet.showData(\""+outlet.id+"\")'>"+outlet.specifedName+"</button>";
		}
		for (out12v of device.pwmsOutput12V) {
			sensorsList += "<button id = 'button::"+out12v.id+"' class = 'buttonDeviceMob'   onclick = 'out12v.showData(\""+out12v.id+"\")'>"+out12v.specifedName+"</button>";
		}
		for (out5v of device.pwmsOutput5V) {
			sensorsList += "<button id = 'button::"+out5v.id+"' class = 'buttonDeviceMob'   onclick = 'out5v.showData(\""+out5v.id+"\")'>"+out5v.specifedName+"</button>";
		}
		for (solenoid24vac of device.solenoids24vac) {
			sensorsList += "<button id = 'button::"+solenoid24vac.id+"' class = 'buttonDeviceMob'   onclick = 'solenoid24vac.showData(\""+solenoid24vac.id+"\")'>"+solenoid24vac.specifedName+"</button>";
		}
		for (analogIn of device.analogInputs) {
			sensorsList += "<button id = 'button::"+analogIn.id+"' class = 'buttonDeviceMob'   onclick = 'analogIn.showData(\""+analogIn.id+"\")'>"+analogIn.specifedName+"</button>";
		}
	}
	sensorsList = sensorsList + 
			"<div class = 'footer'>"+
				"<button style = 'width:100%; font-size:500%; background:#1E90FF;' onclick = 'returnBack()'>&lt;&lt;</button>"+
			"</div>";
	document.getElementById("devicesBlock").innerHTML = sensorsList;
}
function showDeviceSettingsMob(deviceId) {
	oldViewDevices.push(document.getElementById("devicesBlock").innerHTML);
	var device = devicesMap.get(deviceId);
	var settings = "<p align = 'center'><strong> settings ["+device.specifedName+"]</strong></p><hr/>";;
	switch (device.type) {
		case ("Mega") :
		case ("PowerStrip") : settings += getDeviceSettingsMob(device); break;
		case ("MegaSensor") : settings += getMegaSensorSettingsMob(device); break;
		case ("outlet") : settings += getOutletSettingsMob(device); break;
		case ("out12v") :
		case ("out5v") : settings += getPWM_OutputSettingsMob(device); break;
		case ("solenoid") : settings += getSolenoidSettingsMob(device); break;
		case ("AnalogInput") : settings += getAnalogInputSettingsMob(device); break;
	}
	settings = "<div class = 'settingsMobBlock'>"+settings+"</div>";
	document.getElementById("devicesBlock").innerHTML = settings;

	document.querySelector('form').onsubmit = function(e) {
		e.preventDefault();
	//	return applyDevSettings(device);
		return alert("not working");
	};
}

function getDeviceSettingsMob(device) {		
	return 	"<form>"+
			"<table align = 'center'>"+
				"<tr>"+
					"<td align = 'left'><strong>SpecifedName:</strong></td>"+
					"<td align = 'left'><input type='text' id = 'specifedName::"+device.id+"' value = '"+device.specifedName+"' required pattern='[a-zA-Z0-9:_]{1,32}'/></td>"+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>IsEnable:</strong></td>"+
					"<td align = 'left'><input type='checkbox' id = 'isEnable::"+device.id+"' checked/></td>"+
				"</tr>"+
			"</table>"+
				"<button type='submit'>Ok</button>"+
				"<button onclick = 'returnBack()'>Cansel</button>"+
			"</form>";	
}

function getMegaSensorSettingsMob(device) {
	return 	"<form>"+
			"<table align = 'center'>"+
				"<tr>"+
					"<td align = 'left'><strong>SpecifedName:</strong></td>"+
					"<td align = 'left'><input type='text' id = 'specifedName::"+device.id+"' value = '"+device.specifedName+"' required pattern='[a-zA-Z0-9:_]{1,32}'/></td>"+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>IsEnable:</strong></td>"+
					"<td align = 'left'><input type='checkbox' id = 'isEnable::"+device.id+"' checked/></td>"+
				"</tr>"+
			"</table>"+
				"<button type='submit'>Ok</button>"+
				"<button onclick = 'returnBack()'>Cansel</button>"+
			"</form>";	
	/*var settings = 	"<form><ul>"+
				"<td>"+
					"<label for='specifedName'>SpecifedName: </label>"+
					"<input type='text' id = 'specifedName::"+device.id+"' value = '"+device.specifedName+"' required pattern='[a-zA-Z0-9:_]{1,32}'/>"+
				"</li>"+
				"<li>"+
					"<label for='isEnable'>IsEnable:</label>"+
					"<input type='checkbox' id = 'isEnable::"+device.id+"' checked/>"+
				"</li>";
			for (channel of device.channels) {
				settings += "<br/><li>"+
					"<p align = 'center'><strong>"+channel.specifedName+"</strong></p>"+
				"</li>"+
				"<li>"+
					"<label>SpecifedName: </label>"+
					"<input type='text'  id = 'specifedName::"+channel.id+"' value = '"+channel.specifedName+"' required pattern='[a-zA-Z0-9:_]{1,32}'/>"+
				"</li>"+
				"<li>"+
					"<label>corrector1: </label>"+
					"<input type='text'  id = 'corrector1::"+channel.id+"' value = '"+channel.corrector1+"'/>"+
				"</li>"+
				"<li>"+
					"<label>corrector2: </label>"+
					"<input type='text'  id = 'corrector2::"+channel.id+"' value = '"+channel.corrector2+"'/>"+
				"</li>";
			}
			settings +=	"<li class='dialogLiButton'>"+
					"<button type='submit'>Ok</button> <button type='submit' onclick = 'dialogClose()'>Cansel</button>"+
				"</li>"+
			"</ul></form>";
			return settings;*/
}

function getOutletSettingsMob(device) {
	return 	"<form>"+
			"<table align = 'center'>"+
				"<tr>"+
					"<td align = 'left'><strong>SpecifedName:</strong></td>"+
					"<td align = 'left'><input type='text' id = 'specifedName::"+device.id+"' value = '"+device.specifedName+"' required pattern='[a-zA-Z0-9:_]{1,32}'/></td>"+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>IsEnable:</strong></td>"+
					"<td><input type = 'checkbox' onclick = 'OnOffPSDev(this,\""+device.id+"\")' "+(device.isEnable == true ? 'checked':'')+"></td>"+
				"</tr>"+
			"</table>"+
				"<button type='submit'>Ok</button>"+
				"<button onclick = 'returnBack()'>Cansel</button>"+
			"</form>";	
}
function getPWM_OutputSettingsMob(device) {
	return 	"<form>"+
			"<table align = 'center'>"+
				"<tr>"+
					"<td align = 'left'><strong>SpecifedName:</strong></td>"+
					"<td align = 'left'><input type='text' id = 'specifedName::"+device.id+"' value = '"+device.specifedName+"' required pattern='[a-zA-Z0-9:_]{1,32}'/></td>"+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>value:</strong></td>"+
					"<td align = 'left'><input type='number' min = '0' max = '100' value = '"+device.value+"' id = 'value::"+device.id+"'/>%</td>"+
				"</tr>"+
			"</table>"+
				"<button type='submit'>Ok</button>"+
				"<button onclick = 'returnBack()'>Cansel</button>"+
			"</form>";	
}
function getSolenoidSettingsMob(device) {
	return 	"<form>"+
			"<table align = 'center'>"+
				"<tr>"+
					"<td align = 'left'><strong>SpecifedName:</strong></td>"+
					"<td align = 'left'><input type='text' id = 'specifedName::"+device.id+"' value = '"+device.specifedName+"' required pattern='[a-zA-Z0-9:_]{1,32}'/></td>"+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>IsEnable:</strong></td>"+
					"<td align = 'left'><input type = 'checkbox' onclick = 'OnOffPSDev(this,\""+device.id+"\")' "+(device.isEnable == true ? 'checked':'')+"></td>"+
				"</tr>"+
			"</table>"+
				"<button type='submit'>Ok</button>"+
				"<button onclick = 'returnBack()'>Cansel</button>"+
			"</form>";		
}
function getAnalogInputSettingsMob(device) {
	return 	"<form>"+
			"<table align = 'center'>"+
				"<tr>"+
					"<td align = 'left'><strong>SpecifedName:</strong></td>"+
					"<td align = 'left'><input type='text' id = 'specifedName::"+device.id+"' value = '"+device.specifedName+"' required pattern='[a-zA-Z0-9:_]{1,32}'/></td>"+
				"</tr>"+
			"</table>"+
				"<button type='submit'>Ok</button>"+
				"<button onclick = 'returnBack()'>Cansel</button>"+
			"</form>";	
}

function showSchedulesTableMob () {
	var table = document.getElementById("schTable");
	if (!table) return;

	if (schedulesMap.size == 0) {
		table.innerHTML = "";
		return;
	}
	var tableContext = "<tr><th>Name</th><th>Type</th><th>Script Name</th></tr>";
	for (var [key,value] of schedulesMap) {
		var tr = "<tr onclick = 'showScheduleSettings(\""+key+"\")' style = \"background : #CD5C5C\">";
		if (value.isActive) {
			tr = "<tr onclick = 'showScheduleSettings(\""+key+"\")' style = \"background : #9ACD32\">";
		}
		tableContext +=
				tr +
				"<td style = 'text-align:left; padding-left : 10px'><strong>"+value.schName+"</strong></td>"+
					"<td>"+value.type+"</td>"+
					"<td>"+value.scriptName+"</td>"+
				"</tr>";
	}
	table.innerHTML = tableContext;
	for (e of document.querySelectorAll('#schTable td')){
		e.setAttribute('style','padding:50px;');
	}
}
function addScheduleMob() {
	oldViewSchedule.push(document.getElementById("Schedule").innerHTML);
	var scheduleId = getNextScheduleId();
	schedulesMap.set(scheduleId,new Schedule(scheduleId,"","",true,null));
	showScheduleSettingsMob(scheduleId);
	
}
function showScheduleSettingsMob(scheduleId) {
	oldViewSchedule.push(document.getElementById("Schedule").innerHTML);
	var settingsWindow = document.getElementById("Schedule");
	var settings = 	"<div class = 'settingsMobBlock'>"+
						"<select id = 'schTypesSelect' onchange = 'showScheduleChangedSettings(\""+scheduleId+"\",this.value)'>"+
							"<option value = 'oneTime'>one-time</option>"+
							"<option value = 'daily'>daily</option>"+
							"<option value = 'periodically'>periodically</option>"+
							"<option value = 'byValue'>by value</option>"+
							"<option value = 'sunsetSunrise'>sunset | sunrise</option>"+
						"</select>"+
						"<hr/>"+
						"<div id = 'changedSchSettingsMob'></div>"+
					"</div>";
	settingsWindow.innerHTML = settings;
	document.getElementById('schTypesSelect').value = schedulesMap.get(scheduleId).type? schedulesMap.get(scheduleId).type : 'oneTime';
	showScheduleChangedSettings(scheduleId);
}
function showScheduleChangedSettings(scheduleId,changedValue) {
	var schedule = schedulesMap.get(scheduleId);
	if (!changedValue) changedValue = schedule.type;
	switch (changedValue) {
		case ('oneTime'): {
			showScheduleOneTimeSettingsMob(schedule);
			break;
		}
		case ('daily'): {
			showScheduleDailySettingsMob(schedule);
			break;
		}
		case ('periodically'): {
			showSchedulePeriodicallySettingsMob(schedule);
			break;
		}
		case ('byValue'): {
			showScheduleByValueSettingsMob(schedule);
			break;
		}
		case ('sunsetSunrise'): {
			showScheduleSunsetSunriseSettingsMob(schedule);
			break;
		}
		default: {
			showScheduleOneTimeSettingsMob(schedule);
			break;
		}
	}
}
function showScheduleOneTimeSettingsMob(schedule) {
	var block = document.getElementById("changedSchSettingsMob");
	var dateInput;
	var timeInput;
	if (schedule.date) {
		dateInput = "<input type='date' id='date' value = '"+schedule.date+"' required pattern='.{10}'>";
	} else {
		schedule.date = new Date().YYYYMMDD();
		dateInput = "<input type='date' id='date' value = '"+schedule.date+"' required pattern='.{10}'>";
	}

	if (schedule.time) {
		timeInput = "<input type='time' id='time' value = '"+schedule.time+"' required pattern='[\d]{2}:[\d]{2}'>";
	} else {
		timeInput = "<input type='time' id='time' required pattern='[\d]{2}:[\d]{2}'>";
	}
	block.innerHTML =  "<form>"+
							"<table align = 'center'>"+
								"<tr>"+
									"<td align = 'left'><strong>ScheduleName:</strong></td>"+
									"<td align = 'right'><input type='text' id = 'scheduleName' value = '"+schedule.schName+"' required pattern='[a-zA-Z0-9]{1,8}'/></td>"+
								"</tr>"+
								"<tr>"+
									"<td align = 'left'><strong>ScriptName:</strong></td>"+
									"<td align = 'right'><select id = 'scriptName'>"+
										getScriptsOptionList()+
									"</select></td>"+
								"</tr>"+
								"<tr>"+
									"<td align = 'left'><strong>Select date:</strong></td>"+
									"<td align = 'right'>"+dateInput+"</td>"+
								"</tr>"+
								"<tr>"+
									"<td align = 'left'><strong>Select time:</strong></td>"+
									"<td align = 'right'>"+timeInput+"</td>"+
								"</tr>"+
								"<tr>"+
									"<td align = 'left'><strong>is Active:</strong></td>"+
									"<td align = 'right'><input type = 'checkbox' onclick = 'OnOffSchedule(this,\""+schedule.id+"\")' "+(schedule.isActive ? 'checked':'')+"></td>"+
								"</tr>"+
							"</table>"+
								"<button type='submit'>Ok</button>"+
								"<button onclick = 'removeNotValidSchedules();returnBack()'>Cansel</button>"+
								"<button onclick = 'removeSchedule(\""+schedule.id+"\");returnBack()'>Remove</button>"+
						"</form>";	
	if (schedule.scriptName) {
		document.querySelector("#scriptName").value = schedule.scriptName;
	}
	document.querySelector('form').onsubmit = function(e) {
		e.preventDefault();
		applyScheduleSettings(schedule,'oneTime');
	};
}
function showSchedulePeriodicallySettingsMob(schedule) {
	var block = document.getElementById("changedSchSettingsMob");
	var periodInput;
	if (schedule.period) {
		var timeData = calcHH_MM_SS(schedule.period);
		var hrs = timeData[0];
		var mins = timeData[1];
		var secs = timeData[2];
		periodInput = 	"<tr>"+
							"<td>hours</td>"+"<td><input type='number' id = 'period_hh' min = '0' value = '"+hrs+"' required pattern='[\d]+'></td>"+
						"</tr>"+
						"<tr>"+
							"<td>minutes</td>"+"<td><input type='number' id = 'period_mm' min = '0' value = '"+mins+"' required pattern='[\d]+'></td>"+
						"</tr>"+
						"<tr>"+
							"<td>seconds</td>"+"<td><input type='number' id = 'period_ss' min = '0' value = '"+secs+"' required pattern='[\d]+'></td>"+
						"</tr>";
	} else {
		periodInput = 	"<tr>"+
							"<td>hours</td>"+"<td><input type='number' id = 'period_hh' min = '0' value = '0' required pattern='[\d]+'></td>"+
						"</tr>"+
						"<tr>"+
							"<td>minutes</td>"+"<td><input type='number' id = 'period_mm' min = '0' value = '0' required pattern='[\d]+'></td>"+
						"</tr>"+
						"<tr>"+
							"<td>seconds</td>"+"<td><input type='number' id = 'period_ss' min = '0' value = '0' required pattern='[\d]+'></td>"+
						"</tr>";
	}
	block.innerHTML = "<form><table>"+
				"<tr>"+
					"<td>ScheduleName: </td>"+
					"<td><input type='text' id = 'scheduleName' value = '"+schedule.schName+"' required pattern='[a-zA-Z0-9]{1,8}' /></td>"+
				"</tr>"+
				"<tr>"+
					"<td>ScriptName:</td>"+
					"<td><select id = 'scriptName'>"+
						getScriptsOptionList()+
					"</select></td>"+
				"</tr>"+
			//	"<tr><p style = 'text-align:center'><strong>Period : </strong></p><tr>"+
				periodInput+
				"<tr>"+
					"<td align = 'left'><strong>is Active:</strong></td>"+
					"<td align = 'right'><input type = 'checkbox' onclick = 'OnOffSchedule(this,\""+schedule.id+"\")' "+(schedule.isActive ? 'checked':'')+"></td>"+
				"</tr>"+
			"</table>"+
			"<button type='submit'>Ok</button>"+
			"<button onclick = 'removeNotValidSchedules();returnBack()'>Cansel</button>"+
			"<button onclick = 'removeSchedule(\""+schedule.id+"\");returnBack()'>Remove</button></form>";
	if (schedule.scriptName) {
		document.querySelector("#scriptName").value = schedule.scriptName;
	}
	document.querySelector('form').onsubmit = function(e) {
		e.preventDefault();
		return applyScheduleSettings(schedule,'periodically');
	};
}
function showScheduleDailySettingsMob(schedule) {
	var block = document.getElementById("changedSchSettingsMob");
	var timeInput;
	if (schedule.dailyTime) {
		timeInput = "<td><input type='time' id = 'dailyTime' value = '"+schedule.dailyTime+"' required pattern='[\d]{2}:[\d]{2}'></td>"
	} else {
		timeInput =	"<td><input type='time' id = 'dailyTime' required pattern='[\d]{2}:[\d]{2}'></td>"
	}
	block.innerHTML =  "<form><table>"+
				"<tr>"+
					"<td>ScheduleName: </td>"+
					"<td><input type='text' id = 'scheduleName' value = '"+schedule.schName+"' required pattern='[a-zA-Z0-9]{1,8}'></td>"+
				"</tr>"+
				"<tr>"+
					"<td>ScriptName:</td>"+
					"<td><select id = 'scriptName'>"+
						getScriptsOptionList()+
					"</select></td>"+
				"</tr>"+
				"<tr>"+
					"<td>Select time:</td>"+
					timeInput+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>is Active:</strong></td>"+
					"<td align = 'right'><input type = 'checkbox' onclick = 'OnOffSchedule(this,\""+schedule.id+"\")' "+(schedule.isActive ? 'checked':'')+"></td>"+
				"</tr>"+
			"</table><br>"+
			"<button type='submit'>Ok</button>"+
			"<button onclick = 'removeNotValidSchedules();returnBack()'>Cansel</button>"+
			"<button onclick = 'removeSchedule(\""+schedule.id+"\");returnBack()'>Remove</button></form>";
			if (schedule.scriptName) {
				document.querySelector("#scriptName").value = schedule.scriptName;
			}
	if (schedule.scriptName) {
		document.querySelector("#scriptName").value = schedule.scriptName;
	}
	document.querySelector('form').onsubmit = function(e) {
		e.preventDefault();
		return applyScheduleSettings(schedule,'daily');
	};
}
function showScheduleByValueSettingsMob(schedule) {
	var block = document.getElementById("changedSchSettingsMob");
	block.innerHTML =  "<form><table>"+
				"<tr>"+
					"<td>ScheduleName: </td>"+
					"<td><input type='text' id = 'scheduleName' value = '"+schedule.schName+"' required pattern='[a-zA-Z0-9]{1,8}'/></td>"+
				"</tr>"+
				"<tr>"+
					"<td>ScriptName:</td>"+
					"<td><select id = 'scriptName'>"+
						getScriptsOptionList()+
					"</select></td>"+
				"</tr>"+
				"<tr>"+
					"<td>SensorName:</td>"+
					"<td><select id = 'sensorId'  onchange='if (this.selectedIndex || this.selectedIndex == 0) drawSchByValUlMob(\""+schedule.id+"\",\""+true+"\");'>"+
						getSensorsOptionList()+
					"</select></td>"+
				"</tr>"+
				"<tr>"+
				"<td><strong>value:</strong></td><td>&nbsp</td>"+
				"</tr>"+
				"<tr id = 'byValueLi'>"+
				//---------------------
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>is Active:</strong></td>"+
					"<td align = 'right'><input type = 'checkbox' onclick = 'OnOffSchedule(this,\""+schedule.id+"\")' "+(schedule.isActive ? 'checked':'')+"></td>"+
				"</tr>"+
			"</table><br>"+
			"<button type='submit'>Ok</button>"+
			"<button onclick = 'removeNotValidSchedules();returnBack()'>Cansel</button>"+
			"<button onclick = 'removeSchedule(\""+schedule.id+"\");returnBack()'>Remove</button></form>";

	drawSchByValUlMob(schedule.id);

	if (schedule.valueComparison) {
			document.querySelector("#valueComparison").value = schedule.valueComparison;
	}
	if (schedule.scriptName) {
		document.querySelector("#scriptName").value = schedule.scriptName;
	}
	if (schedule.sensorId) {
		document.querySelector("#sensorId").value = schedule.sensorId;
	}
	document.querySelector('form').onsubmit = function(e) {
		e.preventDefault();
		return applyScheduleSettings(schedule,'byValue');
	};
}
function drawSchByValUlMob (scheduleId, isSelectLoaded) {
	var schedule = schedulesMap.get(scheduleId);
	var sensor;
	if (isSelectLoaded) {
		sensor = devicesMap.get(document.getElementById("sensorId").value);
	} else {
			sensor = devicesMap.get(schedule.sensorId);
	}
	if (sensor instanceof Outlet) {
		var sensorValueInput = "";
		if (schedule.currentSensor || schedule.currentSensor == 0) {
			sensorValueInput = "<td><input type='number' id = 'sensorValue' value = '"+schedule.currentSensor+"' required pattern='[\d]+'></td>";
		} else if (schedule.overCurrentFlag || schedule.overCurrentFlag == 0) {
			sensorValueInput = "<td><input type='number' id = 'sensorValue' value = '"+schedule.overCurrentFlag+"' required pattern='[\d]+'></td>";
		} else {
			sensorValueInput = "<td><input type='number' id = 'sensorValue' required pattern='[\d]+'></td>";
		}
		document.getElementById("byValueLi").innerHTML = "<td>"+
		"<select id = 'outletValue' style='margin-bottom:30px'>"+
			"<option value = 'currentSensor'>currentSensor</option>"+
			"<option value = 'overCurrentFlag'>overCurrentFlag</option>"+
		"</select>"+
		"<select id = 'valueComparison'>"+
					"<option value = '='>equally</option>"+
					"<option value = 'more'>more than</option>"+
					"<option value = 'less'>less than</option>"+
				"<select></td>"+
				sensorValueInput;
		if (schedule.currentSensor || schedule.currentSensor == 0) {
			document.querySelector("#outletValue").value = "currentSensor";
		}
		if (schedule.overCurrentFlag || schedule.overCurrentFlag == 0 ) {
			document.querySelector("#outletValue").value = "overCurrentFlag";
		}
	} else {
		var sensorValueInput = schedule.sensorValue ? "<td><input type='number' id = 'sensorValue' value = '"+schedule.sensorValue+"' ></td>" : "<td><input type='number' id = 'sensorValue' required pattern='[\d]+'></td>";
		document.getElementById("byValueLi").innerHTML = "<td>value <select id = 'valueComparison'>"+
					"<option value = '='>equally</option>"+
					"<option value = 'more'>more than</option>"+
					"<option value = 'less'>less than</option>"+
				"<select></td>"+
				sensorValueInput;
	}
}
function showScheduleSunsetSunriseSettingsMob(schedule) {
	var block = document.getElementById("changedSchSettingsMob");
	block.innerHTML =  "<form><table id = 'sunsetSunriseTable'>"+
				"<tr>"+
					"<td>ScheduleName: </td>"+
					"<td><input type='text' id = 'scheduleName' value = '"+schedule.schName+"' required pattern='[a-zA-Z0-9]{1,8}'/></td>"+
				"</tr>"+
				"<tr>"+
					"<td>ScriptName:</td>"+
					"<td><select id = 'scriptName'>"+
						getScriptsOptionList()+
					"</select></td>"+
				"</tr>"+
				"<tr>"+
					"<td>sunset | sunrise:</td>"+
					"<td><select id = 'sunsetSunrise'>"+
						"<option value = 'sunset'>sunset</option>"+
						"<option value = 'sunrise'>sunrise</option>"+
					"</select></td>"+
				"</tr>"+
				"<tr>"+
					"<td>time settings</td>"+
					"<td><select id = 'sunTimeSettings' onchange='if (this.selectedIndex || this.selectedIndex == 0) fillSunTimeLiMob(this,\""+schedule.id+"\");'>"+
					"<option value = '='>no time shifting</option>"+
					"<option value = '+'>after</option>"+
					"<option value = '-'>before</option>"+
					"</select></td>"+
				"</tr>"+
				"<tr>"+
					"<td align = 'left'><strong>is Active:</strong></td>"+
					"<td align = 'right'><input type = 'checkbox' onclick = 'OnOffSchedule(this,\""+schedule.id+"\")' "+(schedule.isActive ? 'checked':'')+"></td>"+
				"</tr>"+
			"</table><br>"+
			"<button type='submit'>Ok</button>"+
			"<button onclick = 'removeNotValidSchedules();returnBack()'>Cansel</button>"+
			"<button onclick = 'removeSchedule(\""+schedule.id+"\");returnBack()'>Remove</button></form>";
	if (schedule.scriptName) {
		document.querySelector("#scriptName").value = schedule.scriptName;
	}
	document.querySelector('form').onsubmit = function(e) {
		e.preventDefault();
		return applyScheduleSettings(schedule,'sunsetSunrise');
	};
}
function fillSunTimeLiMob(option,scheduleId) {
	var schedule = schedulesMap.get(scheduleId);
	var timeData = [0,0,0];
	if (schedule.timeShifting) {
		if (schedule.timeShifting > 0) {
			timeData = calcHH_MM_SS(schedule.timeShifting);
		} else {
			timeData = calcHH_MM_SS(schedule.timeShifting * -1);
		}
	}
	switch (option.value) {
		case ('=') : {
			document.getElementById('tr_hrs').remove();
			document.getElementById('tr_mins').remove();
			document.getElementById('tr_secs').remove();
			break;
		}
		case ('+') : {
			
		}
		case ('-') : {
			if(!document.getElementById('tr_hrs')) {
				var tr_hrs = document.createElement('tr');
				var tr_mins = document.createElement('tr');
				var tr_secs = document.createElement('tr');
	
				tr_hrs.setAttribute('id','tr_hrs');
				tr_mins.setAttribute('id','tr_mins');
				tr_secs.setAttribute('id','tr_secs');
	
				document.getElementById('sunsetSunriseTable').appendChild(tr_hrs);
				document.getElementById('sunsetSunriseTable').appendChild(tr_mins);
				document.getElementById('sunsetSunriseTable').appendChild(tr_secs);
			}
			
			document.getElementById('tr_hrs').innerHTML = "<td>hours</td>"+"<td><input type='number' id = 'timeShifting_hh' min = '0' value = '"+timeData[0]+"' required pattern='[\d]+'></td>";
			document.getElementById('tr_mins').innerHTML = "<td>minutes</td>"+"<td><input type='number' id = 'timeShifting_mm' min = '0' value = '"+timeData[1]+"' required pattern='[\d]+'></td>";
			document.getElementById('tr_secs').innerHTML = "<td>seconds</td>"+"<td><input type='number' id = 'timeShifting_ss' min = '0' value = '"+timeData[2]+"' required pattern='[\d]+'></td>";
		}
	}
	return;
}
function showAddDeviceDialogMob() {
	oldViewDevices.push(document.getElementById("devicesBlock").innerHTML);
	var settings = "<form><table align = 'center'>"+
						"<tr>"+
							"<td >Specifed name: </td>"+
							"<td><input type='text' id = 'newDeviceNameInput' required pattern='[a-zA-Z0-9]{1,16}'/></td>"+
						"</tr>"+
						"<tr>"+
							"<td>Device ID: </td>"+
							"<td><input type='text' id = 'newDeviceIdInput' required pattern='[0-9a-fA-F]{12}' /></td>"+
						"</tr>"+
						"<tr>"+
							"<td >Device type:</td>"+
							"<td><select>"+
								"<option value = 'mega'>Mega</option>"+
								"<option value = 'powerStrip'>Power Strip</option>"+
							"<select/></td>"+
						"</tr>"+
					"</table>"+
						"<button type='submit'>Ok</button>"+
						"<button onclick = 'returnBack()'>Cansel</button>"+
					"</form>";
	settings = "<div class = 'settingsMobBlock'>"+settings+"</div>";
	document.getElementById("devicesBlock").innerHTML = settings;
	if(document.querySelector('form')) {
		document.querySelector('form').onsubmit = function(e) {
			e.preventDefault();
			return addDevice();
		};
	}
}
function showTasksBlockMob() {
	let listBlock = document.getElementById("tasksBlock");
	var data = '';
	for (let scrName of scriptsNamesList) {
		data += "<button class = 'buttonDeviceMob' onclick = 'showTask(\""+scrName+"\")'>"+scrName+"</button>";
	}
	data += "<button  class = 'buttonDeviceMob' onclick = 'showAddTaskMenu()'><img src = 'images/plus.png'><strong>&nbsp Add task</strong></button>";
	listBlock.innerHTML = data;
}

function showTaskMob() {
	var code_type_select = 	"<select  onchange='if (this.selectedIndex || this.selectedIndex == 0) showTaskVarsOrCode(this);'>"+
								"<option value = 'vars' >variables</option>"+
								"<option value = 'code' >code</option>"+
							"</select>";
	var exe_select = 	"<select onchange='if (this.selectedIndex || this.selectedIndex == 0) changeTaskExecution(this);' >"+
								"<option value = '-'>once</option>"+
								"<option value = '+'>repeatedly</option>"+
							"</select>";	
	var console_btn = "<button id = 'consoleBtn' style = 'float:right' >console</button>";
	var start_btn = "<button id = 'startBtn' style = 'float:right;' onclick = 'startScript()'>start</button>";
	var return_btn = "<button onclick = 'showTasksBlock()' >&lt;&lt;</button>";
	var settings_btn = "<button onclick = 'showTaskSettingsMob()' style = 'float:right;'>settings</button>";
	var save_btn = "<button onclick = 'taskSave()'>Save</button>";				
	var data = 	"<p class = 'p_mob' >"+
					return_btn+
					code_type_select+
					save_btn+
					settings_btn+
					start_btn+
					console_btn+
				"</p>"+
				"<textarea id = 'taskTextArea' wrap='off'></textarea>";
	document.getElementById("tasksBlock").innerHTML = data;
	document.getElementById("taskTextArea").style.cssText = "border: 1px solid gray; font: 300% 'Consolas', arial, sans-serif;"
	showTaskVars();
	oldViewTasks.push(document.getElementById("tasksBlock").innerHTML);
}
function showTaskSettingsMob() {
	var execution = "<select id = 'taskExecutionSelect'>"+
						"<option value = '-'>once</option>"+
						"<option value = '+'>repeatedly</option>"+
					"</select>";
	data =	"<div class = 'settingsMobBlock'>"+
					"<form>"+
						"<table>"+
							"<tr>"+
								"<td>name : </td>"+
								"<td><input id = 'taskNameInput' type = 'text' value = '"+currentTask.name+"'></td>"+
							"</tr>"+
							"<tr>"+
								"<td>execution  : </td>"+
								"<td>"+execution+"</td>"+
							"</tr>"+
						"</table>"+
						"<button type='submit'>Ok</button>"+
						
					"</form>"+
					"<button onclick = showTask('"+currentTask.name+"')>Cansel</button>"+
					"<button onclick = 'removeTask()'>Remove</button>"+
				"</div>"
	document.getElementById("tasksBlock").innerHTML = data;
	document.querySelector('form').onsubmit = function(e) {
		e.preventDefault();
	//	return applyDevSettings(device);
	//	return alert("not working");
		applyTaskSettingsMob();
	};
}
function showAddTaskMenu() {
	var execution = "<select id = 'taskExecutionSelect'>"+
						"<option value = '-'>once</option>"+
						"<option value = '+'>repeatedly</option>"+
					"</select>";
	data =	"<div class = 'settingsMobBlock'>"+
					"<form>"+
						"<table>"+
							"<tr>"+
								"<td>name : </td>"+
								"<td><input id = 'taskNameInput' type = 'text'' required pattern='[a-zA-Z0-9:_]{1,16}'></td>"+
							"</tr>"+
							"<tr>"+
								"<td>execution  : </td>"+
								"<td>"+execution+"</td>"+
							"</tr>"+
						"</table>"+
						"<button type='submit'>Ok</button>"+
						
					"</form>"+
					"<button onclick = showTasksBlock()>Cansel</button>"+
				"</div>"
	document.getElementById("tasksBlock").innerHTML = data;
	document.querySelector('form').onsubmit = function(e) {
		e.preventDefault();
	//	return applyDevSettings(device);
	//	return alert("not working");
		applyTaskSettingsMob(true);
	};
}
function applyTaskSettingsMob(isNew) {
	if (isNew) {
		var taskNameInput = document.getElementById('taskNameInput');
		scriptsNamesList.push(taskNameInput.value);
		showTasksBlock();
	} else {
		var taskNameInput = document.getElementById('taskNameInput');
		if (taskNameInput.value != currentTask.name) {
			renameTask(taskNameInput.value);
		}
		
		var taskExecutionSelect = document.getElementById('taskExecutionSelect');
		if (taskExecutionSelect.value != currentTask.taskExecution) {
			changeTaskExecution(taskExecutionSelect)
		}
	}
}
function showTaskVarsOrCode(e) {
	if (e.value == 'vars') {
		showTaskVars();
	}
	if (e.value == 'code') {
		showTaskCode();
	}
}

function showSettingsListMob(settingsList) {
	var settingsButtons = "";
	for (settings of settingsList) {
		if (settings != 'DateFormat')
		settingsButtons += "<button class = 'buttonDeviceMob' onclick = showSettingsData('"+settings+"',this) >"+settings+"</button>";
	}
	document.getElementById("Settings").innerHTML = settingsButtons;
}