function showDevicesDesktop(devOnly) { // devOnly - boolean (if true then redraw only devices btns)
	var btnToHighlightID = null;
	if (document.querySelector('.buttonDevice.borderedButton')) {
		btnToHighlightID = document.querySelector('.buttonDevice.borderedButton').id;
	}
	
		var devicesList= "";
		for (var [key,value] of devicesMap) {
			if (value.type == "Mega" || value.type == "PowerStrip")
				devicesList += "<button id = 'button::"+key+"' class = 'buttonDevice' onClick = highlightButton(this);showDeviceChilds('"+key+"') title = '"+value.type+"' >"+value.specifedName+"</button>";
		}
		devicesList += "<button  class = 'buttonDevice' onclick = 'showAddDeviceDialog()'><img src = 'images/plus.png'><strong>&nbspAdd new device</strong></button>";
		document.getElementById("devicesListBlock").innerHTML = devicesList;
		if (!devOnly) {
			document.getElementById("sensorsListBlock").innerHTML = "<p style = 'text-align :center'><strong>Select device!</strong></p>";
			document.getElementById("devicesDataBlock").innerHTML = "";
		} else {
			highlightButton(document.getElementById(btnToHighlightID));
		}
}
function showDeviceChildsDesktop(deviceId, isRedrawData = true) {
	currentShowDevice = deviceId;
	var device = devicesMap.get(deviceId);
	if (isRedrawData) {
		device.showData(deviceId);
	}
	var sensorsList = "";
	if (device.type == "Mega") {
		for (sensor of device.sensors) {
			sensorsList += "<button id = 'button::"+sensor.id+"' class = 'buttonSensor' onclick = 'onClick = highlightButton(this);sensor.showData(\""+sensor.id+"\")' >"+sensor.specifedName+"</button>";
		}
	} else if (device.type == "PowerStrip"){
		for (outlet of device.outlets) {
			sensorsList += "<button id = 'button::"+outlet.id+"' class = 'buttonSensor'  title = '"+outlet.type+"' onclick = 'onClick = highlightButton(this);outlet.showData(\""+outlet.id+"\")'>"+outlet.specifedName+"</button>";
		}
		for (out12v of device.pwmsOutput12V) {
			sensorsList += "<button id = 'button::"+out12v.id+"' class = 'buttonSensor'  title = '"+out12v.type+"' onclick = 'onClick = highlightButton(this);out12v.showData(\""+out12v.id+"\")'>"+out12v.specifedName+"</button>";
		}
		for (out5v of device.pwmsOutput5V) {
			sensorsList += "<button id = 'button::"+out5v.id+"' class = 'buttonSensor'  title = '"+out5v.type+"' onclick = 'onClick = highlightButton(this);out5v.showData(\""+out5v.id+"\")'>"+out5v.specifedName+"</button>";
		}
		for (solenoid24vac of device.solenoids24vac) {
			sensorsList += "<button id = 'button::"+solenoid24vac.id+"' class = 'buttonSensor'  title = '"+solenoid24vac.type+"' onclick = 'onClick = highlightButton(this);solenoid24vac.showData(\""+solenoid24vac.id+"\")'>"+solenoid24vac.specifedName+"</button>";
		}
		for (analogIn of device.analogInputs) {
			sensorsList += "<button id = 'button::"+analogIn.id+"' class = 'buttonSensor'  title = '"+analogIn.type+"' onclick = 'onClick = highlightButton(this);analogIn.showData(\""+analogIn.id+"\")'>"+analogIn.specifedName+"</button>";
		}
	}
	document.getElementById("sensorsListBlock").innerHTML = sensorsList;
}

function showDeviceSettingsDesktop(deviceId) {
	var settingsWindow = document.createElement("dialog");
	settingsWindow.setAttribute("class","settings");
	var device = devicesMap.get(deviceId);
	var settings;
	switch (device.type) {
		case ("Mega") :
		case ("PowerStrip") : settings = getDeviceSettings(device); break;
		case ("MegaSensor") : settings = getMegaSensorSettings(device); break;
		case ("outlet") : settings = getOutletSettings(device); break;
		case ("out12v") :
		case ("out5v") : settings = getPWM_OutputSettings(device); break;
		case ("solenoid") : settings = getSolenoidSettings(device); break;
		case ("AnalogInput") : settings = getAnalogInputSettings(device); break;
	}
	settingsWindow.innerHTML = settings;
	document.body.appendChild(settingsWindow);
	document.querySelector('form').onsubmit = function(e) {
		e.preventDefault();
		return applyDevSettings(device);
	};
	document.querySelector('dialog').showModal();
}


function getDeviceSettings(device) {
	return	"<form><ul>"+
				"<li>"+
					"<label >SpecifedName: </label>"+
					"<input type='text' id = 'specifedName::"+device.id+"' value = '"+device.specifedName+"' required pattern='[a-zA-Z0-9:_]{1,32}'/>"+
				"</li>"+
				"<li>"+
					"<label >IsEnable:</label>"+
					"<input type='checkbox' id = 'isEnable::"+device.id+"' checked/>"+
				"</li>"+
				"<li class='dialogLiButton'>"+
					"<button type='submit'>Ok</button> <button type='submit' onclick = 'dialogClose()'>Cansel</button>"+
				"</li>"+
			"</ul></form>";
}
function getMegaSensorSettings(device) {
	var settings = 	"<form><ul>"+
				"<li>"+
					"<label for='specifedName'>SpecifedName: </label>"+
					"<input type='text' id = 'specifedName::"+device.id+"' value = '"+device.specifedName+"' required pattern='[a-zA-Z0-9:_]{1,32}'/>"+
				"</li>"+
				"<li>"+
					"<label for='isEnable'>IsEnable:</label>"+
					"<input type='checkbox' id = 'isEnable::"+device.id+"' checked/>"+
				"</li>";
			for (channel of device.channels) {
				settings += "<br/><li>"+
					"<p align = 'center'><strong>"+channel.specifedName+"</strong></p>"+
				"</li>"+
				"<li>"+
					"<label>SpecifedName: </label>"+
					"<input type='text'  id = 'specifedName::"+channel.id+"' value = '"+channel.specifedName+"' required pattern='[a-zA-Z0-9:_]{1,32}'/>"+
				"</li>"+
				"<li class='dialogLiButton'>"+
					"<button onclick = 'dialogClose();showCalibrationMenu(\""+channel.id+"\")'>calibration</button>"+
				"</li>";
			}
			settings +=	"<li class='dialogLiButton'>"+
					"<button type='submit'>Ok</button> <button onclick = 'dialogClose()'>Cansel</button>"+
				"</li>"+
			"</ul></form>";
			return settings;
}
function getOutletSettings(device) {
	return	"<form><ul>"+
				"<li>"+
					"<label for='specifedName'>SpecifedName: </label>"+
					"<input type='text' id = 'specifedName::"+device.id+"' value = '"+device.specifedName+"' required pattern='[a-zA-Z0-9:_]{1,32}'/>"+
				"</li>"+
				"<li>"+
					"<label for='isEnable'>Enabled:</label>"+
					"<input type = 'checkbox' onclick = 'OnOffPSDev(this,\""+device.id+"\")' "+(device.isEnable == true ? 'checked':'')+">"+
				"</li>"+
				"<li class='dialogLiButton'>"+
					"<button type='submit'>Ok</button> <button type='submit' onclick = 'dialogClose()'>Cansel</button>"+
				"</li>"+
			"</ul></form>";
}
function getPWM_OutputSettings(device) {
	return	"<form><ul>"+
				"<li>"+
					"<label for='specifedName'>SpecifedName: </label>"+
					"<input type='text' id = 'specifedName::"+device.id+"' value = '"+device.specifedName+"' required pattern='[a-zA-Z0-9:_]{1,32}'/>"+
				"</li>"+
				"<li>"+
					"<label for='value'>value: </label>"+
					"<input type='number' min = '0' max = '100' value = '"+device.value+"' id = 'value::"+device.id+"'/>%"+
				"</li>"+
				"<li class='dialogLiButton'>"+
					"<button type='submit'>Ok</button> <button type='submit' onclick = 'dialogClose()'>Cansel</button>"+
				"</li>"+
			"</ul></form>";
}
function getSolenoidSettings(device) {
	return	"<form><ul>"+
				"<li>"+
					"<label for='specifedName'>SpecifedName: </label>"+
					"<input type='text' id = 'specifedName::"+device.id+"' value = '"+device.specifedName+"' required pattern='[a-zA-Z0-9:_]{1,32}'/>"+
				"</li>"+
				"<li>"+
					"<label for='isEnable'>IsEnable:</label>"+
					"<input type = 'checkbox' onclick = 'OnOffPSDev(this,\""+device.id+"\")' "+(device.isEnable == true ? 'checked':'')+">"+
				"</li>"+
				"<li class='dialogLiButton'>"+
					"<button type='submit'>Ok</button> <button type='submit' onclick = 'dialogClose()'>Cansel</button>"+
				"</li>"+
			"</ul></form>";		
}
function getAnalogInputSettings(device) {
	return	"<form><ul>"+
				"<li>"+
					"<label for='specifedName'>SpecifedName: </label>"+
					"<input type='text' id = 'specifedName::"+device.id+"' value = '"+device.specifedName+"' required pattern='[a-zA-Z0-9:_]{1,32}'/>"+
				"</li>"+
				"<li class='dialogLiButton'>"+
					"<button type='submit'>Ok</button> <button type='submit' onclick = 'dialogClose()'>Cansel</button>"+
				"</li>"+
			"</ul></form>";
}

function showSchedulesTableDesktop () {
	var table = document.getElementById("schTable");
	if (schedulesMap.size == 0) {
		table.innerHTML = "";
		return;
	}
	var tableContext = "<tr><th>Name</th><th>Type</th><th>Script Name</th><th>Is active</th><th>&nbsp</th></tr>";
	for (var [key,value] of schedulesMap) {
		var trTitle = getTrTitle(value);
		tableContext +=
				"<tr title = '"+trTitle+"' class = 'trSchTable'>"+
				"<td style = 'text-align:left; padding-left : 10px'><strong>"+value.schName+"</strong></td>"+
				"<td>"+value.type+"</td>"+
				"<td>"+value.scriptName+"</td>"+
				"<td><input type = 'checkbox' onclick = 'OnOffSchedule(this,\""+key+"\")' "+(value.isActive ? 'checked':'')+"></td>"+
				"<td>"+
					"<button style = 'cursor : pointer'> <img src = 'images/settings.png' onclick = 'showScheduleSettings(\""+key+"\")'> </button>"+
					"<button style = 'cursor : pointer'> <img src = 'images/remove.png' onclick = 'removeSchedule(\""+key+"\")'> </button>"+	
				"</td>"+
				"</tr>";
	}
	table.innerHTML = tableContext;
}
function addScheduleDesktop() {
	var scheduleId = getNextScheduleId();
	schedulesMap.set(scheduleId,new Schedule(scheduleId,"","",true,null));
	var settingsWindow = document.createElement("dialog");
	settingsWindow.setAttribute("class","schedule");
	var settings = "<button onclick = 'dialogClose()' class = 'closeButton'>close</button>"+
				"<div class = 'row'>"+
					"<div id = 'scheduleTypes'>"+
						"<button class = 'buttonDevice' id = 'oneTimeBtn' onclick = 'highlightButton(this);showScheduleOneTimeSettings(\""+scheduleId+"\")'>one-time</button>"+
						"<button class = 'buttonDevice' id = 'dailyBtn' onclick = 'highlightButton(this);showScheduleDailySettings(\""+scheduleId+"\")'>daily</button>"+
						"<button class = 'buttonDevice' id = 'periodicallyBtn' onclick = 'highlightButton(this);showSchedulePeriodicallySettings(\""+scheduleId+"\")'>periodically</button>"+
						"<button class = 'buttonDevice' id = 'byValueBtn' onclick = 'highlightButton(this);showScheduleByValueSettings(\""+scheduleId+"\")'>by value</button>"+
						"<button class = 'buttonDevice' id = 'sunBtn' onclick = 'highlightButton(this);showScheduleSunsetSunriseSettings(\""+scheduleId+"\")'>sunset | sunrise</button>"+
					"</div>"+
					"<div id = 'scheduleSettings'>"+
					"</div>"+
				"</div>";
	settingsWindow.innerHTML = settings;
	document.body.appendChild(settingsWindow);
	highlightButton(document.getElementById("oneTimeBtn"));
	showScheduleOneTimeSettings(scheduleId);
	document.querySelector('dialog').showModal();
}
function showScheduleSettingsDesktop(scheduleId) {
	var settingsWindow = document.createElement("dialog");
	settingsWindow.setAttribute("class","schedule");
	var settings;
	settings = "<button onclick = 'dialogClose()' class = 'closeButton'>close</button>"+
				"<div class = 'row'>"+
					"<div id = 'scheduleTypes'>"+
						"<button class = 'buttonDevice' id = 'oneTimeBtn' onclick = 'highlightButton(this);showScheduleOneTimeSettings(\""+scheduleId+"\")'>one-time</button>"+
						"<button class = 'buttonDevice' id = 'dailyBtn' onclick = 'highlightButton(this);showScheduleDailySettings(\""+scheduleId+"\")'>daily</button>"+
						"<button class = 'buttonDevice' id = 'periodicallyBtn' onclick = 'highlightButton(this);showSchedulePeriodicallySettings(\""+scheduleId+"\")'>periodically</button>"+
						"<button class = 'buttonDevice' id = 'byValueBtn' onclick = 'highlightButton(this);showScheduleByValueSettings(\""+scheduleId+"\")'>by value</button>"+
						"<button class = 'buttonDevice' id = 'sunBtn' onclick = 'highlightButton(this);showScheduleSunsetSunriseSettings(\""+scheduleId+"\")'>sunset | sunrise</button>"+
					"</div>"+
					"<div id = 'scheduleSettings'>"+
					"</div>"+
				"</div>";
	settingsWindow.innerHTML = settings;
	document.body.appendChild(settingsWindow);
	switch (schedulesMap.get(scheduleId).type) {
		case ("oneTime") : {
			highlightButton(document.getElementById("oneTimeBtn"));
			showScheduleOneTimeSettings(scheduleId);
			break;
		}
		case ("daily") : {
			highlightButton(document.getElementById("dailyBtn"));
			showScheduleDailySettings(scheduleId);
			break;
		}
		case ("periodically") : {
			highlightButton(document.getElementById("periodicallyBtn"));
			showSchedulePeriodicallySettings(scheduleId);
			break;
		}
		case ("byValue") : {
			highlightButton(document.getElementById("byValueBtn"));
			showScheduleByValueSettings(scheduleId);
			break;
		}
		case ("sunsetSunrise") : {
			highlightButton(document.getElementById("sunBtn"));
			showScheduleSunsetSunriseSettings(scheduleId);
			break;
		}
	}
	document.querySelector('dialog').showModal();
}
function showScheduleOneTimeSettings(scheduleId) {
	var schedule = schedulesMap.get(scheduleId);
	var block = document.getElementById("scheduleSettings");
	var dateInput;
	var timeInput;
	if (calendarDateFormat == 0) {
		if (schedule.date) {
			dateInput = "<input type='date' id='date' value = '"+schedule.date+"' required pattern='.{10}'>";
		} else {
			schedule.date = new Date().YYYYMMDD();
			dateInput = "<input type='date' id='date' value = '"+schedule.date+"' required pattern='.{10}'>";
		}
	} else {
		var value = '';
		if (!schedule.date) {
			schedule.date = new Date().YYYYMMDD();
		}
		switch (calendarDateFormat) {
			/* 
			  t_mdy - mm-dd-yyyy
			  t_ymd - yyyy-mm-dd
			  p_dmy - dd.mm.yyyy
			  t_dmy - dd-mm-yyyy
			  s_dmy - dd/mm/yyyy
			*/
			// ymd
			case ('t_mdy') : {
				var date = 	schedule.date.split("-");
				value = [date[1],date[2],date[0]].join("-");
				break;
			}
			case ('t_ymd') : {
				value = schedule.date;
				break;
			}
			case ('p_dmy') : {
				var date = 	schedule.date.split("-");
				value = [date[2],date[1],date[0]].join(".");
				break;
			} 
			case ('t_dmy') : {
				var date = 	schedule.date.split("-");
				value = [date[2],date[1],date[0]].join("-");
				break;
			} 
			case ('s_dmy') : {
				var date = 	schedule.date.split("-");
				value = [date[2],date[1],date[0]].join("/");
				break;
			} 
		}
		dateInput = "<input type='text' id = 'my_date' class='calendar_input' onclick = 'showCalendar(this)' value = '"+value+"' readonly/>";
	}
	if (schedule.time) {
		timeInput = "<input type='time' id='time' value = '"+schedule.time+"' required pattern='[\d]{2}:[\d]{2}'>";
	} else {
		timeInput = "<input type='time' id='time' required pattern='[\d]{2}:[\d]{2}'>";
	}
	block.innerHTML =  "<form><ul>"+
				"<li>"+
					"<label>ScheduleName: </label>"+
					"<input type='text' id = 'scheduleName' value = '"+schedule.schName+"' required pattern='[a-zA-Z0-9]{1,8}'/>"+
				"</li>"+
				"<li>"+
					"<label>ScriptName:</label>"+
					"<select id = 'scriptName'>"+
						getScriptsOptionList()+
					"</select>"+
				"</li>"+
				"<li>"+
					"<label>Select date:</label>"+
					dateInput+
				"</li>"+
				"<li>"+
					"<label>Select time:</label>"+
					timeInput+
				"</li>"+
			"</ul><br>"+
			"<button class = 'closeButton'>apply</button></form>";

	if (schedule.scriptName) {
		document.querySelector("#scriptName").value = schedule.scriptName;
	}
	document.querySelector('form').onsubmit = function(e) {
		e.preventDefault();
		return applyScheduleSettings(schedule,'oneTime');
	};
	
}
function showSchedulePeriodicallySettings(scheduleId) {
	var schedule = schedulesMap.get(scheduleId);
	var block = document.getElementById("scheduleSettings");
	var periodInput;
	if (schedule.period) {
		var timeData = calcHH_MM_SS(schedule.period);
		var hrs = timeData[0];
		var mins = timeData[1];
		var secs = timeData[2];
		periodInput = 	"<li>"+
							"<label>hours</label>"+"<input type='number' id = 'period_hh' min = '0' value = '"+hrs+"' required pattern='[\d]+'>"+
						"</li>"+
						"<li>"+
							"<label>minutes</label>"+"<input type='number' id = 'period_mm' min = '0' value = '"+mins+"' required pattern='[\d]+'>"+
						"</li>"+
						"<li>"+
							"<label>seconds</label>"+"<input type='number' id = 'period_ss' min = '0' value = '"+secs+"' required pattern='[\d]+'>"+
						"</li>";
	} else {
		periodInput = 	"<li>"+
							"<label>hours</label>"+"<input type='number' id = 'period_hh' min = '0' value = '0' required pattern='[\d]+'>"+
						"</li>"+
						"<li>"+
							"<label>minutes</label>"+"<input type='number' id = 'period_mm' min = '0' value = '0' required pattern='[\d]+'>"+
						"</li>"+
						"<li>"+
							"<label>seconds</label>"+"<input type='number' id = 'period_ss' min = '0' value = '0' required pattern='[\d]+'>"+
						"</li>";
	}
	block.innerHTML = "<form><ul>"+
				"<li>"+
					"<label>ScheduleName: </label>"+
					"<input type='text' id = 'scheduleName' value = '"+schedule.schName+"' required pattern='[a-zA-Z0-9]{1,8}' />"+
				"</li>"+
				"<li>"+
					"<label>ScriptName:</label>"+
					"<select id = 'scriptName'>"+
						getScriptsOptionList()+
					"</select>"+
				"</li>"+
				"<li><p style = 'text-align:center'><strong>Period : </strong></p><li>"+
				periodInput+
			"</ul><br>"+
			"<button type = 'submit' class = 'closeButton'>apply</button></form>";
	if (schedule.scriptName) {
		document.querySelector("#scriptName").value = schedule.scriptName;
	}
	document.querySelector('form').onsubmit = function(e) {
		e.preventDefault();
		return applyScheduleSettings(schedule,'periodically');
	};
}
function showScheduleDailySettings(scheduleId) {
	var schedule = schedulesMap.get(scheduleId);
	var block = document.getElementById("scheduleSettings");
	var timeInput;
	if (schedule.dailyTime) {
		timeInput = "<input type='time' id = 'dailyTime' value = '"+schedule.dailyTime+"' required pattern='[\d]{2}:[\d]{2}'>"
	} else {
		timeInput =	"<input type='time' id = 'dailyTime' required pattern='[\d]{2}:[\d]{2}'>"
	}
	block.innerHTML =  "<form><ul>"+
				"<li>"+
					"<label>ScheduleName: </label>"+
					"<input type='text' id = 'scheduleName' value = '"+schedule.schName+"' required pattern='[a-zA-Z0-9]{1,8}'>"+
				"</li>"+
				"<li>"+
					"<label>ScriptName:</label>"+
					"<select id = 'scriptName'>"+
						getScriptsOptionList()+
					"</select>"+
				"</li>"+
				"<li>"+
					"<label>Select time:</label>"+
					timeInput+
				"</li>"+
			"</ul><br>"+
			"<button class = 'closeButton'>apply</button></form>";
			if (schedule.scriptName) {
				document.querySelector("#scriptName").value = schedule.scriptName;
			}
	if (schedule.scriptName) {
		document.querySelector("#scriptName").value = schedule.scriptName;
	}
	document.querySelector('form').onsubmit = function(e) {
		e.preventDefault();
		return applyScheduleSettings(schedule,'daily');
	};
}
function showScheduleByValueSettings(scheduleId) {
	var schedule = schedulesMap.get(scheduleId);
	var block = document.getElementById("scheduleSettings");
	block.innerHTML =  "<form><ul>"+
				"<li>"+
					"<label>ScheduleName: </label>"+
					"<input type='text' id = 'scheduleName' value = '"+schedule.schName+"' required pattern='[a-zA-Z0-9]{1,8}'/>"+
				"</li>"+
				"<li>"+
					"<label>ScriptName:</label>"+
					"<select id = 'scriptName'>"+
						getScriptsOptionList()+
					"</select>"+
				"</li>"+
				"<li>"+
					"<label>SensorName:</label>"+
					"<select id = 'sensorId'  onchange='if (this.selectedIndex || this.selectedIndex == 0) drawSchByValUl(\""+scheduleId+"\",\""+true+"\");'>"+
						getSensorsOptionList()+
					"</select>"+
				"</li>"+
				"<li id = 'byValueLi'>"+
				//---------------------
				"</li>"+
			"</ul><br>"+
			"<button type = 'submit' class = 'closeButton'>apply</button></form>";

	drawSchByValUl(scheduleId);

	if (schedule.valueComparison) {
			document.querySelector("#valueComparison").value = schedule.valueComparison;
	}
	if (schedule.scriptName) {
		document.querySelector("#scriptName").value = schedule.scriptName;
	}
	if (schedule.sensorId) {
		document.querySelector("#sensorId").value = schedule.sensorId;
	}
	document.querySelector('form').onsubmit = function(e) {
		e.preventDefault();
		return applyScheduleSettings(schedule,'byValue');
	};
}
function drawSchByValUl (scheduleId, isSelectLoaded) {
	var schedule = schedulesMap.get(scheduleId);
	var sensor;
	if (isSelectLoaded) {
		sensor = devicesMap.get(document.getElementById("sensorId").value);
	} else {
			sensor = devicesMap.get(schedule.sensorId);
	}
	if (sensor instanceof Outlet) {
		var sensorValueInput = "";
		if (schedule.currentSensor || schedule.currentSensor == 0) {
			sensorValueInput = "<input type='number' id = 'sensorValue' value = '"+schedule.currentSensor+"' required pattern='[\d]+'>";
		} else if (schedule.overCurrentFlag || schedule.overCurrentFlag == 0) {
			sensorValueInput = "<input type='number' id = 'sensorValue' value = '"+schedule.overCurrentFlag+"' required pattern='[\d]+'>";
		} else {
			sensorValueInput = "<input type='number' id = 'sensorValue' required pattern='[\d]+'>";
		}
		document.getElementById("byValueLi").innerHTML = "<label>"+
		"<select id = 'outletValue'>"+
			"<option value = 'currentSensor'>currentSensor</option>"+
			"<option value = 'overCurrentFlag'>overCurrentFlag</option>"+
		"</select>"+
		"<select id = 'valueComparison'>"+
					"<option value = '='>equally</option>"+
					"<option value = 'more'>more than</option>"+
					"<option value = 'less'>less than</option>"+
				"<select></label>"+
				sensorValueInput;
		if (schedule.currentSensor || schedule.currentSensor == 0) {
			document.querySelector("#outletValue").value = "currentSensor";
		}
		if (schedule.overCurrentFlag || schedule.overCurrentFlag == 0 ) {
			document.querySelector("#outletValue").value = "overCurrentFlag";
		}
	} else {
		var sensorValueInput = schedule.sensorValue ? "<input type='number' id = 'sensorValue' value = '"+schedule.sensorValue+"' >" : "<input type='number' id = 'sensorValue' required pattern='[\d]+'>";
		document.getElementById("byValueLi").innerHTML = "<label>value <select id = 'valueComparison'>"+
					"<option value = '='>equally</option>"+
					"<option value = 'more'>more than</option>"+
					"<option value = 'less'>less than</option>"+
				"<select></label>"+
				sensorValueInput;
	}
}
function showScheduleSunsetSunriseSettings(scheduleId) {
	var schedule = schedulesMap.get(scheduleId);
	var block = document.getElementById("scheduleSettings");
	block.innerHTML =  "<form><ul>"+
				"<li>"+
					"<label>ScheduleName: </label>"+
					"<input type='text' id = 'scheduleName' value = '"+schedule.schName+"' required pattern='[a-zA-Z0-9]{1,8}'/>"+
				"</li>"+
				"<li>"+
					"<label>ScriptName:</label>"+
					"<select id = 'scriptName'>"+
						getScriptsOptionList()+
					"</select>"+
				"</li>"+
				"<li>"+
					"<label>sunset | sunrise:</label>"+
					"<select id = 'sunsetSunrise'>"+
						"<option value = 'sunset'>sunset</option>"+
						"<option value = 'sunrise'>sunrise</option>"+
					"</select>"+
				"</li>"+
				"<li>"+
					"<label>time settings</label>"+
					"<select id = 'sunTimeSettings' onchange='if (this.selectedIndex || this.selectedIndex == 0) fillSunTimeLi(this,\""+scheduleId+"\");'>"+
					"<option value = '='>no time shifting</option>"+
					"<option value = '+'>after</option>"+
					"<option value = '-'>before</option>"+
					"</select>"+
				"</li>"+
				"<li id = 'sunTimeLi'>"+
				"</li>"+
			"</ul><br>"+
			"<button type = 'submit' class = 'closeButton'>apply</button></form>";
	if (schedule.scriptName) {
		document.querySelector("#scriptName").value = schedule.scriptName;
	}
	document.querySelector('form').onsubmit = function(e) {
		e.preventDefault();
		return applyScheduleSettings(schedule,'sunsetSunrise');
	};
}
function fillSunTimeLi(option,scheduleId) {
	var schedule = schedulesMap.get(scheduleId);
	var timeData = [0,0,0];
	if (schedule.timeShifting) {
		if (schedule.timeShifting > 0) {
			timeData = calcHH_MM_SS(schedule.timeShifting);
		} else {
			timeData = calcHH_MM_SS(schedule.timeShifting * -1);
		}
	}
	switch (option.value) {
		case ('=') : {
			document.getElementById('sunTimeLi').innerHTML = '';
			break;
		}
		case ('+') : {
			
		}
		case ('-') : {
			document.getElementById('sunTimeLi').innerHTML = 
			"<li>"+
				"<label>hours</label>"+"<input type='number' id = 'timeShifting_hh' min = '0' value = '"+timeData[0]+"' required pattern='[\d]+'>"+
			"</li>"+
			"<li>"+
				"<label>minutes</label>"+"<input type='number' id = 'timeShifting_mm' min = '0' value = '"+timeData[1]+"' required pattern='[\d]+'>"+
			"</li>"+
			"<li>"+
				"<label>seconds</label>"+"<input type='number' id = 'timeShifting_ss' min = '0' value = '"+timeData[2]+"' required pattern='[\d]+'>"+
			"</li>";
		}
	}
	return;
}
function showAddDeviceDialogDesktop() {
	var settingsWindow = document.createElement("dialog");
	settingsWindow.setAttribute("class","settings");
	var settings = "<form><ul>"+
						"<li>"+
							"<label >Specifed name: </label>"+
							"<input type='text' id = 'newDeviceNameInput' required pattern='[a-zA-Z0-9]{1,16}'/>"+
						"</li>"+
						"<li>"+
							"<label>Device ID: </label>"+
							"<input type='text' id = 'newDeviceIdInput' required pattern='[0-9a-fA-F]{12}' />"+
						"</li>"+
						"<li>"+
							"<label >Device type:</label>"+
							"<select>"+
								"<option value = 'mega'>Mega</option>"+
								"<option value = 'powerStrip'>Power Strip</option>"+
							"<select/>"+
						"</li>"+
						"<li class='dialogLiButton'>"+
							"<button type='submit'>Ok</button> <button onclick = 'dialogClose()'>Cansel</button>"+
						"</li>"+
					"</ul></form>"
	settingsWindow.innerHTML = settings;
	document.body.appendChild(settingsWindow);
	document.querySelector('dialog').showModal();
	if(document.querySelector('form')) {
		document.querySelector('form').onsubmit = function(e) {
			e.preventDefault();
			return addDevice();
		};
	}
}
function showTasksBlockDesktop() {
	let listBlock = document.getElementById("tasksList");
	let data = '<p>&nbsp</p>';
	for (let scrName of scriptsNamesList) {
		data += "<button class = 'buttonDevice' onclick = 'highlightButton(this);showTask(\""+scrName+"\")'>"+scrName+"</button>";
	}
	data += "<button  class = 'buttonDevice' onclick = 'showTaskSettings(true)'><img src = 'images/plus.png'><strong>&nbsp Add task</strong></button>";
	listBlock.innerHTML = data;
}

function showSettingsListDesktop(settingsList) {
	var settingsButtons = "";
	for (settings of settingsList) {
		settingsButtons += "<button class = 'buttonDevice' onClick = highlightButton(this);showSettingsData('"+settings+"',this) >"+settings+"</button>";
	}
	document.getElementById("settingsListBlock").innerHTML = settingsButtons;
}

// ------------
//  calibration
// ------------

var currentCalibration = {
	calibration : '',
	ch_num : '',
	ch_parent : ''
};

var savedDevicesBlock = "";

function showCalibrationMenu(channelId) {
	savedDevicesBlock = document.getElementById("devicesBlock").innerHTML;
	let ch_data = channelId.split(":");
	let ch_num = ch_data[1];
	let ch_parent = devicesMap.get(ch_data[0]);
	let ch_int = Number.parseInt(ch_num) + 1;
	var dataToView = "<p><button style = 'float:right'onclick = 'restoreDevices()'>X</button>Calibration channel #["+ch_int+"] of sensor ["+ch_parent.specifedName+"] on ["+ch_parent.parent.specifedName+"]</p>";
	dataToView += "<hr/>";
	dataToView += "<div id = 'calibrationResDiv'><div>";
	document.getElementById("devicesBlock").innerHTML = dataToView;
	document.getElementById("calibrationResDiv").innerHTML = "loading...";
	currentCalibration.ch_num = ch_num;
	currentCalibration.ch_parent = ch_parent;
	getCalibrationResult();
}

async function getCalibrationResult() {
	let fetchQuery = '/gCalData/'+currentCalibration.ch_parent.id+'/'+currentCalibration.ch_num;
	fetch(fetchQuery)
	.then(function(response){
		if (response.status !== 200) {
			alert("error : "+response.status);
			return;
		}
	//	{"result":"dnf"} - device not found - ни на одной меге не найден датчик с таким id
	//	{"result":"cioor"} - channel index out of range - неверный индекс канала
	//	{"result":{"sensorID":"0123456789AB","channelIndex":"4","calType":"2","calData":["0","32","99","555"]}} - данные калибровки
		response.json().then(function(jsn){
			if (jsn.result == "dnf") {
				alert("device not found");
				return;
			}
			if (jsn.result == "cioor") {
				alert("channel index out of range");
				return;
			}
			currentCalibration.calibration = jsn.result;
			showCalibrationData();
		})
	})
	.catch(function(err){
		console.log("error on fetch : "+err);
	})
}

function showCalibrationData() {
	//{"result":{"sensorID":"0123456789AB","channelIndex":"4","calType":"2","calData":["0","32","99","555"]}} - данные калибровки
	var dataToView = "";
	dataToView += "<p>current calibration type: [ "+getCalibrationNameByType(currentCalibration.calibration.calType)+" ]</p>";
	if(currentCalibration.calibration.calType > 1) {
		dataToView += getCalibrationDataList(currentCalibration.calibration.calData);
	}
	dataToView += getCalibrationSelect();
	dataToView += "<div id = 'calibrationBtnsDiv'></div>";
	dataToView += "<div id = 'manuallyCalibrationDiv'></div>";
	document.getElementById('calibrationResDiv').innerHTML = dataToView;
	showCalibrationButtons('0');
}

function getCalibrationNameByType(type_number) {
	//	0 - не определено (считать аналогом "без калибровки"),
	//	1 - no calibration,
	//	2 - linear,
	//	3 - parabolic,
	type_number = Number.parseInt(type_number);
	switch(type_number) {
		case (0) : {
			return "no defined";
		}
		case (1) : {
			return "no calibration";
		}
		case (2) : {
			return "linear";
		}
		case (3) : {
			return "parabolic";
		}
	}
	return "no data";
}

function getCalibrationDataList(calData) {
	var dataToView = "<ul style = 'margin-left:25px;margin-bottom:10px;'>";
	for (var i = 0; i < calData.length; i++) {
		var i_tView = i+1;
		dataToView += "<li> constant "+i_tView+" : [ "+calData[i]+" ]</li>";
	}
	dataToView += "</ul>";
	return dataToView;
}

function getCalibrationSelect() {
	return 	"<select id = 'calSelect' onchange='if (this.selectedIndex || this.selectedIndex == 0) showCalibrationButtons(this.value);'>"+
			"	<option value = '0' >no calibration</option>"+
			"	<option value = 'linear' >linear</option>"+
			"	<option value = 'parabolic' >parabolic</option>"+
			"</select>";
}

function showCalibrationButtons(selectedValue) {
	let saveBtn = "<button>save</button>";
	let calManualyBtn = "<button onclick = 'showManualyCalMenu()'>enter calibration data manually</button>";
	let calProcBtn = "<button onclick = 'startCalibrationProcedure()'>start calibration procedure</button>";
	switch (selectedValue) {
		case('0') : {
			document.getElementById("calibrationBtnsDiv").innerHTML = saveBtn;
			return;
		}
		case('linear') : {
			document.getElementById("calibrationBtnsDiv").innerHTML = calManualyBtn+calProcBtn;
			return;
		}
		case('parabolic') : {
			document.getElementById("calibrationBtnsDiv").innerHTML = calManualyBtn+calProcBtn;
			return;
		}
	}	
}

function showManualyCalMenu() {
	var dataToView = "<form><ul>";
	for (var i = 0; i < currentCalibration.calibration.calData.length; i++) {
		var i_tView = i+1;
		dataToView += "<li>";
		dataToView += "<label>option : "+i_tView+"</label>";
		dataToView += "<input type = 'text' required pattern='[\\d]{1,10}'>"
		dataToView += "</li>";
	}
	dataToView += "</ul>";
	dataToView += "<button type = 'submit'>save</button>"
	dataToView += "</form>";
	document.getElementById('manuallyCalibrationDiv').innerHTML = dataToView;
	document.querySelector('form').onsubmit = function(e) {
		e.preventDefault();
		return applyCalibration();
	};
}

async function applyCalibration(autoCalibrationArr) {
	let fetchQuery = '/saveCalData/'+currentCalibration.ch_parent.id+'/'+currentCalibration.ch_num;
	var fetchBody = "::SCA:";
	if (autoCalibrationArr) {
		for (var i = 0; i < capturingPointsCalProcArr.length; i++) {
			fetchBody += capturingPointsCalProcArr[i][1]+":";
		}
	} else {
		var calInps = document.querySelectorAll("#manuallyCalibrationDiv > form > ul > li > input");
		for (var i = 0; i < calInps.length; i++) {
			fetchBody += calInps[i].value+":";
		}
	}
	fetchBody += ":";
	fetch(fetchQuery, {
		method: 'POST',
		body: fetchBody
	})
	.then(function(response) {
		if (response.status !== 200) {
			alert("respons.status err code = "+response.status);
			return;
		}
		response.json().then(function(jsn) {
		//	{"result":"dnf"} - device not found - ни на одной меге не найден датчик с таким id
		//	{"result":"cioor"} - channel index out of range - неверный индекс канала
		//	{"result":"mdna"} - mega does not answer
		//	{"result":"pe"} - parse error - не смог распарсить тело fetch-а
		//	{"result":"swe"} - sensor write error
		//	{"result":"OK"} 
			if (jsn.result == "dnf") {
				alert("device not found");
				return;
			} else if (jsn.result == "cioor") {
				alert("channel index out of range");
				return;
			} else if (jsn.result == "mdna") {
				alert("mega does not answer");
				return;
			} else if (jsn.result == "pe") {
				alert("parse error");
				return;
			} else if (jsn.result == "swe") {
				alert("sensor write error");
				return;
			} else if (jsn.result == "OK") {
				alert("success");
				return;
			} else {
				alert(jsn.result);
				return;
			}
		}) 
	})
	.catch(function(err){
		alert('fetch on error '+err);
		return;
	})
}
var capturingPointCalProc = 1;
var capturingPointsCalProcArr = [];
function startCalibrationProcedure() {
	if (!confirm("You are now about to calibrate the sensor. You will be prompted to capture points, on the basis of which the calibration will be applied. Please prepare the equipment according to the instructions before starting.")) {
		return;
	}
	capturingPointCalProc = 1;
	capturingPointsCalProcArr = [];
	var curChn = devicesMap.get(currentCalibration.calibration.sensorID+":"+currentCalibration.calibration.channelIndex);
	var dataToView = "<div id = 'capturingPointCalProcDiv'>"+"<p>capturig point [ "+capturingPointCalProc+" ]</p>"+"</div>";
	dataToView += 	"<div id = 'curValCalProcDiv'><form>"+
						"<p id = 'curValCalProcP'>current value: [ "+curChn.value+" ]</p>"+
						"<p>actual value: <input type = 'text' id = 'actualValCalProc'  required pattern='[\\d]{1,10}' /></p>"+
						"<button id = 'captureCalProcBtn' type = 'submit'>capture</button>"+
					"</form></div>";
	document.getElementById('calibrationResDiv').innerHTML = dataToView;
	document.querySelector('form').onsubmit = function(e) {
		e.preventDefault();
		return nextCapturePoint();
	};
}
function nextCapturePoint() {
	capturingPointsCalProcArr.push([ '0',  document.getElementById('actualValCalProc').value]);
	if (capturingPointCalProc == 3) {
		var dataToView = "<p>calculated calibration data:</p>";
		dataToView += "<ul style = 'margin-left:25px;margin-bottom:10px;'>";
		for (var i = 0; i < 3; i++) {
			var i_tView = i+1;
			dataToView += "<li> constant "+i_tView+" : [ "+capturingPointsCalProcArr[i][1]+" ]</li>";
		}
		dataToView += "</ul>";
		dataToView += "<button onclick = 'applyCalibration(true)'>apply</button>";
		dataToView += "<button onclick = 'restoreDevices()'>cansel</button>";
		document.getElementById('calibrationResDiv').innerHTML = dataToView;
	} else {
		capturingPointCalProc++;
		document.getElementById('capturingPointCalProcDiv').innerHTML = "<p>capturig point [ "+capturingPointCalProc+" ]</p>";
		document.getElementById('actualValCalProc').value = '';
	}
}
function restoreDevices() {
	document.getElementById('devicesBlock').innerHTML = savedDevicesBlock;
}