var userDeviceType = "";
async function onLoad() {
	userDeviceType = devTypeDef(); // mobile or desktop
	if (userDeviceType == 'desktop') {
		document.getElementById('consoleBtn').onclick = function() {
				var f=window.open("/log.htm",'targetWindow',
                                   `toolbar=no,
                                    location=no,
                                    status=no,
                                    menubar=no,
                                    scrollbars=no,
                                    resizable=yes,
                                    width=700,
                                    height=500`);
			}
			// -------------
			// -------------
			document.getElementById('taskTextArea').addEventListener('keydown', function(e) { // for tab button working in textarea
		if(currentTask) {
			currentTask.isEdited = true;
		}
		if (e.key == 'Tab') {
		  e.preventDefault();
		  var start = this.selectionStart;
		  var end = this.selectionEnd;
	  
		  // set textarea value to: text before caret + tab + text after caret
		  this.value = this.value.substring(0, start) +
			"\t" + this.value.substring(end);
	  
		  // put caret at right position again
		  this.selectionStart =
			this.selectionEnd = start + 1;
		}
	  });
	} else {
		editStylesForMobile();
	}
	 fetch('/names.txt')
  .then(
    function(response) {
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' +
          response.status);
      //  return;
      }
      response.text().then(function(fileData) {
		var idNames = fileData.split("\n");
		for ( var idName of idNames ) {
			var data = idName.trim();
			data = data.split("=");
			if (data.length != 2) continue;
			namesMap.set(data[0].trim(),data[1].trim());
		}
      })
	  .then(function(){
		  fetch('/getDevices/')
		  .then(function(devicesResponse) {
			if (devicesResponse.status !== 200) {
				console.log('Looks like there was a problem. Status Code: ' +
							 devicesResponse.status);
				return;
			}
			devicesResponse.text().then(function(devicesData){
				updateDevicesMap(devicesData);
				initSchedules();
				initScriptsList();
				// ---------------
				if (userDeviceType == 'desktop') {
					document.getElementById("Devices").style.display = "block";
					var devicesTab = document.getElementById("devicesTab");
					devicesTab.className += " active";
					showDevices();
					showTime();
					setInterval ('showTime()',1000)
					setInterval ('update()',1000)
				} else {
					// for mobile view
					document.getElementById("Devices").style.display = "block";
					var devicesTab = document.getElementById("devicesTab");
					devicesTab.className += " active";
					showDevices();
				}
			})
		  })
	  });
    }
  )
  .catch(function(err) {
    console.log('Fetch Error :-S', err);
  });
 }
document.addEventListener("DOMContentLoaded", onLoad);

function devTypeDef(){
	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		return 'mobile';
	} else {
		return 'desktop';
	}
}

function editStylesForMobile() { 
	document.getElementById('timeNow').remove();
	document.getElementById('sensorsListBlock').remove();
	document.getElementById('devicesDataBlock').remove();
	document.getElementById('devicesListBlock').remove();
	
	for (el of document.querySelectorAll('.tab button')) {
		el.style.cssText = 'width : 20%; height : 200px; font-size:250%';
	}


	
	document.getElementById("schTable").setAttribute(	'style',
															'font-size:250%;'+
															'border-collapse: separate;'+
															'border-spacing: 0px 40px;'+
															'border: none;');

	document.getElementById("addScheduleButton").setAttribute('class','footer');
	document.getElementById("addScheduleButton").innerHTML = "<button "+
											"style = 'width:100%; font-size:500%; background:#1E90FF; color: white; "+
											"' onclick = 'addSchedule()'>(+) Add schedule</button>";

	document.getElementById('tasksList').remove();
	document.getElementById('taskBlock').remove();

	document.getElementById('settingsListBlock').remove();
	document.getElementById('settingsDataBlock').remove();
}