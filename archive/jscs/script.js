function onKeyUp(e) {
	if (e.keyCode == 27) {
		if (document.querySelector('dialog')) {
			dialogClose();
		}
	}
}
document.addEventListener("keyup", onKeyUp);

var allowUpdateDevices = true;
var updateState = 0;
var controllerTime = 0;
var devicesMap = new Map();
var namesMap = new Map();
var schedulesNamesMap = new Map()
var schedulesMap = new Map();
var scriptsNamesList = [];
var currentShowDevice = "";
var currentShowData = "";
var currentTask = null;
var calendarDateFormat = 0;
var selectedSettings = null;
var currentMenu = 'Devices';
var oldViewDevices = [];
var oldViewSchedule = [];
var oldViewTasks = [];

async function onLoad() {
	document.getElementById('consoleBtn').onclick = function() {
				var f=window.open("/log.htm",'targetWindow',
                                   `toolbar=no,
                                    location=no,
                                    status=no,
                                    menubar=no,
                                    scrollbars=no,
                                    resizable=yes,
                                    width=700,
                                    height=500`);
			}
	document.getElementById('taskTextArea').addEventListener('keydown', function(e) {
		if(currentTask) {
			currentTask.isEdited = true;
		}
		if (e.key == 'Tab') {
		  e.preventDefault();
		  var start = this.selectionStart;
		  var end = this.selectionEnd;
	  
		  // set textarea value to: text before caret + tab + text after caret
		  this.value = this.value.substring(0, start) +
			"\t" + this.value.substring(end);
	  
		  // put caret at right position again
		  this.selectionStart =
			this.selectionEnd = start + 1;
		}
	  });


	 fetch('/names.txt')
  .then(
    function(response) {
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' +
          response.status);
      //  return;
      }
      response.text().then(function(fileData) {
		var idNames = fileData.split("\n");
		for ( var idName of idNames ) {
			var data = idName.trim();
			data = data.split("=");
			if (data.length != 2) continue;
			namesMap.set(data[0].trim(),data[1].trim());
		}
      })
	  .then(function(){
		  fetch('/getDevices/')
		  .then(function(devicesResponse) {
			if (devicesResponse.status !== 200) {
				console.log('Looks like there was a problem. Status Code: ' +
							 devicesResponse.status);
				return;
			}
			devicesResponse.text().then(function(devicesData){
				updateDevicesMap(devicesData);
				initSchedules();
				initScriptsList();
				document.getElementById("Devices").style.display = "block";
				var devicesTab = document.getElementById("devicesTab");
				devicesTab.className += " active";
				showDevices();
				showTime();
				setInterval ('showTime()',1000)
				setInterval ('update()',1000)
			})
		  })
	  });
    }
  )
  .catch(function(err) {
    console.log('Fetch Error :-S', err);
  });
 }
document.addEventListener("DOMContentLoaded", onLoad);

function openTab(evt, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
	switch (tabName){
		case ('Devices') : {
			currentMenu = 'Devices';
			break;
		}
		case ("Schedule") : {
			showSchedulesTable(); 
			currentMenu = 'Schedule';
			break;
		}
		case ("TasksList"): {
			currentMenu = 'TasksList';
			showTasksBlock();
			break;
		} 
		case ("Charts"): {
			showChartBlock();
			break;
		}
		case ("Settings") : showSettingsList(); break;
	}
}

function highlightButton(e){
	if(!e){return;}
	if(!e.parentElement){return;}

	for(var i=0;i<e.parentElement.children.length;i++){
		e.parentElement.children[i].classList.remove("borderedButton");
	}
	e.classList.add("borderedButton");
}

function  calcHH_MM_SS(seconds) {
	var hh = parseInt(seconds/3600);
	var mm = parseInt((seconds % 3600)/60);
	var ss = parseInt(seconds % (3600/60));
	return [hh,mm,ss];
}

function dialogClose() {
	removeNotValidSchedules();
	if (document.querySelector('dialog')) {
		document.querySelector('dialog').close();
		document.querySelector('dialog').remove();
	}
	
}
Date.prototype.YYYYMMDD = function() {
	var mm = this.getMonth() + 1;
	var dd = this.getDate();
  
	return [this.getFullYear(),
			(mm>9 ? '-' : '-0') + mm,
			(dd>9 ? '-' : '-0') + dd
		   ].join('');
 }

 Date.prototype.HHmm = function() {
	var hh = this.getHours();
	var mm = this.getMinutes();
  
	return [(hh>9 ? '' : '0') + hh,
			(mm>9 ? ':' : ':0') + mm
		   ].join('');
 }
function showTime() {
	if (controllerTime) {
		var date = new Date(controllerTime*1000);
		var hh = date.getHours();
		var mm = date.getMinutes();
		var ss =date.getSeconds();
		var time = 	[(hh>9 ? '' : '0') + hh,
					 (mm>9 ? ':' : ':0') + mm,
					 (ss>9 ? ':' : ':0') + ss
	   				].join('');
		document.getElementById('timeNow').innerHTML = time;
	}
}
function getCookie(name) {
  let matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}
function update() {
	//if (getCookie("session") == "0") {
	//	window.location.href = '/login/';
	//}
	if (updateState == 2) {
		if (allowUpdateDevices) {
			updateDevices();
		} else {
			controllerTime++;
		}
		updateState = 0;
	} else {
		controllerTime++;
		updateState++;
	}
}