const GRAPH_ACTIONS_FILE_PATH = "/gs.dat";
const GRAPH_ACTIONS_NUMBER_LIMIT = 200;

const GA_HEX_TYPES = {
    dev_virtual : "0000",
    dev_sensor : "4D53",
    dev_timer : "746D",
    dev_clock : "5354",
    dev_sunset : "5353",
    dev_sunrise : "5352",
    dev_ps : "5053",
    dev_hub : "4855",
    dev_outlet : "444F",
    dev_solenoid : "4453",
    dev_analog_in : "4441",
    dev_pwm : "4450",
    dev_script : "5453",
    type_default : "00",
    type_default_waiting : "01",
    type_complementary : "02"
}


let graphicalActionsMap = new Map();

function downloadGraphicalActions() {
    return new Promise((resolve,reject) => {
        fetch(GRAPH_ACTIONS_FILE_PATH)
        .then(response => {
            if (response.status === 200) {
                response.text().then(graphicalActionsHex => {
                    resolve(graphicalActionsHex);
                })
            } else {
                reject(`error in ${downloadGraphicalActions.name} (response.status : ${response.status})`);
            }
        })
        .catch(error => {
            reject(`error in ${downloadGraphicalActions.name} (${error})`);
        })
    })
}

function createGraphicalActionsFile() {
    var data = "";
	for (let [id,ga] of graphicalActionsMap) {
		data += ga.createHexContext() + "\r\n";
	}
	return new File([data], "ga", {
						type: "text/plain",
				});
}

function uploadGraphicalActions() {
    return new Promise((resolve,reject) => {
        let formData = new FormData();
        formData.append('file',createGraphicalActionsFile());
        fetch ('/rdrok/updMP'+GRAPH_ACTIONS_FILE_PATH, {
            method : 'POST',
            body : formData
        })
        .then(response => {
            if (response.status === 200) {
                resolve(true);
            } else {
                reject(`error in ${uploadGraphicalActions.name} (response.status : ${response.status})`);
            }
        })
        .catch(error => {
            reject(`error in ${uploadGraphicalActions.name} (${error})`);
        })
    })
}

function createGraphicalActionId() {
   let freeIDsArr = [];
    for (let i = 0; i < 256; i++) {
        if (!graphicalActionsMap.get(i))
            freeIDsArr.push(i);
    }
    if (freeIDsArr.length == 0) {
        throw("Limit of GraphicalActions reached (on createGraphicalActionId())"); 
    } else {
        let randPos = Math.random()*freeIDsArr.length;
        randPos = Math.trunc(randPos);
        return freeIDsArr[randPos];
    }
}

function initGraphicalActions(graphicalActionsHex) {
    let gaArray = graphicalActionsHex.split("\r\n");
    for (gaHex of gaArray) {

        if (gaHex == "") continue;

        let ga = new GraphicalAction('hex',gaHex);
        if (ga.crc16match()) {
            graphicalActionsMap.set(ga.id,ga);
        }
    }
}


class GraphicalAction {

    constructor(type,gaData) {
        switch (type) {
            case ("hex") : {
                this.hexContext = gaData;
                this.initFromHex();
                break;
            }
            case ("gui") : {
                this.gaData = gaData;
                this.initFromGUI();
            }
        }
    }

    initFromGUI() {

        this.handlerVersion = this.gaData.handlerVersion || 1;
        this.id = this.gaData.id || createGraphicalActionId();
        this.devCondition1 = this.gaData.devCondition1;
        this.comparison1 = this.gaData.comparison1;
        this.valueForCondition1 = this.gaData.valueForCondition1;
        this.devCondition2 = this.gaData.devCondition2;
        this.comparison2 = this.gaData.comparison2;
        this.valueForCondition2 = this.gaData.valueForCondition2;
        this.deviceUsed = this.gaData.deviceUsed;
        this.valueForDevice = this.gaData.valueForDevice;
        this.gaStatus = this.gaData.gaStatus; // ga type + is ga active
    }

    initFromHex() {
        let gaDataArr = this.parseHexContext(this.hexContext);
        this.handlerVersion = gaDataArr[0];
        this.id = gaDataArr[1];
        this.devCondition1 = gaDataArr[2]; 
        this.comparison1 = gaDataArr[3];
        this.valueForCondition1 = gaDataArr[4];
        this.devCondition2 = gaDataArr[5];
        this.comparison2 = gaDataArr[6];
        this.valueForCondition2 = gaDataArr[7];
        this.deviceUsed = gaDataArr[8];
        this.valueForDevice = gaDataArr[9];
        this.gaStatus = gaDataArr[10]; // ga type + is ga active
        this.crc16 = gaDataArr[11];

        if (this.devCondition1.devType == GA_HEX_TYPES.dev_sensor) { 
            this.valueForCondition1 = this.valueForCondition1 / 100;
        }

        if (this.devCondition2.devType == GA_HEX_TYPES.dev_sensor) { 
            this.valueForCondition2 = this.valueForCondition2 / 100;
        }

        if (this.devCondition1.devType == GA_HEX_TYPES.dev_ps) { 
            this.valueForCondition1 = this.valueForCondition1 / 100;
        }

        if (this.devCondition2.devType == GA_HEX_TYPES.dev_ps) { 
            this.valueForCondition2 = this.valueForCondition2 / 100;
        }
    }
    parseHexContext() {
        this.hex = this.hexContext; // hex for cutting
        let paramsArray = []
        while(this.hex.length > 0) {
            paramsArray.push(this.cutNextHexParamFromHexContext(paramsArray));
        }
        return paramsArray;
    }
    cutNextHexParamFromHexContext(paramsArray) {
        switch (paramsArray.length) {
            case (0) : { // handler version
                let version = parseInt(this.cutNextStaticHexParam(2),16);
                return version;
            }
            case (1) : { // id
                let id = parseInt(this.cutNextStaticHexParam(2),16);
                return id;
            }
            case (2) : { // dev condotion 1
                return this.cutDevConditionParam();
            }
            case (3) : { // comparison 1
                let comparison = decodeStrHex(this.cutNextStaticHexParam(2))
                return comparison;
            }
            case (4) : { // int value for condition 1 (4 bytes)
                let value = Number.parseInt(this.cutNextStaticHexParam(8),16);
                return value;
            }
            case (5) : { // dev condotion 2
                return this.cutDevConditionParam();
            }
            case (6) : { // comparison 2
                let comparison = decodeStrHex(this.cutNextStaticHexParam(2))
                return comparison;
            }
            case (7) : { // int value for condition 2 (4 bytes)
                let value = Number.parseInt(this.cutNextStaticHexParam(8),16);
                return value;
            }
            case (8) : { // devise used param
                return this.cutDeviceUsedParam();
            }
            case (9) : { // value for used device
                let value = Number.parseInt(this.cutNextStaticHexParam(8),16);
                return value;
            }
            case(10) : { // graphic action status
                let statusHex = this.cutNextStaticHexParam(4);
                let status = {
                    type : statusHex.substring(0,2),
                    active : decodeBoolHex(statusHex.substring(3,4))
                }
                return status;
            }
            case(11) : {
                return this.cutNextStaticHexParam(4);
            }
            default : {
                this.hex = "";
            }
        }
    }

    cutDevConditionParam() {
        let devType = this.hex.substring(0,4);
        this.hex =  this.hex.substring(4,this.hex.length);

        if (devType == GA_HEX_TYPES.dev_sensor) { // mega sensor
            let sensorId = this.hex.substring(0,12);
            this.hex =  this.hex.substring(12,this.hex.length);
            let channelNumber = this.hex.substring(0,2);
            this.hex =  this.hex.substring(2,this.hex.length);
            this.hex =  this.hex.substring(4,this.hex.length); // for \x00\x00
            return {
                devType : devType,
                sensorId : sensorId,
                channelNumber : channelNumber
            }
        } else if (devType == GA_HEX_TYPES.dev_ps) {
            let psId = this.hex.substring(0,12);
            this.hex =  this.hex.substring(12,this.hex.length);
            let psNode = this.hex.substring(0,4);
            this.hex =  this.hex.substring(4,this.hex.length);
            let nodeIndex = this.hex.substring(0,4);
            this.hex =  this.hex.substring(4,this.hex.length);
            this.hex =  this.hex.substring(8,this.hex.length); // for \x00\x00\x00\x00
            return {
                devType : devType,
                psId : psId,
                psNode : psNode,
                nodeIndex : nodeIndex
            }
        } else if (devType == GA_HEX_TYPES.dev_script) {
            let scriptName =  this.hex.substring(0,50);
            scriptName = decodeStrHex(scriptName).replace(/^.*[\\\/]/, '').split('.')[0];
            this.hex =  this.hex.substring(50,this.hex.length);
            return {
                devType : devType,
                scriptName : scriptName
            }
        } else {
            return {devType : devType}
        }
    }

    cutDeviceUsedParam() {
        let devType = this.hex.substring(0,4);
        this.hex =  this.hex.substring(4,this.hex.length);
        if (devType == GA_HEX_TYPES.dev_ps) { // powerstrip
            let psId = this.hex.substring(0,12);
            this.hex =  this.hex.substring(12,this.hex.length);
            let psNode = this.hex.substring(0,4);
            this.hex =  this.hex.substring(4,this.hex.length);
            let nodeIndex = this.hex.substring(0,4);
            this.hex =  this.hex.substring(4,this.hex.length);
            this.hex =  this.hex.substring(8,this.hex.length); // for \x00\x00\x00\x00
            return {
                devType : devType,
                psId : psId,
                psNode : psNode,
                nodeIndex : nodeIndex
            }
        } else if (devType == GA_HEX_TYPES.dev_hub) { // main hub
            let hubNode = this.hex.substring(0,4);
            this.hex =  this.hex.substring(4,this.hex.length);
            return {
                devType : devType,
                hubNode : hubNode
            }
        } else if (devType == GA_HEX_TYPES.dev_script) {
            let scriptName =  this.hex.substring(0,50);
            scriptName = decodeStrHex(scriptName).replace(/^.*[\\\/]/, '').split('.')[0];
            this.hex =  this.hex.substring(50,this.hex.length);
            return {
                devType : devType,
                scriptName : scriptName
            }
        }
    }

    cutNextStaticHexParam(paramSize) {
        let data = this.hex.substring(0,paramSize);
        this.hex =  this.hex.substring(paramSize,this.hex.length);
        return data;
    }

    createHexContext() {
        if (this.devCondition1.devType == GA_HEX_TYPES.dev_sensor) { 
            this.valueForCondition1 = Math.round(this.valueForCondition1 * 100);
        }

        if (this.devCondition2.devType == GA_HEX_TYPES.dev_sensor) { 
            this.valueForCondition2 = Math.round(this.valueForCondition1 * 100);
        }

        if (this.devCondition1.devType == GA_HEX_TYPES.dev_ps) { 
            this.valueForCondition1 = Math.round(this.valueForCondition1 * 100);
        }

        if (this.devCondition2.devType == GA_HEX_TYPES.dev_ps) { 
            this.valueForCondition2 = Math.round(this.valueForCondition1 * 100);
        }

        let tempHex = "";
        tempHex += numToHex(this.handlerVersion,1);
        tempHex += numToHex(this.id,1);
        // condition 1
        tempHex += this.devCondition1.devType;
        if (this.devCondition1.devType == GA_HEX_TYPES.dev_sensor) {
            tempHex += this.devCondition1.sensorId;
            tempHex += numToHex(this.devCondition1.channelNumber,1);
            tempHex += "0000";
        }
        if (this.devCondition1.devType == GA_HEX_TYPES.dev_ps) {
            tempHex += this.devCondition1.psId;
            tempHex += this.devCondition1.psNode;
            tempHex += numToHex(this.devCondition1.nodeIndex,2);
            tempHex += "00000000";
        }
        if (this.devCondition1.devType == GA_HEX_TYPES.dev_script) {
            tempHex += strToHex(this.devCondition1.scriptName+".wws",25);
        }
        tempHex += strToHex(this.comparison1,1);
        tempHex += numToHex(this.valueForCondition1,4,true);
        // end condition 1
        //condition 2
        tempHex += this.devCondition2.devType;
        if (this.devCondition2.devType == GA_HEX_TYPES.dev_sensor) {
            tempHex += this.devCondition2.sensorId;
            tempHex += numToHex(this.devCondition2.channelNumber,1);
            tempHex += "0000";
        }
        if (this.devCondition2.devType == GA_HEX_TYPES.dev_ps) {
            tempHex += this.devCondition2.psId;
            tempHex += this.devCondition2.psNode;
            tempHex += numToHex(this.devCondition2.nodeIndex,2);
            tempHex += "00000000";
        }
        if (this.devCondition2.devType == GA_HEX_TYPES.dev_script) {
            tempHex += strToHex(this.devCondition2.scriptName+".wws",25);
        }
        tempHex += strToHex(this.comparison2,1);
        tempHex += numToHex(this.valueForCondition2,4,true);
        //end condition 2
        tempHex += this.deviceUsed.devType;
        if (this.deviceUsed.devType == GA_HEX_TYPES.dev_ps) { // powerstrip
            tempHex += this.deviceUsed.psId;
            tempHex += this.deviceUsed.psNode;
            tempHex += numToHex(this.deviceUsed.nodeIndex,2);
            tempHex += "00000000";
        } else if (this.deviceUsed.devType == GA_HEX_TYPES.dev_hub) { // main hub
            tempHex += this.deviceUsed.hubNode;  
        }
        else if (this.deviceUsed.devType == GA_HEX_TYPES.dev_script) { // text script
            tempHex += strToHex(this.deviceUsed.scriptName+".wws",25);
        }
        tempHex += numToHex(this.valueForDevice,4,true);
        tempHex += this.gaStatus.type;
        tempHex += boolToHex(this.gaStatus.active,1);
        tempHex = tempHex.toUpperCase();
        tempHex += crc16_BUYPASS(tempHex);
        return tempHex;
    }
    
    crc16match() {
        if (!this.hexContext) return false;
        let crc16 = this.hexContext.substring(this.hexContext.length - 4);
	    let gaData = this.hexContext.substring(0,this.hexContext.length-4);
	    return crc16_BUYPASS(gaData) == crc16;
    }
}

function createGAObject(gaData) {
    let devCondition1;
    let _devCondition1 = devicesMap.get(gaData.condition1.dev);
    let valueForCondition1;
    let comparison1 = gaData.condition1.comparison;

    if (_devCondition1 instanceof MegaSensorChannel) {
        devCondition1 = {
            devType : GA_HEX_TYPES.dev_sensor,
            sensorId : _devCondition1.parent.id,
            channelNumber : _devCondition1.arrayIndex
        }
        valueForCondition1 = gaData.condition1.value.value;
    } else if (_devCondition1 instanceof AnalogInput) {
        devCondition1 = {
            devType : GA_HEX_TYPES.dev_ps,
            psId : _devCondition1.parent.id,
            psNode : GA_HEX_TYPES.dev_analog_in,
            nodeIndex : _devCondition1.arrayIndex
        }
        valueForCondition1 = gaData.condition1.value.value;
    }  else if (_devCondition1 instanceof DeviceClock) {
        devCondition1 = {
            devType : GA_HEX_TYPES.dev_clock,
        }
        let dateString1 = gaData.condition1.value.date+"T"+gaData.condition1.value.time + convertTimeZoneToViewFormat(systemTimeZone);
        valueForCondition1 = Math.trunc(new Date(dateString1)/1000);
        comparison1 = ">";
    }  else if (_devCondition1 instanceof DeviceTimer) {
        devCondition1 = {
            devType : GA_HEX_TYPES.dev_timer,
        }
        valueForCondition1 = gaData.condition1.value.hh * 3600 + gaData.condition1.value.mm * 60 + gaData.condition1.value.ss;
        comparison1 = ">";
    } else if (_devCondition1 instanceof DeviceScript) {
        devCondition1 = {
            devType : GA_HEX_TYPES.dev_script,
            scriptName : _devCondition1.id
        }
        valueForCondition1 = gaData.condition1.value.value;
    }
    
    

    // ----------------------

    let devCondition2;
    let _devCondition2 = devicesMap.get(gaData.condition2.dev);
    let valueForCondition2;
    let comparison2 = gaData.condition2.comparison;

    if (_devCondition2 instanceof MegaSensorChannel) {
        devCondition2 = {
            devType : GA_HEX_TYPES.dev_sensor,
            sensorId : _devCondition2.parent.id,
            channelNumber : _devCondition2.arrayIndex
        }
        valueForCondition2 = gaData.condition2.value.value;
    } else if (_devCondition2 instanceof AnalogInput) {
        devCondition2 = {
            devType : GA_HEX_TYPES.dev_ps,
            psId : _devCondition2.parent.id,
            psNode : GA_HEX_TYPES.dev_analog_in,
            nodeIndex : _devCondition2.arrayIndex
        }
        valueForCondition2 = gaData.condition2.value.value;
    }  else if (_devCondition2 instanceof DeviceClock) {
        devCondition2 = {
            devType : GA_HEX_TYPES.dev_clock,
        }
        let dateString2 = gaData.condition2.value.date+"T"+gaData.condition2.value.time + convertTimeZoneToViewFormat(systemTimeZone);
        valueForCondition2 = Math.trunc(new Date(dateString2)/1000);
        comparison2 = ">";
    }  else if (_devCondition2 instanceof DeviceTimer) {
        devCondition2 = {
            devType : GA_HEX_TYPES.dev_timer,
        }
        valueForCondition2 = gaData.condition2.value.hh * 3600 + gaData.condition2.value.mm * 60 + gaData.condition2.value.ss;
        comparison2 = ">";
    }  else if (_devCondition2 instanceof DeviceScript) {
        devCondition2 = {
            devType : GA_HEX_TYPES.dev_script,
            scriptName : _devCondition2.id
        }
        valueForCondition2 = gaData.condition2.value.value;
    }
    
    

    // ----------------------

    let deviceUsed;
    let _deviceUsed = devicesMap.get(gaData.action.dev)


    if (_deviceUsed instanceof Outlet) {
        deviceUsed = {
            devType : GA_HEX_TYPES.dev_ps,
            psId : _deviceUsed.parent.id,
            psNode : GA_HEX_TYPES.dev_outlet,
            nodeIndex : _deviceUsed.arrayIndex
        }
    } else if (_deviceUsed instanceof PWM_Output) {
        deviceUsed = {
            devType : GA_HEX_TYPES.dev_ps,
            psId : _deviceUsed.parent.id,
            psNode : GA_HEX_TYPES.dev_pwm,
            nodeIndex : _deviceUsed.arrayIndex
        }
    } else if (_deviceUsed instanceof Solenoid24vac) {
        deviceUsed = {
            devType : GA_HEX_TYPES.dev_ps,
            psId : _deviceUsed.parent.id,
            psNode : GA_HEX_TYPES.dev_solenoid,
            nodeIndex : _deviceUsed.arrayIndex
        }
    } else if (_deviceUsed instanceof DeviceScript) { 
        deviceUsed = {
            devType : GA_HEX_TYPES.dev_script,
            scriptName : _deviceUsed.id
        }
    }

    // ----------------------

    let valueForDevice = gaData.action.value;

    let gaStatus = {
        type : "00",
        active : gaData.isActive
    }

    let newGAObject = {
        devCondition1 : devCondition1,
        comparison1 : comparison1,
        valueForCondition1 : valueForCondition1,
        devCondition2 : devCondition2,
        comparison2 : comparison2,
        valueForCondition2 : valueForCondition2,
        deviceUsed : deviceUsed,
        valueForDevice : valueForDevice,
        gaStatus : gaStatus
    }

    let ga = new GraphicalAction('gui',newGAObject);

    ga.id = currentGaID || createGraphicalActionId();

    ga.name = gaData.name;

    ga.color = gaData.color;

    ga.imgSrc = gaData.imgSrc;
    
    graphicalActionsMap.set(ga.id,ga);
}
function getGADeviceID(GADeviceObject) {
    switch (GADeviceObject.devType) {
        case (GA_HEX_TYPES.dev_ps) : {
            GADeviceObject.nodeIndex = Number.parseInt(GADeviceObject.nodeIndex);
            switch (GADeviceObject.psNode) {
                case (GA_HEX_TYPES.dev_outlet) : {
                    return [GADeviceObject.psId,"outlet",GADeviceObject.nodeIndex].join("-");
                }
                case (GA_HEX_TYPES.dev_pwm) : {
                    return [GADeviceObject.psId,"pwm",GADeviceObject.nodeIndex].join("-");
                }
                case (GA_HEX_TYPES.dev_analog_in) : {
                    return [GADeviceObject.psId,"analog",GADeviceObject.nodeIndex].join("-");
                }
                case (GA_HEX_TYPES.dev_solenoid) : {
                    return [GADeviceObject.psId,"solenoid",GADeviceObject.nodeIndex].join("-");
                }
            }
            break;
        }
        case (GA_HEX_TYPES.dev_sensor) : {
            GADeviceObject.channelNumber = Number.parseInt(GADeviceObject.channelNumber);
            return [GADeviceObject.sensorId,GADeviceObject.channelNumber].join("-");
        }
        case (GA_HEX_TYPES.dev_timer) : {
            let timer = new DeviceTimer();
            timer.id = "timer";
            timer.specifedName = "timer";
            devicesMap.set("timer",timer);
            return "timer";
        }
        case (GA_HEX_TYPES.dev_clock) : {
            let clock = new DeviceClock();
            clock.id = "clock";
            clock.specifedName = "clock";
            devicesMap.set("clock",clock);
            return "clock";
        }
        case (GA_HEX_TYPES.dev_script) : {
            devicesMap.set(GADeviceObject.scriptName,new DeviceScript(GADeviceObject.scriptName));
            return GADeviceObject.scriptName;
        }
    }
}

function initGaViewData (json) {
	try{
		json = JSON.parse(json);
		for (viewObj of json.gaViewData) {
            let ga = graphicalActionsMap.get(viewObj.gaID);
            if (ga) {
                ga.color = viewObj.gaColor;
                ga.name = viewObj.gaName;
                ga.imgSrc = viewObj.gaImgSrc;
            }
		}
 	} catch (e) {

	}
}

function downloadGaViewData() {
    return new Promise((resolve,reject) => {
        fetch("/GA_ViewData.json")
        .then(response => {
            if (response.status === 200) {
                response.text().then(json => {
                    resolve(json);
                })
            }
        })
        .catch(error => {
            reject(error);
        })
    })
}

function uploadGaViewData() {
    return new Promise((resolve,reject) => {
        let file = new File([getGaViewData()],"gaViewData", {
            type: "text/plain"
        });
        let formData = new FormData();
        formData.append('file',file);
        fetch('/rdrok/updMP/GA_ViewData.json', {
            method: 'POST',
            body: formData
        })
        .then(response => {
            resolve(response)
        })
    })
}

function getGaViewData() {
	let data = {
		gaViewData : []
	};
	for ([id,ga] of graphicalActionsMap) {
        data.gaViewData.push({
            gaID : id,
            gaColor : ga.color,
            gaName : ga.name,
            gaImgSrc :  ga.imgSrc
        });
	}
	return JSON.stringify(data);
}