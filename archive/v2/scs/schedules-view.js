function fillSchsTable() {
	var table =  document.querySelector('tbody');
	if (!table) return;

    var tableContext = '';

    for (var [id,sch] of schedulesMap) {
        var trClass = sch.isActive ? 'table-success' : 'table-danger';
        var imgState = sch.isActive ? "<i class='bi bi-check'></i>" : "<i class='bi bi-x'></i>";
        tableContext += "<tr id = '"+id+"' class = '"+trClass+"' onclick = 'showSchSettingsMenu(this)'>"+
                            "<td>"+sch.schName+"</td>"+
                            "<td>"+ sch.getIcon() + "&nbsp;" + sch.type+"</td>"+
                            "<td>"+sch.scriptName+"</td>"+
                            "<td>"+imgState+"</td>"+
                        "</tr>";
    }
    if (tableContext == "") {
        tableContext = "No schedules yet..."
    }
    table.innerHTML = tableContext;
}

function showSchSettingsMenu(e) {
    document.getElementById('container').innerHTML = `
        <h3 style = 'margin:20px;'>Schedule settings</h3>
        <form id = 'sch-form'>
            <div class="form-group mb-3">
                <select id="sch-type-select" class="form-select"  onchange = "showSchSettingsMenu(this)" required>
                    <option value = "oneTime">One time</option>
                    <option value = "daily">Daily</option>
                    <option value = "periodically">Periodically</option>
                    <!--option value = "byValue">By value</option-->
                    <option value = "sunset">Sunset</option>
                    <option value = "sunrise">Sunrise</option>
                </select>
                <div class="form-text">Select the schedule type</div>
            </div>
            <div id = "sch-settings-context">
            </div>
            <hr>
            <button type="submit" class="btn btn-primary"><i class='bi bi-check'></i>&nbsp;Apply</button>
            <button class="btn btn-secondary" onclick = 'returnBack()'><i class="bi bi-arrow-left-short"></i>&nbsp;Cancel</button>
            <button id = "remove-sch-btn" class="btn btn-danger"><i class="bi bi-trash"></i>&nbsp;Remove</button>
        </form>
    `;
    if (e.id == "add-new-sch-btn") {
        showScheduleOneTimeSettings();
        document.getElementById("remove-sch-btn").remove();
    } else {
        var schType;
        var schedule;
        if (e.id == "sch-type-select") {
            schType = e.value;
        } else {
            schedule = schedulesMap.get(e.id);
            schType = schedule.type;
            document.getElementById("remove-sch-btn").onclick = function() {
                removeScheduleForm(schedule);
            }
        }
        switch (schType) {
            case ('oneTime'): {
                showScheduleOneTimeSettings(schedule);
                break;
            }
            case ('daily'): {
                showScheduleDailySettings(schedule);
                break;
            }
            case ('periodically'): {
                showSchedulePeriodicallySettings(schedule);
                break;
            }
            case ('byValue'): {
                showScheduleByValueSettings(schedule);
                break;
            }
            case ('sunset'): {
                showScheduleSunsetSettings(schedule);
                break;
            }
            case ('sunrise'): {
                showScheduleSunriseSettings(schedule);
                break;
            }
            default: {
                showScheduleOneTimeSettings(schedule);
                break;
            }
        } 
    }
    if(document.querySelector('form')) {
        document.querySelector('form').onsubmit = function(e) {
            e.preventDefault();
            return applyScheduleSettings(e,schedule);
        };
    }
}
function showScheduleOneTimeSettings(schedule) {
    document.getElementById('sch-type-select').value = 'oneTime';
    document.getElementById("sch-settings-context").innerHTML = `
        <div class="mb-3">
            <label for="sch-name-input" class="form-label">Schedule name</label>
            <input type="text" class="form-control" id="sch-name-input" required pattern="[a-zA-Z0-9_]{1,16}">
            <div class="form-text">Only letters , numbers and _ (from 1 to 8 characters)</div>
        </div>
        <div class="form-group mb-3">
            <select id="sch-action-select" class="form-select" required>
                <option value = "null" selected>Select the action</option>
            </select>
            <div class="form-text">Select the action</div>
        </div>
        <div class="mb-3">
            <label for="sch-date-input" class="form-label">Date</label>
            <input type="date" class="form-control" id="sch-date-input" required>
        </div>
        <div class="mb-3">
            <label for="sch-time-input" class="form-label">Time</label>
            <input type="time" class="form-control" id="sch-time-input" required>
        </div>
        <div class="form-check form-switch">
            <input class="form-check-input" type="checkbox" value="" id="sch-active-input" > 
            <label class="form-check-label" for="sch-active-input">
                Schedule is active
            </label>
        </div>
    `;
    document.querySelector("form").id = "oneTime-sch-form";
    document.getElementById("sch-action-select").innerHTML = getActionsOptionList();
    if (schedule) {
        document.getElementById("sch-name-input").value = schedule.schName;
        document.getElementById("sch-date-input").value = schedule.date;
        document.getElementById("sch-time-input").value = schedule.time;
        document.getElementById("sch-active-input").checked  = schedule.isActive;
    }
}
function showScheduleDailySettings(schedule) {
    document.getElementById('sch-type-select').value = 'daily';
    document.getElementById("sch-settings-context").innerHTML = `
        <div class="mb-3">
            <label for="sch-name-input" class="form-label">Schedule name</label>
            <input type="text" class="form-control" id="sch-name-input" required pattern="[a-zA-Z0-9_]{1,16}">
            <div class="form-text">Only letters , numbers and _ (from 1 to 8 characters)</div>
        </div>
        <div class="input-group mb-3">
            <select id="sch-action-select" class="form-select" required>
                <option value = "null" selected>Select the action</option>
            </select>
        </div>
        <div class="mb-3">
            <label for="sch-time-input" class="form-label">Time</label>
            <input type="time" class="form-control" id="sch-time-input" required>
        </div>
        <div class="form-check form-switch">
            <input class="form-check-input" type="checkbox" value="" id="sch-active-input" > 
            <label class="form-check-label" for="sch-active-input">
                Schedule is active
            </label>
        </div>
    `;
    document.querySelector("form").id = "daily-sch-form";
    document.getElementById("sch-action-select").innerHTML = getActionsOptionList();
    if (schedule) {
        document.getElementById("sch-name-input").value = schedule.schName;
        document.getElementById("sch-time-input").value = schedule.dailyTime;
        document.getElementById("sch-active-input").checked  = schedule.isActive;
    }
}
function showSchedulePeriodicallySettings(schedule) {
    document.getElementById('sch-type-select').value = 'periodically';
    document.getElementById("sch-settings-context").innerHTML = `
        <div class="mb-3">
            <label for="sch-name-input" class="form-label">Schedule name</label>
            <input type="text" class="form-control" id="sch-name-input" required pattern="[a-zA-Z0-9_]{1,16}">
            <div class="form-text">Only letters , numbers and _ (from 1 to 8 characters)</div>
        </div>
        <div class="form-group mb-3">
            <select id="sch-action-select" class="form-select" required>
                <option value = "null" selected>Select the action</option>
            </select>
            <div class="form-text">Select the action</div>
        </div>
        <div class="mb-3">
            <label for="sch-hours-input" class="form-label">Period (hours)</label>
            <input type="number" class="form-control" id="sch-hours-input" required pattern = "^[0-1]?[0-9]{0,4}$">
        </div>
        <div class="mb-3">
            <label for="sch-minutes-input" class="form-label">Period (minutes)</label>
            <input type="number" class="form-control" id="sch-minutes-input" required pattern = "^[0-1]?[0-9]{0,4}$">
        </div>
        <div class="mb-3">
            <label for="sch-seconds-input" class="form-label">Period (seconds)</label>
            <input type="number" class="form-control" id="sch-seconds-input" required pattern = "^[0-1]?[0-9]{0,4}$">
        </div>
        <div class="form-check form-switch">
            <input class="form-check-input" type="checkbox" value="" id="sch-active-input" > 
            <label class="form-check-label" for="sch-active-input">
                Schedule is active
            </label>
        </div>
    `;
    document.querySelector("form").id = "periodically-sch-form";
    document.getElementById("sch-action-select").innerHTML = getActionsOptionList();
    if (schedule) {
        var timeData = calcHH_MM_SS(schedule.period);
		var hrs = timeData[0];
		var mins = timeData[1];
		var secs = timeData[2];
        document.getElementById("sch-name-input").value = schedule.schName;  
        document.getElementById("sch-hours-input").value = hrs;
        document.getElementById("sch-minutes-input").value = mins;
        document.getElementById("sch-seconds-input").value = secs;
        document.getElementById("sch-active-input").checked  = schedule.isActive;
    } else {
        document.getElementById("sch-hours-input").value = "0";
        document.getElementById("sch-minutes-input").value = "0";
        document.getElementById("sch-seconds-input").value = "0";
    }
}
function showScheduleByValueSettings(schedule) {
    document.getElementById('sch-type-select').value = 'byValue';
    document.getElementById("sch-settings-context").innerHTML = `
        <div class="mb-3">
            <label for="sch-name-input" class="form-label">Schedule name</label>
            <input type="text" class="form-control" id="sch-name-input" required pattern="[a-zA-Z0-9_]{1,16}">
            <div class="form-text">Only letters , numbers and _ (from 1 to 8 characters)</div>
        </div>
        <div class="form-group mb-3">
            <select id="sch-action-select" class="form-select" required>
                <option value = "null" selected>Select the action</option>
            </select>
            <div class="form-text">Select the action</div>
        </div>
        <div class="input-group mb-3">
            <select id="sch-sensor-select" class="form-select" >
                <option value = "null" selected>Select the sensor</option>
            </select>
        </div>
        <div class="row mb-3">
            <div class="col">
                <select id="sch-sensor-select" class="form-select" >
                    <option value = "null" selected>equaly</option>
                    <option value = "null" >more than</option>
                    <option value = "null" >less than</option>
                </select>
            </div>
            <div class="col">
                <input type="number" class="form-control" placeholder="sensor value">
            </div>
        </div>
        <div class="form-check form-switch">
            <input class="form-check-input" type="checkbox" value="" id="sch-active-input" > 
            <label class="form-check-label" for="sch-active-input">
                Schedule is active
            </label>
        </div>
    `;
    document.querySelector("form").id = "byValue-sch-form";
    if (schedule) {
        document.getElementById("sch-name-input").value = schedule.schName;
        document.getElementById("sch-action-select").innerHTML = getActionsOptionList();
        document.getElementById("sch-sensor-select").innerHTML = getSensorsOptionList();
        document.getElementById("sch-active-input").checked  = schedule.isActive;
    }
}
function showScheduleSunsetSettings(schedule) {
    document.getElementById('sch-type-select').value = 'sunset';
    document.getElementById("sch-settings-context").innerHTML = `
        <div class="mb-3">
            <label for="sch-name-input" class="form-label">Schedule name</label>
            <input type="text" class="form-control" id="sch-name-input" required pattern="[a-zA-Z0-9_]{1,16}">
            <div class="form-text">Only letters , numbers and _ (from 1 to 8 characters)</div>
        </div>
        <div class="form-group mb-3">
            <select id="sch-action-select" class="form-select" required>
                <option value = "null" selected>Select the action</option>
            </select>
            <div class="form-text">Select the action</div>
        </div>
        <div class="input-group mb-3">
            <select id="sch-timeShifting-select" class="form-select" onchange = "disableEnableTimeShiftingInputs(this)">
                <option value = "=" selected>no time shifting</option>
                <option value = "+" >after sunset</option>
                <option value = "-" >before sunset</option>
            </select>
        </div>
        <div class="mb-3">
            <label for="sch-hours-input" class="form-label">Time shifting (hours)</label>
            <input type="number" class="form-control" id="sch-hours-input" value = "0" disabled required pattern = "^[0-1]?[0-9]{0,4}$">
        </div>
        <div class="mb-3">
            <label for="sch-minutes-input" class="form-label">Time shifting (minutes)</label>
            <input type="number" class="form-control" id="sch-minutes-input"  value = "0" disabled required pattern = "^[0-1]?[0-9]{0,4}$">
        </div>
        <div class="mb-3">
            <label for="sch-seconds-input" class="form-label">Time shifting (seconds)</label>
            <input type="number" class="form-control" id="sch-seconds-input"   value = "0" disabled required pattern = "^[0-1]?[0-9]{0,4}$">
        </div>
        <div class="form-check form-switch">
            <input class="form-check-input" type="checkbox" value="" id="sch-active-input"> 
            <label class="form-check-label" for="sch-active-input">
                Schedule is active
            </label>
        </div>
    `;
    document.querySelector("form").id = "sunset-sch-form";
    document.getElementById("sch-action-select").innerHTML = getActionsOptionList();
    if (schedule) {
        var timeData = [0,0,0];
        if (schedule.timeShifting) {
            if (schedule.timeShifting > 0) {
                timeData = calcHH_MM_SS(schedule.timeShifting);
                document.getElementById("sch-timeShifting-select").value = "+";
            } else {
                timeData = calcHH_MM_SS(schedule.timeShifting * -1);
                document.getElementById("sch-timeShifting-select").value = "-";
            }
        } else {
            document.getElementById("sch-timeShifting-select").value = "=";
        }
        disableEnableTimeShiftingInputs(document.getElementById("sch-timeShifting-select"));
        document.getElementById("sch-name-input").value = schedule.schName;
        document.getElementById("sch-hours-input").value = timeData[0];
        document.getElementById("sch-minutes-input").value = timeData[1];
        document.getElementById("sch-seconds-input").value = timeData[2];
        document.getElementById("sch-active-input").checked  = schedule.isActive;
    }
}
function showScheduleSunriseSettings(schedule) {
    document.getElementById('sch-type-select').value = 'sunrise';
    document.getElementById("sch-settings-context").innerHTML = `
        <div class="mb-3">
            <label for="sch-name-input" class="form-label">Schedule name</label>
            <input type="text" class="form-control" id="sch-name-input" required pattern="[a-zA-Z0-9_]{1,16}">
            <div class="form-text">Only letters , numbers and _ (from 1 to 8 characters)</div>
        </div>
        <div class="form-group mb-3">
            <select id="sch-action-select" class="form-select" required>
                <option value = "null" selected>Select the action</option>
            </select>
            <div class="form-text">Select the action</div>
        </div>
        <div class="input-group mb-3">
            <select id="sch-timeShifting-select" class="form-select" onchange = "disableEnableTimeShiftingInputs(this)">
                <option value = "=" selected>no time shifting</option>
                <option value = "+" >after sunset</option>
                <option value = "-" >before sunset</option>
            </select>
        </div>
        <div class="mb-3">
            <label for="sch-hours-input" class="form-label">Time shifting (hours)</label>
            <input type="number" class="form-control" id="sch-hours-input" value = "0" disabled required pattern = "^[0-1]?[0-9]{0,4}$">
        </div>
        <div class="mb-3">
            <label for="sch-minutes-input" class="form-label">Time shifting (minutes)</label>
            <input type="number" class="form-control" id="sch-minutes-input"  value = "0" disabled required pattern = "^[0-1]?[0-9]{0,4}$">
        </div>
        <div class="mb-3">
            <label for="sch-seconds-input" class="form-label">Time shifting (seconds)</label>
            <input type="number" class="form-control" id="sch-seconds-input"   value = "0" disabled required pattern = "^[0-1]?[0-9]{0,4}$">
        </div>
        <div class="form-check form-switch">
            <input class="form-check-input" type="checkbox" value="" id="sch-active-input"> 
            <label class="form-check-label" for="sch-active-input">
                Schedule is active
            </label>
        </div>
    `;
    document.querySelector("form").id = "sunrise-sch-form";
    document.getElementById("sch-action-select").innerHTML = getActionsOptionList();
    if (schedule) {
        var timeData = [0,0,0];
        if (schedule.timeShifting) {
            if (schedule.timeShifting > 0) {
                timeData = calcHH_MM_SS(schedule.timeShifting);
                document.getElementById("sch-timeShifting-select").value = "+";
            } else {
                timeData = calcHH_MM_SS(schedule.timeShifting * -1);
                document.getElementById("sch-timeShifting-select").value = "-";
            }
        } else {
            document.getElementById("sch-timeShifting-select").value = "=";
        }
        disableEnableTimeShiftingInputs(document.getElementById("sch-timeShifting-select"));
        document.getElementById("sch-name-input").value = schedule.schName;
        document.getElementById("sch-hours-input").value = timeData[0];
        document.getElementById("sch-minutes-input").value = timeData[1];
        document.getElementById("sch-seconds-input").value = timeData[2];
        document.getElementById("sch-active-input").checked  = schedule.isActive;
    }
}
function disableEnableTimeShiftingInputs(e){
    if (e.value == "=") {
        document.getElementById("sch-hours-input").disabled = true;
        document.getElementById("sch-minutes-input").disabled = true;
        document.getElementById("sch-seconds-input").disabled = true;
    } else {
        document.getElementById("sch-hours-input").disabled = false;
        document.getElementById("sch-minutes-input").disabled = false;
        document.getElementById("sch-seconds-input").disabled = false;
    }
}
function getActionsOptionList() {
	var result = "";
	for (var scrName of scriptsNamesList) {
		result += "<option value = '"+scrName+"'>"+scrName+"</option>";
	}
	return result;
}
function getSensorsOptionList() {
	var result = "";
	for (var [key,value] of devicesMap) {
		if (value.schEnable && value.parent.isEnable) {
			if (value instanceof AnalogInput) {
				result += "<option value = '"+key+"'>"+value.specifedName+"</option>";
			}
			if (value instanceof MegaSensorChannel) {
				if (value.parent.parent.isEnable) {
					result += "<option value = '"+key+"'>"+value.specifedName+"</option>";
				}
			}
			if (value instanceof Outlet) {
				if (value.isEnable) {
					result += "<option value = '"+key+"'>"+value.specifedName+"</option>";
				}
			}
		}
	}
	return result;
}
function returnBack(){
    document.location.href = document.location.href;
    return;
}
function removeScheduleForm(schedule) {
    if (confirm("Remove schedule?")) {
        removeSchedule(schedule);
        document.querySelector('#container').innerHTML = mainScr;
        fillSchsTable();
    }
    return;
}
function applyScheduleSettings(e,schedule) {
    if (e.submitter.classList[1] == 'btn-primary') {
        switch (e.currentTarget.id) {
            case ("oneTime-sch-form") : {
                applyOneTimeSchedule(schedule);
                break;
            }
            case ("daily-sch-form") : {
                applyDailySchedule(schedule);
                break;
            }
            case ("periodically-sch-form") : {
                applyPeriodicallySchedule(schedule);
                break;
            }
            case ("sunset-sch-form") : {
                applySunsetSchedule(schedule);
                break;
            }
            case ("sunrise-sch-form") : {
                applySunriseSchedule(schedule);
                break;
            }
        }   
    }
}
function applyOneTimeSchedule(schedule) {
    if (!schedule) {
        var scheduleId = getNextScheduleId();
        schedule = new Schedule(scheduleId,"","",true,null);
	    schedulesMap.set(scheduleId,schedule);
    }
    schedule.type = "oneTime";
    schedule.schName = document.getElementById("sch-name-input").value;
    schedule.date = document.getElementById("sch-date-input").value;
    schedule.time = document.getElementById("sch-time-input").value;
    schedule.isActive = document.getElementById("sch-active-input").checked;
    schedule.scriptName = document.getElementById("sch-action-select").value;
    document.querySelector('#container').innerHTML = mainScr;
    fillSchsTable();
    uploadSchedulesFile();
	uploadSchedulesNamesFile();
}
function applyDailySchedule(schedule) {
    if (!schedule) {
        var scheduleId = getNextScheduleId();
        schedule = new Schedule(scheduleId,"","",true,null);
	    schedulesMap.set(scheduleId,schedule);
    }
    schedule.type = "daily";
    schedule.schName = document.getElementById("sch-name-input").value;
    schedule.dailyTime = document.getElementById("sch-time-input").value;
    schedule.isActive = document.getElementById("sch-active-input").checked;
    schedule.scriptName = document.getElementById("sch-action-select").value;
    document.querySelector('#container').innerHTML = mainScr;
    fillSchsTable();
    uploadSchedulesFile();
	uploadSchedulesNamesFile();
}
function applyPeriodicallySchedule(schedule) {
    if (!schedule) {
        var scheduleId = getNextScheduleId();
        schedule = new Schedule(scheduleId,"","",true,null);
	    schedulesMap.set(scheduleId,schedule);
    }
    schedule.type = "periodically";
    schedule.schName = document.getElementById("sch-name-input").value;
    schedule.isActive = document.getElementById("sch-active-input").checked;
    schedule.scriptName = document.getElementById("sch-action-select").value;
    schedule.period = 0;
    schedule.period += 3600 * document.getElementById("sch-hours-input").value;
    schedule.period += 60 * document.getElementById("sch-minutes-input").value;
    schedule.period += 1 * document.getElementById("sch-seconds-input").value;
    document.querySelector('#container').innerHTML = mainScr;
    fillSchsTable();
    uploadSchedulesFile();
	uploadSchedulesNamesFile();
}
function applySunsetSchedule(schedule) {
    if (!schedule) {
        var scheduleId = getNextScheduleId();
        schedule = new Schedule(scheduleId,"","",true,null);
	    schedulesMap.set(scheduleId,schedule);
    }
    schedule.type = "sunset";
    schedule.schName = document.getElementById("sch-name-input").value;
    schedule.isActive = document.getElementById("sch-active-input").checked;
    schedule.scriptName = document.getElementById("sch-action-select").value;
    schedule.timeShifting = 0;
    if (document.getElementById("sch-timeShifting-select").value != "=") {
        schedule.timeShifting += 3600 * document.getElementById("sch-hours-input").value;
        schedule.timeShifting += 60 * document.getElementById("sch-minutes-input").value;
        schedule.timeShifting += 1 * document.getElementById("sch-seconds-input").value;
        if (document.getElementById("sch-timeShifting-select").value == "-") {
            schedule.timeShifting *= -1;
        }
    }
    document.querySelector('#container').innerHTML = mainScr;
    fillSchsTable();
    uploadSchedulesFile();
	uploadSchedulesNamesFile();
}
function applySunriseSchedule(schedule) {
    if (!schedule) {
        var scheduleId = getNextScheduleId();
        schedule = new Schedule(scheduleId,"","",true,null);
	    schedulesMap.set(scheduleId,schedule);
    }
    schedule.type = "sunrise";
    schedule.schName = document.getElementById("sch-name-input").value;
    schedule.isActive = document.getElementById("sch-active-input").checked;
    schedule.scriptName = document.getElementById("sch-action-select").value;
    schedule.timeShifting = 0;
    if (document.getElementById("sch-timeShifting-select").value != "=") {
        schedule.timeShifting += 3600 * document.getElementById("sch-hours-input").value;
        schedule.timeShifting += 60 * document.getElementById("sch-minutes-input").value;
        schedule.timeShifting += 1 * document.getElementById("sch-seconds-input").value;
        if (document.getElementById("sch-timeShifting-select").value == "-") {
            schedule.timeShifting *= -1;
        }
    }
    document.querySelector('#container').innerHTML = mainScr;
    fillSchsTable();
    uploadSchedulesFile();
	uploadSchedulesNamesFile();
}