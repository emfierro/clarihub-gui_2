function blockApplyBtn() {
    let applyBtn = document.getElementById('applyBtn');
    applyBtn.disabled = true;
    let loadingView = `<div id = "loadingView" class="spinner-border spinner-border-sm" role="status">
                        <span class="visually-hidden">Загрузка...</span>
                    </div>&nbsp;`;
    applyBtn.innerHTML = loadingView + "Applying";
}

function unblockApplyBtn() {
    let applyBtn = document.getElementById("applyBtn");
    applyBtn.innerHTML = "<i class='bi bi-check'></i>&nbsp;Apply";
    applyBtn.disabled = false;
}