class HUB {
	constructor(id,specifedName, systemTime) {
		this.id = id;
		this.specifedName = specifedName;
		this.time = systemTime;
	}
}
class Mega {
	constructor(id,lastUpdate, specifedName, isEnable, isOnline) {
		this.type= "Mega";
		this.id = id;
		this.lastUpdate = lastUpdate;
		this.specifedName = specifedName;
		this.isEnable = isEnable;
		this.isOnline = isOnline;
		this.sensors = [];
	}
}
class MegaSensor {
	constructor(id,isEnable, specifedName, sensorType, parent) {
		this.parent = parent;
		this.type = "MegaSensor";
		this.sensorType = sensorType;
		this.id = id;
		this.specifedName = specifedName;
		this.isEnable = isEnable;
		this.channels = [];
	}
}

class MegaSensorChannel {
	constructor(id, type, value, corrector1, corrector2, specifedName, parent, arrayIndex) {
		this.parent = parent;
		this.id = id;
		this.type = type;
		this.value = value;
		this.corrector1 = corrector1;
		this.corrector2 = corrector2;
		this.specifedName = specifedName;
		this.nestingLevel = 3;
		this.schEnable = true;
		this.arrayIndex = arrayIndex;
	}
}

class PowerStrip {
	constructor(id,lastUpdate,specifedName, isEnable, isOnline) {
		this.type= "PowerStrip";
		this.id = id;
		this.lastUpdate = lastUpdate;
		this.specifedName = specifedName;
		this.isEnable = isEnable;
		this.isOnline = isOnline;
		this.outlets = [];
		this.pwmsOutput12V = [];
		this.solenoids24vac = [];
		this.analogInputs = [];
		this.nestingLevel = 1;
	}
}

class Outlet {
	constructor(id,currentSensor, isEnable, overCurrentFlag, specifedName, parent, arrayIndex){
		this.arrayIndex = arrayIndex;
		this.parent = parent;
		this.id = id;
		this.type = "outlet";
		this.currentSensor = currentSensor;
		this.isEnable = isEnable;
		this.overCurrentFlag = overCurrentFlag;
		this.specifedName = specifedName;
		this.schEnable = true;
	}
}

class PWM_Output {
	constructor(id, value, isEnable, specifedName, parent, arrayIndex) {
		this.parent = parent;
		this.arrayIndex = arrayIndex;
		this.id = id;
		this.type= "out12v";
		this.value = value;
		this.isEnable = isEnable;
		this.specifedName = specifedName;
	}
}


class Solenoid24vac {
	constructor(id ,isEnable, specifedName, parent, arrayIndex) {
		this.arrayIndex = arrayIndex;
		this.parent = parent;
		this.id = id;
		this.type= "solenoid";
		this.isEnable = isEnable;
		this.specifedName = specifedName;
	}
}

class AnalogInput {
	constructor(id, value , specifedName, parent, arrayIndex) {
		this.arrayIndex = arrayIndex;
		this.parent = parent;
		this.id = id;
		this.type= "AnalogInput";
		this.value = value;
		this.specifedName = specifedName;
		this.schEnable = true;
	}
}

class DeviceTimer {

}

class DeviceClock {

}

class DeviceScript {
	constructor(id) {
		this.id = id;
	}
}


function removeDevice(deviceId) {
	fetch('/dropDevice/'+deviceId)
	.then(function(response){
		if (response.status == 200) {
			devicesMap.delete(deviceId);
			showDevices();
		} else {
			alert ("error : ["+response.status+"]");
		}
	})
	.catch(function(err){
		alert(err);
	})
}
function updateDevicesMap(inData) {
	let devData = JSON.parse(inData);
	for (var i = 0; i < devData.length; i++) {
		devData[i].device = devData[i].device.toLowerCase();
		switch (devData[i].device) {
			case ("system") : {
				controllerTime = devData[i].time;
				var hub = devicesMap.get("system");
				if (hub) {
					hub.time = controllerTime;
				} else {
					var specifedName = namesMap.get("system");
					if (!specifedName) {
						specifedName = "HUB";
					}
					hub = new HUB (	"system",
						specifedName,
						controllerTime
					) ;
					devicesMap.set(hub.id,hub);
				}
				break;
			}
			case ("mega") : {
				var mega = devicesMap.get(devData[i].hwUid);
				if (mega) {
					mega.lastUpdate = devData[i].lastUpdate;
					mega.isEnable = devData[i].isEnable;
					mega.isOnline = devData[i].isOnline;
				} else {
					var specifedName = namesMap.get(devData[i].hwUid);
					if (!specifedName) {
						specifedName = devData[i].hwUid;
					}
					mega = new Mega (	devData[i].hwUid,
						devData[i].lastUpdate,
						specifedName,
						devData[i].isEnable,
						devData[i].isOnline
					) ;
					devicesMap.set(mega.id,mega);
				}
				for (var sind = 0; sind < devData[i].sensors.length; sind++) {
					
					//ignore sensors [id == 000000000000]
					if (devData[i].sensors[sind].hwUid === "000000000000") continue;
					//----------------------------------
					
					var sensor = devicesMap.get(devData[i].sensors[sind].hwUid);
					if (sensor) {
						sensor.isEnable = devData[i].sensors[sind].isEnable;
					} else {
						var specifedName = namesMap.get(devData[i].sensors[sind].hwUid);
						if (!specifedName) {
							specifedName = devData[i].sensors[sind].hwUid;
						}
						sensor = new MegaSensor	(	devData[i].sensors[sind].hwUid,
							devData[i].sensors[sind].isEnable,
							specifedName,
							devData[i].sensors[sind].type,
							mega
						);
						devicesMap.set(sensor.id,sensor);
						mega.sensors.push(sensor);
					}
					for (var chnum = 0; chnum < devData[i].sensors[sind].channels.length; chnum++) {
						var channel = devicesMap.get(sensor.id+"-"+chnum);
						if (channel) {
							channel.value = devData[i].sensors[sind].channels[chnum].value;
						} else {
							var specifedName = namesMap.get(sensor.id+"-"+chnum);
							if (!specifedName) {
								specifedName = "channel-"+Number.parseInt(chnum+1);
							}
							var channel = new MegaSensorChannel	(	sensor.id+"-"+chnum,
																	devData[i].sensors[sind].channels[chnum].type,
																	devData[i].sensors[sind].channels[chnum].value,
																	devData[i].sensors[sind].channels[chnum].corrector1,
																	devData[i].sensors[sind].channels[chnum].corrector2,
																	specifedName,
																	sensor,
																	chnum
																);
							devicesMap.set(channel.id,channel);
							sensor.channels.push(channel);
						}
					}

				}
				break;
			}
			case ("powerstrip") : {
				var powerStrip = devicesMap.get(devData[i].hwUid);
				if (powerStrip) {
					powerStrip.lastUpdate = devData[i].lastUpdate;
					powerStrip.isEnable = devData[i].isEnable;
					powerStrip.isOnline = devData[i].isOnline;
				} else {
					var specifedName = namesMap.get(devData[i].hwUid);
					if (!specifedName) {
						specifedName = devData[i].hwUid;
					}
					powerStrip = new PowerStrip	(	devData[i].hwUid,
														devData[i].lastUpdate,
														specifedName,
														devData[i].isEnable,
														devData[i].isOnline
													) ;
					devicesMap.set(powerStrip.id,powerStrip);
				}
				for (var j = 0; j < devData[i].outlets.length; j++) {
					var outlet = devicesMap.get(powerStrip.id+"-outlet-"+j);
					if (outlet) {
						outlet.overCurrentFlag = devData[i].outlets[j].overCurrentFlag;
						outlet.currentSensor = devData[i].outlets[j].currentSensor;
						outlet.isEnable = devData[i].outlets[j].isEnable;
					} else {
						var specifedName = namesMap.get(powerStrip.id+"-outlet-"+j);
						if (!specifedName) {
							specifedName = "outlet-"+Number.parseInt(j+1);
						}
						var outlet = new Outlet	(	powerStrip.id+"-outlet-"+j,
													devData[i].outlets[j].currentSensor,
													devData[i].outlets[j].isEnable,
													devData[i].outlets[j].overCurrentFlag,
													specifedName,
													powerStrip,
													j
												);
						devicesMap.set(outlet.id,outlet);
						powerStrip.outlets.push(outlet);
					}
				}
				for (var j = 0; j < devData[i].pwmOutput12V.length; j++) {
					var out12v = devicesMap.get(powerStrip.id+"-pwm-"+j);
					if (out12v) {
						out12v.value = devData[i].pwmOutput12V[j].value;
						out12v.isEnable = devData[i].pwmOutput12V[j].isEnable;
					} else {
						var specifedName = namesMap.get(powerStrip.id+"-pwm-"+j);
						if (!specifedName) {
							specifedName = "pwm-"+Number.parseInt(j+1);
						}
						var out12v = new PWM_Output	(	powerStrip.id+"-pwm-"+j,
															devData[i].pwmOutput12V[j].value,
															devData[i].pwmOutput12V[j].isEnable,
															specifedName,
															powerStrip,
															j
														);
						devicesMap.set(out12v.id,out12v);
						powerStrip.pwmsOutput12V.push(out12v);
					}
				}
				for (var j = 0; j < devData[i].solenoid24vac.length; j++) {
					var solenoid = devicesMap.get(powerStrip.id+"-solenoid-"+j);
					if (solenoid) {
						solenoid.isEnable = devData[i].solenoid24vac[j]; 
					} else {
						var specifedName = namesMap.get(powerStrip.id+"-solenoid-"+j);
						if (!specifedName) {
							specifedName = "valve-"+Number.parseInt(j+1);
						}
						var solenoid = new Solenoid24vac	(	powerStrip.id+"-solenoid-"+j,
																devData[i].solenoid24vac[j],
																specifedName,
																powerStrip,
																j
															);
						devicesMap.set(solenoid.id,solenoid);
						powerStrip.solenoids24vac.push(solenoid);
					}
				}
				for (var j = 0; j < devData[i].analogInputs.length; j++) {
					var analog = devicesMap.get(powerStrip.id+"-analog-"+j);
					if (analog) {
						analog.value = devData[i].analogInputs[j];
					} else {
						var specifedName = namesMap.get(powerStrip.id+"-analog-"+j);
						if (!specifedName) {
							specifedName = "analog-"+Number.parseInt(j+1);
						}
						var analog = new AnalogInput	(	powerStrip.id+"-analog-"+j,
															devData[i].analogInputs[j],
															specifedName,
															powerStrip,
															j
														);
						devicesMap.set(analog.id,analog);
						powerStrip.analogInputs.push(analog);
					}
				}
				break;
			}
		}
	}
}
function updateDevices() {
	fetch('/getDevices/').then(function(devicesResponse) {
	if (devicesResponse.status !== 200) {
		console.log('Looks like there was a problem. Status Code: ' +
						devicesResponse.status);
		return;
	}
	devicesResponse.text().then(function(inData){
		updateDevicesMap(inData);
	//	renderDevices();
	})
	}).catch(function(err){
		console.log('fetch err : '+err);
	})
} 
function uploadNamesFile() {
	var data = new FormData()
	data.append('file', createNamesFile())
	return fetch('/rdrok/updMP/names.txt', {
		method: 'POST',
		body: data
	})
}
function createNamesFile() {
	var data = "";
	for (var [key,value] of devicesMap) {
		data += key+" = "+value.specifedName+"\n";
	}
	return new File([data], "names", {
						type: "text/plain",
				});
}

function addNewDevice() {
	return new Promise((resolve,reject) => {
		let id = document.getElementById("deviceIDInput").value;
		let name = document.getElementById("deviceNameInput").value; 
		let type = document.getElementById("deviceTypeSelect").value;
		let url = null;

		if (devicesMap.get(id.toUpperCase())) {
			reject("a device with this ID already exists");
			return;
		}

		switch (type) {
			case ('mega') : {
				url = '/addMega/'+id;
				break;
			}
			case ('powerStrip') : {
				url = '/addSPS/'+id;
				break;
			}
		}

		fetch(url)
		.then(response => {
			if (response.status !== 200) {
				reject ("response status : " + response.status)
			} else {
				response.json()
				.then(answer => {
					if (answer.result == "OK") {
						//Mega = function(id,lastUpdate, specifedName, isEnable, isOnline)
						if (type == 'mega') {
							var mega = new Mega (	id,
								null,
								name,
								null,
								null,
								null
							) ;
							devicesMap.set(id,mega);
						}
						if (type == 'powerStrip') {
							var ps = new PowerStrip (	id,
								null,
								name,
								null,
								null,
								null
							) ;
							devicesMap.set(id,ps);
						}
						resolve(true);
					} else {
						if (answer.result == "error1") {
							reject("Mega limit exceeded ("+answer.hubMaxSPS+")")
						}
						if (answer.result == "error2") {
							reject("PowerStrip limit exceeded ("+answer.hubMaxMEGAS+")")
						}
					}
				})
			}
		})
		.catch(error => {
			reject(error);
		})
	})
}

function getDisEnErrDescr(errorName) {
	switch (errorName) {
		case ('sl_TO') : {return  'slave timeout (not answer from PowerStrip)'}
		case ('ID_nf') : {return  'ID is not found'}
		case ('s_err') : {return  'syntax error'}
		case ('el_or') : {return  'element out of range'}
		case ('pa_or') : {return  'paramenter out of range'}
		default : {return  'unknown error'}
	}
}
function OnOffPSDev(devCheckbox,devId) {
	// ### outlet/out12v/out5v/solenoid
	devCheckbox.disabled = true;
	var device = devicesMap.get(devId);
	var isEnable = 0;
	if (devCheckbox.checked) {
		isEnable = 1;
	}
	let query = '::set:powerStrip:'+device.parent.id+':'+device.type+':E:'+device.arrayIndex+':'+isEnable+'::';
	fetch("/stel/",{
		method: 'POST',
		body: query
	})
	.then(function(response){
		if (response.status == 200) {
			response.json().then(function(answer){
				if (answer.result != "ok") {
					alert('error : ' +getDisEnErrDescr(answer.result));
				}	
			})
		} else {
			alert("error ["+response.status+"]");
		}
		devCheckbox.disabled = false;
	})
	.catch(function(error){
		devCheckbox.disabled = false;
		alert("error ["+error+"]");
		
	})
}
function sendIsEnable(device,enabled) {
	var enabled = enabled === true ? 'enSens' : 'disSens'; 
	if (device instanceof MegaSensor) {
		console.log('fetch :: /'+enabled+'/'+device.id)
		fetch('/'+enabled+'/'+device.id);
	}
}
function applyDevSettings(deviceId) {
	var elems = document.querySelectorAll('ul > li > input');
	for (var i = 0; i < elems.length; i++){
		var elNameData = elems[i].id.split('::',2);
		var elId = elNameData [1];
		var elParam = elNameData[0];
		var device = devicesMap.get(elId);
		if (elems[i].type == "text" || elems[i].type == "number") {
			switch(elParam) {
				case ("specifedName") : {
					device.specifedName = elems[i].value;
					break;
				}
				case ("value") : {
					if (device.type == 'out12v' || device.type == 'out5v') {
						let query = '::set:powerStrip:'+device.parent.id+':'+device.type+':V:'+device.arrayIndex+':'+elems[i].value+'::';
						fetch("/stel/",{
							method: 'POST',
							body: query
						})
						.then(function(response){
							if (response.status == 200) {
								response.json().then(function(answer){
									if (answer.result != "ok") {
										alert('error : ' +getDisEnErrDescr(answer.result));
									}	
								})
							} else {
								alert("error ["+response.status+"]");
							}
							devCheckbox.disabled = false;
						})
						.catch(function(error){
							devCheckbox.disabled = false;
							alert("error ["+error+"]");
							
						})
					} else {
						device.value = elems[i].value;
					}
					break;
				}
				case ("corrector1") : {
					device.corrector1 = elems[i].value;
					break;
				}
				case ("corrector2") : {
					device.corrector2 = elems[i].value;
					break;
				}
			}
		}
	}
	showDeviceChilds(currentShowDevice,false);
	showDevices();
	var showDevice = devicesMap.get(currentShowData);
	showDevice.showData(currentShowData);
	dialogClose();
	uploadNamesFile();
}

function deviceNameIsUnique(device,name) {
	if (device instanceof Mega 
		|| device instanceof PowerStrip) {

			for ([id,devInMap] of devicesMap) {
				if (devInMap instanceof Mega || (devInMap instanceof PowerStrip)) {
					if (devInMap.specifedName == name && device.specifedName != name && device.specifedName != name) {
						return false;
					}
				}
			}

    } else if (device instanceof MegaSensor) {

		for (sensor of device.parent.sensors) {
			if (sensor.specifedName == name && device.specifedName != name) {
				return false;
			}
		}

    } else if (device instanceof MegaSensorChannel) {

        for (channel of device.parent.channels) {
			if (channel.specifedName == name && device.specifedName != name) {
				return false;
			}
		}

    } else if (device instanceof Outlet) {

       for (outlet of device.parent.outlets) {
		   if (outlet.specifedName == name && device.specifedName != name) {
			   return false;
		   }
	   }

    } else if (device instanceof PWM_Output) {

        for (pwm of device.parent.pwmsOutput12V) {
			if (pwm.specifedName == name && device.specifedName != name) {
				return false;
			}
		}

    } else if (device instanceof Solenoid24vac) {

        for (solenoid of device.parent.solenoids24vac) {
			if (solenoid.specifedName == name && device.specifedName != name) {
				return false;
			}
		}

    } else if (device instanceof AnalogInput) {

		for (analog of device.parent.analogInputs) {
			if (analog.specifedName == name && device.specifedName != name) {
				return false;
			}
		}

    }
	return true;
}

function initDevicesColors (json) {
	try{
		json = JSON.parse(json);
		for (colorObj of json.DevicesColors) {
			let device = devicesMap.get(colorObj.deviceID);
			if (device) {
				device.color = colorObj.deviceColor;
			}
		}
 	} catch (e) {

	}
}

function downloadDevicesColors() {
    return new Promise((resolve,reject) => {
        fetch("/DevicesColors.json")
        .then(response => {
            if (response.status === 200) {
                response.text().then(json => {
                    resolve(json);
                })
            }
        })
        .catch(error => {
            reject(error);
        })
    })
}



function uploadDevicesColors() {
    return new Promise((resolve,reject) => {
        let file = new File([getDevicesColors()],"DevicesColors", {
            type: "text/plain"
        });
        let formData = new FormData();
        formData.append('file',file);
        fetch('/rdrok/updMP/DevicesColors.json', {
            method: 'POST',
            body: formData
        })
        .then(response => {
            resolve(response)
        })
    })
}

function getDevicesColors() {
	let data = {
		DevicesColors : []
	};
	for ([id,dev] of devicesMap) {
		if (dev instanceof Mega || dev instanceof PowerStrip || dev instanceof HUB) {
			data.DevicesColors.push({
				deviceID : id,
				deviceColor : dev.color
			});
		}
	}
	return JSON.stringify(data);
}