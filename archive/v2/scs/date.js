Date.prototype.YYYYMMDD = function() {
	var mm = this.getMonth() + 1;
	var dd = this.getDate();
  
	return [this.getFullYear(),
			(mm>9 ? '' : '0') + mm,
			(dd>9 ? '' : '0') + dd
		   ].join('-');
 }

 Date.prototype.HHmm = function() {
	var hh = this.getHours();
	var mm = this.getMinutes();
  
	return [(hh>9 ? '' : '0') + hh,
			(mm>9 ? '' : '0') + mm
		   ].join(':');
 }
 
 function calcHH_MM_SS(seconds) {
	var hh = parseInt(seconds/3600);
	var mm = parseInt((seconds % 3600)/60);
	var ss = parseInt(seconds % (3600/60));
	return [hh,mm,ss];
}