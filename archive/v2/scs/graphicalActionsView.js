let currentGaID;
let currentGaName = "";
let currentGaColor = "#CCCCCC";
let currentGaImgSrc;

const GA_STATES = {
    view_params : 0,
    condition1_devs : 1,
    condition1 : 2,
    action_devs_select : 3,
    action_device : 4,
    condition2_devs : 5,
    condition2 : 6
}

const ACTION_IMAGE_WIDTH = 75;

let gaFormValuesMap = new Map();

let gaCreateState = 0;
let savedGAScreen = "";

let isEditingGaSectionNow = false;

let showGACreateDialogParams = [];

function saveGAScreen() {
    let inputs = document.querySelectorAll('input');
    let selects = document.querySelectorAll('select');
    for (e of inputs) {
        gaFormValuesMap.set (e.id,e.value);
    }
    for (e of selects) {
        gaFormValuesMap.set (e.id,e.value);
    }
    savedGAScreen = document.getElementById('actions-container').innerHTML;
}
function showSavedGAScreen() {
    document.getElementById('actions-container').innerHTML = savedGAScreen;
    for ([key,value] of gaFormValuesMap) {
        if (document.getElementById(key)) {
            document.getElementById(key).value = value;
        }
    }
}
function viewGaParamsScreen(imgSrc,formValues) {
    formValues = formValues || [null,null];
    let gaName = formValues[0] || currentGaName;
    let gaColor = formValues[1] || currentGaColor;

    if (!imgSrc) {
        imgSrc = currentGaImgSrc || SENSORS_IMAGES.script;
    } 

    document.getElementById('actions-container').innerHTML = `
    <h5>Graphical action main settings:</h5>
    <form>
    <div class="my-3">
        <label for="ga-name-input" class="form-label">Action name</label>
        <input type="text" class="form-control" id="ga-name-input" value = "${gaName}" required pattern="[a-zA-Z0-9_-]{1,16}">
        <div class="form-text">Only letters , numbers and _ or - (from 1 to 16 characters)</div>
        <label for="ga-color" class="form-label">Color for <strong>action</strong></label>
        <input type="color" value = "${gaColor}" class="form-control form-control-lg" id="ga-color">
    </div>

    <div id = "sel_icon_div" class="text-center mb-3 p-1 border bg-light row" style="border-radius : 1em;cursor:pointer">
        <div class="col-2">
            <img width="50" src="${imgSrc}" class="img-fluid" alt="...">
        </div>
        <div class="col-9 my-auto">
            <h5>Select the icon</h5>
        </div>
    </div>

    <button type="submit" class="btn btn-primary" id = "applyBtn">
        <i class="bi bi-check"></i> Apply
    </button>
    <button type="button" id = "returnGABackBtn" class="btn btn-secondary">
        <i class="bi bi-arrow-left-short"></i> Cancel
    </button>
    </form>
    `;

    document.getElementById("sel_icon_div").onclick = function() {
        let formValues = [document.getElementById("ga-name-input").value, document.getElementById("ga-color").value];
        showSelectImagesScreen('actions-container',viewGaParamsScreen,formValues)
    }

    if (document.getElementById("returnGABackBtn")) {
        document.getElementById("returnGABackBtn").onclick = function () { 
            returnGABack();
        }
    }
    if(document.querySelector('form')) {
        document.querySelector('form').onsubmit = function(e) {
            e.preventDefault();
            let nameInput = document.getElementById("ga-name-input");
            let colorInput = document.getElementById("ga-color");
            for (let [id,ga] of graphicalActionsMap) {
                if (ga.name == nameInput.value && id != currentGaID) {
                   return alert("Error! An graphical action with the same name already exists!");
                }
            }
            currentGaName = nameInput.value;
            currentGaColor = colorInput.value;
            currentGaImgSrc = imgSrc;
            return showGACreateDialog(); 
        };
    }
}

function showGAParams () {
    document.getElementById('params_ga_div').innerHTML = `
        <div class="text-center p-2 row border" style="border-radius : 1em; background:${currentGaColor}">
            <div class="col-2">
                <img width="50" src="${currentGaImgSrc}" class="img-fluid" alt="...">
            </div>
            <div class="col-8 my-auto">
                <h5>${currentGaName}</h5>
            </div>
            <div class="col-2 my-auto">
                <button type="button" class="btn btn-secondary" onclick = "editGACreateSection(${GA_STATES.view_params})">
                    <i class="bi bi-pen"></i>
                </button>
            </div>
        </div>
        <hr/>
    `;
}

function showGACreateDialog(dataFromCaller) {
    showGACreateDialogParams.push(dataFromCaller);
    switch (gaCreateState) {
        case (GA_STATES.view_params) : {
            if (isEditingGaSectionNow) {
                showSavedGAScreen();
                showGAParams();
                isEditingGaSectionNow = false;
                gaCreateState = GA_STATES.condition1;
                break;
            } else {
                viewGaParamsScreen();
                gaCreateState++;
                return;
            }
        }
        case (GA_STATES.condition1_devs) : {
            showGACreateDevCondition("Select trigger condition device:")
            gaCreateState++;
            break;
        }
        case (GA_STATES.condition1) : {
            if (isEditingGaSectionNow) {
                showSavedGAScreen();
                showGACreateConditionValue(dataFromCaller,'condition1_ga_div','Trigger condition');
                isEditingGaSectionNow = false;
            } else {
                let devicesHeader = "Select action device:";
                showGACreateMainScreen();
                showGAParams();
                showGACreateConditionValue(dataFromCaller,'condition1_ga_div','Trigger condition');
                document.getElementById('device_for_action_div').innerHTML = `
                <div onClick = 'gaCreateState=${GA_STATES.action_devs_select};saveGAScreen();showGACreateDialog()' class = 'col-12'>
                    <div class="text-center p-3 border bg-light" style = "border-radius : 1em; cursor:pointer;">
                        <h4>Click to set action</h4>
                    </div>
                </div>`;
                gaCreateState++;
            }
            break;
        }
        case (GA_STATES.action_devs_select) : {
            showGACreateDevAction("Select action device:");
            gaCreateState++;
            break;
        }
        case (GA_STATES.action_device) : {
            if (isEditingGaSectionNow) {
                showSavedGAScreen();
                showGACreateDevforAction(dataFromCaller);
                isEditingGaSectionNow = false;
            } else {
                showSavedGAScreen();
                showGACreateDevforAction(dataFromCaller);
                document.getElementById('condition2_ga_div').innerHTML = ` 
                <div onclick = 'gaCreateState=${GA_STATES.condition2_devs};saveGAScreen();showGACreateDialog()' class = 'col-12'>
                    <div class="text-center p-3 border bg-light" style = "border-radius : 1em; cursor:pointer;">
                        <h4>Click to set the termination condition</h4>
                    </div>
                </div>`;
                gaCreateState++;
            }
            break;
        }
        case (GA_STATES.condition2_devs) : {
            showGACreateDevCondition("Select termination condition device:");
            gaCreateState++;
            break;
        }
        case (GA_STATES.condition2) : {
            if (isEditingGaSectionNow) {
                showSavedGAScreen();
                showGACreateConditionValue(dataFromCaller,'condition2_ga_div','Termination condition');
                isEditingGaSectionNow = false;
            } else {
                showSavedGAScreen();
                showGACreateConditionValue(dataFromCaller,'condition2_ga_div','Termination condition');
                unblockApplyBtn();
                gaCreateState++;
            }
            break;
        }
    }

    if (document.getElementById("returnGABackBtn")) {
        document.getElementById("returnGABackBtn").onclick = function () { 
            returnGABack();
        }
    }
    if(document.querySelector('form')) {
        document.querySelector('form').onsubmit = function(e) {
            e.preventDefault();
            return applyGASettings(e); 
        };
    }

    
}

function showGACreateDevAction(headerText) {
    let divStyle = 'style="background-color:  #8FBC8F; border-radius: 5px; cursor:pointer;"';
    let psImg = `<img width='80' src = '${SENSORS_IMAGES.PowerStrip}' class='img-fluid' alt='...'>`;
    let scriptImg = `<img width='80' src = '${SENSORS_IMAGES.script}' class='img-fluid' alt='...'>`;

    document.getElementById("actions-container").innerHTML = `
            <h4 class = "my-3">${headerText}</h4>
            <div id = 'ga-ps-select' class="row my-3 py-1" ${divStyle}>
                <div class="col-3">${psImg}</div>
                <div class="col-9 my-auto"><h5>device on Power Strip</h5></div>
            </div>
            <div id = 'ga-script-select' class="row my-3 py-1" ${divStyle}>
                <div class="col-3">${scriptImg}</div>
                <div class="col-9 my-auto"><h5>Text script</h5></div>
            </div>
            <button class="btn btn-secondary" onclick = "location.href = location.href">
                <i class="bi bi-arrow-left-short"></i>
                &nbsp; Back
            </button>`;
    
    document.getElementById('ga-ps-select').onclick = function() {
        showDevices(headerText);
    };
    document.getElementById('ga-script-select').onclick = function() {
        initScriptsList()
        .then(res => {
            let script = new DeviceScript();
            script.id = "script";
            script.specifedName = "script";
            devicesMap.set("script",script);
            showGACreateDialog("script");
        })
        .catch(err => {
            alert ("[at showGACreateDevAction :: on text scripts loading] " + error);
        })
        
    }
}

function showGACreateDevCondition(headerText) {
    let divStyle = 'style="background-color:  #8FBC8F; border-radius: 5px; cursor:pointer;"';
    let megaImg = `<img width='80' src = '${SENSORS_IMAGES.Mega}' class='img-fluid' alt='...'>`;
    let clockImg = `<img width='80' src = '${SENSORS_IMAGES.clock}' class='img-fluid' alt='...'>`;
    let timerImg = `<img width='80' src = '${SENSORS_IMAGES.timer}' class='img-fluid' alt='...'>`;
    let scriptImg = `<img width='80' src = '${SENSORS_IMAGES.script}' class='img-fluid' alt='...'>`;
    let manualImg = `<img width='80' src = '${SENSORS_IMAGES.analog_input}' class='img-fluid' alt='...'>`;

    document.getElementById("actions-container").innerHTML = `
            <h4 class = "my-3">${headerText}</h4>
            <div id = 'ga-mega-select' class="row my-3 py-1" ${divStyle}>
                <div class="col-3">${megaImg}</div>
                <div class="col-9 my-auto"><h5>Sensor or analog input</h5></div>
            </div>
            <div id = 'ga-clock-select' class="row my-3 py-1" ${divStyle}>
                <div class="col-3">${clockImg}</div>
                <div class="col-9 my-auto"><h5>Clock</h5></div>
            </div>
            <div id = 'ga-timer-select' class="row my-3 py-1" ${divStyle}>
                <div class="col-3">${timerImg}</div>
                <div class="col-9 my-auto"><h5>Timer</h5></div>
            </div>
            <div id = 'ga-script-select' class="row my-3 py-1" ${divStyle}>
                <div class="col-3">${scriptImg}</div>
                <div class="col-9 my-auto"><h5>Text script</h5></div>
            </div>
            <div id = 'ga-manual-select' class="row my-3 py-1" ${divStyle}>
                <div class="col-3">${manualImg}</div>
                <div class="col-9 my-auto"><h5>Manual control (no trigger condition)</h5></div>
            </div>
            <button class="btn btn-secondary" onclick = "location.href = location.href">
                <i class="bi bi-arrow-left-short"></i>
                &nbsp; Back
            </button>`;
    
    document.getElementById('ga-mega-select').onclick = function() {
        showDevices(headerText);
    };
    document.getElementById('ga-clock-select').onclick = function() {
        let clock = new DeviceClock();
        clock.id = "clock";
        clock.specifedName = "clock";
        devicesMap.set("clock",clock);
        showGACreateDialog("clock");
    };
    document.getElementById('ga-timer-select').onclick = function() {
        let timer = new DeviceTimer();
        timer.id = "timer";
        timer.specifedName = "timer";
        devicesMap.set("timer",timer);
        showGACreateDialog("timer");
    };
    document.getElementById('ga-script-select').onclick = function() {
        initScriptsList()
        .then(res => {
            let script = new DeviceScript();
            script.id = "script";
            script.specifedName = "script";
            devicesMap.set("script",script);
            showGACreateDialog("script");
        })
        .catch(err => {
            alert ("[at showGACreateDevCondition :: on text scripts loading] " + error);
        })
    }
   // document.getElementById('ga-manual-select').onclick = function() {
    //    showGACreateDialog("script");
    //}
}

function getGACreateMainScreen() {
    return `
    <div  class = 'container'>
        <form>
            <div id = "params_ga_div" class = "my-2 gy-3">
            </div>
            <div id = "condition1_ga_div" class = "row my-2 gy-3">
            </div>
            <div id = "device_for_action_div" class = "row my-2 gy-3">
            </div>
            <div id = "condition2_ga_div" class = "row my-2 gy-3">
            </div>
            <div class="form-check form-switch my-3">
                <input class="form-check-input" type="checkbox" value="" id="ga-active-input"> 
                <label class="form-check-label" for="ga-active-input">
                    Action is active
                </label>
            </div>
            <div class = "row mt-2">
                <div class = "col-6">
                    <button type="submit" class="btn btn-primary" id = "applyBtn" disabled>
                        <i class="bi bi-check"></i> Apply
                    </button>
                    <button type="button" id = "returnGABackBtn" class="btn btn-secondary">
                        <i class="bi bi-arrow-left-short"></i> Cancel
                    </button>
                </div>
                <div class = "col-3">
                </div>
                <div class = "col-3">
                    <button type="button" id = "removeGA" class="btn btn-danger" disabled>
                        <i class="bi bi-trash mr-1"></i>Remove
                    </button>
                </div>
            </div>
        </form>
    </div>
    `;
}

function showGACreateMainScreen() {
    document.getElementById('actions-container').innerHTML = getGACreateMainScreen();
}
function applyGASettings(e) {
    if (e.submitter.classList[1] == 'btn-primary') {
        let inputs = document.querySelectorAll('input');
        let selects = document.querySelectorAll('select');

        let devCondition1;
        let comparison1;
        let valueForCondition1 = {};

        let devCondition2;
        let comparison2;
        let valueForCondition2 = {};

        let deviceUsedID;

        let valueForDeviceUsed;

        let gaIsActive = document.getElementById("ga-active-input").checked;

        for (e of inputs) {
            let idData = e.id.split("_");
            if (idData.length < 3) {
                continue;
            }
            let eType = idData[1];
            let eGAState = Number.parseInt(idData[2]);
            let eDeviceID = idData[3];
            switch (eGAState) {
                case (GA_STATES.condition1) : {
                    devCondition1 = eDeviceID;
                    switch (eType) {
                        case ("value") : {
                            valueForCondition1.value = e.value;
                            break;
                        }
                        case ("date") : {
                            valueForCondition1.date = e.value;
                            break;
                        }
                        case ("time") : {
                            valueForCondition1.time = e.value;
                            break;
                        }
                        case ("hh") : {
                            valueForCondition1.hh = Number.parseInt(e.value);
                            break;
                        }
                        case ("mm") : {
                            valueForCondition1.mm = Number.parseInt(e.value);
                            break;
                        }
                        case ("ss") : {
                            valueForCondition1.ss = Number.parseInt(e.value);
                            break;
                        }
                    }
                    break;
                }
                case (GA_STATES.condition2) : {
                    devCondition2 = eDeviceID;
                    switch (eType) {
                        case ("value") : {
                            valueForCondition2.value = e.value;
                            break;
                        }
                        case ("date") : {
                            valueForCondition2.date = e.value;
                            break;
                        }
                        case ("time") : {
                            valueForCondition2.time = e.value;
                            break;
                        }
                        case ("hh") : {
                            valueForCondition2.hh =Number.parseInt(e.value);
                            break;
                        }
                        case ("mm") : {
                            valueForCondition2.mm = Number.parseInt(e.value);
                            break;
                        }
                        case ("ss") : {
                            valueForCondition2.ss = Number.parseInt(e.value);
                            break;
                        }
                    }
                    break;
                }
                case (GA_STATES.action_device) : {
                    if (eDeviceID !== "script") {
                        deviceUsedID = eDeviceID;
                        valueForDeviceUsed = e.value;
                    }
                    break;
                }
            }
        }
        for (e of selects) {
            let idData = e.id.split("_");
            if (idData.length < 3) {
                continue;
            }
            let eType = idData[1];
            let eGAState = Number.parseInt(idData[2]);
            let eDeviceID = idData[3];
            switch (eGAState) {
                case (GA_STATES.condition1) : {
                    if (eType == "scriptName") {
                        devicesMap.set(e.value,new DeviceScript(e.value));
                        devCondition1 = e.value;
                    } else {
                        if (eDeviceID !== "script") {
                            devCondition1 = eDeviceID;
                        }
                    }
                    if (eType == "comparison") {
                        comparison1 = e.value;
                    }
                    break;
                }
                case (GA_STATES.condition2) : {
                    if (eType == "scriptName") {
                        devicesMap.set(e.value,new DeviceScript(e.value));
                        devCondition2 = e.value;
                    } else {
                        if (eDeviceID !== "script") {
                            devCondition2 = eDeviceID;
                        }
                    }
                    if (eType == "comparison") {
                        comparison2 = e.value;
                    }
                    break;
                }
                case (GA_STATES.action_device) : {
                    if (eType == "scriptName") {
                        devicesMap.set(e.value,new DeviceScript(e.value));
                        deviceUsedID = e.value;
                        valueForDeviceUsed = 0;
                    } else {
                        deviceUsedID = eDeviceID;
                        valueForDeviceUsed = e.value;
                    }
                    break;
                }
            }
        }
        let gaData = {

            id : currentGaID,

            name : currentGaName,
            
            color : currentGaColor,

            imgSrc : currentGaImgSrc,

            condition1 : {
                dev :devCondition1,
                comparison : comparison1,
                value : valueForCondition1
            },

            condition2 : {
                dev :devCondition2,
                comparison : comparison2,
                value : valueForCondition2
            },
            
            action : {
                dev : deviceUsedID,
                value : valueForDeviceUsed
            },

    
            isActive : gaIsActive
        }
        createGAObject(gaData);
        blockApplyBtn()
        uploadGraphicalActions()
        .then(res => {
            uploadGaViewData()
            .then(resVD =>{
                alert("OK");
                unblockApplyBtn();
                returnGABack();
            })
        })
        .catch(err => {
            alert("err :" + err);
            unblockApplyBtn();
            returnGABack();
        })
    } else {
        returnGABack();
    }
}
function returnGABack() {
    location.href = location.href;
    //gaCreateState -= 2;
    //showGACreateDialog(showGACreateDialogParams.pop());
}
function getGACreateDeviceIconBlock(device,block_class) {

    let device_img_src;
    let figureCaption;
    if (device instanceof MegaSensorChannel) {
        device_img_src = getImageObjBySensorType(device.type).img;
        figureCaption = `<figcaption class="figure-caption">${device.specifedName}</figcaption>`;
    } else if (device instanceof AnalogInput) {
        device_img_src = SENSORS_IMAGES.analog_input;
        figureCaption = `<figcaption class="figure-caption">${device.specifedName}</figcaption>`;
    } else if (device instanceof PWM_Output) {
        device_img_src = SENSORS_IMAGES.pwm_output;
        figureCaption = `<figcaption class="figure-caption">${device.specifedName}</figcaption>`;
    } else if (device instanceof Outlet) {
        device_img_src = SENSORS_IMAGES.outlet;
        figureCaption = `<figcaption class="figure-caption">${device.specifedName}</figcaption>`;
    } else if (device instanceof Solenoid24vac) {
        device_img_src = SENSORS_IMAGES.solenoid_water_valve;
        figureCaption = `<figcaption class="figure-caption">${device.specifedName}</figcaption>`;
    } else if (device instanceof DeviceClock) {
        device_img_src = SENSORS_IMAGES.clock;
        figureCaption = `<figcaption class="figure-caption">${device.specifedName}</figcaption>`;
    } else if (device instanceof DeviceTimer) {
        device_img_src = SENSORS_IMAGES.timer;
        figureCaption = `<figcaption class="figure-caption">${device.specifedName}</figcaption>`;
    } else if (device instanceof DeviceScript) {
        device_img_src = SENSORS_IMAGES.script;
        let scsList = "";
        for (let i = 0; i < scriptsNamesList.length; i++) {
            scsList += `<option value = "${scriptsNamesList[i]}">${scriptsNamesList[i]}</option>`;
        }
        figureCaption = `<figcaption class="figure-caption">
            <select class="form-select form-select-sm" id="ga_scriptName_${gaCreateState}_${device.id}" required>
                ${scsList}
            </select>
        </figcaption>`;
    }
    return `
    <div class = '${block_class}'>
        <div class="text-center p-3 ">
        <figure class="figure">
            <img width="${ACTION_IMAGE_WIDTH}" src = "${device_img_src}" class="figure-img img-fluid rounded" alt="..."></img>
            ${figureCaption}
        </figure>
        </div>
    </div>
    `;
}
function getGACreateDeviceComparisonBlock(device,block_class) {
    return `
    <div class = '${block_class}'>
        <div class="text-center p-3 ">
            <select id = 'ga_comparison_${gaCreateState}_${device.id}' class="form-select" required>
                <option value = "=">=</option>
                <option value = ">">&gt;</option>
                <option value = "<">&lt;</option>
            </select>
        </div>
    </div>
    `;
}
function getGACreateDeviceTextActionBlock(text_action,block_class) {
    return `
    <div class = '${block_class}'>
        <div class="text-center p-3 ">
            <h4>${text_action}</h4>
        </div>
    </div>
    `;
}
function getGACreateDeviceValueBlock(device,block_class) {
    if (device instanceof MegaSensorChannel) {
        return `
        <div class = '${block_class}'>
            <div class="text-center p-3 ">
                <input type="text" class="form-control" id="ga_value_${gaCreateState}_${device.id}" required>
            </div>
        </div>
        `;
    } else if (device instanceof AnalogInput) {
        return `
        <div class = '${block_class}'>
            <div class="text-center p-3 ">
                <input type="text" class="form-control" id="ga_value_${gaCreateState}_${device.id}" required>
            </div>
        </div>
        `;
    } else if (device instanceof PWM_Output) {
        return `
        <div class = '${block_class}'>
            <div class="text-center p-3 ">
                <input type="text" class="form-control" id="ga_value_${gaCreateState}_${device.id}" required>
                <label class="form-label" for="firmware_input">0-100%</label>
            </div>
        </div>
        `;
    } else if (device instanceof Outlet) {
        return `
        <div class = '${block_class}'>
            <div class="text-center p-3 ">
                <select class="form-select" id="ga_value_${gaCreateState}_${device.id}" required>
                    <option value = "1">Turn on</option>
                    <option value = "0">Turn off</option>
                </select>
            </div>
        </div>
        `;
    } else if (device instanceof Solenoid24vac) {
        return `
        <div class = '${block_class}'>
            <div class="text-center p-3 ">
                <select class="form-select" id="ga_value_${gaCreateState}_${device.id}" required>
                    <option value = "1">Turn on</option>
                    <option value = "0">Turn off</option>
                </select>
            </div>
        </div>
        `;
    } else if (device instanceof DeviceClock) {
        return `
        <div class = '${block_class}'>
            <div class="text-center p-3 ">
                <div class="mb-3">
                    <label for="ga_date_${gaCreateState}_${device.id}" class="form-label">Date</label>
                    <input type="date" class="form-control" id="ga_date_${gaCreateState}_${device.id}" required>
                </div>
                <div class="mb-3">
                    <label for="ga_time_${gaCreateState}_${device.id}" class="form-label">Time</label>
                    <input type="time" class="form-control" id="ga_time_${gaCreateState}_${device.id}" required>
                </div>
            </div>
        </div>
        `;
    } else if (device instanceof DeviceTimer) {
        return `
        <div class = '${block_class}'>
            <div class="row text-center p-3">
                <div class = "col-4">
                    <input type="number" class="form-control" id="ga_hh_${gaCreateState}_${device.id}" value = "00" required>
                    <label class="form-label" for="ga_hh_${gaCreateState}_${device.id}">hh</label>
                </div>
                <div class = "col-4">
                    <input type="number" class="form-control" id="ga_mm_${gaCreateState}_${device.id}" value = "00" required>
                    <label class="form-label" for="ga_mm_${gaCreateState}_${device.id}">mm</label>
                </div>
                <div class = "col-4">
                    <input type="number" class="form-control" id="ga_ss_${gaCreateState}_${device.id}" value = "00" required>
                    <label class="form-label" for="ga_ss_${gaCreateState}_${device.id}">ss</label>
                </div>
            </div>
        </div>
        `;
    } else if (device instanceof DeviceScript) {
        if (gaCreateState == GA_STATES.action_device) {
            return `
            <div class = '${block_class} text-center'>
            <h4>execute script</h4>
            </div>
            `;
        } else {
            return `
            <div class = '${block_class}'>
                <div class="text-center p-3 ">
                    <input type="text" class="form-control" id="ga_value_${gaCreateState}_${device.id}" required>
                </div>
            </div>
            `;
        }
    }
    
}

function getGACreateDeviceTextHeaderBlock(divHeaderText) {
    return `
    <div class="col-12">
        <div class="text-center p-3 border bg-light row" style="border-radius : 1em;">
            <div class="col-10">
                <h5>${divHeaderText}</h5>
            </div>
             <div class="col-1">
                <button type="button" class="btn btn-secondary" onclick = "editGACreateSection(${gaCreateState})">
                    <i class="bi bi-pen"></i>
                </button>
            </div>
        </div>
    </div>
    `
}
function editGACreateSection(ga_state) {
    isEditingGaSectionNow = true;
    saveGAScreen();
    gaCreateState = ga_state;
    switch (ga_state) {
        case (GA_STATES.view_params) : {
            viewGaParamsScreen();
            break;
        }
        case (GA_STATES.condition1) : {
            showGACreateDevCondition("Select trigger condition device:");
            break;
        }
        case (GA_STATES.condition2) : {
            showGACreateDevCondition("Select termination condition device:");
            break;
        }
        case (GA_STATES.action_device) : {
            showGACreateDevAction("Select action device:")
            break;
        }
    }
}

function showGACreateConditionValue(deviceID,divToViewID,divHeaderText) {
    
    let divToView = document.getElementById(divToViewID);
    let device = devicesMap.get(deviceID);
    if (device instanceof DeviceTimer || device instanceof DeviceClock) {
        divToView.innerHTML = getGACreateDeviceTextHeaderBlock(divHeaderText) +
        getGACreateDeviceIconBlock(device,"col-4 my-auto") +
        getGACreateDeviceValueBlock(device,"col-8 my-auto");
    } else {
        let block_class = "col-4 my-auto";
        divToView.innerHTML = getGACreateDeviceTextHeaderBlock(divHeaderText) +
        getGACreateDeviceIconBlock(device,block_class) +
        getGACreateDeviceComparisonBlock(device,block_class) +
        getGACreateDeviceValueBlock(device,block_class);
    }
}

function showGACreateDevforAction(deviceID) {
    let device = devicesMap.get(deviceID);
    let dataToView = getGACreateDeviceTextHeaderBlock("Executable action") +
        getGACreateDeviceIconBlock(device,"col-4 my-auto");
    if (device instanceof Outlet) {
        dataToView += getGACreateDeviceValueBlock(device,"col-8 my-auto");
    } else if (device instanceof PWM_Output) {
        dataToView += getGACreateDeviceTextActionBlock("set value","col-4 my-auto");
        dataToView += getGACreateDeviceValueBlock(device,"col-4 my-auto"); 
    } else if (device instanceof Solenoid24vac){
        dataToView += getGACreateDeviceValueBlock(device,"col-8 my-auto");
    } else if (device instanceof DeviceScript) {
        dataToView += getGACreateDeviceValueBlock(device,"col-8 my-auto");
    }
    document.getElementById('device_for_action_div').innerHTML = dataToView;
}


function getGraphicalActionsListView() {
    let dataToView = "";
    for ([id,ga] of graphicalActionsMap) {
        let gaButton;
        if (ga.gaStatus.active) {
            gaButton = `<button type="button" class="btn btn-danger" onclick = "stop_start_GA('${id}',this)">
                            stop
                        </button>`;
        } else {
            gaButton = `<button type="button" class="btn btn-success" onclick = "stop_start_GA('${id}',this)">
                            start
                        </button>`;
        }
        ga.imgSrc = ga.imgSrc || SENSORS_IMAGES.script;
        dataToView += `
        <div class="row mx-1 my-3 py-1" style="background:${ga.color};border-radius : 5px;cursor:pointer">
            <div class="col-3" onclick = "showGAData('${id}')">
                <img width="80" src="${ga.imgSrc}" class="img-fluid" alt="...">
            </div>
            <div class="col-6 text-center my-auto" onclick = "showGAData('${id}')">
                <h5>${ga.name}</h5>
            </div>
            <div class="col-2 my-auto">
                ${gaButton}
            </div>
        </div>`
    }

    
    var addNewBtn = `
    <button type="button" class="btn btn-primary" id = 'add-new-act-btn'  onclick = 'gaCreateState=0;showGACreateDialog()' >
        <i class="bi bi-plus"></i> Add new
    </button>`;

    return dataToView + addNewBtn;
}

function stop_start_GA(ga_id,caller) {
    caller.disabled = true;
    old 
    caller.innerHTML = `<div class="spinner-border" role="status">
                            <span class="visually-hidden">loading...</span>
                        </div>`;
    let ga =graphicalActionsMap.get(Number.parseInt(ga_id));
    ga.gaStatus.active = !ga.gaStatus.active;
    uploadGraphicalActions()
    .then(res => {
        showActions('ga');
    })
}

function showGAData(gaID) {
    initScriptsList()
    .then(res => {
        gaID = Number.parseInt(gaID);
        currentGaID = gaID;
        let ga = graphicalActionsMap.get(gaID);
    
        currentGaColor = ga.color;
        currentGaName = ga.name;
        currentGaImgSrc = ga.imgSrc;
    
        showGACreateMainScreen();
    
        showGAParams();
    
        let nextDevID = getGADeviceID(ga.devCondition1);
        gaCreateState = GA_STATES.condition1;
        showGACreateConditionValue(nextDevID,'condition1_ga_div','Trigger condition');
        nextDevID = getGADeviceID(ga.deviceUsed);
        gaCreateState = GA_STATES.action_device;
        showGACreateDevforAction(nextDevID);
        nextDevID = getGADeviceID(ga.devCondition2);
        gaCreateState = GA_STATES.condition2;
        showGACreateConditionValue(nextDevID,'condition2_ga_div','Termination condition');
    
        init_gaFormValuesMap(ga);
        showGAValuesFromGAMap();
    
        if (document.getElementById("returnGABackBtn")) {
            document.getElementById("returnGABackBtn").onclick = function () { 
                returnGABack();
            }
        }
        if(document.querySelector('form')) {
            document.querySelector('form').onsubmit = function(e) {
                e.preventDefault();
                return applyGASettings(e); 
            };
        }
        document.getElementById("ga-active-input").checked = ga.gaStatus.active;
        unblockApplyBtn();
        document.getElementById("removeGA").disabled = false;
        document.getElementById("removeGA").onclick = function () {
            if (confirm("Remove graphical action?")) {
                document.getElementById("removeGA").disabled = true;
                document.getElementById("removeGA").innerHTML = `<div class="spinner-border" role="status">
                                                                    <span class="visually-hidden">loading...</span>
                                                                </div>`;
                graphicalActionsMap.delete(gaID);
                uploadGraphicalActions()
                .then(res => {
                    showActions('ga');
                })
            }
        }
    })

} 

function setGAMapValuesByDeviceType(ga,GADeviceObject,ga_create_state) {
    switch (GADeviceObject.devType) {
        case (GA_HEX_TYPES.dev_ps) : {
            GADeviceObject.nodeIndex = Number.parseInt(GADeviceObject.nodeIndex);
            switch (GADeviceObject.psNode) {
                case (GA_HEX_TYPES.dev_outlet) : {
                    let dev_id = [GADeviceObject.psId,"outlet",GADeviceObject.nodeIndex].join("-");
                    gaFormValuesMap.set(`ga_value_${ga_create_state}_${dev_id}`,ga.valueForDevice);
                    break;
                }
                case (GA_HEX_TYPES.dev_pwm) : {
                    let dev_id = [GADeviceObject.psId,"pwm",GADeviceObject.nodeIndex].join("-");
                    gaFormValuesMap.set(`ga_value_${ga_create_state}_${dev_id}`,ga.valueForDevice);
                    break;
                }
                case (GA_HEX_TYPES.dev_analog_in) : {
                    let dev_id = [GADeviceObject.psId,"analog",GADeviceObject.nodeIndex].join("-");
                    if (ga_create_state == GA_STATES.condition1) {
                        gaFormValuesMap.set(`ga_value_${ga_create_state}_${dev_id}`,ga.valueForCondition1);
                        gaFormValuesMap.set(`ga_comparison_${ga_create_state}_${dev_id}`,ga.comparison1);
                    } else if (ga_create_state == GA_STATES.condition2) {
                        gaFormValuesMap.set(`ga_value_${ga_create_state}_${dev_id}`,ga.valueForCondition2);
                        gaFormValuesMap.set(`ga_comparison_${ga_create_state}_${dev_id}`,ga.comparison2);
                    }
                    break;
                }
                case (GA_HEX_TYPES.dev_solenoid) : {
                    let dev_id = [GADeviceObject.psId,"solenoid",GADeviceObject.nodeIndex].join("-");
                    gaFormValuesMap.set(`ga_value_${ga_create_state}_${dev_id}`,ga.valueForDevice);
                    break;
                }
            }
            break;
        }
        case (GA_HEX_TYPES.dev_sensor) : {
            GADeviceObject.channelNumber = Number.parseInt(GADeviceObject.channelNumber);
            let dev_id =  [GADeviceObject.sensorId,GADeviceObject.channelNumber].join("-");
            if (ga_create_state == GA_STATES.condition1) {
                gaFormValuesMap.set(`ga_value_${ga_create_state}_${dev_id}`,ga.valueForCondition1);
                gaFormValuesMap.set(`ga_comparison_${ga_create_state}_${dev_id}`,ga.comparison1);
            } else if (ga_create_state == GA_STATES.condition2) {
                gaFormValuesMap.set(`ga_value_${ga_create_state}_${dev_id}`,ga.valueForCondition2);
                gaFormValuesMap.set(`ga_comparison_${ga_create_state}_${dev_id}`,ga.comparison2);
            }
            break;
        }
        case (GA_HEX_TYPES.dev_timer) : {
            let dev_id = "timer";
            if (ga_create_state == GA_STATES.condition1) {
                let timeData = calcHH_MM_SS(ga.valueForCondition1);
		        let hh = timeData[0];
		        let mm = timeData[1];
		        let ss = timeData[2];
                gaFormValuesMap.set(`ga_hh_${ga_create_state}_${dev_id}`,hh);
                gaFormValuesMap.set(`ga_mm_${ga_create_state}_${dev_id}`,mm);
                gaFormValuesMap.set(`ga_ss_${ga_create_state}_${dev_id}`,ss);
            } else if (ga_create_state == GA_STATES.condition2) {
                let timeData = calcHH_MM_SS(ga.valueForCondition2);
		        let hh = timeData[0];
		        let mm = timeData[1];
		        let ss = timeData[2];
                gaFormValuesMap.set(`ga_hh_${ga_create_state}_${dev_id}`,hh);
                gaFormValuesMap.set(`ga_mm_${ga_create_state}_${dev_id}`,mm);
                gaFormValuesMap.set(`ga_ss_${ga_create_state}_${dev_id}`,ss);
            }
            break;
        }
        case (GA_HEX_TYPES.dev_clock) : {
            let dev_id = "clock";
            let dateToViewMS;
            if (ga_create_state == GA_STATES.condition1) {
                dateToViewMS = (ga.valueForCondition1 + new Date().getTimezoneOffset()*60 + systemTimeZone*60) * 1000;
            }
            if (ga_create_state == GA_STATES.condition2) {
                dateToViewMS = (ga.valueForCondition2 + new Date().getTimezoneOffset()*60 + systemTimeZone*60) * 1000;
            }
			var dateToView = new Date(dateToViewMS);
            gaFormValuesMap.set(`ga_date_${ga_create_state}_${dev_id}`,dateToView.YYYYMMDD());
            gaFormValuesMap.set(`ga_time_${ga_create_state}_${dev_id}`,dateToView.HHmm());
            break;
        }
        case (GA_HEX_TYPES.dev_script) : {
            let dev_id = GADeviceObject.scriptName;
            if (ga_create_state == GA_STATES.condition1) {
                gaFormValuesMap.set(`ga_scriptName_${ga_create_state}_${dev_id}`,dev_id);
                gaFormValuesMap.set(`ga_value_${ga_create_state}_${dev_id}`,ga.valueForCondition1);
                gaFormValuesMap.set(`ga_comparison_${ga_create_state}_${dev_id}`,ga.comparison1);
            } else if (ga_create_state == GA_STATES.condition2) {
                gaFormValuesMap.set(`ga_scriptName_${ga_create_state}_${dev_id}`,dev_id);
                gaFormValuesMap.set(`ga_value_${ga_create_state}_${dev_id}`,ga.valueForCondition2);
                gaFormValuesMap.set(`ga_comparison_${ga_create_state}_${dev_id}`,ga.comparison2);
            } else if (ga_create_state == GA_STATES.action_device) {
                gaFormValuesMap.set(`ga_scriptName_${ga_create_state}_${dev_id}`,dev_id);
            }
            break;
        }
    }
}

function init_gaFormValuesMap(ga) {
    gaFormValuesMap = new Map();
    setGAMapValuesByDeviceType(ga, ga.devCondition1,GA_STATES.condition1);
    setGAMapValuesByDeviceType(ga, ga.deviceUsed,GA_STATES.action_device);
    setGAMapValuesByDeviceType(ga, ga.devCondition2,GA_STATES.condition2);
}

function showGAValuesFromGAMap() {
    for ([key,value] of gaFormValuesMap) {
        document.getElementById(key).value = value;
    }
}