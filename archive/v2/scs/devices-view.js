let devicesHeaderText = "";

function getImageObjBySensorType(sensorType) {
    sensorType = Number.parseInt(sensorType);
    switch(sensorType) {
        case (1) :{
            return {
                img : SENSORS_IMAGES.air_humidity,
                descr : 'air humidity'
            }
        }
        case (2) :{
            return {
                img : SENSORS_IMAGES.temperature,
                descr : 'temperature'
            }
        }
        case (3) :{
            return {
                img : SENSORS_IMAGES.luminosity,
                descr : 'luminosity'
            }
        }
        case (4) :{
            return {
                img : SENSORS_IMAGES.co2_level,
                descr : 'co2 level'
            }
        }
        case (5) :{
            return {
                img : SENSORS_IMAGES.soil_humidity,
                descr : 'soil humidity'
            }
        }
    }
}

function showDevices(headerText) {
    if (headerText) {
        devicesHeaderText = `<h4>${headerText}</h4>`;
    } else {
        devicesHeaderText = "";
    }
    currentDeviceMenuLocation = function () {
        showDevices();
    }
    let container;
    switch (location.pathname) {
        case (LOCATION_PATHS.settings) : {
            container = "settings-container";
            var devicesList= "";
            for (var [devID, device] of devicesMap) {
                if (device instanceof Mega || device instanceof PowerStrip || device instanceof HUB) {
                    devicesList += getNextGlobalDeviceView(device);
                }
            }
            if (devicesList.length == 0) {
                devicesList =   "<p class = 'my-3'>No devices yet ... </p>";
            }
            var btns = 
            `<button class="btn btn-primary" onclick = "showAddNewDeviceMenu()"><i class='bi bi-plus'></i>&nbsp;Add new</button>
            <button class="btn btn-secondary" onclick = "location.href = location.href">
                <i class="bi bi-arrow-left-short"></i>
                &nbsp; Back
            </button>`;
            devicesList = devicesList + btns;
            break;
        }
        case (LOCATION_PATHS.actions) : {
            container = "actions-container";
            var devicesList= "";
            for (var [devID, device] of devicesMap) {
                if (gaCreateState == GA_STATES.condition2 || gaCreateState == GA_STATES.condition1) {
                    if (device instanceof Mega || device instanceof PowerStrip) {
                        devicesList += getNextGlobalDeviceView(device);
                    }
                } else if (gaCreateState == GA_STATES.action_device) {
                    if (device instanceof PowerStrip) {
                        devicesList += getNextGlobalDeviceView(device);
                    }
                }
            }
            if (devicesList.length == 0) {
                devicesList =   "<p class = 'my-3'>No devices yet ... </p>";
            }
            let backBtn =   `<button class="btn btn-secondary" onclick = "location.href = location.href">
                                <i class="bi bi-arrow-left-short"></i>
                                &nbsp; Back
                            </button>`;
            devicesList = devicesList + backBtn;
            break;
        }
        case (LOCATION_PATHS.status) : {
            container = "status-container";
            var devicesList= "";
            for (var [devID, device] of devicesMap) {
                if (device instanceof Mega || device instanceof PowerStrip || device instanceof HUB) {
                    devicesList += getNextGlobalDeviceView(device);
                }
            }
            if (devicesList.length == 0) {
                devicesList =   "<p class = 'my-3'>No devices yet ... </p>";
            }
            break;
        }
    }
    document.getElementById(container).innerHTML = devicesHeaderText + devicesList;
}

function getNextGlobalDeviceView(device) {
    var deviceImg;
    //var isActiveStyle;
    var isActiveImg;
    var onClick = "";
    let colorStyle
   

    if ((device.isEnable && device.isEnable !== "0") || (device.isOnline && device.isOnline !== "0")) {
    //    isActiveStyle = 'style="background-color: #8FBC8F; border-radius: 5px;"';
        isActiveImg = "<i class='bi bi-check'></i>";
    } else {
    //   isActiveStyle = 'style="background-color: #FA8072; border-radius: 5px;"';
        isActiveImg = "<i class='bi bi-x'></i>"; 
    }
    
    device.color = device.color || DEFAULT_DEVICE_COLOR;

    if (device instanceof HUB) {
        colorStyle = 'style="background-color: '+device.color+'; border-radius: 5px; cursor:pointer;"';
        deviceImg = "<img width='80' src = '"+SENSORS_IMAGES.Mega+"' class='img-fluid' alt='...'>";
        isActiveImg = "<i class='bi bi-check'></i>";
    }

    if (device instanceof Mega) {
        colorStyle = 'style="background-color: '+device.color+'; border-radius: 5px; cursor:pointer;"';
        deviceImg = "<img width='80' src = '"+SENSORS_IMAGES.Mega+"' class='img-fluid' alt='...'>";
        onClick = `onclick = 'showMegaSensors("`+device.id+`")'`;

        
    }
    if (device instanceof PowerStrip) {
        colorStyle = 'style="background-color: '+device.color+'; border-radius: 5px; cursor:pointer;"';
        deviceImg = "<img width='80' src = '"+SENSORS_IMAGES.PowerStrip+"' class='img-fluid' alt='...'>";
        onClick = `onclick = 'showPowerStripDevices("`+device.id+`")'`;
        
    }
    switch (location.pathname) {
        case (LOCATION_PATHS.settings) : {
            return `<div class="row mx-1 my-3 py-1" `+colorStyle+`>
                        <div class="col-3" `+onClick+`>`+deviceImg+`</div>
                        <div class="col-6 text-center my-auto" `+onClick+`><h5>`+device.specifedName+`</h5></div>
                        <div class="col-1 my-auto" `+onClick+`>`+isActiveImg+`</div>
                        <div class="col-1 my-auto">
                            <button type="button" class="btn btn-light"   onclick = showDeviceSettings('`+device.id+`')>
                                <i class="bi bi-gear-fill"></i>
                            </button>
                        </div>
                    </div>`;
        }
        case (LOCATION_PATHS.actions) : {
            return `<div class="row mx-1 my-3 py-1" `+colorStyle+` `+onClick+`>
                <div class="col-3">`+deviceImg+`</div>
                <div class="col-7 text-center my-auto"><h5>`+device.specifedName+`</h5></div>
                <div class="col-1 my-auto">`+isActiveImg+`</div>
            </div>`;
        }
        case (LOCATION_PATHS.status) : {
            return `<div class="row mx-1 my-3 py-1" `+colorStyle+` `+onClick+`>
                <div class="col-3">`+deviceImg+`</div>
                <div class="col-7 text-center my-auto"><h5>`+device.specifedName+`</h5></div>
                <div class="col-1 my-auto">`+isActiveImg+`</div>
            </div>`;
        }
    }    
}
function showMegaSensors(megaID) {
    currentDeviceMenuLocation = currentDeviceMenuLocation = function (){
        showMegaSensors(megaID)
    };
    let container; 
    var mega = devicesMap.get(megaID);
 //   if (!mega.isEnable || mega.isEnable == "0") return;
 //   if (!mega.isOnline || mega.isOnline == "0") return;
    var imgSize = 80;
    var accordionMegaSensors = 
        `<div class="accordion accordion-flush my-2" id="accordionMegaSensors">
        </div>`;
    var data = "";
    
	for (sensor of mega.sensors) {
        let sensorImg = "";
        switch (location.pathname) {
            case (LOCATION_PATHS.settings) : {
                container = 'settings-container';
                data += 
                `<div class = "row">
                    <div class = "col-10">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-heading-`+sensor.id+`">
                            <button id = "btn-`+sensor.id+`" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-`+sensor.id+`" aria-expanded="false" aria-controls="flush-collapse-`+sensor.id+`">
                                `+sensorImg+`
                                `+sensor.specifedName+`
                            </button>
                            </h2>
                            <div id="flush-collapse-`+sensor.id+`" class="accordion-collapse collapse" aria-labelledby="flush-heading-`+sensor.id+`" data-bs-parent="#accordionMegaSensors">
                                <div class="accordion-body">
                                `+getMegaSensorChannelsView(sensor.id)+`
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class = "col-1">
                        <button type="button" class="btn btn-light" onclick = showDeviceSettings('`+sensor.id+`')>
                            <i class="bi bi-gear-fill"></i>
                        </button>
                    </div>
                </div>`;
                break;
            }
            case (LOCATION_PATHS.actions) : {
                container = 'actions-container';
                data += 
                `<div class="accordion-item">
                    <h2 class="accordion-header" id="flush-heading-`+sensor.id+`">
                    <button id = "btn-`+sensor.id+`" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-`+sensor.id+`" aria-expanded="false" aria-controls="flush-collapse-`+sensor.id+`">
                        `+sensorImg+`
                        `+sensor.specifedName+`
                    </button>
                    </h2>
                    <div id="flush-collapse-`+sensor.id+`" class="accordion-collapse collapse" aria-labelledby="flush-heading-`+sensor.id+`" data-bs-parent="#accordionMegaSensors">
                        <div class="accordion-body">
                        `+getMegaSensorChannelsView(sensor.id)+`
                        </div>
                    </div>
                </div>
                `;
                break;
            }
            case (LOCATION_PATHS.status) : {
                container = 'status-container';
                data += 
                `<div class="accordion-item">
                    <h2 class="accordion-header" id="flush-heading-`+sensor.id+`">
                    <button id = "btn-`+sensor.id+`" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-`+sensor.id+`" aria-expanded="false" aria-controls="flush-collapse-`+sensor.id+`">
                        `+sensorImg+`
                        `+sensor.specifedName+`
                    </button>
                    </h2>
                    <div id="flush-collapse-`+sensor.id+`" class="accordion-collapse collapse" aria-labelledby="flush-heading-`+sensor.id+`" data-bs-parent="#accordionMegaSensors">
                        <div class="accordion-body">
                        `+getMegaSensorChannelsView(sensor.id)+`
                        </div>
                    </div>
                </div>
                `;
                break;
            }
        }        
	}
    if (data == "") {
        data = "no sensors yet..."
    }
    document.getElementById(container).innerHTML = devicesHeaderText +  accordionMegaSensors + 
    `<button class="btn btn-secondary" onclick = "showDevices()">
        <i class="bi bi-arrow-left-short"></i> &nbsp; Back
    </button>`;
    document.getElementById('accordionMegaSensors').innerHTML = data;
}

function getMegaSensorChannelsView(sensorID) {
    currentDeviceMenuLocation = currentDeviceMenuLocation = function (){
        getMegaSensorChannelsView(sensorID)
    };
    let sensor = devicesMap.get(sensorID);
    let chanelActionBtn;
    var data = "";
    for (var channel of sensor.channels) {
        if (channel.type == "0") continue;
        switch (location.pathname) {
            case (LOCATION_PATHS.status) : {
                chanelActionBtn =   `<button type="button" class="btn btn-dark"   disabled>
                                        <i class="bi bi-graph-up"></i>
                                    </button>`;
                break;
            }
            case (LOCATION_PATHS.actions) : { 
                chanelActionBtn =   `<button type="button" class="btn btn-light" onclick = showGACreateDialog('`+channel.id+`')>
                                        <i class="bi bi-plus"></i>
                                    </button>`;
                break;
            }
            case (LOCATION_PATHS.settings) : {
                chanelActionBtn =   `<button type="button" class="btn btn-light" onclick = showDeviceSettings('`+channel.id+`')>
                                        <i class="bi bi-gear-fill"></i>
                                    </button>`;
                break;
            }
        }
        var channelImg = getImageObjBySensorType(channel.type);
        data += 
            `<div class = "row my-2 gy-1">
                <div class = "col-4">
                    <div class="text-center">
                        <figure class="figure">
                            <img width="50" src = "`+channelImg.img+`" class="figure-img img-fluid rounded" alt="..."></img>
                            <figcaption class="figure-caption">`+channel.specifedName+`</figcaption>
                        </figure>
                    </div>
                </div>
                <div class = "col-6 my-auto" >
                    value : `+channel.value+`
                </div>
                <div class = "col-1 my-auto" >
                    `+chanelActionBtn+`
                </div>
            </div>`;
    }
    return data;
}

function showPowerStripDevices(psID) {
    currentDeviceMenuLocation = currentDeviceMenuLocation = function (){
        showPowerStripDevices(psID)
    };
    let container;
    switch (location.pathname) {
        case (LOCATION_PATHS.settings) : {
            container = 'settings-container';
            break;
        }
        case (LOCATION_PATHS.status) : {
            container = 'status-container';
            break;
        }
        case (LOCATION_PATHS.actions) : {
            container = 'actions-container';
            break;
        }
    }
    var powerStrip = devicesMap.get(psID);
    var accordionPSDevices = 
        `<div class="accordion accordion-flush my-2" id="accordionPSDevices">
        </div>`;
    let data = "";
    if (!(location.pathname == LOCATION_PATHS.actions && gaCreateState == GA_STATES.condition1) && 
        !(location.pathname == LOCATION_PATHS.actions && gaCreateState == GA_STATES.condition2)) {
        data += 
            `<div class="accordion-item">
                <h2 class="accordion-header" id="flush-heading-outlet">
                <button id = "btn-outlet" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-outlet" aria-expanded="false" aria-controls="flush-collapse-outlet">
                    <img width="50" src = "`+SENSORS_IMAGES.outlet+`" class="img-fluid" alt="..."></img>
                    &nbsp;
                    outletls
                </button>
                </h2>
                <div id="flush-collapse-outlet" class="accordion-collapse collapse" aria-labelledby="flush-heading-outlet" data-bs-parent="#accordionPSDevices">
                    <div class="accordion-body">
                    `+getPSOutletsView(powerStrip)+`
                    </div>
                </div>
            </div>`;
        data +=   `  
            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-heading-out12v">
                <button id = "btn-out12v" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-out12v" aria-expanded="false" aria-controls="flush-collapse-out12v">
                    <img width="50" src = "`+SENSORS_IMAGES.pwm_output+`" class="img-fluid" alt="..."></img>
                    &nbsp;
                    PWM-12V Outputs
                </button>
                </h2>
                <div id="flush-collapse-out12v" class="accordion-collapse collapse" aria-labelledby="flush-heading-out12v" data-bs-parent="#accordionPSDevices">
                    <div class="accordion-body">
                    `+getPSPWMOutputsView(powerStrip)+`
                    </div>
                </div>
            </div>`;
            data +=   `  
            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-heading-solenoid">
                <button  id = "btn-solenoid" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-solenoid" aria-expanded="false" aria-controls="flush-collapse-solenoid">
                    <img width="50" src = "`+SENSORS_IMAGES.solenoid_water_valve+`" class="img-fluid" alt="..."></img>
                    &nbsp;
                    Valves 24VAC
                </button>
                </h2>
                <div id="flush-collapse-solenoid" class="accordion-collapse collapse" aria-labelledby="flush-heading-solenoid" data-bs-parent="#accordionPSDevices">
                    <div class="accordion-body">
                    `+getPSSolenoids24vacView(powerStrip)+`
                    </div>
                </div>
            </div>`;
        }
        if (!(location.pathname == LOCATION_PATHS.actions && gaCreateState == GA_STATES.action_device)) {
            data +=   `  
            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-heading-AnalogInput">
                <button  id = "btn-AnalogInput" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-AnalogInput" aria-expanded="false" aria-controls="flush-collapse-AnalogInput">
                    <img width="50" src = "`+SENSORS_IMAGES.analog_input+`" class="img-fluid" alt="..."></img>
                    &nbsp;
                    Analog Inputs
                </button>
                </h2>
                <div id="flush-collapse-AnalogInput" class="accordion-collapse collapse" aria-labelledby="flush-heading-AnalogInput" data-bs-parent="#accordionPSDevices">
                    <div class="accordion-body">
                    `+getPSAnalogInputsView(powerStrip)+`
                    </div>
                </div>
            </div>`;
        }
        

    document.getElementById(container).innerHTML = devicesHeaderText + accordionPSDevices +
    `<button class="btn btn-secondary" onclick = "showDevices()">
        <i class="bi bi-arrow-left-short"></i>&nbsp;Back
    </button>`;
    document.getElementById('accordionPSDevices').innerHTML = data;
}

function getPSOutletsView(powerStrip) {
    var data = "";
    for (outlet of powerStrip.outlets) {
        var devActionBtn;
        switch (location.pathname) {
            case (LOCATION_PATHS.status) : {
                devActionBtn =   `<button type="button" class="btn btn-dark"   disabled>
                                        <i class="bi bi-graph-up"></i>
                                    </button>`;
                break;
            }
            case (LOCATION_PATHS.actions) : {
                devActionBtn =   `<button type="button" class="btn btn-light" onclick = showGACreateDialog('`+outlet.id+`')>
                                        <i class="bi bi-plus"></i>
                                    </button>`;
                break;
            }
            case (LOCATION_PATHS.settings) : {
                devActionBtn =   `<button type="button" class="btn btn-light"   onclick = showDeviceSettings('`+outlet.id+`')>
                                        <i class="bi bi-gear-fill"></i>
                                    </button>`;
                break;
            }
        }
        data += 
        `<div class = "row my-2 gy-1">
            <div class = "col-4">
                <div class="text-center">
                    <figure class="figure">
                        <img width="50" src = "`+SENSORS_IMAGES.outlet+`" class="figure-img img-fluid rounded" alt="..."></img>
                        <figcaption class="figure-caption"> `+outlet.specifedName+`</figcaption>
                    </figure>
                </div>
            </div>
            <div class = "col-6 my-auto" >
                currentSensor : `+outlet.currentSensor+`
            </div>
            <div class = "col-1 my-auto" >
                `+devActionBtn+`
            </div>
        </div>`
    }
    return data;
}

function getPSPWMOutputsView(powerStrip) {
    var data = "";
    var i = 1;
    for (out12v of powerStrip.pwmsOutput12V) {
        var devActionBtn;
        switch (location.pathname) {
            case (LOCATION_PATHS.status) : {
                devActionBtn =   `<button type="button" class="btn btn-dark"   disabled>
                                        <i class="bi bi-graph-up"></i>
                                    </button>`;
                break;
            }
            case (LOCATION_PATHS.actions) : {
                devActionBtn =   `<button type="button" class="btn btn-light" onclick = showGACreateDialog('`+out12v.id+`')>
                                        <i class="bi bi-plus"></i>
                                    </button>`;
                break;
            }
            case (LOCATION_PATHS.settings) : {
                devActionBtn =   `<button type="button" class="btn btn-light"   onclick = showDeviceSettings('`+out12v.id+`')>
                                        <i class="bi bi-gear-fill"></i>
                                    </button>`;
                break;
            }
        }

        data += 
        `<div class = "row my-2 gy-1">
            <div class = "col-4">
                <div class="text-center">
                    <figure class="figure">
                        <img width="50" src = "`+SENSORS_IMAGES.pwm_output+`" class="figure-img img-fluid rounded" alt="..."></img>
                        <figcaption class="figure-caption">`+out12v.specifedName+`</figcaption>
                    </figure>
                </div>
            </div>
            <div class = "col-6 my-auto" >
                value : `+out12v.value+`
            </div>
            <div class = "col-1 my-auto" >
                `+devActionBtn+`
            </div>
        </div>`
        i++;
    }
    return data;
}

function getPSSolenoids24vacView(powerStrip) {
    var data = "";
    
    for (solenoid of powerStrip.solenoids24vac) {
        var devActionBtn;
        switch (location.pathname) {
            case (LOCATION_PATHS.status) : {
                devActionBtn =   `<button type="button" class="btn btn-dark"   disabled>
                                        <i class="bi bi-graph-up"></i>
                                    </button>`;
                break;
            }
            case (LOCATION_PATHS.actions) : {
                devActionBtn =   `<button type="button" class="btn btn-light" onclick = showGACreateDialog('`+solenoid.id+`')>
                                        <i class="bi bi-plus"></i>
                                    </button>`;
                break;
            }
            case (LOCATION_PATHS.settings) : {
                devActionBtn =   `<button type="button" class="btn btn-light"   onclick = showDeviceSettings('`+solenoid.id+`')>
                                        <i class="bi bi-gear-fill"></i>
                                    </button>`;
                break;
            }
        }

        let solenoidIsEnableText = (solenoid.isEnable == 1) ? 'enabled' : 'disabled';
        data += 
        `<div class = "row my-2 gy-1">
            <div class = "col-4">
                <div class="text-center">
                    <figure class="figure">
                        <img width="50" src = "`+SENSORS_IMAGES.solenoid_water_valve+`" class="figure-img img-fluid rounded" alt="..."></img>
                        <figcaption class="figure-caption">`+solenoid.specifedName+`</figcaption>
                    </figure>
                </div>
            </div>
            <div class = "col-6 my-auto" >
                `+solenoidIsEnableText+`
            </div>
            <div class = "col-1 my-auto" >
                `+devActionBtn+`
            </div>
        </div>`

    }
    return data;
}

function getPSAnalogInputsView(powerStrip) {
    var data = "";
    var i = 1;
    for (analogInput of powerStrip.analogInputs) {
        var devActionBtn;
        switch (location.pathname) {
            case (LOCATION_PATHS.status) : {
                devActionBtn =   `<button type="button" class="btn btn-dark"   disabled>
                                        <i class="bi bi-graph-up"></i>
                                    </button>`;
                break;
            }
            case (LOCATION_PATHS.actions) : {
                devActionBtn =   `<button type="button" class="btn btn-light" onclick = showGACreateDialog('`+analogInput.id+`')>
                                        <i class="bi bi-plus"></i>
                                    </button>`;
                break;
            }
            case (LOCATION_PATHS.settings) : {
                devActionBtn =   `<button type="button" class="btn btn-light"   onclick = showDeviceSettings('`+analogInput.id+`')>
                                        <i class="bi bi-gear-fill"></i>
                                    </button>`;
                break;
            }
        }

        data += 
        `<div class = "row my-2 gy-1">
            <div class = "col-4">
                <div class="text-center">
                    <figure class="figure">
                        <img width="50" src = "`+SENSORS_IMAGES.analog_input+`" class="figure-img img-fluid rounded" alt="..."></img>
                        <figcaption class="figure-caption">`+analogInput.specifedName+`</figcaption>
                    </figure>
                </div>
            </div>
            <div class = "col-6 my-auto" >
                value : `+analogInput.value+`
            </div>
            <div class = "col-1 my-auto" >
                `+devActionBtn+`
            </div>
        </div>`
        i++;
    }
    return data;
}

function showAddNewDeviceMenu() {
    currentDeviceMenuLocation = null;
    var dataToView = 
        `<h3 class = "m3">New device settings</h3>
        <form id = 'action-settings-form' class = "needs-validation">
        <div class="mb-3">
            <label for="deviceNameInput" class="form-label">Device name</label>
            <input type="text" class="form-control" id="deviceNameInput" required pattern="[a-zA-Z0-9]{1,16}">
            <div class="form-text">Device name required pattern [a-zA-Z0-9]{1,16}</div>
        </div>
        <div class="mb-3">
            <label for="deviceIDInput" class="form-label">Device ID</label>
            <input type="text" class="form-control" id="deviceIDInput" required pattern="[0-9ABCDEFabcdef]{12}">
            <div class="form-text">Device name required pattern [0-9ABCDEFabcdef]{12}</div>
        </div>
        <div class="input-group mb-3">
            <select id = "deviceTypeSelect" class="form-select" multiple aria-label="multiple select example" required>
                <option disabled>Select the device type</option>
                <option value = "mega" >Mega</option>
                <option value = "powerStrip" >Power strip</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary"><i class='bi bi-check'></i>&nbsp;Apply</button>
        <button id = "cancelBtn" class="btn btn-secondary"><i class="bi bi-arrow-left-short"></i>&nbsp;Cancel</button>
        </form>`;
    document.getElementById("settings-container").innerHTML = dataToView;
    document.getElementById('cancelBtn').onclick = function () {
        document.location.href = document.location.href;
    }
    if(document.querySelector('form')) {
        document.querySelector('form').onsubmit = function(e) {
            e.preventDefault();
                if (e.submitter.classList[1] == 'btn-primary') {
                    addNewDevice()
                    .then(function(){
                        showDevices(); 
                        uploadNamesFile();
                    })
                    .catch(error => alert(error))
                }
        };
    }
}

function showDeviceSettings(deviceID) {
    currentDeviceMenuLocation = null;
    let device = devicesMap.get(deviceID);
    let dataToView = 
    `
    <h3 class = "my-2" id = "devSettingsH3">device settings</h3>
    <form id = '`+deviceID+`_form'>
        <div id = "dev-settings-context">
        </div>
        <hr>
        <button id = "applyBtn" type="submit" class="btn btn-primary"><i class='bi bi-check'></i>&nbsp;Apply</button>
        <button id = "cancelBtn" class="btn btn-secondary"><i class="bi bi-arrow-left-short"></i>&nbsp;Cancel</button>
    </form>
    `;

    document.getElementById('settings-container').innerHTML = dataToView;
    if (device instanceof HUB) {
        document.getElementById("dev-settings-context").innerHTML = getHUBSettings();
        document.getElementById("device-color").value = device.color;
    } else if (device instanceof Mega) {

        document.getElementById("dev-settings-context").innerHTML = getMegaSettings();
        document.getElementById("device-color").value = device.color;

    } else if (device instanceof PowerStrip) {

        document.getElementById("dev-settings-context").innerHTML = getPowerStripSettings();
        document.getElementById("device-color").value = device.color;

    } else if (device instanceof MegaSensor) {

        document.getElementById("dev-settings-context").innerHTML = getMegaSensorSettings();

    } else if (device instanceof MegaSensorChannel) {

        document.getElementById("dev-settings-context").innerHTML = getMegaSensorChannelSettings();

    } else if (device instanceof Outlet) {

        document.getElementById("dev-settings-context").innerHTML = getOutletSettings();

    } else if (device instanceof PWM_Output) { // pwm 12 v

        document.getElementById("dev-settings-context").innerHTML = getOut12vSettings();

    } else if (device instanceof Solenoid24vac) {

        document.getElementById("dev-settings-context").innerHTML = getSolenoidSettings();

    } else if (device instanceof AnalogInput) {

        document.getElementById("dev-settings-context").innerHTML = getAnalogInputSettings();

    }

    document.getElementById("cancelBtn").onclick = function(){
        document.location.href = document.location.href;
    }

    document.getElementById("dev-name-input").value = device.specifedName;

    document.querySelector('form').onsubmit = function(e) {
        e.preventDefault();
        return applyDeviceSettings(e);
    };
}

function getHUBSettings() {
    return `
    <div class="mb-3">
        <label for="dev-name-input" class="form-label">HUB name</label>
        <input type="text" class="form-control" id="dev-name-input" required pattern="[a-zA-Z0-9_-]{1,16}">
        <div class="form-text">Only letters , numbers and _ or - (from 1 to 16 characters)</div>
        <label for="device-color" class="form-label">Color for <strong>HUB</strong></label>
        <input type="color" class="form-control form-control-lg" id="device-color">
    </div>
    `;
}
function getMegaSettings() {
    return `
    <div class="mb-3">
        <label for="dev-name-input" class="form-label">Mega name</label>
        <input type="text" class="form-control" id="dev-name-input" required pattern="[a-zA-Z0-9_-]{1,16}">
        <div class="form-text">Only letters , numbers and _ or - (from 1 to 16 characters)</div>
        <label for="device-color" class="form-label">Color for <strong>Mega</strong></label>
        <input type="color" class="form-control form-control-lg" id="device-color">
    </div>
    `;
}

function getPowerStripSettings() {
    return `
    <div class="mb-3">
        <label for="dev-name-input" class="form-label">PowerStrip name</label>
        <input type="text" class="form-control" id="dev-name-input" required pattern="[a-zA-Z0-9_-]{1,16}">
        <div class="form-text">Only letters , numbers and _ or - (from 1 to 16 characters)</div>
        <label for="device-color" class="form-label">Color for <strong>PowerStrip</strong></label>
        <input type="color" class="form-control form-control-lg" id="device-color">
    </div>
    `;
}

function getMegaSensorSettings() {
    return `
    <div class="mb-3">
        <label for="dev-name-input" class="form-label">Sensor name</label>
        <input type="text" class="form-control" id="dev-name-input" required pattern="[a-zA-Z0-9_-]{1,16}">
        <div class="form-text">Only letters , numbers and _ or - (from 1 to 16 characters)</div>
    </div>
    `;
}

function getMegaSensorChannelSettings() {
    return `
    <div class="mb-3">
        <label for="dev-name-input" class="form-label">Channel name</label>
        <input type="text" class="form-control" id="dev-name-input" required pattern="[a-zA-Z0-9_-]{1,16}">
        <div class="form-text">Only letters , numbers and _ or - (from 1 to 16 characters)</div>
    </div>
    `;
}

function getOutletSettings() {
    return `
    <div class="mb-3">
        <label for="dev-name-input" class="form-label">Outlet name</label>
        <input type="text" class="form-control" id="dev-name-input" required pattern="[a-zA-Z0-9_-]{1,16}">
        <div class="form-text">Only letters , numbers and _ or - (from 1 to 16 characters)</div>
    </div>
    `;
}

function getOut5vSettings() {
    return `
    <div class="mb-3">
        <label for="dev-name-input" class="form-label">PWM out 5V name</label>
        <input type="text" class="form-control" id="dev-name-input" required pattern="[a-zA-Z0-9_-]{1,16}">
        <div class="form-text">Only letters , numbers and _ or - (from 1 to 16 characters)</div>
    </div>
    `;
}

function getOut12vSettings() {
    return `
    <div class="mb-3">
        <label for="dev-name-input" class="form-label">PWM out 12v name</label>
        <input type="text" class="form-control" id="dev-name-input" required pattern="[a-zA-Z0-9_-]{1,16}">
        <div class="form-text">Only letters , numbers and _ or - (from 1 to 16 characters)</div>
    </div>
    `;
}

function getSolenoidSettings() {
    return `
    <div class="mb-3">
        <label for="dev-name-input" class="form-label">solenoid 24V AC name</label>
        <input type="text" class="form-control" id="dev-name-input" required pattern="[a-zA-Z0-9_-]{1,16}">
        <div class="form-text">Only letters , numbers and _ or - (from 1 to 16 characters)</div>
    </div>
    `;
}

function getAnalogInputSettings() {
    return `
    <div class="mb-3">
        <label for="dev-name-input" class="form-label">Analog input name</label>
        <input type="text" class="form-control" id="dev-name-input" required pattern="[a-zA-Z0-9_-]{1,16}">
        <div class="form-text">Only letters , numbers and _ or - (from 1 to 16 characters)</div>
    </div>
    `;
}

function applyDeviceSettings(e) {

    if (e.submitter.classList[1] != 'btn-primary') {
        return;
    }

    let deviceID = e.target.id.split("_")[0];
    device = devicesMap.get(deviceID);

    if (!deviceNameIsUnique(device,document.getElementById("dev-name-input").value)) {
        alert ("a device with this name already exists");
        return;
    }
    blockApplyBtn();
    device.specifedName = document.getElementById("dev-name-input").value;
    if (device instanceof Mega || device instanceof PowerStrip || device instanceof HUB) {
        device.color =  document.getElementById("device-color").value;
    }
    uploadNamesFile().then(response => {
        if (response.status === 200) {
            uploadDevicesColors()
            .then(uploadResponse => {
                if (uploadResponse.status === 200) {
                    if (device instanceof HUB) {

                        showDevices(); 
                    } else 
                    if (device instanceof Mega) {

                        showDevices(); 
        
                    } else if (device instanceof PowerStrip) {

                        showDevices();

                    } else if (device.parent instanceof PowerStrip) {
        
                        showPowerStripDevices(device.parent.id); 
                        document.getElementById("btn-"+device.type+"").setAttribute("class","accordion-button")
                        document.getElementById("flush-collapse-"+device.type+"").setAttribute("class","accordion-collapse collapse show")
        
                    } else if (device.parent instanceof MegaSensor) {
        
                        showMegaSensors(device.parent.parent.id); 
                        document.getElementById("btn-"+device.parent.id+"").setAttribute("class","accordion-button")
                        document.getElementById("flush-collapse-"+device.parent.id+"").setAttribute("class","accordion-collapse collapse show")
        
                    } else if (device.parent instanceof Mega) {
                        showMegaSensors(device.parent.id, true);
                    }
                } else {
                    unblockApplyBtn();
                    alert ("error on upload colors data : "+response.status);
                }
            })
        } else {
            unblockApplyBtn();
            alert ("error on upload names file : "+response.status);
        }
    })
    .catch(err => {
        unblockApplyBtn();
        alert (err);
    })
}
function setCurrentAccordionElementOpened() {
    let collapseShowId = "";
    let accordionBtnID = "";
    let accordionCollapseShow = document.querySelector(".accordion-collapse.collapse.show");
    if (accordionCollapseShow) {
        collapseShowId =  accordionCollapseShow.id;
        let accordionBtns = document.querySelectorAll(".accordion-button");
        for (btn of accordionBtns) {
            if (btn.className == "accordion-button") {
                accordionBtnID = btn.id;
                currentAccordionElementOpened = {
                    buttonID : accordionBtnID,
                    collapseShowID :  collapseShowId
                }
                return;
            }
        }
    }
    currentAccordionElementOpened = null;
}

function renderDevices() {
    if (currentDeviceMenuLocation) {
        setCurrentAccordionElementOpened();
        currentDeviceMenuLocation();
        if (currentAccordionElementOpened) {
            document.getElementById(currentAccordionElementOpened.buttonID).setAttribute("class","accordion-button")
            document.getElementById(currentAccordionElementOpened.collapseShowID).setAttribute("class","accordion-collapse collapse show")
        }
    }
}
