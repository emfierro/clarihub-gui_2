function decodeStrHex(hex) {
	var hex = hex.toString();
	var str = '';
	for (var i = 0; i < hex.length; i += 2) {
		var int = parseInt(hex.substr(i, 2), 16);
		if (int == 0) break;
		str += String.fromCharCode(int);
	}
	return str;
}
function decodeBoolHex(hex) {
	var data = Number(hex)
	return data == 0 || !data ? false : true ;
}
function boolToHex(data,bytes) {
	if (bytes) {
		var zeroStr = "";
		for (var i = 0; i < (bytes*2 - 1); i++) {
			zeroStr += "0";
		}
		return zeroStr+Number(data);
	} else {
			return "0"+Number(data);
	}
}
function numToHex(data,bytes,signed) {
	var numStr = Number(data) ;
	if (signed) {
		switch (bytes) {
			case (4): {
				if (numStr < 0) {
					numStr = 4294967296 + numStr;
				}
				break;
			}
		}
	}
	str = numStr.toString(16);
	var zeroStr = "";
	if (bytes) {
		for (var i = 0; i < (bytes*2 - str.length); i++) {
			zeroStr += "0";
		}
	}
	return zeroStr+str;
}

function strToHex(data,bytes) {
	var hex = '';
	for(var i=0;i<data.length;i++) {
		hex += ''+data.charCodeAt(i).toString(16);
	}
	var zeroStr = "";
	if (bytes) {
		for (var i = 0; i < (bytes*2 - hex.length); i++) {
			zeroStr += "0";
		}
	}
	return hex+zeroStr;
}