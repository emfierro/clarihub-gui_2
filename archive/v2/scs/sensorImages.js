const SENSORS_IMAGES = {
    temperature				: '../SD/images/Icon_01.png',
    air_humidity 			: '../SD/images/Icon_02.png',
    soil_humidity 			: '../SD/images/Icon_03.png',
    luminosity 				: '../SD/images/Icon_04.png',
    co2_level 				: '../SD/images/Icon_05.png',
    soil_ph 				: '../SD/images/Icon_06.png',
    water_conductivity 		: '../SD/images/Icon_07.png',
    solenoid_water_valve	: '../SD/images/Icon_08.png',
    water_pump				: '../SD/images/Icon_09.png',
    fan						: '../SD/images/Icon_10.png',
    ventilation_damper		: '../SD/images/Icon_11.png',
    co2_valve				: '../SD/images/Icon_12.png',
    water_ph				: '../SD/images/Icon_13.png',
    light_led				: '../SD/images/Icon_14.png',
    peristaltic_pump		: '../SD/images/Icon_15.png',
    water_level_sensor		: '../SD/images/Icon_16.png',
    outlet					: '../SD/images/Icon_17.png',
    pwm_output				: '../SD/images/Icon_18.png',
    analog_input			: '../SD/images/Icon_19.png',
    alert_alarm				: '../SD/images/Icon_20.png',
    PowerStrip 				: '../SD/images/Icon_21.png',
    Mega 					: '../SD/images/Icon_22.png',
    script					: '../SD/images/Icon_23.png',
    clock					: '../SD/images/Icon_24.png',
    timer					: '../SD/images/Icon_25.png'
}


function showSelectImagesScreen(location,caller_func, func_params) {
    if (document.getElementById(location)) {
        location = document.getElementById(location)
        let dataToView = `<div class = "row">
        <h5>Select the icon : </h5>`;
        for (let imgSrc of Object.values(SENSORS_IMAGES)) {
            dataToView += `
            <div class = 'col-3 my-3' style = "cursor:pointer;" >
                <img width = "75px" src = "${imgSrc}" class = "img-fluid"/>
            </div>`
        }
        dataToView += `</div>`
        location.innerHTML = dataToView;
        for (let img of document.querySelectorAll("img")) {
            img.onclick = function() {
                caller_func(img.src,func_params);
            }
        }
    }
    
}