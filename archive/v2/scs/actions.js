var actionIsnotSaved = false;

class Script {
	constructor(name,fullCode) {
		let data = fullCode.split(String.fromCharCode(7));
		if (data.length != 2) {
			data = ['',''];
		}
		this.name = name;
		this.vars = data[0] ? data[0].substring(1,data[0].length) : '';
		this.code = data[1] ? data[1] : '';

		this.execution = data[0] ? data[0].charAt(0) : '-';
		this.type = 'script'; // script or graphic
	}
	save(codeMirrorElem) {

		if (document.getElementById("varsOrCodeSelect")) {

			if(document.getElementById("varsOrCodeSelect").value == "vars") {
				this.vars = codeMirrorElem.getValue();
			} else if(document.getElementById("varsOrCodeSelect").value == "code") {
				this.code = codeMirrorElem.getValue();
			}
			
		}

        
		var data = 	this.execution+
					this.vars+
					String.fromCharCode(7)+
					this.code;

		var file =  new File([data], "file", {
						type: "text/plain",
				});
		data = new FormData();
		data.append('file', file)
		return fetch('/rdrok/updMP/scs/'+this.name+'.wws', {
			method: 'POST',
			body: data
		})

	}
}
function initScriptsList() {
    return new Promise((resolve, reject) => {
        fetch('/getScripts/')
	    .then( response => {
            if (response.status !== 200) {
                reject("http error : "+ response.status);
            } else {
                scriptsNamesList = [];
                response.text().then(data => {
                    data = JSON.parse(data);
					if(data.scripts) {
						for (let i = 0; i < data.scripts.length; i++) {
							scriptsNamesList.push(data.scripts[i].replace(/^.*[\\\/]/, '').split('.')[0]);
						}
					}
                    resolve(true);
                })
            }
        })
        .catch(err => {
            reject(err);
        })
    })
}

function showActions(actionType) {

    document.getElementById("actions-container").innerHTML = `
    <div class="spinner-border mt-5" role="status">
        <span class="visually-hidden">loading...</span>
    </div>`;

    let dataToView = `
    <div class="form-group my-3">
        <select id="action-type-select" class="form-select"  onchange = "showActions(this.value)">
            <option value = "txt">Script actions</option>
            <option value = "ga">Graphical actions</option>
        </select>
    </div>`;

    if (!actionType) {
        downloadGraphicalActions()
        .then(hex => {
            initGraphicalActions(hex);
            downloadGaViewData()
            .then(json => {
                initGaViewData(json);
                dataToView += getGraphicalActionsListView();
                document.getElementById("actions-container").innerHTML = dataToView;
                document.getElementById("action-type-select").value = "ga";
            })
        })
        .catch(err => {
            dataToView += `<div>error : ${err}</div>`;
            document.getElementById("actions-container").innerHTML = dataToView;
            document.getElementById("action-type-select").value = "ga";
        })
    }
    if (actionType == 'txt') {
        initScriptsList()
        .then(res => {
            dataToView += getTXTActionsListView();
            document.getElementById("actions-container").innerHTML = dataToView;
            document.getElementById("action-type-select").value = "txt";
        })
        .catch(err => {
            dataToView += `<div>error : ${err}</div>`;
            document.getElementById("actions-container").innerHTML = dataToView;
            document.getElementById("action-type-select").value = "txt";
        })
    }
    if (actionType == 'ga') {
        downloadGraphicalActions()
        .then(hex => {
            initGraphicalActions(hex);
            downloadGaViewData()
            .then(json => {
                initGaViewData(json);
                dataToView += getGraphicalActionsListView();
                document.getElementById("actions-container").innerHTML = dataToView;
                document.getElementById("action-type-select").value = "ga";
            })
        })
        .catch(err => {
            dataToView += `<div>error : ${err}</div>`;
            document.getElementById("actions-container").innerHTML = dataToView;
            document.getElementById("action-type-select").value = "ga";
        })
    }
}

function getTXTActionsListView() {
    var addNewBtn = `<button type="button" class="btn btn-primary" id = 'add-new-act-btn'  onclick = 'showAddNewScriptActionMenu()' >
                        <i class="bi bi-plus"></i> Add new
                    </button>`;
    var res = "";
    for (let scrName of scriptsNamesList) {
        let colapse = 
        `<div class="collapse" id="collapseExample-`+scrName+`">
            <div class="card card-body">`
                +scrName+ ` :  description ` +
            `</div>
        </div>`;
        res +=  "<div class=\"row my-3 py-1\" style=\"background-color: #8FBC8F; border-radius: 5px;\" >"+
            "    <div class=\"col-3 \"><img width=\"80\" src = \""+SENSORS_IMAGES.script+"\" class=\"img-fluid\" alt=\"...\"></div>"+
            "    <div class=\"col-7 my-auto\" onclick = \"showAction('"+scrName+"')\"><h5>"+scrName+"</h5></div>"+
            "    <div class=\"col-1 my-auto\"><button type=\"button\" class=\"btn btn-light\" data-bs-toggle=\"collapse\" data-bs-target=\"#collapseExample-"+scrName+"\" aria-expanded=\"false\" aria-controls=\"collapseExample-"+scrName+"\"><i class=\"bi bi-caret-down\"></i></button></div>"+
            colapse+
            "</div>";
    } 
    if (res == "") {
        res = "<div class = 'my-3'>No actions yet...</div>";
    }

    return res + addNewBtn;
}

function getActionData(actionName) {
    return new Promise((resolve, reject) => {
        fetch("/scs/"+actionName+".wws")
        .then(function(response){
            if (response.status == 200) {
                response.text().then(function(taskData){
                    if (!taskData) taskData = '[404]';
                    resolve(new Script(actionName,taskData));
                })
            } else {
                reject("Error");
            }
        })
        .catch(function(err){
            alert("fetch err : "+err);
            reject("Error");
        })
    })
  }
function showAction(actionName) {
getActionData(actionName).then(function (action) {
        document.getElementById('actions-container').innerHTML =
            `<div class = "row my-3">
            <div class = "col-3 my-3">
                <select id = "varsOrCodeSelect" class = "form-select">
                    <option value = "vars" selected>variables</option>
                    <option value = "code" >code</option>
                </select>
            </div>
            <div id = "actionNameDiv" class = "col-6 my-3 text-center">
                <h4>`+action.name+`</h4>
            </div>
            <div class = "col-1 my-3">
                <button id = "consoleBtn" type="button" class="btn btn-light border">
                    <i class="bi bi-card-text"></i>
                </button>
            </div>
            <div class = "col-1 my-3">
                <button id = 'runActionBtn' type="button" class="btn btn-light border">
                    <i class="bi bi-caret-right-fill"></i>
                </button>
            </div>
            <div class = "col-1 my-3">
                <button id = 'settingsActionBtn'  type="button" class="btn btn-light border">
                    <i class="bi bi-gear"></i>
                </button>
            </div>
            <div class = "col-12">
                <div id = "code" class = "border" style = "height: 35em;"></div>
            </div>
        </div>
        <p id = "savedStatus">
        
        </p>
        <button id = "returnBackActionBtn" type="button" class="btn btn-secondary" >
            <i class="bi bi-arrow-left-short"></i> Back
        </button>
        <button id = "saveActionBtn" type="button" class="btn btn-primary">
            <i class="bi bi-save px-1"></i>Save
        </button>
        <button id = "removeActionBtn" type="button" class="btn btn-danger">
            <i class="bi bi-trash"></i>&nbsp;Remove
        </button>
        `;
        document.getElementById('consoleBtn').onclick = function() {
            var f=window.open("/log.htm",'targetWindow',
                               `toolbar=no,
                                location=no,
                                status=no,
                                menubar=no,
                                scrollbars=no,
                                resizable=yes,
                                width=700,
                                height=500`);
        }

        var myCodeMirror = CodeMirror(document.querySelector('#code'), {
            value: action.vars,
            mode:  "javascript",
            lineNumbers: true
          });
        
        myCodeMirror.setSize(null,'100%');

        document.getElementById("settingsActionBtn").onclick = function() {
            showScriptActionSettings(action);
        }

        document.getElementById("runActionBtn").onclick = function() {
            let runAction = confirm("Realy run action?");
            if (runAction) {
                fetch('/startScript/'+action.name);
            }
        }

        document.getElementById("saveActionBtn").onclick = function() {
            action.save(myCodeMirror)
            .then(function (response) {
                if (response.status == 200 ) {
                    changeVisualActionSavedState(false);
                }
            })
        }

        document.getElementById("removeActionBtn").onclick = function () {
            let remove = confirm("Realy remove action?");
            if (remove) {
                removeAction(action).then(r => {
                     window.location.reload();
                })
            }
        }

        document.getElementById("returnBackActionBtn").onclick = function () {
            if (actionIsnotSaved) {
                let exit = confirm("No data saved.\nReally get out?");
                if (exit) {
                    window.location.reload();
                }
            } else {
                window.location.reload();
            }
        }

        document.getElementById("varsOrCodeSelect").onchange = function() {
            if (this.value == "vars") {
                action.code = myCodeMirror.getValue();
                myCodeMirror.setValue(action.vars);
            } else if (this.value == "code") {
                action.vars = myCodeMirror.getValue();
                myCodeMirror.setValue(action.code);
            }
        }

        document.addEventListener("keydown", function(e) {
            if ((window.navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)  && e.code == "KeyS") {
              e.preventDefault();
              action.save(myCodeMirror)
              .then(function (response) {
                if (response.status == 200 ) {
                    changeVisualActionSavedState(false);
                }
            });
            } else {
                changeVisualActionSavedState(true);
            }
          }, false);
    });
}

function showScriptActionSettings(action) {

    document.getElementById('actions-container').innerHTML =
        `<h3 class = "m3">Action settings</h3>
        <form id = 'action-settings-form'>
        <div class="mb-3">
            <label for="actionNameInput" class="form-label">Action name</label>
            <input type="text" class="form-control" id="actionNameInput">
        </div>
        <div class="input-group mb-3">
            <select id = "actionExecutionSelect" class="form-select" multiple aria-label="multiple select example">
                <option disabled>Select the execution type</option>
                <option value = "-" >once</option>
                <option value = "+" >repeatedly</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary"><i class='bi bi-check'></i>&nbsp;Apply</button>
        <button id = "cancelBtn" class="btn btn-secondary"><i class="bi bi-arrow-left-short"></i>&nbsp;Cancel</button>
        </form>`;
        document.getElementById("cancelBtn").onclick = function() {
            showAction(action.name)
        }
        document.getElementById('actionNameInput').value = action.name;
        document.getElementById("actionExecutionSelect").value = action.execution;
        if(document.querySelector('form')) {
            document.querySelector('form').onsubmit = function(e) {
                e.preventDefault();
                return applyScriptActionSettings(e,action);
            };
        }
}


function showAddNewScriptActionMenu() {

    document.getElementById('actions-container').innerHTML =
        `<h3 class = "m3">Action settings</h3>
        <form id = 'action-settings-form' class = "needs-validation">
        <div class="mb-3">
            <label for="actionNameInput" class="form-label">Action name</label>
            <input type="text" class="form-control" id="actionNameInput" required pattern="[a-zA-Z0-9]{1,16}">
            <div class="form-text">Only letters and numbers (from 1 to 16 characters)</div>
        </div>
        <div class="input-group mb-3">
            <select id = "actionExecutionSelect" class="form-select" multiple aria-label="multiple select example" required>
                <option disabled>Select the execution type</option>
                <option value = "-" >once</option>
                <option value = "+" >repeatedly</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary"><i class='bi bi-check'></i>&nbsp;Apply</button>
        <button id = "cancelBtn" class="btn btn-secondary"><i class="bi bi-arrow-left-short"></i>&nbsp;Cancel</button>
        </form>`;
        document.getElementById("cancelBtn").onclick = function(){
            showActions();
        };
        if(document.querySelector('form')) {
            document.querySelector('form').onsubmit = function(e) {
                e.preventDefault();
                return applyScriptActionSettings(e,null,true);
            };
        }
}

function applyScriptActionSettings(e,action,isAddNewAction) {
    if (e.submitter.classList[1] == 'btn-primary') {
        let actionNameInput = document.querySelector("#actionNameInput");
        let actionExecutionSelect = document.querySelector("#actionExecutionSelect");
        if (isAddNewAction) {
            action = new Script(actionNameInput.value,"");
            var lowerCaseActionsList = [];
            for (e of scriptsNamesList) {
                lowerCaseActionsList.push(e.toLowerCase())
            }
            if (lowerCaseActionsList.includes(actionNameInput.value.toLowerCase())) {
                alert ("A action with the same name already exists");
                return;
            }
        }
        action.execution = actionExecutionSelect.value;
        action.save()
        .then(function (response) {
            if (response.status == 200 ) {
                if (isAddNewAction) {
                    showAction(actionNameInput.value)
                } else {
                    renameAction(action, actionNameInput.value)
                    .then(res => {
                        showAction(actionNameInput.value)
                    })
                }
            } else {
                alert("error; response.status = "+response.status);
            }
        })

        .catch(error =>{
            alert(error)
        })
    }
}

function removeAction(action) {
    return new Promise((resolve,reject) => {
        fetch ('/rm/scs/'+action.name+'.wws')
        .then(response => {
            if (response.status !== 200) {
                reject("Error : " + response.status );
            } else {
                resolve(true);
            }
        })
        .catch(error => {
            reject(error)
        })
    })
}

function renameAction(currentAction,newActionName) {
    return new Promise((resolve,reject) => {
        var scsData;
        scsData = "rename:/scs/"+currentAction.name+".wws:/scs/"+newActionName+".wws::";

        fetch("/rnSc/",{
            method: 'POST',
            body: scsData
        })
        .then(function(response) {
            if (response.status == 200) {
                resolve(true);
            } else {
                reject("Error : " + response.status );
            }
        })
        .catch(error => {
            reject(error)
        })
    })
}

function changeVisualActionSavedState(isChanges) {
    let e = document.getElementById("savedStatus");
    if (!e) return;
    if (isChanges) {
        actionIsnotSaved = true;
        e.innerHTML = 
        `<figcaption class="blockquote-footer">
            not saved
        </figcaption>`;
    } else {
        actionIsnotSaved = false;
        e.innerHTML = "";
    }
}
