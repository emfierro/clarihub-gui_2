class Schedule {
	constructor(id,schName,scriptName,isActive,type) {
		this.id = id;
		this.schName = schName;
		this.scriptName = scriptName;
		this.isActive = isActive;
		this.type = type;
		this.date = null;
		this.time = null;
		this.dailyTime = null;
		this.period = null;
		this.sensorId = null;
		this.sensorValue = null;
		this.valueComparison = null;
		this.timeShifting = null;
		this.currentSensor = null;
		this.overCurrentFlag = null;
	}
	getIcon() {
		switch (this.type) {
			case ('periodically') : {
				return '<i class="bi bi-arrow-clockwise"></i>';
			}
			case ('oneTime') : {
				return '<i class="bi bi-clock"></i>';
			}
			case ('daily') : {
				return '<i class="bi bi-sun"></i>';
			}
			case ('byValue') : {
				return '<i class="bi bi-arrow-clockwise"></i>';
			}
			case ('sunset') : {
				return '<i class="bi bi-sunset"></i>';
			}
			case ('sunrise') : {
				return '<i class="bi bi-sunrise"></i>';
			}
		}
	}
	periodToSeconds() {
		return this.period;
	}
	getHex(schId) {
		var schedule;
		if (schId) {
			schedule = schedulesMap.get(schId);
		} else {
			schedule = this;
		}
		var result = "";
		switch (schedule.type) {
			case ("oneTime"): {
				var date = schedule.date.split("-");
				var time = schedule.time.split(":");
				var datetime = new Date(date[0],date[1]-1,date[2],time[0],time[1]).getTime()/1000;
				var result = "01";
				result += numToHex(datetime,4);
				break;
			}
			case ("daily"): {
				var secondsFromMidnight;
				var time = schedule.dailyTime.split(":");
				secondsFromMidnight = time[0] * 60 * 60 + time[1] * 60;
				var result = "02";
				result += numToHex(secondsFromMidnight,4);
				break;
			}
			case ("periodically"): {
				var periodicity = schedule.periodToSeconds();
				var result = "03";
				result += numToHex(periodicity,4);
				break;
			}
			case ("byValue"): {
				var valueComparison = "=";
				switch (schedule.valueComparison) {
					case ("more") : {
						valueComparison = ">";
						break;
					}
					case ("less") : {
						valueComparison = "<";
						break;
					}
				}
				var result = "";
				let device = devicesMap.get(schedule.sensorId);
				if (device instanceof MegaSensorChannel) {
					var channelData = schedule.sensorId.split(":");
					let sensorId = channelData[0];
					let megaId = devicesMap.get(sensorId).parent.id;
					let channelNumber = channelData[1];
					result += "04" + megaId.padStart(12,"0") + sensorId.padStart(12,"0") + numToHex(channelNumber,1) + strToHex(valueComparison,1) + numToHex(schedule.sensorValue,4);
					break;
				} else if (device instanceof Outlet) {
					var result = "05";
					let deviceData = schedule.sensorId.split(":");
					let psId = deviceData[0];
					let devNum = deviceData[2];
					result += psId.padStart(12,"0"); // powerStrip id
					result += "01"; // sensor type
					result += numToHex(devNum,1); // sensor number
					if (schedule.currentSensor) {
						result += "01"; // value type
						result += strToHex(valueComparison,1);
						result += numToHex(schedule.currentSensor,4); // value
					} else if (schedule.overCurrentFlag) {
						result += "02"; // value type
						result += strToHex(valueComparison,1);
						result += numToHex(schedule.overCurrentFlag,4); // value
					} else {
						return "";
					}
					break;
				} else if (device instanceof AnalogInput) {
					var result = "05";
					let deviceData = schedule.sensorId.split(":");
					let psId = deviceData[0];
					let devNum = deviceData[2];
					result += psId; // powerStrip id
					result += "02"; // sensor type = analog
					result += numToHex(devNum,1); // sensor number
					result += "01";
					result += strToHex(valueComparison,1);
					result += numToHex(schedule.sensorValue,4);
					break;
				}
			}
			case ("sunset"): {
				var result = "";
				result += "06";
				result += numToHex(schedule.timeShifting,4,true);
				break;
			}
			case ("sunrise"): {
				var result = "";
				result += "07";
				result += numToHex(schedule.timeShifting,4,true);
				break;
			}
			default : return;
		}
		result += strToHex(schedule.scriptName,16);
		result += boolToHex(schedule.isActive,1);
		result += numToHex(schedule.id,1);
		result += crc16_BUYPASS(result);
		return result;
	}
}
async function initSchedules() {
	fetch('/schNames.txt')
	.then(
		function(response) {
			if (response.status !== 200) {
				console.log('Looks like there was a problem. Status Code: ' +
         					 response.status);
        		return;
			}
			response.text().then(
				function(fileData) {
					var idNames = fileData.split("\n");
					for ( var idName of idNames ) {
						var data = idName.trim();
						data = data.split("=");
						if (data.length != 2) continue;
						schedulesNamesMap.set(data[0].trim(),data[1].trim());
					}
				}
			)
			.then(function() {
				fetch("/schedules.txt")
				.then(function(schResponse){
					if (schResponse.status !== 200) {
						console.log('Looks like there was a problem. Status Code: ' +
									 devicesResponse.status);
						return;
					}
					schResponse.text().then( function(schData){
						initSchedulesMap(schData)
						if (document.getElementById('schedules-table')) {
							fillSchsTable()
						}
					})
				})
			})
		}
	)
	.catch(function(err) {
		console.log('Fetch Error :-S', err);
	});
	return 1;
}
function initSchedulesMap(inData) {
	var schedulesArr = inData.split("\n");
	for (var i = 0; i < schedulesArr.length; i++) {
		if (!checkScheduleCRC16 (schedulesArr[i]) ) {
			continue;
		}
		var scheduleType = schedulesArr[i].substring(0,2);
		var schedule = null;
		switch (scheduleType) {
			case ("01") : {
				schedule = createOneTimeSchedule(schedulesArr[i]);
				break;
			}
			case ("02") : {
				schedule = createdailySchedule(schedulesArr[i])
				break;
			}
			case ("03") : {
				schedule = createPeriodicallySchedule(schedulesArr[i]);
				break;
			}
			case ("04") : {
				schedule = createByValueMegaSchedule(schedulesArr[i]);
				break;
			}
			case ("05") : {
				schedule = createByValuePowerStripSchedule(schedulesArr[i]);
				break;
			}
			case ("06") : {
				schedule = createSunsetSchedule(schedulesArr[i]);
				break;
			}
			case ("07") : {
				schedule = createSunriseSchedule(schedulesArr[i]);
				break;
			}
		}
		schedulesMap.set(schedule.id,schedule);
	}
}
function createSchedulesNamesFile() {
	var data = "";
	for (var [key,value] of schedulesMap) {
		data += key+" = "+value.schName+"\n";
	}
	return new File([data], "names", {
						type: "text/plain",
				});
}
function uploadSchedulesNamesFile() {
	var data = new FormData()
	data.append('file', createSchedulesNamesFile())
	fetch('/rdrok/updMP/schNames.txt', {
		method: 'POST',
		body: data
	})
}

function createScheduleFile() {
	var data = "";
	for (var [key,value] of schedulesMap) {
		data += value.getHex()+"\n";
	}
	return new File([data], "schedules", {
		type: "text/plain",
	});
}
function uploadSchedulesFile() {
	var fileData = createScheduleFile();
	if (!fileData) return;
	var data = new FormData()
	data.append('file',fileData)
	fetch('/rdrok/updMP/schedules.txt', {
		method: 'POST',
		body: data
	})
}
function checkScheduleCRC16(data) {
	var crc16 = data.substring(data.length - 4);
	var scheduleData = data.substring(0,data.length-4);
	return crc16_BUYPASS(scheduleData) == crc16;
}
function createOneTimeSchedule(data) {
	var unixTime = parseInt(data.substring(2,2+8),16);
	var date = new Date(unixTime*1000);
	var scriptName = decodeStrHex(data.substring(10,10+32));
	var isActive = decodeBoolHex(data.substring(42,44));
	var id = parseInt(data.substring(44,46),16).toString();
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = "oneTime:"+id;
	}
	var schedule = new Schedule(id,scheduleName,scriptName,isActive,"oneTime");
	schedule.date = date.YYYYMMDD();
	schedule.time = date.HHmm();
	return schedule;
}

function createdailySchedule(data) {
	var dailyTime = parseInt(data.substring(2,2+8),16);
	var scriptName = decodeStrHex(data.substring(10,10+32));
	var isActive = decodeBoolHex(data.substring(42,44));
	var id = parseInt(data.substring(44,46),16).toString();
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = "daily:"+id;
	}
	var schedule = new Schedule(id,scheduleName,scriptName,isActive,"daily")
	var mins = (dailyTime%3600)/60;
	if (mins < 10) {
		mins = '0'+mins;
	}
	var hrs = parseInt(dailyTime/3600);
	if (hrs < 10) {
		hrs = '0'+hrs;
	}
	schedule.dailyTime = hrs+":"+mins;
	return schedule;
}
function createPeriodicallySchedule(data) {
	var periodicity = parseInt(data.substring(2,2+8),16);
	var scriptName = decodeStrHex(data.substring(10,10+32));
	var isActive = decodeBoolHex(data.substring(42,44));
	var id = parseInt(data.substring(44,46),16).toString();
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = "periodically:"+id;
	}
	var schedule = new Schedule(id,scheduleName,scriptName,isActive,"periodically")
	schedule.period = periodicity;
	return schedule;
}
function createByValueMegaSchedule(data) {
	var sensorId = data.substring(14,14+12);
	var chNum = parseInt(data.substring(26,26+2),16);
	var valueComparison = decodeStrHex(data.substring(28,30));
	if (valueComparison == '>') {
		valueComparison = "more";
	}
	if (valueComparison == '<') {
		valueComparison = "less";
	}
	var sensorValue = parseInt(data.substring(30,38),16);
	var scriptName = decodeStrHex(data.substring(38,38+32));
	var isActive = decodeBoolHex(data.substring(70,72));
	var id = parseInt(data.substring(72,74),16).toString();
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = "byValue:"+id;
	}
	var schedule = new Schedule(id,scheduleName,scriptName,isActive,"byValue")
	schedule.sensorValue = sensorValue;
	schedule.valueComparison = valueComparison;
	schedule.sensorId = sensorId+":"+chNum;
	return schedule;
}
function createByValuePowerStripSchedule(data) {
	var psId = data.substring(2,2+12);
	var sensorType = data.substring(14,16);
	var sensorNumber =  parseInt(data.substring(16,18),16);
	var valueType = data.substring(18,20);
	var valueComparison = decodeStrHex(data.substring(20,22));
	if (valueComparison == '>') {
		valueComparison = "more";
	}
	if (valueComparison == '<') {
		valueComparison = "less";
	}
	var value = parseInt(data.substring(22,30),16);
	var scriptName = decodeStrHex(data.substring(30,62));
	var isActive = decodeBoolHex(data.substring(62,64));
	var id = parseInt(data.substring(64,66),16).toString();
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = "byValue:"+id;
	}
	var schedule = new Schedule(id,scheduleName,scriptName,isActive,"byValue")
	schedule.valueComparison = valueComparison;
	switch (sensorType) {
		case ("01") : {
			schedule.sensorId = psId+":outlet:"+sensorNumber;
			switch (valueType) {
				case ("01") : {
					schedule.currentSensor = value;
					break;}
				case ("02") : {
					schedule.overCurrentFlag = value;
					break;}
			}
			break;
		}
		case ("02") : {
			schedule.sensorId = psId+":analog:"+sensorNumber;
			schedule.sensorValue = value;
			break;
		}
	}
	return schedule;
}
function createSunsetSchedule(data) {
	var timeShifting = parseInt(data.substring(2,2+8),16);
	var scriptName = decodeStrHex(data.substring(10,10+32));
	var isActive = decodeBoolHex(data.substring(42,44));
	var id = parseInt(data.substring(44,46),16).toString();
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = "sunset:"+id;
	}
	var schedule = new Schedule(id,scheduleName,scriptName,isActive,"sunset");
	if (timeShifting > (4294967296/2)-1) {
		timeShifting = (4294967296 - timeShifting) * -1;
	}
	schedule.timeShifting = timeShifting;
	schedule.type = "sunset";
	return schedule;
}
function createSunriseSchedule(data) {
	var timeShifting = parseInt(data.substring(2,2+8),16);
	var scriptName = decodeStrHex(data.substring(10,10+32));
	var isActive = decodeBoolHex(data.substring(42,44));
	var id = parseInt(data.substring(44,46),16).toString();
	var scheduleName = schedulesNamesMap.get(id);
	if (!scheduleName) {
		scheduleName = "sunrise:"+id;
	}
	var schedule = new Schedule(id,scheduleName,scriptName,isActive,"sunrise");
	if (timeShifting > (4294967296/2)-1) {
		timeShifting = (4294967296 - timeShifting) * -1;
	}
	schedule.timeShifting = timeShifting;
	schedule.type = "sunrise";
	return schedule;
}
function removeSchedule(schedule) {
	schedulesMap.delete(schedule.id);
	uploadSchedulesFile();
	uploadSchedulesNamesFile();
}
function getNextScheduleId() {
	var id;
	while(true) {
		id = Math.floor(Math.random() * Math.floor(256)).toString();
		if (!schedulesMap.get(id)) break;
	}
	return id;
}