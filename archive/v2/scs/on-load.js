let LOCATION_PATHS = {
	actions : '/v2/actions.html',
	status : '/v2/index.html',
	settings : '/v2/settings.html'
}
let systemTimeZone = 0;
var allowUpdateDevices = true;
let updateState = 0;
var controllerTime = 0;
var devicesMap = new Map();
var namesMap = new Map();
var schedulesNamesMap = new Map()
var schedulesMap = new Map();
var scriptsNamesList = [];
var currentShowDevice = "";
var currentShowData = "";

var selectedSettings = null;

var currentAccordionElementOpened = null;
var currentDeviceMenuLocation = null;

const DEFAULT_DEVICE_COLOR  = "#8FBC8F";

function onLoad() {
	if (location.pathname == "/v2/") {
		LOCATION_PATHS.status = "/v2/";
	}
	fetch('/names.txt')
	.then(
		function(response) {
		  if (response.status !== 200) {
			console.log('Looks like there was a problem. Status Code: ' +
			  response.status);
		  //  return;
		  }
		  response.text().then(function(fileData) {
			var idNames = fileData.split("\n");
			for ( var idName of idNames ) {
				var data = idName.trim();
				data = data.split("=");
				if (data.length != 2) continue;
				namesMap.set(data[0].trim(),data[1].trim());
			}
		  })
		  .then(function(){
			downloadDevicesColors()
			.then(json => {
				fetch('/getDevices/')
			  	.then(function(devicesResponse) {
					if (devicesResponse.status !== 200) {
						alert('Looks like there was a problem. Status Code: ' +
									devicesResponse.status);
						return;
					}
					devicesResponse.text().then(function(devicesData){
						fetch('/sysinfo/?timezone')
						.then(time_resp => {
							time_resp.json().then(json_time => {
								systemTimeZone =  json_time.timezone;
								updateDevicesMap(devicesData);
								showSystemTime();
								initDevicesColors(json)
								initSchedules();
								if (document.getElementById('actions-container')) {
									showActions();
								}
								if (document.getElementById('status-container')) {
									showDevices();
								}
								setInterval ('updatePage()',1000);
							})
						})
					})
			  	})
			})
		  });
		}
	)
  .catch(function(err) {
	console.log('(on load) \n Fetch Error : ' + err);
  });
}

document.addEventListener("DOMContentLoaded", onLoad);


function showSystemTime() {
	if (document.getElementById('navbar-systemTime') ) {
		if (controllerTime != 0) {
			controllerTime = Number.parseInt(controllerTime);
			let timeToView = (controllerTime + new Date().getTimezoneOffset()*60) * 1000;
			var date = new Date(timeToView);
			var hh = date.getHours();
			var mm = date.getMinutes();
			var ss =date.getSeconds();
			var time = 	[(hh>9 ? '' : '0') + hh,
						(mm>9 ? ':' : ':0') + mm,
						(ss>9 ? ':' : ':0') + ss
						].join('');
			document.getElementById('navbar-systemTime').innerHTML = time + `<span class = "mx-2">[${devicesMap.get("system").specifedName}]</span>`;
		}
	}
}

function updatePage() {
	switch (updateState) {
		case(0) : {
		}
		case(1) : {
		}
		case(2) : {
		}
		case(3) : {
			controllerTime++;
			showSystemTime();
			updateState++;
			break;
		}
		case(4) : {
			updateDevices();
			showSystemTime();
			updateState = 0;
			break;
		}
	}	
}
