function switchSettingsScr(settingsName) {
    switch (settingsName) {
        case ("general") : {
            showGeneralSettings()
            break;
        }
        case ("network") : {
            showNetworkSettings();
            break;
        }
        case ("wifi") : {
            showWiFiSettings();
            break;
        }
        case ("geolocation") : {
            showGeolocationSettings();
            break;
        }
        case ("clock") : {
            showClockSettings();
            break;
        }
        case ("firmware") : {
            showFirmwareSettings();
            break;
        }
        case ("devices") : {
            showDevicesSettings();
            break;
        }
    }
    if ( document.getElementById('cancelBtn')) {
        document.getElementById('cancelBtn').onclick = function () {
        document.location.href = document.location.href;
    }
    }
    
    if(document.querySelector('form')) {
        document.querySelector('form').onsubmit = function(e) {
            e.preventDefault();
            return applySettings(e);
        };
    }
}
function showGeneralSettings() {
 var dataToView = `
    <h3 style = 'margin:20px;'>General settings</h3>
    <form id = 'general-form'>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Old password</label>
            <input type="password" class="form-control" id="exampleInputEmail1">
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">New password</label>
            <input type="password" class="form-control" id="exampleInputPassword1">
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Confirm password</label>
            <input type="password" class="form-control" id="exampleInputPassword1">
        </div>
        <button id = "applyBtn" type="submit" class="btn btn-primary"><i class='bi bi-check'></i>&nbsp;Apply</button>
        <button id = "cancelBtn" class="btn btn-secondary"><i class="bi bi-arrow-left-short"></i>&nbsp;Cancel</button>
    </form>
    `;
    document.getElementById('settings-container').innerHTML = dataToView;

}
function showNetworkSettings() {
    var dataToView = `
        <h3 style = 'margin:20px;'>Network settings</h3>
        <form id = 'network-form'>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="flexRadioDefault" id="DHCP_Check" onchange = "disableInputs();removeAllRequired();">
                <label class="form-check-label" for="DHCP_Check">
                    Get ip address automatically
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="flexRadioDefault" id="manualIP_Check" onchange = "enableInputs();setNetworkRequired();" checked>
                <label class="form-check-label" for="manualIP_Check">
                    Use the following ip address
                </label>
            </div>
            <div class="mb-3">
                <label for="IPInput" class="form-label">IP</label>
                <input type="text" class="form-control" id="IPInput">
            </div>
            <div class="mb-3">
                <label for="maskInput" class="form-label">Subnet mask</label>
                <input type="text" class="form-control" id="maskInput">
            </div>
            <div class="mb-3">
                <label for="gatewayInput" class="form-label">Gateway</label>
                <input type="text" class="form-control" id="gatewayInput">
            </div>
            <div class="mb-3">
                <label for="dnsInput" class="form-label">DNS:</label>
                <input type="text" class="form-control" id="dnsInput">
            </div>
            <button id = "applyBtn" type="submit" class="btn btn-primary"><i class='bi bi-check'></i>&nbsp;Apply</button>
            <button id = "cancelBtn" class="btn btn-secondary"><i class="bi bi-arrow-left-short"></i>&nbsp;Cancel</button>
        </form>
    `;
    document.getElementById('settings-container').innerHTML = dataToView;
    setNetworkRequired();
}
function setNetworkRequired() {
    const IPPattern = "((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
    
    document.getElementById("IPInput").pattern = IPPattern;
    document.getElementById("IPInput").required = true;

    document.getElementById("maskInput").pattern = IPPattern;
    document.getElementById("maskInput").required = true;

    document.getElementById("gatewayInput").pattern = IPPattern;
    document.getElementById("gatewayInput").required = true;

    document.getElementById("dnsInput").pattern = IPPattern;
    document.getElementById("dnsInput").required = true;
}
function showWiFiSettings() {
    var dataToView = `
        <h3 style = 'margin:20px;'>WiFi settings</h3>
        <form id = 'wifi-form'>
            <div class="mb-3">
                <label for="ssidInput" class="form-label">WiFi SSID</label>
                <input type="text" class="form-control" id="ssidInput" required>
            </div>
            <div class="mb-3">
                <label for="passwordInput" class="form-label">Password</label>
                <input type="password" class="form-control" id="passwordInput">
            </div>
            <button id = "applyBtn" type="submit" class="btn btn-primary"><i class='bi bi-check'></i>&nbsp;Apply</button>
            <button id = "cancelBtn" class="btn btn-secondary"><i class="bi bi-arrow-left-short"></i>&nbsp;Cancel</button>
        </form>
    `;
    document.getElementById('settings-container').innerHTML = dataToView;
}
function showGeolocationSettings() {
    var dataToView = `
        <h3 style = 'margin:20px;'>Geolocation settings</h3>
        <form id = 'geolocation-form'>

            <div class="input-group mb-3">
                <select id="geo-state-select" class="form-select" onchange = "fillGeoCitiesSelect(this.value)" disabled>
                    <option value = "null" selected>states loading...</option>
                </select>
            </div>
            <div class="input-group mb-3">
                <select id="geo-city-select" class="form-select" onchange = "fillCoordinatesFields()" disabled>
                    <option value = "null" selected>cities loading...</option>
                </select>
            </div>
            <div class="mb-3">
                <label for="latitude-input" class="form-label">Latitude</label>
                <input type="text" class="form-control" id="latitude-input">
            </div>
            <div class="mb-3">
                <label for="latitude-input" class="form-label">Longitude</label>
                <input type="text" class="form-control" id="longitude-input">
            </div>
            <button id = "applyBtn" type="submit" class="btn btn-primary"><i class='bi bi-check'></i>&nbsp;Apply</button>
            <button id = "cancelBtn" class="btn btn-secondary"><i class="bi bi-arrow-left-short"></i>&nbsp;Cancel</button>
        </form>
    `;
    document.getElementById('settings-container').innerHTML = dataToView;
    fillGeoStatesSelect();
}
function showClockSettings() {
    var dataToView = `
        <h3 style = 'margin:20px;'>Clock settings</h3>
        <form id = 'clock-form'>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="flexRadioDefault" id="espTime" onchange = "fillClockSettingsForm(this)">
                <label class="form-check-label" for="espTime">
                    Show time on hub
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="flexRadioDefault" id="pcTime" onchange = "fillClockSettingsForm(this)" checked>
                <label class="form-check-label" for="pcTime">
                    Show time on computer
                </label>
            </div>
            <div class="mb-3">
                <label for="dateInput" class="form-label">Date</label>
                <input type="date" class="form-control" id="dateInput" required>
            </div>
            <div class="mb-3">
                <label for="timeInput" class="form-label">Time</label>
                <input type="time" class="form-control" id="timeInput" required>
            </div>
            <div class="mb-3">
                <label for="timeZoneInput" class="form-label">Time-zone</label>
                <input type="text" class="form-control" id="timeZoneInput" required pattern = "[+-]?[\\d]{2}:[\\d]{2}">
                <div id="time-zone-help" class="form-text">Example -03:15</div>
            </div>
            <button id = "applyBtn" type="submit" class="btn btn-primary"><i class='bi bi-check'></i>&nbsp;Apply</button>
            <button id = "cancelBtn" onclick = "document.location.href = document.location.href" class="btn btn-secondary"><i class="bi bi-arrow-left-short"></i>&nbsp;Cancel</button>
        </form>
    `;
    document.getElementById('settings-container').innerHTML = dataToView;
    
    let date = new Date();

    let timeZone = convertTimeZoneToViewFormat();

    document.getElementById('dateInput').value = date.YYYYMMDD();
    document.getElementById('timeInput').value = date.HHmm();
    document.getElementById('timeZoneInput').value = timeZone;
    
}

function convertTimeZoneToViewFormat(timeZone) {
    timeZone = timeZone || new Date().getTimezoneOffset() * -1;
    let tzHours = Math.trunc(timeZone / 60);
    let tzMinutes = Math.abs(timeZone % 60);

    tzMinutes = tzMinutes < 10 ? "0" + tzMinutes : tzMinutes;
    
    if (tzHours >= 0) {
        tzHours = tzHours < 10 ? "+0" + tzHours : tzHours;
    } else {
        tzHours = tzHours > -10 ? "-0" + tzHours * -1 : tzHours;
    }
    return tzHours + ":" + tzMinutes;
}

function fillClockSettingsForm(e) { // e =  check input esp or local time on client pc
    switch (e.id) {
        case ('espTime') : {
                let timeZoneToView = convertTimeZoneToViewFormat(systemTimeZone);
                let timeToView = Number.parseInt(controllerTime) + new Date().getTimezoneOffset()*60;
                let date = new Date(timeToView * 1000);

                document.getElementById('dateInput').value = date.YYYYMMDD();
                document.getElementById('timeInput').value = date.HHmm();
                document.getElementById('timeZoneInput').value = timeZoneToView;
            break;
        }
        case ('pcTime') : {
            let date = new Date();

            let timeZone = convertTimeZoneToViewFormat();
        
            document.getElementById('dateInput').value = date.YYYYMMDD();
            document.getElementById('timeInput').value = date.HHmm();
            document.getElementById('timeZoneInput').value = timeZone;
            break;
        }
    }
}
function showFirmwareSettings() {
    var dataToView = `
        <h3 style = 'margin:20px;'>Update firmware</h3>
        <form id = 'firmware-form'>
        <div class="form-group mb-3">
            <select id = 'firmware_select' class="form-select" required>
                <option>Mega</option>
            </select>
            <div class="form-text">Select the device</div>
        </div>
        <div class="input-group mb-3">
            <input type="file" class="form-control" id="firmware_input" required>
            <label class="input-group-text" for="firmware_input">Upload</label>
        </div>
        <button id = "applyBtn" type="submit" class="btn btn-primary"><i class='bi bi-check'></i>&nbsp;Apply</button>
        <button id = "cancelBtn" class="btn btn-secondary"><i class="bi bi-arrow-left-short"></i>&nbsp;Cancel</button>
        </form>
    `;
    document.getElementById('settings-container').innerHTML = dataToView;
}
function showDevicesSettings() {
    showDevices(); 
}

function applySettings(e) {
    if (e.submitter.classList[1] == 'btn-primary') {
        switch (e.currentTarget.id) {
            case('general-form') : {
                break;
            }
            case('network-form') : {
                applyNetworkSettings();
                break;
            }
            case('wifi-form') : {
                applyWiFiSettings();
                break;
            }
            case('clock-form') : {
                applyClocKSettings();
                break;
            }
            case('firmware-form') : {
                updateFirmwareSettings();
                break;
            }
            case('geolocation-form') : {
                break;
            }
        }
    }
}



function removeAllRequired() {
    var inputs = document.querySelectorAll("input");
    for (input of inputs) {
        if (input.type != "radio" && input.type != "checkbox") {
            input.required = false;
        }
    }
}

function disableInputs() {
    var inputs = document.querySelectorAll("input");
    for (input of inputs) {
        if (input.type != "radio" && input.type != "checkbox") {
            input.disabled = true;
        }
    }
}

function enableInputs() {
    var inputs = document.querySelectorAll("input");
    for (input of inputs) {
        if (input.type != "radio" && input.type != "checkbox") {
            input.disabled = false;
        }
    }
}
var geoData;
var cities = [];

async function fillGeoStatesSelect() {
    fetch('/SD/geo/us.csv')
    .then(function(response) {
        if (response.status !== 200) {return;}
        response.text().then(function(geo){
            geoData = geo;
            var statesList = [];
            var result = "<option value = 'null' selected>Select the state</option>";
            var data = geo.split("\n")
            for (line of data) {
                var e = line.split(";");
                if (!statesList.includes(e[1]) && e[1]) {
                    statesList.push(e[1]);
                }
            }
            statesList.sort();
            for (e of statesList) {
                result += "<option value = '"+e+"'>"+e+"</option>";
            }
            document.getElementById("geo-state-select").innerHTML = result;
            document.getElementById("geo-state-select").disabled = false;
            document.getElementById("geo-city-select").disabled = false;
            document.getElementById("geo-city-select").innerHTML = "<option value = 'null' selected>Select the city</option>";
        })
    })
}

function getGeoCitiesList(state) {
    var result = "<option value = 'null' selected>Select the city</option>";
    var data = geoData.split("\n")
	for (var line of data) {
        var elems = line.split(';');
        if (elems[1] == state)
		    result += "<option value = '"+elems[0]+"'>"+elems[0]+"</option>";
	}
	return result;
}
function fillGeoCitiesSelect(state) {
    document.getElementById("geo-city-select").innerHTML = getGeoCitiesList(state);
}

function fillCoordinatesFields() {
    var state =  document.getElementById("geo-state-select").value;
    var city =  document.getElementById("geo-city-select").value;
    for (var line of geoData.split("\n")) {
        var data = line.split(";");
        if (data[0] == city && data[1] == state) {
            document.getElementById("latitude-input").value = data[2];
            document.getElementById("longitude-input").value = data[3];
        }
    }
}

// ---------------------------------- firmware --------------------------
// ---------------------------------- firmware --------------------------
// ---------------------------------- firmware --------------------------

function getFirmwareSize(firmware_len) {
	var temp = (firmware_len).toString(16);
	var res = ['0','0','0','0','0','0','0','0'];
	for (var i = 7, tempIdx = temp.length-1; tempIdx >= 0; i--, tempIdx--) {
		res[i] = temp[tempIdx];
	}
	return res.join('').toUpperCase();
}
function convertFirmwareToHexString(firmware) {
	var res = '';
	for (var i = 0; i < firmware.length; i++) {
		var temp = firmware[i].toString(16);
		if (temp.length < 2) temp = '0'+temp;
		res += temp;
	}
	return res.toUpperCase();
}
async function updateFirmwareSettings() { // temp function
    
    blockApplyBtn()

	var updDev = document.getElementById('firmware_select').value;
	var firmware = document.getElementById('firmware_input').files[0];
	getFileData(firmware,updDev)
	.then(function(fileData){
			if (updDev == "Mega") {
				fileData = hexFirmwareParse(fileData);
				var megaID = '1E9801';
				var crc = '00000000';
				var data =  megaID + crc + getFirmwareSize(fileData[0].length);
				fileData = [data + convertFirmwareToHexString(fileData[0])];
				firmware = new File(fileData, "firmware.hex", {
						type: "text/plain",
				});
				var form_data = new FormData()
				form_data.append('file', firmware)
			} else {
				unblockApplyBtn()
				return;
			} 
		fetch("/rdrok/updMP/flashm.hex",{
			method: 'POST',
			body: form_data
		})
		.then(function(response) {
			if (response.status == 200) {
				fetch("/flashAVR/flashm.hex")
				.then(function(useFirmwareResponse){
                    unblockApplyBtn();
					if(useFirmwareResponse.status == 200) {
						useFirmwareResponse.json().then(function(jsonRes){
							if (jsonRes.result == "0") {
								alert ("firmware applied successfully");
							} else if (jsonRes.result == "1") {
								alert ("ferror : bad filename");
							} else if (jsonRes.result == "2") {
								alert ("ferror :  can't open the file");
							} else if (jsonRes.result == "3") {
								alert ("ferror :  file is too short");
							} else if (jsonRes.result == "4") {
								alert ("ferror :   chip signature mismatch");
							} else if (jsonRes.result == "5") {
								alert ("ferror :  flash file is too short or corrupted");
							}
						})
					} else {
						alert ("(firmware not applied) err code : "+response.status)
					}
				})
			} else {
                unblockApplyBtn()
				alert ("(firmware not upload) err code : "+response.status)
			}
		})
		.catch(function(err){
            unblockApplyBtn();
			alert(err);
			return;
		})
	})
	
}
async function _updateFirmwareSettings() { // real function
	var updDev = document.getElementById('firmware_select').value;
	var firmware = document.getElementById('firmware_input').files[0];
	if (!firmware){
		alert("firmware is not selected");
		return;
	}
	var devType;
	var devUid = '000000000000';
	var flqueryBody;
	getFileData(firmware,updDev)
	.then(function(fileData){
			var isBytesData = true;
			if (updDev == "Mega") {
				devType = 1;
				fileData = hexFirmwareParse(fileData);
				firmware = new File(fileData, "firmware.hex", {
						type: "text/plain",
				});
			//	download(firmware);
				for (var [key,value] of devicesMap) {
					if (value instanceof Mega) {
						devUid = value.id;
					}
				}
			} else if (updDev == "hub") {
			//	download(firmware);
				devType = 0;
			} else if (updDev == "all_ps") {
				return;
			} else {
				// power strip
				devType = 2;
				devUid = updDev;
			}
		flqueryBody = [devType,devUid,firmware.size,crc16_BUYPASS(fileData,isBytesData)].join(";");
		fetch("/flquery/",{
			method: 'POST',
			body: flqueryBody
		})
		.then(function(response){
			if (response.status == 200) {
				response.json().then(function(jsonResp){
					if (jsonResp.result == "ready") {
						formData = new FormData();
						formData.append('file', firmware);
						fetch("/pflash/", {
							method: 'POST',
							body: formData
						})
						.then(function(response_pflash){
							if (response_pflash.status == 200) {
								response_pflash.json().then(function(jsonResp_pflash){
									if (jsonResp_pflash.result = "prfm") {
										allowUpdateDevices = false;
										var intervalID = setInterval(() => {
											fetch()
											.then(function(uploadResponse){
												if(uploadResponse.status == 200) {
													uploadResponse.json().then(function(uploadResult){
														if (uploadResult.result == "ready") {
															clearInterval(intervalID);
															allowUpdateDevices = true;
														}
													})
												}
											})
										},10000)
										
									} else {
										alert("error : "+jsonResp_pflash.result);
										return;
									}
								})
							} else {
								alert(response_pflash.status);
								return;
							}
						})
					} else {
						alert("error : "+jsonResp.result);
						return;
					}
				})
			} else {
				alert(response.status);
				return;
			}
		})
		.catch(function(err){
			alert(err);
			return;
		})
	})
}
function getFileData(file,updDev) {
	var fileReader = new FileReader();
	if (updDev == "Mega") {
		fileReader.readAsText(file);
	} else {
		fileReader.readAsArrayBuffer(file);
	}
	
	return 	new Promise((resolve, reject) => {
			fileReader.onload = function() {
				if (updDev == "Mega") {
					resolve(fileReader.result)
				} else {
					resolve(new Uint8Array(fileReader.result))
				}
				
			}
		});
}
function hexFirmwareParse(fileData) {
	let memMap = MemoryMap.fromHex(fileData);
	var resultDataArrays = [];
	var lastAddr = 0;
	var lastBlockSize = 0;
	for (let [address, dataBlock] of memMap) {
		if (lastBlockSize) {
			var length = address - (lastAddr+lastBlockSize);
			var emptyBlock = new Uint8Array(length);
			resultDataArrays.push(emptyBlock);
		} else {
			if (address) {
				var length = address;
				var emptyBlock = new Uint8Array(length);
				resultDataArrays.push(emptyBlock);
			}
		}
		lastAddr = address;
		lastBlockSize = dataBlock.length;
		resultDataArrays.push(dataBlock);
	}
	return resultDataArrays;
}
// ------------------------------ firmware end ----------------------
// ------------------------------ firmware end ----------------------
// ------------------------------ firmware end ----------------------

function applyWiFiSettings() {
    if (confirm("Are you sure? The hub may be unavailable after that.")) {

        var applyBtn = document.getElementById('applyBtn');
        applyBtn.disabled = true;
        let loadingView = `<div id = "loadingView" class="spinner-border spinner-border-sm" role="status">
                                <span class="visually-hidden">Загрузка...</span>
                            </div>&nbsp;`;
        applyBtn.innerHTML = loadingView + "Uploading";

        let ssid = document.getElementById("ssidInput").value;
        let pass = document.getElementById("passwordInput").value || '';
        let data = ssid + '\n' + pass;
        let wifiFile = new File([data],"wifiDat", {
            type : "text/plain"
        })
        let formData = new FormData();
        formData.append('file',wifiFile);
        fetch('/rdrok/updMP/configs/ssidpsv.dat', {
            method: 'POST',
            body: formData
        })
        .then(response => {
            applyBtn.disabled = false;
            applyBtn.innerHTML = "<i class='bi bi-check'></i>&nbsp;Apply";
            if (response.status !== 200) {
                alert("error : "+response.status);
            } else {
                alert("success");
                document.location.href = document.location.href;
            }
        })
        .catch(error => {
            alert(error);
            applyBtn.disabled = false;
            applyBtn.innerHTML = "<i class='bi bi-check'></i>&nbsp;Apply";
        })
    }
}
function applyClocKSettings() {
    var applyBtn = document.getElementById('applyBtn');
    applyBtn.disabled = true;
    let loadingView = `<div id = "loadingView" class="spinner-border spinner-border-sm" role="status">
                            <span class="visually-hidden">Загрузка...</span>
                        </div>&nbsp;`;
    applyBtn.innerHTML = loadingView + "Uploading";
    let timeZoneData = document.getElementById("timeZoneInput").value.split(":");
    let time1 = Number.parseInt(timeZoneData[0])
    let time2 = Number.parseInt(timeZoneData[1])
    let timeZone = 0;
    if (timeZoneData[0].match(/^\d{2}/)) {
        timeZoneData[0] = "+" + timeZoneData[0];
    }
    let dateString = document.getElementById("dateInput").value+
                        "T"+
                        document.getElementById("timeInput").value+":00"+
                        timeZoneData[0]+":"+timeZoneData[1];
    let  date  = new Date(dateString);
    if (time1 < 0) {
        timeZone = time1*60 - time2;
    } else {
        timeZone = time1*60 + time2;   
    }
    
    let dateTime = Math.trunc((date.getTime())/1000);

    sendClockSettings(dateTime,timeZone)
    .then(jsonResp => {
        if (jsonResp.result != "OK") {
            switch (jsonResp.result) {
                case ("badcm") : {
                    alert ("error : not nt at start request");
                    break;
                }
                case ("badtime") : {
                    alert ("error : time year must be at [2000-2060]");
                    break;
                }
                case ("badtz") : {
                    alert ("time zone error");
                    break;
                }
                default : {
                    alert ("unknown error");
                    break;
                }
            }
            applyBtn.innerHTML = "<i class='bi bi-check'></i>&nbsp;Apply";
            document.getElementById("applyBtn").disabled = false;
        } else {
            alert("success");
            document.location.href = document.location.href;
        }
    })
    .catch(error => {
        applyBtn.innerHTML = "<i class='bi bi-check'></i>&nbsp;Apply";
        document.getElementById("applyBtn").disabled = false;
        alert(error)
    })
    
}
function sendClockSettings(date,timeZone) {
    return new Promise((resolve, reject) => {
        var bodyData =  ":nt:"+date+":"+timeZone+"::";
        fetch("/setTime/",{
            method: 'POST',
            body: bodyData
        })
        .then(function(response){
            if (response.status !== 200) {
                reject(response.status)
            }
            response.json().then(function(jsonResp){
                resolve(jsonResp)
            })
        })
        .catch(function(err){
            reject(err)
        });
    })
}

function applyNetworkSettings() {
    let req = "/setnetCFG/";
    let reqData = "";
    if (document.getElementById("manualIP_Check").checked) {

        let ip = document.getElementById("IPInput").value;
        let gateway =  document.getElementById("gatewayInput").value;
        let subnet = document.getElementById("maskInput").value;
        let dns = document.getElementById("dnsInput").value;

        reqData = "||dhcp|0|ip|"+ip+"|gateway|"+gateway+"|subnet|"+subnet+"|dns|"+dns+"||";

    } else if (document.getElementById("DHCP_Check").checked) {

        reqData = "||dhcp|1||";

    }

    blockApplyBtn();

    fetch(req, {
        method : "POST",
        body : reqData
    })
    .then(response => {
        if (response.status === 200) {
            alert("success");
            location.href = location.href;
        } else {
            unblockApplyBtn();
            alert("error code on applying IP : "+response.status);
        }
    })
    .catch(error => {
        unblockApplyBtn();
        alert(error);
    })
}