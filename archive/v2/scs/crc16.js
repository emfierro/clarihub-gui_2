function crc16_BUYPASS(str,isBytesData){
	var arr = [];
	if (isBytesData) {
		for (var i = 0; i < str.length; i++) {
			if (str[i].length) {
				for (var j = 0; j < str[i].length; j++) {
				arr[i+j] = str[i][j];
			}
			} else {
				arr[i] = str[i];
			}
			
		}
	} else {
		for (let i = 0; i < str.length; i++) {
			arr[i] = str.charCodeAt(i);
		}
	}
	var ptr= Array.from(arr) ;
	var count = ptr.length;
	var  crc=0;
	var i=0;
	var ptri=0;
	while (count-- > 0)
	{
	  crc = crc ^ (ptr[ptri++] << 8);
	  i = 8;
	  while(i--)
	  {
		if ( crc  & 0x8000)
		  crc = (crc << 1) ^ 0x8005;
		else
		  crc = crc << 1;
	  };
	}
	return ((crc&0xffff).toString(16).padStart(4,"0").toLocaleUpperCase());
}