function showDevicesToChartSelecData() {
  devicesMap = new Map();
  devicesMap.set('1',{specifedName : 'dev-1'});
  devicesMap.set('2',{specifedName : 'dev-2'});
  devicesMap.set('3',{specifedName : 'dev-3'});
    var selectElem = document.getElementById("chartDevSelect");
    var selData = "Select the device...";
    for(var [deviceId, device] of devicesMap) {
     //   if (device instanceof MegaSensorChannel) {
            selData += "<option value = \""+deviceId+"\">"+device.specifedName+"</option>";
     //   }
    }
    selectElem.innerHTML = selData;
}
function getChartData(selectedDeviceName) {
    var label = selectedDeviceName;
    var dataArr1 = [];
    var dataArr2 = [];
    var labsArr = [];
    for (var i = 0; i < 24; i++) {
        labsArr.push(i+"");
        dataArr1.push(Math.random()*50);
        dataArr2.push(Math.random()*50+20);
    }
    return {
        labels : labsArr,
        datasets : [{
            label: 'temperature',
            data: dataArr1,
            fill : false,
            borderColor: 'orange'
        },{
            label: 'humidity',
            data: dataArr2,
            fill : false,
            borderColor: 'grey'
        }],
    }
}
function showChart() {
   // restoreChartTimeDiapasonBlock();
    var dev = devicesMap.get(document.getElementById("chartDevSelect").value);
    if (!dev) return; 
    var dateNow = new Date();
    var dateView = "0"+Number(dateNow.getMonth()+1)+"/"+ dateNow.getDate() + "/" + dateNow.getFullYear();
    chartData = getChartData(dev.specifedName);
    document.getElementById('devChart').remove();
    document.getElementById('chartP').innerHTML = '<canvas id="devChart"></canvas>';
    var ctx = document.getElementById('devChart');
    var options =  {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    };


    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: chartData,
        options: options
    });
}

function showChartTimeDiapason(selEl) {
    switch (selEl.value) {
        case ("today") : {
            document.getElementById("chartTimeDiapason").innerHTML = "";
            break;
        }
        case ("date") : {
            document.getElementById("chartTimeDiapason").innerHTML = "<input type = 'date' onchange = ''>";
            break;
        }
        case ("diapason") : {
            document.getElementById("chartTimeDiapason").innerHTML = "start : <input type = 'date'> finish : <input type = 'date'>";
            break;
        }
    }
}

function showChartBlock() {
    showDevicesToChartSelecData();
    showChart(document.getElementById("chartDevSelect"));
}

function restoreChartTimeDiapasonBlock() {
    document.getElementById("chartTimeSelect").value = 'today';
    document.getElementById("chartTimeDiapason").innerHTML = "";
}