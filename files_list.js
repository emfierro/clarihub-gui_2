var fs = require('fs');
var path = require('path');


let indexHTML = "build/index.html";

var getFiles = function (dir, files_){
    
    files_ = files_ || [];
      var files = fs.readdirSync(dir);
      for (var i in files){
          var name = dir + '/' + files[i];
          if (fs.statSync(name).isDirectory()){
              getFiles(name, files_);
          } else {
              files_.push(name);
          }
      }
      return files_;
  };


let fileData = fs.readFileSync(indexHTML, "utf8");

fileData = fileData.replace(/static\//g,"SD/app/");

fs.writeFileSync(indexHTML, fileData);



 

let ccsFiles = getFiles('build/static/css/');

for (f of ccsFiles) {

    var fileData_css = fs.readFileSync(f, "utf8");
    fileData_css = fileData_css.replace(/static\//g,"SD/app/");
    
    fs.writeFileSync(f, fileData_css);

}


let files = getFiles('build/');

for (f of files) {
	if(f.indexOf('/SD/app/js/') >= 0 || true) {
		f = f.split("//");
		f = f[f.length-1]
		f_data = f.split("."); 
		if(f_data[f_data.length-1] === "map"){continue;}
		if(f.endsWith("allowedFiles.txt")){continue;}
		if(f.endsWith(".ino.mega.hex")){continue;}
		if(f.endsWith("log.htm")){continue;}
		if(f.endsWith("manifest.json")){continue;}
		if(f.endsWith("asset-manifest.json")){continue;}
        
        console.log("build/"+f);
		
	}
}



